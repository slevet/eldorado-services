package com.publigroupe.eldorado.service;

import com.publigroupe.eldorado.dto.EldoradoFlowStateDto;
import com.publigroupe.eldorado.dto.EldoradoPaymentDatatransDto;
import com.publigroupe.eldorado.dto.EldoradoPaymentModeEnumDto;
import com.publigroupe.eldorado.manager.EldoradoOrderManager;

public interface EldoradoFlowService {
  /**
   * Retrieve the flow state of ther order
   * 
   * @param OrderDto
   *          order Informations
   * @return
   */
  EldoradoFlowStateDto retrieveFlowState(EldoradoOrderManager orderManager);

  /**
   * Store the user in the command
   * 
   * @param OrderDto
   *          order Informations
   */
  void saveUserInfo(EldoradoOrderManager orderManager);

  /**
   * Store the payment authorization information related to the user command
   * The payment authorization process (connection to DataTrans) is a
   * prerequisite of this service call
   * 
   * @param OrderDto
   *          order Informations
   * @param paymentDatatrans
   *          DataTrans Informations
   * @return PaymentDto Object corresponding to what is recorded in database
   */
  void savePaymentAuthorization(EldoradoOrderManager orderManager, EldoradoPaymentDatatransDto paymentDatatrans);

  /**
   * retrieve the DataTrans transaction Id for cancellation purpose
   * The cancellation of payment authorization process (connection to DataTrans)
   * is a prerequisite of this service call
   * cancel the command
   * 
   * @param OrderDto
   *          order Informations
   */
  void cancelPaymentAuthorization(EldoradoOrderManager orderManager);

  /**
   * Insert order in P2000
   * 
   * @param OrderDto
   *          order Informations
   */
  // Mrt
  // void insertP2000(EldoradoOrderManager orderManager);

  /**
   * Call P2000 back-end to start the validation of the command
   * 
   * @param OrderDto
   *          order Informations
   */
  // void sendP2000Validation(EldoradoOrderManager orderManager);

  /**
   * Call P2000 back-end to start cancellation of the command
   * 
   * @param OrderDto
   *          order Informations
   */
  void sendP2000Cancelation(EldoradoOrderManager orderManager);

  /**
   * Retrieves the payment mode for the given user id.
   * 
   * @param userId
   *          The user id to get the payment mode for.
   * @return The payment mode.
   */
  EldoradoPaymentModeEnumDto retrievePaymentMode(String userId);

  // temporary for update ActiveDirectoryProcess
  EldoradoPaymentModeEnumDto retrieveP2000PaymentMode(String userId);

  /**
   * Updates the payment mode of the user corresponding to the given user id.
   * 
   * @param userId
   *          The id of the user to update.
   * @param paymentMode
   *          The payment mode to set.
   */
  void modifyPaymentMode(String userId, String bpoUserId, EldoradoPaymentModeEnumDto paymentMode);
}
