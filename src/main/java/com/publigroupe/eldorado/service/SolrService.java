package com.publigroupe.eldorado.service;

import java.net.MalformedURLException;

import com.publigroupe.eldorado.dto.SolrSearchCriteriasDto;
import com.publigroupe.eldorado.dto.SolrSearchResultDto;

/**
 * A service for retrieving EldoradoBrandDtos from the Solr offer documents.
 * 
 * @author Barman Dominique
 */
public interface SolrService {

  /** The Constant ABROAD_NPA. */
  public static final String ABROAD_NPA = "ABROAD";

  /** The Constant ALL_NPA. */
  public static final String ALL_NPA = "ALL";

  /** The Constant KEY_SEPARATOR. */
  public static final String KEY_SEPARATOR = "#";

  /** The Constant ID. */
  public final static String ID = "id";

  /** The Constant HEADING. */
  public final static String HEADING = "heading";

  /** The Constant MEDIA_SUPPORT. */
  public final static String MEDIA_SUPPORT = "mediaSupport";

  /** The Constant CLIENT_STATUS. */
  public static final String CLIENT_STATUS = "clientStatus";

  /** The Constant BRAND_ID. */
  public final static String BRAND_ID = "brandId";

  /** The Constant MEDIA_ID. */
  public final static String MEDIA_ID = "mediaId";

  /** The Constant MEDIA_ID. */
  public final static String MEDIA_SEQUENCE = "mediaSequence";

  /** The Constant OFFER_ID. */
  public final static String OFFER_ID = "offerId";

  /** The Constant OFFER_START_DATE. */
  public final static String OFFER_START_DATE = "offerStartDate";

  /** The Constant OFFER_END_DATE. */
  public final static String OFFER_END_DATE = "offerEndDate";

  /** The Constant BRAND_JNLCOD. */
  public final static String BRAND_JNLCOD = "brandJNLCOD";

  /** The Constant BRAND_JNLCODS. */
  public final static String BRAND_JNLCODS = "brandJNLCODS";

  /** The Constant OFFER_DUPLICATE_KEY. */
  public final static String OFFER_DUPLICATE_KEY = "offerDuplicateKey";

  /** The Constant SPONSORED. */
  public final static String SPONSORED = "sponsored";

  /** The Constant SPONSORED_SEQUENCE. */
  public final static String SPONSORED_SEQUENCE = "sponsoredSequence";

  /** The Constant SITE. */
  public final static String SITE = "site";

  /** The Constant BRAND_CIRCULATION. */
  public final static String BRAND_CIRCULATION = "brandCirculation";

  /** The Constant BRAND_IS_CIRCULATION. */
  public final static String BRAND_IS_CIRCULATION = "brandIsCirculation";

  /** The Constant BRAND_CIRCULATION_UNCERTIFIED. */
  public final static String BRAND_CIRCULATION_UNCERTIFIED = "brandCirculationUncertified";

  /** The Constant BRAND_IS_CIRCULATION_SOLD. */
  public final static String BRAND_DISTRIBUTION_TYPE = "brandDistributionType";

  /** The Constant BRAND_CIRCULATION_SOLD. */
  public final static String BRAND_CIRCULATION_SOLD = "brandCirculationSold";

  /** The Constant BRAND_CIRCULATION_FREE. */
  public final static String BRAND_CIRCULATION_FREE = "brandCirculationFree";

  /** The Constant MEDIA_CIRCULATION. */
  public final static String MEDIA_CIRCULATION = "mediaCirculation";

  /** The Constant MEDIA_IS_CIRCULATION. */
  public final static String MEDIA_IS_CIRCULATION = "mediaIsCirculation";

  /** The Constant MEDIA_CIRCULATION_UNCERTIFIED. */
  public final static String MEDIA_CIRCULATION_UNCERTIFIED = "mediaCirculationUncertified";

  /** The Constant MEDIA_DISTRIBUTION_TYPE. */
  public final static String MEDIA_DISTRIBUTION_TYPE = "mediaDistributionType";

  /** The Constant MEDIA_CIRCULATION_SOLD. */
  public final static String MEDIA_CIRCULATION_SOLD = "mediaCirculationSold";

  /** The Constant MEDIA_CIRCULATION_FREE. */
  public final static String MEDIA_CIRCULATION_FREE = "mediaCirculationFree";

  /** The Constant STATE_PROVINCE. */
  public final static String STATE_PROVINCE = "stateProvince";

  /** The Constant ECONOMIC_AREA. */
  public final static String ECONOMIC_AREA = "economicArea";

  /** The Constant KEYWORDS. */
  public final static String KEYWORDS = "keywords";

  /** The Constant LANGUAGE. */
  public final static String LANGUAGE = "language";

  /** The Constant PRICE. */
  public final static String PRICE = "price";

  /** The Constant CURRENCY. */
  public final static String CURRENCY = "currency";

  /** The Constant NPA_INCLUDE. */
  public final static String NPA_INCLUDE = "npaInclude";

  /** The Constant NPA_EXCLUDE. */
  public final static String NPA_EXCLUDE = "npaExclude";

  /** The Constant BRAND_NAME. */
  public final static String BRAND_NAME = "brandName";

  /** The Constant BRAND_DESC. */
  public final static String BRAND_DESC = "brandDesc";

  /** The Constant BRAND_GIF. */
  public final static String BRAND_GIF = "brandGif";

  /** The Constant MEDIA_NAME. */
  public final static String MEDIA_NAME = "mediaName";

  /** The Constant MEDIA_DESC. */
  public final static String MEDIA_DESC = "mediaDesc";

  /** The Constant MEDIA_GIF. */
  public final static String MEDIA_GIF = "mediaGif";

  /** The Constant OFFER_NAME. */
  public final static String OFFER_NAME = "offerName";

  /** The Constant OFFER_DESC. */
  public final static String OFFER_DESC = "offerDesc";

  /** The Constant OFFER_GIF. */
  public final static String OFFER_GIF = "offerGif";

  /** The Constant OFFER_COLOR. */
  public final static String COLOR_OPTION = "colorOption";

  /** The Constant OFFER_PACKAGE. */
  public final static String OFFER_PACKAGE = "offerPackage";

  /** The Constant INFORMATION_SERVICE_URL. */
  public final static String INFORMATION_SERVICE_URL = "informationServiceUrl";

  /** The Constant PREFIX_ELDORADO_BRAND. */
  public static final String PREFIX_ELDORADO_BRAND = "ELDORADOBRAND";

  public void init(String companyId) throws MalformedURLException;

  /**
   * Index solr.
   * 
   * @param brandId
   *          the brand id (if null, index all)
   * @throws Exception
   *           the exception
   */
  public void index(String companyId, String brandId) throws Exception;

  /**
   * Finds the brand tree corresponding to the given.
   * 
   * @param criterias
   *          the criterias
   * @return the solr search result dto {@link SolrSearchCriteriasDto}
   * @return
   */
  SolrSearchResultDto findByFacet(SolrSearchCriteriasDto criterias);
}
