package com.publigroupe.eldorado.service;

import com.publigroupe.basis.dto.LanguageEnumDto;
import com.publigroupe.basis.manager.TranslationManager;
import com.publigroupe.eldorado.dto.EldoradoPaymentModeEnumDto;
import com.publigroupe.eldorado.manager.EldoradoPartnerManager;
import com.publigroupe.javaBase.property.PropertyLoader;

/**
 * @author U930PL
 */
public class EldoradoCheckServiceImpl implements EldoradoCheckService {

  private EldoradoPartnerManager eldoradoPartnerManager;
  private EldoradoFlowService eldoradoFlowService;

  @Override
  public boolean checkDatabaseUDB() {
    final PropertyLoader pl = new PropertyLoader();
    String userId = pl.getProperty("checkservice.userid");
    try {
      EldoradoPaymentModeEnumDto payment = eldoradoFlowService.retrieveP2000PaymentMode(userId);
      return payment != null;
    }
    catch (Exception e) {
      return false;
    }
  }

  @Override
  public boolean checkLdapAdam() {
    final PropertyLoader pl = new PropertyLoader();
    String userId = pl.getProperty("checkservice.userid");
    try {
      return eldoradoPartnerManager.isUserInActiveDirectory(userId);
    }
    catch (Exception e) {
      return false;
    }
  }

  @Override
  public boolean checkECMSService() {
    return false;
  }

  @Override
  public boolean checkTranslationService() {
    if (TranslationManager.getLabel("ELDORADO.WEB.ELDORADO_HOME.PUBLICITAS_TITLE", LanguageEnumDto.FRENCH).contains(
        "ELDORADO")) {
      return false;
    }
    return true;
  }

  public void setEldoradoPartnerManager(EldoradoPartnerManager eldoradoPartnerManager) {
    this.eldoradoPartnerManager = eldoradoPartnerManager;
  }

  public void setEldoradoFlowService(EldoradoFlowService eldoradoFlowService) {
    this.eldoradoFlowService = eldoradoFlowService;
  }

}
