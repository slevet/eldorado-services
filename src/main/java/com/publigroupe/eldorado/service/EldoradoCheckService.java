package com.publigroupe.eldorado.service;

public interface EldoradoCheckService {

  /**
   * Check UDB Access
   * Check service ELDORADO_001
   * 
   * @return
   */
  public abstract boolean checkDatabaseUDB();

  /**
   * Check Adam Access
   * Check service ELDORADO_002
   * 
   * @return
   */
  public abstract boolean checkLdapAdam();

  /**
   * Check Oracle Access
   * Check service ELDORADO_003
   * 
   * @return
   */

  public abstract boolean checkTranslationService();

  /**
   * Check ECMS Access
   * Check service ELDORADO_004
   * 
   * @return
   */

  public abstract boolean checkECMSService();

}
