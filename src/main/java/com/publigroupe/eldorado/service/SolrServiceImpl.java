package com.publigroupe.eldorado.service;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.publigroupe.eldorado.batch.solr.SolrIndexer;
import com.publigroupe.eldorado.dto.EldoradoBrandDto;
import com.publigroupe.eldorado.dto.EldoradoMediaDto;
import com.publigroupe.eldorado.dto.SolrSearchCriteriasDto;
import com.publigroupe.eldorado.dto.SolrSearchResultDto;
import com.publigroupe.javaBase.base.StringUtil;
import com.publigroupe.javaBase.property.PropertyLoader;
import com.publigroupe.media.dto.MediaDto;
import com.publigroupe.media.dto.MediaIdDto;
import com.publigroupe.media.dto.MediaInformationDto;
import com.publigroupe.media.dto.MediaNameDto;
import com.publigroupe.media.dto.MediaSupportEnumDto;
import com.publigroupe.media.dto.MediaTypeDto;
import com.publigroupe.order.dao.p2000.OrderAccessor;

/**
 * The default implementation of {@link SolrService}.
 */
public class SolrServiceImpl implements SolrService {

  private static final String[] LANGUAGES = new String[] {"de", "fr", "it"};

  private static final String PREFIX_SEPARATOR = "_";

  private static Log LOG = LogFactory.getLog(SolrServiceImpl.class);

  private SolrIndexer solrIndexer;

  private SolrServer server;

  public SolrServiceImpl() {
    // final PropertyLoader pl = new PropertyLoader();
    // final String url = pl.getProperty("Solr.url");
    // this.server = new CommonsHttpSolrServer(url);
  }

  public void init(String companyId) throws MalformedURLException {
    final PropertyLoader pl = new PropertyLoader();
    String url;
    if (companyId == null || companyId.trim().equals("") || companyId.equals("10")) {
      url = pl.getProperty("Solr.url");
    }
    else {
      url = pl.getProperty("Solr.url." + companyId);
    }
    this.server = new CommonsHttpSolrServer(url);
  }

  public void setSolrIndexer(SolrIndexer solrIndexer) {
    this.solrIndexer = solrIndexer;
  }

  @Override
  public void index(String companyId, String brandId) throws Exception {
    solrIndexer.setConnection(server);
    try {
      if (brandId == null) {
        solrIndexer.deleteAll();
      }
      else {
        solrIndexer.deleteBrand(brandId);
      }
      solrIndexer.indexAll(companyId, brandId);
      solrIndexer.commit();
    }
    catch (final Exception e) {
      solrIndexer.rollback();
      throw e;
    }
    finally {
      OrderAccessor.returnConnectionToPool();
    }
  }

  @Override
  public SolrSearchResultDto findByFacet(final SolrSearchCriteriasDto criterias) {
    final SolrQuery query = new SolrQuery();

    // main query
    query.setQuery(criterias.buildQuery());

    // additional facet filters
    StringBuffer facetFilters = new StringBuffer();
    // additional facet filters included in Or when Sponsored offers
    StringBuffer facetFiltersWithOr = new StringBuffer();
    boolean addSponsored = true;
    boolean isFirstFilterWithOr = true;
    boolean isFirstFilter = true;
    for (int i = 0; i < criterias.getFilters().size(); i++) {
      if (criterias.getFilters().get(i).startsWith(SITE)) {
        addSponsored = false;
      }
      if (criterias.getFilters().get(i).startsWith(BRAND_CIRCULATION)
          || criterias.getFilters().get(i).startsWith(STATE_PROVINCE)
          || criterias.getFilters().get(i).startsWith(MEDIA_SUPPORT)) {
        // additional facet filters included in Or when Sponsored offers
        if (isFirstFilterWithOr) {
          isFirstFilterWithOr = false;
        }
        else {
          facetFiltersWithOr.append(" AND ");
        }
        facetFiltersWithOr.append(criterias.getFilters().get(i));
      }
      else {
        // additional facet filters NOT included in Or when Sponsored offers
        if (isFirstFilter) {
          isFirstFilter = false;
        }
        else {
          facetFilters.append(" AND ");
        }
        facetFilters.append(criterias.getFilters().get(i));
      }
    }
    if (facetFiltersWithOr.length() > 0) {
      if (addSponsored) {
        facetFilters.append("((" + facetFiltersWithOr + ") OR sponsored:TRUE)");
      }
      else {
        facetFilters.append(facetFiltersWithOr);
      }
    }
    // si pas de crit�re suppl�mentaire, de toute fa�on l'offre sponsoris�e
    // devrait sortir avec les crit�res de base

    // for (int i = 0; i < criterias.getFilters().size(); i++) {
    // facetFilters.append(criterias.getFilters().get(i));
    // if (criterias.getFilters().get(i).startsWith(SITE)) {
    // addSponsored = false;
    // }
    // if (criterias.getFilters().get(i).startsWith(CLIENT_STATUS)) {
    // facetFilters.append(" AND ((");
    // }
    // else if (i == criterias.getFilters().size() - 1) {
    // if (addSponsored) {
    // facetFilters.append(") OR sponsored:TRUE)");
    // }
    // else {
    // facetFilters.append("))");
    // }
    // }
    // else {
    // facetFilters.append(" AND ");
    // }
    // }
    query.setFilterQueries(facetFilters.toString());
    // facet count parameterization
    if (!criterias.getFacetFields().isEmpty() || !criterias.getFacetQueries().isEmpty()) {
      query.setFacet(true);
      query.setFacetSort("index");
      query.setFacetLimit(-1); // no limit
      for (final String facetField : criterias.getFacetFields()) {
        query.addFacetField(facetField);
      }
      for (final String facetQuery : criterias.getFacetQueries()) {
        query.addFacetQuery(facetQuery);
      }
    }

    // sorting
    query.addSortField(SPONSORED, ORDER.desc);
    query.addSortField(SPONSORED_SEQUENCE, ORDER.asc);

    if (!StringUtil.isEmpty(criterias.getSortField()) && criterias.getSortOrder() != null) {
      query.addSortField(criterias.getSortField(), criterias.getSortOrder());
    }

    query.addSortField(MEDIA_SEQUENCE, ORDER.asc);
    query.addSortField("priceAsDouble", ORDER.asc);

    // paging
    query.setStart(criterias.getStartRow());
    query.setRows(criterias.getRowCount());

    // actual query
    try {
      final QueryResponse rsp = this.server.query(query);
      final List<SolrSearchResultDto.FacetField> fields = new ArrayList<SolrSearchResultDto.FacetField>();
      if (rsp.getFacetFields() != null) {
        for (final FacetField field : rsp.getFacetFields()) {
          final List<SolrSearchResultDto.FacetValue> values = new ArrayList<SolrSearchResultDto.FacetValue>();
          if (field.getValues() != null) {
            for (final Count count : field.getValues()) {
              values.add(new SolrSearchResultDto.FacetValue(count.getName(), count.getCount()));
            }
          }
          fields.add(new SolrSearchResultDto.FacetField(field.getName(), field.getValueCount(), values));
        }
      }
      if (rsp.getFacetQuery() != null) {
        for (final Map.Entry<String, Integer> field : rsp.getFacetQuery().entrySet()) {
          fields.add(new SolrSearchResultDto.FacetField(field.getKey(), field.getValue(),
              new ArrayList<SolrSearchResultDto.FacetValue>()));
        }
      }
      // For our application, ints are big enough.
      final int startRow = (int) rsp.getResults().getStart();
      final int rowCount = (int) rsp.getResults().getNumFound();
      return new SolrSearchResultDto(fields, buildBrandTrees(rsp.getResults()), startRow, rowCount);
    }
    catch (final SolrServerException e) {
      LOG.error("Could not retrieve results from Solr instance", e);
      // Do not fail, instead simply return empty result.
      return new SolrSearchResultDto(new ArrayList<SolrSearchResultDto.FacetField>(),
          new ArrayList<EldoradoBrandDto>(), 0, 0);
    }
  }

  private List<EldoradoBrandDto> buildBrandTrees(final SolrDocumentList results) {
    final HashMap<String, EldoradoBrandDto> brandsMap = new LinkedHashMap<String, EldoradoBrandDto>();
    final HashMap<EldoradoBrandDto, HashMap<String, EldoradoMediaDto>> mediasMaps = new LinkedHashMap<EldoradoBrandDto, HashMap<String, EldoradoMediaDto>>();
    for (int i = 0; i < results.size(); i++) {
      final SolrDocument doc = results.get(i);
      final String brandId = (String) doc.getFieldValue(BRAND_ID);
      EldoradoBrandDto brand = brandsMap.get(brandId);
      if (brand == null) {
        brand = buildBrand(doc);
        brand.setMedias(new ArrayList<EldoradoMediaDto>());
        brandsMap.put(brandId, brand);
        mediasMaps.put(brand, new LinkedHashMap<String, EldoradoMediaDto>());
      }
      final HashMap<String, EldoradoMediaDto> mediasMap = mediasMaps.get(brand);
      final String mediaId = (String) doc.getFieldValue(MEDIA_ID);
      EldoradoMediaDto media = mediasMap.get(mediaId);
      if (media == null) {
        media = buildMedia(doc);
        media.setOffers(new ArrayList<MediaDto>());
        mediasMap.put(mediaId, media);
        brand.getMedias().add(media);
      }
      final MediaDto offer = buildOffer(doc);
      media.getOffers().add(offer);
      propagatePriceUpwards(brand, media, offer);
    }
    return new ArrayList<EldoradoBrandDto>(brandsMap.values());
  }

  private void propagatePriceUpwards(final EldoradoBrandDto brand, final EldoradoMediaDto media, final MediaDto offer) {
    if (media.getMedia().getBasicPrice() == null
        || offer.getBasicPrice().compareTo(media.getMedia().getBasicPrice()) < 0) {
      media.getMedia().setBasicPrice(offer.getBasicPrice());
      media.getMedia().setBasicCurrency(offer.getBasicCurrency());
      if (brand.getBrand().getBasicPrice() == null
          || media.getMedia().getBasicPrice().compareTo(brand.getBrand().getBasicPrice()) < 0) {
        brand.getBrand().setBasicPrice(media.getMedia().getBasicPrice());
        brand.getBrand().setBasicCurrency(media.getMedia().getBasicCurrency());
      }
    }
  }

  private EldoradoBrandDto buildBrand(final SolrDocument doc) {
    final EldoradoBrandDto eldoradoBrand = new EldoradoBrandDto();

    eldoradoBrand.setSponsored((Boolean) doc.getFieldValue(SPONSORED));
    eldoradoBrand.setSponsoredSequence((Integer) doc.getFieldValue(SPONSORED_SEQUENCE));

    final MediaDto brand = new MediaDto();

    final MediaIdDto mediaId = new MediaIdDto();
    mediaId.setId((String) doc.getFieldValue(BRAND_ID));
    brand.setMediaId(mediaId);

    final List<MediaNameDto> names = new ArrayList<MediaNameDto>();
    for (final String language : LANGUAGES) {
      final MediaNameDto name = new MediaNameDto();
      name.setLanguage(language);
      name.setName((String) doc.getFieldValue(prefix(BRAND_NAME, language)));
      name.setDescription((String) doc.getFieldValue(prefix(BRAND_DESC, language)));
      name.setGif((String) doc.getFieldValue(prefix(BRAND_GIF, language)));
      names.add(name);
    }
    brand.setNames(names);

    final MediaInformationDto info = new MediaInformationDto();
    // note that this may actually be the uncertified circulation, see
    // SolrIndexer for details.
    final Integer brandCirculation = (Integer) doc.getFieldValue(BRAND_CIRCULATION);
    if (brandCirculation != null) {
      info.setCirculationCopy(brandCirculation.intValue());
    }
    final Boolean brandIsCirculation = (Boolean) doc.getFieldValue(BRAND_IS_CIRCULATION);
    info.setCirculation(brandIsCirculation == null ? false : brandIsCirculation);

    final Integer brandCirculationUncertified = (Integer) doc.getFieldValue(BRAND_CIRCULATION_UNCERTIFIED);
    info.setCirculationUncertified(0);
    if (brandCirculationUncertified != null) {
      info.setCirculationUncertified(brandCirculationUncertified.intValue());
    }

    final Integer brandCirculationSold = (Integer) doc.getFieldValue(BRAND_CIRCULATION_SOLD);
    info.setCirculationSold(0);
    if (brandCirculationSold != null) {
      info.setCirculationSold(brandCirculationSold.intValue());
    }
    final Integer brandCirculationFree = (Integer) doc.getFieldValue(BRAND_CIRCULATION_FREE);
    info.setCirculationFree(0);
    if (brandCirculationFree != null) {
      info.setCirculationFree(brandCirculationFree.intValue());
    }
    final String brandDistributionType = (String) doc.getFieldValue(BRAND_DISTRIBUTION_TYPE);
    if (brandDistributionType != null) {
      info.setDistributionType(brandDistributionType);
    }
    brand.setInformation(info);

    final String brandJnlcod = (String) doc.getFieldValue(BRAND_JNLCOD);
    final String brandJnlcods = (String) doc.getFieldValue(BRAND_JNLCODS);
    if (brandJnlcod != null && brandJnlcods != null) {
      brand.setCodeList("JNLCOD-JNLCODS");
      brand.setCodeValue(brandJnlcod + "-" + brandJnlcods);
    }

    eldoradoBrand.setBrand(brand);
    return eldoradoBrand;
  }

  private EldoradoMediaDto buildMedia(final SolrDocument doc) {
    final EldoradoMediaDto eldoradoMedia = new EldoradoMediaDto();

    final MediaDto media = new MediaDto();

    final MediaIdDto mediaId = new MediaIdDto();
    mediaId.setId((String) doc.getFieldValue(MEDIA_ID));
    final MediaTypeDto mediaType = new MediaTypeDto();
    mediaType.setSupport(MediaSupportEnumDto.valueOf((String) doc.getFieldValue(MEDIA_SUPPORT)));
    mediaId.setMediaType(mediaType);
    media.setMediaId(mediaId);

    final List<MediaNameDto> names = new ArrayList<MediaNameDto>();
    for (final String language : LANGUAGES) {
      final MediaNameDto name = new MediaNameDto();
      name.setLanguage(language);
      name.setName((String) doc.getFieldValue(prefix(MEDIA_NAME, language)));
      name.setDescription((String) doc.getFieldValue(prefix(MEDIA_DESC, language)));
      name.setGif((String) doc.getFieldValue(prefix(MEDIA_GIF, language)));
      names.add(name);
    }
    media.setNames(names);

    final MediaInformationDto info = new MediaInformationDto();
    // note that this may actually be the uncertified circulation, see
    // SolrIndexer for details.
    final Integer brandCirculation = (Integer) doc.getFieldValue(MEDIA_CIRCULATION);
    if (brandCirculation != null) {
      info.setCirculationCopy(brandCirculation.intValue());
    }
    final Boolean brandIsCirculation = (Boolean) doc.getFieldValue(MEDIA_IS_CIRCULATION);
    info.setCirculation(brandIsCirculation == null ? false : brandIsCirculation);

    final Integer brandCirculationUncertified = (Integer) doc.getFieldValue(MEDIA_CIRCULATION_UNCERTIFIED);
    info.setCirculationUncertified(0);
    if (brandCirculationUncertified != null) {
      info.setCirculationUncertified(brandCirculationUncertified.intValue());
    }

    final Integer brandCirculationSold = (Integer) doc.getFieldValue(MEDIA_CIRCULATION_SOLD);
    info.setCirculationSold(0);
    if (brandCirculationSold != null) {
      info.setCirculationSold(brandCirculationSold.intValue());
    }
    final Integer brandCirculationFree = (Integer) doc.getFieldValue(MEDIA_CIRCULATION_FREE);
    info.setCirculationFree(0);
    if (brandCirculationFree != null) {
      info.setCirculationFree(brandCirculationFree.intValue());
    }
    final String brandDistributionType = (String) doc.getFieldValue(MEDIA_DISTRIBUTION_TYPE);
    if (brandDistributionType != null) {
      info.setDistributionType(brandDistributionType);
    }
    media.setInformation(info);

    eldoradoMedia.setMedia(media);
    return eldoradoMedia;
  }

  private MediaDto buildOffer(final SolrDocument doc) {
    final MediaDto offer = new MediaDto();

    final MediaIdDto mediaId = new MediaIdDto();
    mediaId.setId((String) doc.getFieldValue(OFFER_ID));
    final MediaTypeDto mediaType = new MediaTypeDto();
    mediaType.setSupport(MediaSupportEnumDto.valueOf((String) doc.getFieldValue(MEDIA_SUPPORT)));
    mediaId.setMediaType(mediaType);
    offer.setMediaId(mediaId);

    offer.setCodeList("UNIQUE");
    offer.setCodeValue((String) doc.getFieldValue(OFFER_DUPLICATE_KEY));

    final List<MediaNameDto> names = new ArrayList<MediaNameDto>();
    for (final String language : LANGUAGES) {
      final MediaNameDto name = new MediaNameDto();
      name.setLanguage(language);
      name.setName((String) doc.getFieldValue(prefix(OFFER_NAME, language)));
      name.setDescription((String) doc.getFieldValue(prefix(OFFER_DESC, language)));
      name.setGif((String) doc.getFieldValue(prefix(OFFER_GIF, language)));
      name.setPackageName((String) doc.getFieldValue(OFFER_PACKAGE));
      if (doc.getFieldValue(COLOR_OPTION) == null) {
        name.setSpecial("00");
      }
      else {
        name.setSpecial(doc.getFieldValue(COLOR_OPTION).toString());
      }

      names.add(name);
    }
    offer.setNames(names);

    offer.setBasicCurrency((String) doc.getFieldValue(CURRENCY));
    offer.setBasicPrice(new BigDecimal((String) doc.getFieldValue(PRICE)));

    SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
    formatter.setTimeZone(TimeZone.getTimeZone("Universal"));
    try {
      offer.setValidityFrom(formatter.parse(doc.getFieldValue(OFFER_START_DATE).toString()));
    }
    catch (ParseException ex) {
      offer.setValidityFrom(new Date());
    }
    try {
      offer.setValidityUntil(formatter.parse(doc.getFieldValue(OFFER_END_DATE).toString()));
    }
    catch (ParseException ex) {
      offer.setValidityUntil(new Date());
    }
    final MediaInformationDto info = new MediaInformationDto();
    // HACK TO PROVIDE THE HEADING INFORMATION TO THE XML EXPORT
    info.setTypology((String) doc.getFieldValue(HEADING));
    offer.setInformation(info);

    return offer;
  }

  private String prefix(final String name, final String language) {
    return language + PREFIX_SEPARATOR + name;
  }
}
