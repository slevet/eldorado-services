package com.publigroupe.eldorado.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.publigroupe.basis.domain.PropertyLoader;
import com.publigroupe.eldorado.dao.Rsgv30Dao;
import com.publigroupe.eldorado.domain.EldoradoFlowState;
import com.publigroupe.eldorado.domain.EldoradoNotification;
import com.publigroupe.eldorado.domain.EldoradoOrder;
import com.publigroupe.eldorado.domain.EldoradoPayment;
import com.publigroupe.eldorado.dto.EldoradoFlowStateDto;
import com.publigroupe.eldorado.dto.EldoradoPaymentDatatransDto;
import com.publigroupe.eldorado.dto.EldoradoPaymentModeEnumDto;
import com.publigroupe.eldorado.exception.EldoradoException;
import com.publigroupe.eldorado.manager.EldoradoOrderManager;
import com.publigroupe.eldorado.utils.EldoradoActiveDirectoryUtil;
import com.publigroupe.media.domain.CodeList;
import com.publigroupe.order.dao.p2000.OrderAccessor;
import com.publigroupe.order.domain.OrderDraft;
import com.publigroupe.order.dto.OrderDto;
import com.publigroupe.order.dto.OrderStatusEnumDto;
import com.publigroupe.partner.dto.PartnerAttributeDto;
import com.publigroupe.partner.service.ActiveDirectoryService;

public class EldoradoFlowServiceImpl implements EldoradoFlowService {

  private EldoradoNotification eldoradoNotification;
  private Rsgv30Dao rsgv30Dao;
  private ActiveDirectoryService activeDirectoryService;

  @Override
  public EldoradoFlowStateDto retrieveFlowState(EldoradoOrderManager orderManager) {
    EldoradoFlowStateDto stateDto = null;

    try {
      OrderDto orderDto = orderManager.getOrderDto();
      String subsidiaryCode = CodeList.extractCodeValue("UTIENTO", orderDto.getCodeList(), orderDto.getCodeValue());
      String orderNr = CodeList.extractCodeValue("CDENUM", orderDto.getCodeList(), orderDto.getCodeValue());

      EldoradoFlowState state = new EldoradoFlowState();
      stateDto = state.retrieve(subsidiaryCode, orderNr);

      OrderAccessor.commit();
    }
    catch (Exception ex) {
      OrderAccessor.rollBack();
      throw new EldoradoException(ex);
    }
    finally {
      OrderAccessor.returnConnectionToPool();
    }

    return stateDto;
  }

  @Override
  public void saveUserInfo(EldoradoOrderManager orderManager) {
    try {
      OrderDto orderDto = orderManager.getOrderDto();
      String subsidiaryCode = CodeList.extractCodeValue("UTIENTO", orderDto.getCodeList(), orderDto.getCodeValue());
      String orderNr = CodeList.extractCodeValue("CDENUM", orderDto.getCodeList(), orderDto.getCodeValue());
      String user = orderDto.getInfo().getUser().getCodeValue();

      EldoradoOrder order = new EldoradoOrder();
      order.saveUserInfo(subsidiaryCode, orderNr, user);

      OrderAccessor.commit();
    }
    catch (Exception ex) {
      OrderAccessor.rollBack();
      throw new EldoradoException(ex);
    }
    finally {
      OrderAccessor.returnConnectionToPool();
    }
  }

  @Override
  public void savePaymentAuthorization(EldoradoOrderManager orderManager, EldoradoPaymentDatatransDto paymentData) {
    try {
      OrderDto orderDto = orderManager.getOrderDto();
      String subsidiaryCode = CodeList.extractCodeValue("UTIENTO", orderDto.getCodeList(), orderDto.getCodeValue());
      String orderNr = CodeList.extractCodeValue("CDENUM", orderDto.getCodeList(), orderDto.getCodeValue());

      // Update state of payment
      EldoradoFlowState state = new EldoradoFlowState();
      EldoradoFlowStateDto paymentStateDto = state.retrieve(subsidiaryCode, orderNr);

      // Test coh�rance commande si touche back utilis�e
      if (paymentStateDto != null) {
        if (orderDto.getStatus() == OrderStatusEnumDto.IN_MODIFICATION
            && !EldoradoFlowState.isSuspended(paymentStateDto)) {
          throw new EldoradoException("REPAYMENT_NOT_SYHCHRONIZED");
        }
        else if (orderDto.getStatus() != OrderStatusEnumDto.IN_MODIFICATION
            && EldoradoFlowState.hasOrderBeenLoadedInPub2000(paymentStateDto)) {
          throw new EldoradoException("NEW_ORDER_NOT_SYHCHRONIZED");
        }
      }

      if (paymentStateDto == null) {
        state.create(subsidiaryCode, orderNr);
      }
      else {
        state.setAuthorized(subsidiaryCode, orderNr);
      }

      // handle payment only if online payment
      if (paymentData != null) {
        EldoradoPayment payment = new EldoradoPayment();
        payment.savePaymentAuthorization(subsidiaryCode, orderNr, orderDto.getInfo().getUser(), paymentData);
      }
      else {
        state.setDebited(subsidiaryCode, orderNr);
      }

      // Mrt
      // Regroupement en une seule transaction des fonctions sendP2000Validation
      // et insertP2000
      if (orderDto.getStatus() == OrderStatusEnumDto.IN_MODIFICATION) {
        sendP2000Validation(orderManager);
      }
      else {
        insertP2000(orderManager);
        // mrt to check
        OrderDraft orderDraft = new OrderDraft();
        orderDraft.delete(orderDto.getId().getId());
      }

      OrderAccessor.commit();
    }
    catch (Exception ex) {
      OrderAccessor.rollBack();
      // Notify both Cap and User
      try {
        this.eldoradoNotification.initVariables(CodeList.extractCodeValue("UTIENTO",
            orderManager.getOrderDto().getCodeList(), orderManager.getOrderDto().getCodeValue()).substring(0, 2));
        this.eldoradoNotification.sendUnresolvedErrorsNotification(
            new StringBuffer("Load P2000 Error \n" + ex.getMessage()), true);
      }
      catch (Exception ex2) {
        ex2.printStackTrace();
      }
      throw new EldoradoException(ex);
    }
    finally {
      OrderAccessor.returnConnectionToPool();
    }
  }

  @Override
  public void cancelPaymentAuthorization(EldoradoOrderManager orderManager) {
    try {
      OrderDto orderDto = orderManager.getOrderDto();
      String subsidiaryCode = CodeList.extractCodeValue("UTIENTO", orderDto.getCodeList(), orderDto.getCodeValue());
      String orderNr = CodeList.extractCodeValue("CDENUM", orderDto.getCodeList(), orderDto.getCodeValue());

      // Update state of payment
      EldoradoFlowState state = new EldoradoFlowState();
      state.cancelAuthorization(subsidiaryCode, orderNr);

      // handle payment
      EldoradoPayment payment = new EldoradoPayment();
      payment.cancelPaymentAuthorization(subsidiaryCode, orderNr);

      OrderAccessor.commit();
    }
    catch (Exception ex) {
      OrderAccessor.rollBack();
      throw new EldoradoException(ex);
    }
    finally {
      OrderAccessor.returnConnectionToPool();
    }
  }

  // @Override
  // public void insertP2000(EldoradoOrderManager orderManager)
  // {
  // try
  // {
  // OrderDto orderDto = orderManager.getOrderDto();
  // String subsidiaryCode = CodeList.extractCodeValue("UTIENTO",
  // orderDto.getCodeList(), orderDto.getCodeValue());
  // String orderNr = CodeList.extractCodeValue("CDENUM",
  // orderDto.getCodeList(), orderDto.getCodeValue());
  //
  // EldoradoFlowState state = new EldoradoFlowState();
  // // According to ELDWEB-56, we use similar subsidiaryCode and orderNr for
  // both Eldorado and P2000
  // state.setLoaded(subsidiaryCode, orderNr, subsidiaryCode,orderNr);
  //
  // // Handle command
  // EldoradoOrder order = new EldoradoOrder();
  // order.insertP2000(orderDto);
  //
  // // Notify both Cap and User
  // EldoradoNotification notification = new EldoradoNotification();
  // notification.notifyInsertP2000(orderManager,
  // orderDto.getInfo().getUser().getQualifierInfo().getEmailAddress());
  //
  // OrderAccessor.commit();
  // }
  // catch (Exception ex)
  // {
  // OrderAccessor.rollBack();
  // throw new EldoradoException(ex);
  // }
  // finally
  // {
  // OrderAccessor.returnConnectionToPool();
  // }
  // }

  // Mrt
  private void insertP2000(EldoradoOrderManager orderManager) {
    OrderDto orderDto = orderManager.getOrderDto();
    String subsidiaryCode = CodeList.extractCodeValue("UTIENTO", orderDto.getCodeList(), orderDto.getCodeValue());
    String orderNr = CodeList.extractCodeValue("CDENUM", orderDto.getCodeList(), orderDto.getCodeValue());

    EldoradoFlowState state = new EldoradoFlowState();
    // According to ELDWEB-56, we use similar subsidiaryCode and orderNr for
    // both Eldorado and P2000
    state.setLoaded(subsidiaryCode, orderNr, subsidiaryCode, orderNr);

    // Handle command
    EldoradoOrder order = new EldoradoOrder();
    order.insertP2000(orderDto);

    // Notify both Cap and User
    this.eldoradoNotification.initVariables(subsidiaryCode.substring(0, 2));
    this.eldoradoNotification.notifyInsertP2000(orderManager,
        orderDto.getInfo().getUser().getQualifierInfo().getEmailAddress());
  }

  @Override
  public void sendP2000Cancelation(EldoradoOrderManager orderManager) {
    try {
      OrderDto orderDto = orderManager.getOrderDto();
      String subsidiaryCode = CodeList.extractCodeValue("UTIENTO", orderDto.getCodeList(), orderDto.getCodeValue());
      String orderNr = CodeList.extractCodeValue("CDENUM", orderDto.getCodeList(), orderDto.getCodeValue());
      EldoradoFlowState state = new EldoradoFlowState();
      // According to ELDWEB-57, we use WWW --> No, it's WWL (Blaise dictat)
      state.setAbanbonned(subsidiaryCode, orderNr, "WWL");

      // Handle command
      EldoradoOrder order = new EldoradoOrder();
      order.sendP2000Cancelation(orderDto);

      OrderAccessor.commit();
    }
    catch (Exception ex) {
      OrderAccessor.rollBack();
      throw new EldoradoException(ex);
    }
    finally {
      OrderAccessor.returnConnectionToPool();
    }
  }

  // @Override
  // public void sendP2000Validation(EldoradoOrderManager orderManager)
  // {
  // try
  // {
  // OrderDto orderDto = orderManager.getOrderDto();
  // String subsidiaryCode = CodeList.extractCodeValue("UTIENTO",
  // orderDto.getCodeList(), orderDto.getCodeValue());
  // String orderNr = CodeList.extractCodeValue("CDENUM",
  // orderDto.getCodeList(), orderDto.getCodeValue());
  //
  // // Update payment state
  // EldoradoFlowState state = new EldoradoFlowState();
  // state.resetClientSuspensionNotification(subsidiaryCode, orderNr);
  //
  // // Handle command
  // EldoradoOrder order = new EldoradoOrder();
  // order.sendP2000Validation(orderDto);
  //
  // OrderAccessor.commit();
  // }
  // catch (Exception ex)
  // {
  // OrderAccessor.rollBack();
  // throw new EldoradoException(ex);
  // }
  // finally
  // {
  // OrderAccessor.returnConnectionToPool();
  // }
  // }

  private void sendP2000Validation(EldoradoOrderManager orderManager) {
    OrderDto orderDto = orderManager.getOrderDto();
    String subsidiaryCode = CodeList.extractCodeValue("UTIENTO", orderDto.getCodeList(), orderDto.getCodeValue());
    String orderNr = CodeList.extractCodeValue("CDENUM", orderDto.getCodeList(), orderDto.getCodeValue());

    // Update payment state
    EldoradoFlowState state = new EldoradoFlowState();
    state.resetClientSuspensionNotification(subsidiaryCode, orderNr);

    // Handle command
    EldoradoOrder order = new EldoradoOrder();
    order.sendP2000Validation(orderDto);

  }

  private EldoradoPaymentModeEnumDto convertPaymentMoteStringToEnum(String paymentMode) {
    if (paymentMode.endsWith("INVOICE_PAYMENT")) {
      return EldoradoPaymentModeEnumDto.INVOICE_PAYMENT;
    }
    if (paymentMode.endsWith("INVOICE_REQUESTED")) {
      return EldoradoPaymentModeEnumDto.INVOICE_REQUESTED;
    }
    return EldoradoPaymentModeEnumDto.ONLINE_PAYMENT;
  }

  private String getRegisterdeOu() {
    return PropertyLoader.getProperty(this, "ActiveDirectory.registeredUsers");
  }

  private String getUnRegisterdeOu() {
    return PropertyLoader.getProperty(this, "ActiveDirectory.unRegisteredUsers");
  }

  private String getLdapType() {
    return PropertyLoader.getProperty(this, "ActiveDirectory.type");
  }

  public EldoradoPaymentModeEnumDto retrievePaymentMode(String userId) {
    List<String> attributeKeys = new ArrayList<String>();
    attributeKeys.add(EldoradoActiveDirectoryUtil.getAttributeNamePaymentMode(getLdapType()));
    try {
      List<PartnerAttributeDto> attributes = activeDirectoryService.readUser(userId, getRegisterdeOu(), attributeKeys);
      Map<String, String> mapAttributes = EldoradoActiveDirectoryUtil.convertListToMap(attributes);
      return convertPaymentMoteStringToEnum(EldoradoActiveDirectoryUtil.getMapValue(mapAttributes,
          EldoradoActiveDirectoryUtil.getAttributeNamePaymentMode(getLdapType())));
    }
    catch (Exception ex) {
      return EldoradoPaymentModeEnumDto.ONLINE_PAYMENT;
    }

    // try {
    // return this.rsgv30Dao.retrieve(userId);
    // }
    // finally {
    // OrderAccessor.returnConnectionToPool();
    // }
  }

  // @deprecated
  public EldoradoPaymentModeEnumDto retrieveP2000PaymentMode(String userId) {
    try {
      return this.rsgv30Dao.retrieve(userId);
    }
    finally {
      OrderAccessor.returnConnectionToPool();
    }
  }

  //
  public void modifyPaymentMode(String userId, String bpoUserId, EldoradoPaymentModeEnumDto paymentMode) {
    List<PartnerAttributeDto> attributes = new ArrayList<PartnerAttributeDto>();
    attributes.add(new PartnerAttributeDto(EldoradoActiveDirectoryUtil.getAttributeNamePaymentMode(getLdapType()),
        EldoradoActiveDirectoryUtil.fillEmptyField(paymentMode.toString())));
    activeDirectoryService.updateUser(userId, getRegisterdeOu(), attributes);
    try {
      this.rsgv30Dao.update(bpoUserId, paymentMode);
      OrderAccessor.commit();
    }
    catch (Exception ex) {
      OrderAccessor.rollBack();
      throw new EldoradoException(ex);
    }
    finally {
      OrderAccessor.returnConnectionToPool();
    }
  }

  public void setEldoradoNotification(EldoradoNotification eldoradoNotification) {
    this.eldoradoNotification = eldoradoNotification;
  }

  public void setRsgv30Dao(Rsgv30Dao rsgv30Dao) {
    this.rsgv30Dao = rsgv30Dao;
  }

  public void setActiveDirectoryService(ActiveDirectoryService activeDirectoryService) {
    this.activeDirectoryService = activeDirectoryService;
  }

}
