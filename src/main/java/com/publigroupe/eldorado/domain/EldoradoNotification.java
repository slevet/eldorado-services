package com.publigroupe.eldorado.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.publigroupe.basis.dto.LanguageEnumDto;
import com.publigroupe.basis.manager.TranslationManager;
import com.publigroupe.eldorado.dao.Rsgv1fDao;
import com.publigroupe.eldorado.dao.bean.Rsgv1f;
import com.publigroupe.eldorado.dto.EldoradoPaymentModeEnumDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferInsertionDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferPlacementDto;
import com.publigroupe.eldorado.exception.EldoradoException;
import com.publigroupe.eldorado.manager.EldoradoOrderManager;
import com.publigroupe.eldorado.manager.EldoradoTools;
import com.publigroupe.javaBase.base.StringUtil;
import com.publigroupe.javaBase.network.mail.Mail;
import com.publigroupe.javaBase.network.mail.MailService;
import com.publigroupe.javaBase.property.PropertyLoader;
import com.publigroupe.media.domain.CodeList;
import com.publigroupe.media.dto.MediaSupportEnumDto;
import com.publigroupe.order.dto.OrderMaterialDto;
import com.publigroupe.order.dto.OrderMaterialUEditorPrintDto;
import com.publigroupe.order.dto.OrderMaterialUploadDto;
import com.publigroupe.order.dto.OrderPlacementDto;
import com.publigroupe.order.dto.OrderPlacementGroupDto;
import com.publigroupe.partner.dto.PartnerCountryEnumDto;
import com.publigroupe.partner.dto.PartnerDto;
import com.publigroupe.partner.dto.PartnerPhysicalAddressDto;
import com.publigroupe.partner.dto.PartnerQualifierInfoDto;

public class EldoradoNotification {
  private static Log log = LogFactory.getLog(EldoradoNotification.class);
  private static final String TRANSLATION_MAIL = ".MAIL.";
  private static final String TRANSLATION_CONTACT = ".CONTACT.";
  private static final String TRANSLATION_PROFILE = ".ELDORADO_PROFILE.";
  private static final String TRANSLATION_PAYMENT = ".ELDORADO_PAYMENT.";
  private static final String TRANSLATION_GREETING = ".GREETING.";
  private static final String TRANSLATION_HEADING = ".STANDARD_HEADING_ELDORADO.";

  private String mailServer;
  private String mailFrom;
  private String mailCap;
  private String mailAdminInvoice;
  private String mailContact;
  private String mailErrors;
  private String mailDeveloppment;
  private String activationURL;
  private String retrieveOrderURL;
  private String environnment;
  private String baseUrl;
  private String ueditorEcmsUrlPrefix;
  private String ueditorOnlineEcmsUrlPrefix;
  private String eldoradoTranslationPrefix = "ELDORADO.WEB";
  private String adminEldoradoTranslationPrefix = "ELDORADO.ADMIN";
  private LanguageEnumDto languageEnum;

  public EldoradoNotification() {
    String bpoVariable = "";
    PropertyLoader pl = new PropertyLoader();
    mailServer = pl.getProperty("Notification.Email.SMTP" + bpoVariable);
    mailFrom = pl.getProperty("Notification.Email.From" + bpoVariable);
    mailCap = pl.getProperty("Notification.Email.Cap" + bpoVariable);
    mailAdminInvoice = pl.getProperty("Notification.Email.AdminInvoice" + bpoVariable);
    mailContact = pl.getProperty("Notification.Email.Contact" + bpoVariable);
    mailErrors = pl.getProperty("Notification.Email.Error" + bpoVariable);
    mailDeveloppment = pl.getProperty("Notification.Email.Developpment" + bpoVariable);
    activationURL = pl.getProperty("Activation.Email.Url" + bpoVariable);
    retrieveOrderURL = pl.getProperty("RetrieveOrder.Email.Url" + bpoVariable);
    environnment = pl.getProperty("OrderServicesDb.environment");
    baseUrl = pl.getProperty("PrintSummary.Eldorado.baseUrl" + bpoVariable);
    ueditorEcmsUrlPrefix = pl.getProperty("UEditorPrint.ecmsurlprefix");
    ueditorOnlineEcmsUrlPrefix = pl.getProperty("UEditorOnline.ecmsurlprefix");
    eldoradoTranslationPrefix = pl.getProperty("Application.translation" + bpoVariable);
    adminEldoradoTranslationPrefix = pl.getProperty("Application.adminTranslation" + bpoVariable);
  }

  /**
   * Send an Notification to provided user email
   * 
   * @param mailTo
   *          receiver of the email
   * @param title
   *          title of the mail
   * @param content
   *          content of the mail
   */

  private String getBpoVariable(String bpoPrefix) {
    if (bpoPrefix == null || bpoPrefix.equals("") || bpoPrefix.equals("10")) {
      return "";
    }
    return "." + bpoPrefix;
  }

  public void initVariables(String bpoPrefix) {
    String bpoVariable = getBpoVariable(bpoPrefix);
    PropertyLoader pl = new PropertyLoader();
    mailServer = pl.getProperty("Notification.Email.SMTP" + bpoVariable);
    mailFrom = pl.getProperty("Notification.Email.From" + bpoVariable);
    mailCap = pl.getProperty("Notification.Email.Cap" + bpoVariable);
    mailAdminInvoice = pl.getProperty("Notification.Email.AdminInvoice" + bpoVariable);
    mailContact = pl.getProperty("Notification.Email.Contact" + bpoVariable);
    mailErrors = pl.getProperty("Notification.Email.Error" + bpoVariable);
    mailDeveloppment = pl.getProperty("Notification.Email.Developpment" + bpoVariable);
    activationURL = pl.getProperty("Activation.Email.Url" + bpoVariable);
    retrieveOrderURL = pl.getProperty("RetrieveOrder.Email.Url" + bpoVariable);
    environnment = pl.getProperty("OrderServicesDb.environment");
    baseUrl = pl.getProperty("PrintSummary.Eldorado.baseUrl" + bpoVariable);
    ueditorEcmsUrlPrefix = pl.getProperty("UEditorPrint.ecmsurlprefix");
    ueditorOnlineEcmsUrlPrefix = pl.getProperty("UEditorOnline.ecmsurlprefix");
    eldoradoTranslationPrefix = pl.getProperty("Application.translation" + bpoVariable);
    adminEldoradoTranslationPrefix = pl.getProperty("Application.adminTranslation" + bpoVariable);
  }

  private void sendMail(String strMailForm, String mailTo, String mailCc, String title, String content,
      List<MailFile> files) {
    MailService mailService = new MailService(mailServer, strMailForm);
    mailService.addAddressTo(mailTo);
    Mail mail = mailService.createMail();
    mail.addBcc(mailDeveloppment);
    if (mailCc.length() > 0) {
      mail.addCc(mailCc);
    }
    mail.setSubject(title);
    mail.setText(content);

    if (files != null && !files.isEmpty()) {
      try {
        MimeMessage mimeMessage = mail.getMessage();
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(content);
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        for (MailFile item : files) {
          messageBodyPart = new MimeBodyPart();
          DataSource source = new FileDataSource(item.file);
          messageBodyPart.setDataHandler(new DataHandler(source));
          messageBodyPart.setFileName(item.filename);
          multipart.addBodyPart(messageBodyPart);
        }
        mimeMessage.setContent(multipart);
      }
      catch (Exception ex) {
        throw new EldoradoException(ex);
      }
    }

    mail.send();
  }

  public void notifyUserActivation(String mailTo, String language) {
    log.info("EldoradoNotification#notifyUserActivation");
    setLanguageEnum(language);
    String key = EldoradoTools.encrypt(mailTo);
    String content = activationURL + key;
    StringBuilder sb = new StringBuilder();
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "ACTIVATION", languageEnum));
    sb.append("\n");
    sb.append(content);
    sb.append("\n\n");
    sb.append(getEndMail());

    sendMail(mailFrom, mailTo, "", getUserSubject("New", mailTo), sb.toString(), null);
    log.info("Sent REGISTRATION ACTIVATION email to '" + mailTo + "'.");
  }

  /**
   * Notify User about the reset password
   */
  public void notifyUserResetPassword(String mailTo, String newPassword, String language) {
    log.info("EldoradoNotification#notifyUserResetPassword");
    setLanguageEnum(language);
    StringBuilder sb = new StringBuilder();
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "PWD_RESET", languageEnum));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "USER", languageEnum));
    sb.append(mailTo);
    sb.append("\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "PWD", languageEnum));
    sb.append(newPassword);
    sb.append("\n\n");
    sb.append(getEndMail());

    sendMail(mailFrom, mailTo, "", getUserSubject("PwdLost", mailTo), sb.toString(), null);
    log.info("Sent RESET PASSWORD email to '" + mailTo + "'.");
  }

  /**
   * Notify User about the suspending of his order
   */
  public void notifySuspend(EldoradoOrderManager orderManager, String mailTo) {
    log.info("EldoradoNotification#notifySuspend");
    setLanguageEnum(orderManager);
    String url = retrieveOrderURL + EldoradoTools.encrypt(orderManager.getOrderDto().getCodeValue());

    StringBuilder sb = new StringBuilder();
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum));
    sb.append("\n\n");
    // replace orderManager.getOrderDto().getCodeValue()
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUSPEND", languageEnum).replace(
        "<%%%%>", orderManager.getOrderDto().getCodeValue()));
    sb.append("\n\n");
    sb.append(retrieveComment(orderManager));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUSPEND_1", languageEnum));
    sb.append("\n");
    sb.append(url);
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUSPEND_2", languageEnum));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUSPEND_3", languageEnum));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUSPEND_4", languageEnum));
    sb.append("\n\n");
    sb.append(getEndMail());

    sendMail(mailCap, mailTo, "", getOrderSubject("Suspend", orderManager.getOrderDto().getCodeValue()), sb.toString(),
        null);
    log.info("Sent SUSPEND ORDER email to '" + mailTo + "' for order=" + orderManager.getOrderDto().getCodeValue());
  }

  /**
   * Notify User or CAP about the cancellation of the order. Depending on the
   * state of RSGV26, we send notification to
   * CAP or User
   */
  public void notifyCancellationUser(EldoradoOrderManager orderManager) {
    notifyCancellation(orderManager,
        orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getEmailAddress());
  }

  /**
   * Notify User or CAP about the cancellation of the order. Depending on the
   * state of RSGV26, we send notification to
   * CAP or User
   */
  public void notifyCancellationCap(EldoradoOrderManager orderManager) {
    notifyCancellation(orderManager, mailCap);
  }

  /**
   * Notify User about the suspending of his order
   */
  public void notifyCancellation(EldoradoOrderManager orderManager, String mailTo) {
    log.info("EldoradoNotification#notifyCancellation");
    setLanguageEnum(orderManager);

    StringBuilder sb = new StringBuilder();
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum));
    sb.append("\n\n");
    // replace orderManager.getOrderDto().getCodeValue()
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "CANCEL", languageEnum).replace(
        "<%%%%>", orderManager.getOrderDto().getCodeValue()));
    sb.append("\n\n");
    sb.append(retrieveComment(orderManager));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "CANCEL_1", languageEnum));
    sb.append("\n\n");
    sb.append(getEndMail());

    sendMail(mailFrom, mailTo, "", getOrderSubject("Cancel", orderManager.getOrderDto().getCodeValue()), sb.toString(),
        null);
    log.info("Sent CANCEL ORDER email to '" + mailTo + "' for order=" + orderManager.getOrderDto().getCodeValue());
  }

  /**
   * Send Notification of the validation of the payment to the user
   */
  public void notifyValidation(EldoradoOrderManager orderManager, String mailTo) {
    log.info("EldoradoNotification#notifyValidation");
    setLanguageEnum(orderManager);
    String url = retrieveOrderURL + EldoradoTools.encrypt(orderManager.getOrderDto().getCodeValue());

    StringBuilder sb = new StringBuilder();
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum));
    sb.append("\n\n");
    // replace orderManager.getOrderDto().getCodeValue()
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "VALIDATION", languageEnum).replace(
        "<%%%%>", orderManager.getOrderDto().getCodeValue()));
    sb.append("\n\n");
    sb.append(retrieveComment(orderManager));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "VALIDATION_1", languageEnum));
    sb.append("\n");
    sb.append(url);
    sb.append("\n\n");
    sb.append(getEndMail());

    sendMail(mailFrom, mailTo, "", getOrderSubject("Validated", orderManager.getOrderDto().getCodeValue()),
        sb.toString(), null);
    log.info("Sent VALIDATED ORDER email to '" + mailTo + "' for order=" + orderManager.getOrderDto().getCodeValue());
  }

  /**
   * Send Notification to the user for the feed back after the first issueDate.
   */
  public void notifyFeedBackIssueDate(EldoradoOrderManager orderManager, String mailTo) {
    log.info("EldoradoNotification#FeedBackIssueDate");
    setLanguageEnum(orderManager);

    StringBuilder sb = new StringBuilder();
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "FEED_BACK_SALUTATION",
        languageEnum));
    sb.append("\n\n");
    // replace orderManager.getOrderDto().getCodeValue()
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "FEED_BACK_1", languageEnum));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "FEED_BACK_2", languageEnum));
    sb.append(" ");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "FEED_BACK_URL", languageEnum));
    sb.append(" ");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "FEED_BACK_3", languageEnum));
    sb.append("\n\n");
    sb.append(getEndMailFeedBack());

    sendMail(mailFrom, mailTo, "", getOrderSubject("FeedBack", orderManager.getOrderDto().getCodeValue()),
        sb.toString(), null);
    log.info("Sent FEEDBACK email to '" + mailTo + "' for order=" + orderManager.getOrderDto().getCodeValue());
  }

  /**
   * Send Notification of the validation of the payment to the Cap
   */
  public void notifyValidationCap(EldoradoOrderManager orderManager) {
    notifyValidation(orderManager, mailCap);
  }

  /**
   * Notification about insertion in P2000
   * 
   * @param mailTo
   */
  public void notifyInsertP2000(EldoradoOrderManager orderManager, String mailTo) {
    log.info("EldoradoNotification#notifyInsertP2000");
    setLanguageEnum(orderManager);
    String url = retrieveOrderURL + EldoradoTools.encrypt(orderManager.getOrderDto().getCodeValue());

    StringBuilder sb = new StringBuilder();

    // Mail CAP
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum));
    sb.append("\n\n");
    // replace orderManager.getOrderDto().getCodeValue()
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUBMITTED", languageEnum).replace(
        "<%%%%>", orderManager.getOrderDto().getCodeValue()));
    sb.append("\n\n");
    sb.append(generateMailMediaData(orderManager));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUBMITTED_1", languageEnum));
    sb.append(" ").append(
        TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUBMITTED_2", languageEnum));
    sb.append("\n");
    sb.append(url);
    sb.append("\n\n");
    sb.append(getEndMail());
    sendMail(mailFrom, mailCap, "", getOrderSubject("Submitted", orderManager.getOrderDto().getCodeValue()),
        sb.toString(), extractContentFiles(orderManager));
    log.info("P2000 INSERTION for order " + orderManager.getOrderDto().getCodeValue() + " OK. Sent email to CAP '"
        + mailCap + "'.");

    // Mail user
    sb.setLength(0);
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum));
    sb.append("\n\n");
    // replace orderManager.getOrderDto().getCodeValue()
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUBMITTED", languageEnum).replace(
        "<%%%%>", orderManager.getOrderDto().getCodeValue()));
    sb.append("\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUBMITTED_1", languageEnum));
    sb.append(" ").append(
        TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUBMITTED_2", languageEnum));
    sb.append("\n");
    sb.append(url);
    sb.append("\n\n");
    sb.append(getEndMail());
    sendMail(mailFrom, mailTo, "", getOrderSubject("Submitted", orderManager.getOrderDto().getCodeValue()),
        sb.toString(), null);
    log.info("P2000 INSERTION for order " + orderManager.getOrderDto().getCodeValue() + " OK. Sent email to user '"
        + mailTo + "'.");
  }

  public void sendUnresolvedErrorsNotification(StringBuffer errMsg, boolean existCriticalError) {
    log.info("EldoradoNotification#sendUnresolvedErrorsNotification");

    String obj;
    obj = "";
    String mailCc;
    mailCc = "";

    if (!environnment.equals("PROD")) {
      obj = obj.concat(environnment).concat(" : ");
    }
    if (existCriticalError) {
      obj = obj.concat(" CRITICAL ERROR");
      mailCc = mailErrors;
    }
    obj = obj.concat(" Eldorado Orders to analyze");

    sendMail(mailFrom, mailDeveloppment, mailCc, obj, errMsg.toString(), null);
    log.info("Sent UNRESOLVED ERRORS email to '" + mailErrors + "'.");
  }

  private void setLanguageEnum(String language) {
    languageEnum = LanguageEnumDto.getByValue(language);
  }

  private void setLanguageEnum(EldoradoOrderManager orderManager) {
    languageEnum = LanguageEnumDto.getByValue(orderManager.getLanguage());
  }

  /**
   * get Mail subject for user
   */
  private String getUserSubject(String action, String user) {
    StringBuilder sb = new StringBuilder();

    if (!environnment.equals("PROD")) {
      sb.append(environnment + " : ");
    }
    if (action.equals("New")) {
      sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "ACTIVATION_SUBJECT",
          languageEnum));
    }
    else if (action.equals("PwdLost")) {
      sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "PWD_RESET_SUBJECT",
          languageEnum));
    }
    sb.append(" ").append(user);

    return sb.toString();
  }

  /**
   * get Mail subject for order
   */
  private String getOrderSubject(String action, String orderNumberAsString) {
    StringBuilder sb = new StringBuilder();

    if (!environnment.equals("PROD")) {
      sb.append(environnment + " : ");
    }
    if (action.equals("Suspend")) {
      // orderNumberAsString
      sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUSPEND_SUBJECT",
          languageEnum).replace("<%%%%>", orderNumberAsString));
    }
    else if (action.equals("Cancel")) {
      // orderNumberAsString
      sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "CANCEL_SUBJECT",
          languageEnum).replace("<%%%%>", orderNumberAsString));
    }
    else if (action.equals("Validated")) {
      // orderNumberAsString
      sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "VALIDATION_SUBJECT",
          languageEnum).replace("<%%%%>", orderNumberAsString));
    }
    else if (action.equals("Submitted")) {
      // orderNumberAsString
      sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SUBMITTED_SUBJECT",
          languageEnum).replace("<%%%%>", orderNumberAsString));
    }
    else if (action.equals("FeedBack")) {
      // orderNumberAsString
      sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "FEED_BACK_SUBJECT",
          languageEnum).replace("<%%%%>", orderNumberAsString));
    }

    return sb.toString();
  }

  private String getEndMail() {
    StringBuilder sb = new StringBuilder();

    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "THANKS", languageEnum)).append(
        "\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SIGNATURE", languageEnum)).append(
        "\n").append(baseUrl).append("\n");

    return sb.toString();
  }

  private String getEndMailFeedBack() {
    StringBuilder sb = new StringBuilder();

    sb.append(
        TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "FEED_BACK_THANKS", languageEnum)).append(
        "\n\n");
    sb.append(
        TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "FEED_BACK_SIGNATURE", languageEnum)).append(
        "\n").append(baseUrl).append("\n");

    return sb.toString();
  }

  private String retrieveComment(EldoradoOrderManager orderManager) {
    String subsidiaryCode = CodeList.extractCodeValue("UTIENTO", orderManager.getOrderDto().getCodeList(),
        orderManager.getOrderDto().getCodeValue());
    String orderNr = CodeList.extractCodeValue("CDENUM", orderManager.getOrderDto().getCodeList(),
        orderManager.getOrderDto().getCodeValue());
    Rsgv1fDao rsgv1fDao = new Rsgv1fDao();
    Rsgv1f rsgv1f = rsgv1fDao.findLast(subsidiaryCode, orderNr);
    if (null == rsgv1f) {
      return "";
    }
    else {
      return rsgv1f.mailtxt;
    }
  }

  public void sendContactFormToCap(String problem, String firstName, String lastName, String firmName, String country,
      String email, String phone, String remarks, String language) {
    setLanguageEnum(language);
    final StringBuilder sb = new StringBuilder();
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum)).append(
        "\n\n");
    sb.append(
        TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "CONTACT_INTRODUCTION", languageEnum)).append(
        "\n\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "FIRSTNAME", languageEnum)).append(
        " :\t").append(firstName).append("\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "LASTNAME", languageEnum)).append(
        " :\t").append(lastName).append("\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "FIRM_NAME", languageEnum)).append(
        " :\t").append(firmName).append("\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "COUNTRY", languageEnum)).append(
        " :\t").append(country).append("\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "EMAIL", languageEnum)).append(
        " :\t").append(email).append("\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "PHONE", languageEnum)).append(
        " :\t").append(phone).append("\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "PROBLEM", languageEnum)).append(
        " :\t").append(problem).append("\n");
    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "REMARKS", languageEnum)).append(
        " :\n").append(remarks).append("\n");

    String subject;
    if (!environnment.equals("PROD")) {
      subject = environnment + " : ";
    }
    else {
      subject = "";
    }

    subject = subject.concat(
        TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "SUBJECT", languageEnum)).concat(
        " : ").concat(problem);

    sendMail(email, mailContact, email, subject, sb.toString(), null);
  }

  /**
   * Notify the CAP that the given user requested invoice payment mode.
   * 
   * @param user
   */
  public void sendInvoicePaymentRequestToCap(PartnerDto user) {
    setLanguageEnum(user.getLanguage());
    String subject;
    if (!environnment.equals("PROD")) {
      subject = environnment + " : ";
    }
    else {
      subject = "";
    }
    subject += TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_SUBJECT",
        languageEnum);
    String content = "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum);
    content += "\n\n" + generateUserDetailsContent(user);
    content += "\n\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_TEXT1",
            languageEnum);
    content += "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_TEXT2",
            languageEnum);
    content += "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_TEXT3",
            languageEnum);
    content += "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_TEXT4",
            languageEnum);
    content += "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_TEXT5",
            languageEnum);
    content += "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_TEXT6",
            languageEnum);
    content += "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_THANKS_ADMIN",
            languageEnum);

    sendMail(mailFrom, mailAdminInvoice, "", subject, content, null);
  }

  /**
   * Notify the User that the given requested invoice payment mode.
   * 
   * @param user
   */
  public void sendInvoicePaymentRequestToUser(PartnerDto user) {
    setLanguageEnum(user.getLanguage());
    String subject;
    if (!environnment.equals("PROD")) {
      subject = environnment + " : ";
    }
    else {
      subject = "";
    }
    subject += TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_CONF_SUBJ",
        languageEnum);
    String content = "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum);
    content += "\n\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_CONF_TEXT1",
            languageEnum);
    content += "\n\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_CONF_TEXT2",
            languageEnum);
    content += "\n\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_CONF_THANKS",
            languageEnum);
    content += "\n\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_CONF_SIGN",
            languageEnum);

    sendMail(mailFrom, user.getQualifierInfo().getEmailAddress(), "", subject, content, null);
  }

  /**
   * Notify the CAP that given user details have changed.
   * Only called for users that have
   * {@link EldoradoPaymentModeEnumDto#INVOICE_PAYMENT} as payment mode.
   * 
   * @param user
   */
  public void notifyUserModificationToCap(PartnerDto user) {
    setLanguageEnum(user.getLanguage());
    String subject;
    if (!environnment.equals("PROD")) {
      subject = environnment + " : ";
    }
    else {
      subject = "";
    }
    subject += TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "PROFILE_UPDATE_SUBJECT",
        languageEnum);
    // i18n
    String content = "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "SALUTATION", languageEnum);
    content += "\n\n" + generateUserDetailsContent(user);
    content += "\n\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_ASKED_TEXT1",
            languageEnum);
    content += "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_UPDATE_TEXT2",
            languageEnum);
    content += "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_UPDATE_TEXT3",
            languageEnum);
    content += "\n"
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "INVOICE_THANKS_ADMIN",
            languageEnum);

    sendMail(mailFrom, mailAdminInvoice, "", subject, content, null);
  }

  /**
   * Generate a text listing the given user details (name, address, etc.).
   * 
   * @param user
   *          The user to generate the text content from.
   * @return The generated text content.
   */
  private String getSwissZip(String zip) {
    if (zip == null) {
      return "";
    }
    if (zip.length() < 5) {
      return zip;
    }
    return zip.substring(0, 4);
  }

  private String generateUserDetailsContent(PartnerDto user) {
    if (user == null) {
      return "";
    }

    LanguageEnumDto language = LanguageEnumDto.getByValue(user.getLanguage());

    String content = "";
    content += TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "USER", language) + " : "
        + user.getId().getId() + "\n\n";

    PartnerQualifierInfoDto qualifierInfo = user.getQualifierInfo();

    if (qualifierInfo != null) {
      if (qualifierInfo.getQualifier().toString().equals("COMPANY")) {
        if (qualifierInfo.getCompanyName() != null)
          content += " - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "FIRM_NAME", languageEnum)
              + " : " + qualifierInfo.getCompanyName() + "\n";
        if (qualifierInfo.getCompanyContactName() != null)
          content += " - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "FIRM_CONTACT",
                  languageEnum) + " : " + qualifierInfo.getCompanyContactName() + "\n";
        if (user.getFree2() != null && !StringUtil.isEmpty(user.getFree2()))
          content += " - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_PAYMENT + "COMPANY_NR",
                  languageEnum) + " : " + user.getFree2() + "\n";
      }
      else {
        if (qualifierInfo.getQualifier() != null)
          content += " - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "QUALIFIER", languageEnum)
              + " : "
              + TranslationManager.getCode(eldoradoTranslationPrefix + TRANSLATION_GREETING
                  + qualifierInfo.getQualifier().toString(), languageEnum) + "\n";
        if (qualifierInfo.getFirstName() != null)
          content += " - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "FIRSTNAME", languageEnum)
              + " : " + qualifierInfo.getFirstName() + "\n";
        if (qualifierInfo.getLastName() != null)
          content += " - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "LASTNAME", languageEnum)
              + " : " + qualifierInfo.getLastName() + "\n";
      }
      if (qualifierInfo.getMobilePhone() != null) {
        content += " - "
            + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "MOBILE", languageEnum)
            + " : ";
        if (qualifierInfo.getMobileCountryCode() != null)
          content += qualifierInfo.getMobileCountryCode() + " ";
        content += qualifierInfo.getMobilePhone() + "\n";
      }
      if (qualifierInfo.getPhoneNumber() != null) {
        content += " - "
            + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "PHONE", languageEnum)
            + " : ";
        if (qualifierInfo.getPhoneCountryCode() != null)
          content += qualifierInfo.getPhoneCountryCode() + " ";
        content += qualifierInfo.getPhoneNumber() + "\n";
      }
      if (qualifierInfo.getEmailAddress() != null)
        content += " - "
            + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "EMAIL", languageEnum)
            + " : " + qualifierInfo.getEmailAddress() + "\n";

    }
    PartnerPhysicalAddressDto address = user.getPhysicalAddress();
    if (address != null) {
      if (address.getStreet() != null) {
        content += " - "
            + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "STREET", languageEnum)
            + " : " + address.getStreet();
        if (address.getStreetNr() != null)
          content += " " + address.getStreetNr();
        if (address.getStreetNrComp() != null)
          content += " " + address.getStreetNrComp() + "\n";
      }
      String strZip;
      strZip = "";
      if (address.getCountry() != null) {
        if (address.getCountry() == PartnerCountryEnumDto.CH) {
          strZip = getSwissZip(address.getZip());
        }
        else {
          strZip = address.getZip();
        }
      }
      content += " - "
          + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "CITY", languageEnum) + " : "
          + strZip + " " + address.getCity() + "\n";
      if (address.getRegion() != null)
        content += " - "
            + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "REGION", languageEnum)
            + " : " + address.getRegion() + "\n";
      if (address.getCountry() != null)
        content += " - "
            + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "COUNTRY", languageEnum)
            + " : " + address.getCountry() + "\n";
      // PO BOX
      if (address.getPoBoxNr() != null)
        content += " - "
            + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_PROFILE + "POBOX", languageEnum)
            + " : " + address.getPoBoxNr() + "\n";
      strZip = "";
      if (address.getCountry() != null) {
        if (address.getCountry() == PartnerCountryEnumDto.CH && !StringUtil.isEmpty(address.getPoBoxZip())) {
          strZip = getSwissZip(address.getPoBoxZip());
        }
        else {
          strZip = address.getPoBoxZip();
        }
      }
      content += " - "
          + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_PROFILE + "CITY_POBOX", languageEnum)
          + " : " + strZip + " " + address.getPoBoxCity() + "\n";
    }

    return content;
  }

  private List<MailFile> extractContentFiles(EldoradoOrderManager orderManager) {
    List<MailFile> mailFiles = new ArrayList<MailFile>();

    for (OrderPlacementGroupDto placementGroupDto : orderManager.getOrderDto().getPlacementGroups()) {
      for (OrderPlacementDto placementDto : placementGroupDto.getPlacements()) {
        if (!StringUtil.isEmpty(placementDto.getMaterialId())) {
          for (OrderMaterialDto materialDto : orderManager.getOrderDto().getMaterials()) {
            if (materialDto.getId().equals(placementDto.getMaterialId())) {
              if (materialDto instanceof OrderMaterialUEditorPrintDto) {
                try {
                  MailFile mailFile = extractUeditorFile(((OrderMaterialUEditorPrintDto) materialDto).getCmsUuid(),
                      "pdf");
                  mailFiles.add(mailFile);
                  MailFile mailFile1 = extractUeditorFile(((OrderMaterialUEditorPrintDto) materialDto).getCmsUuid(),
                      "txt");
                  mailFiles.add(mailFile1);
                }
                catch (Exception ex) {
                  // throw new EldoradoException(ex);
                }
              }
              else if (materialDto instanceof OrderMaterialUploadDto) {
                try {
                  MailFile mailFile = extractUploadedFile(((OrderMaterialUploadDto) materialDto).getFileUrl(),
                      ((OrderMaterialUploadDto) materialDto).getFileName());
                  mailFiles.add(mailFile);
                }
                catch (Exception ex) {
                  // throw new EldoradoException(ex);
                }
              }
            }
          }
        }
      }
    }

    return mailFiles;
  }

  private MailFile extractUeditorFile(String cmsUuid, String extension) throws IOException, FileNotFoundException {
    MailFile mailFile = new MailFile();

    mailFile.filename = cmsUuid + "." + extension;
    URL url = new URL(ueditorEcmsUrlPrefix + mailFile.filename);
    mailFile.file = File.createTempFile(cmsUuid, ".tmp");
    FileOutputStream fos = new FileOutputStream(mailFile.file);
    try {
      InputStream is = url.openStream();
      try {
        byte[] buf = new byte[2048];
        int len;
        while ((len = is.read(buf)) > 0) {
          fos.write(buf, 0, len);
        }
      }
      finally {
        is.close();
      }
    }
    finally {
      fos.close();
    }

    return mailFile;
  }

  private MailFile extractUploadedFile(String fileUrl, String fileName) throws IOException, FileNotFoundException {
    MailFile mailFile = new MailFile();
    mailFile.filename = fileName;
    URL url = new URL(fileUrl + fileName);
    mailFile.file = File.createTempFile(fileName.replace(".pdf", ""), ".tmp");
    FileOutputStream fos = new FileOutputStream(mailFile.file);
    try {
      InputStream is = url.openStream();
      try {
        byte[] buf = new byte[2048];
        int len;
        while ((len = is.read(buf)) > 0) {
          fos.write(buf, 0, len);
        }
      }
      finally {
        is.close();
      }
    }
    finally {
      fos.close();
    }

    return mailFile;
  }

  private class MailFile {
    public String filename;
    public File file;
  }

  private StringBuilder generateMailMediaData(EldoradoOrderManager orderManager) {
    EldoradoSummaryDto summary = orderManager.retrieveSummary(false, false, true);
    StringBuilder sb = new StringBuilder();

    if (orderManager.getOrderDto().getInfo().getUser() != null) {
      sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "PROFIL", languageEnum)
          + "\n");
      if (orderManager.getOrderDto().getInfo().getUser().getLanguage() != null)
        sb.append(" - "
            + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "LANGUAGE", languageEnum)
            + " : " + orderManager.getOrderDto().getInfo().getUser().getLanguage() + "\n");

      if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo() != null) {
        if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getQualifier().toString().equals(
            "COMPANY")) {
          if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getCompanyName() != null)
            sb.append(" - "
                + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "FIRM_NAME",
                    languageEnum) + " : "
                + orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getCompanyName() + "\n");
          if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getCompanyContactName() != null)
            sb.append(" - "
                + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "FIRM_CONTACT",
                    languageEnum) + " : "
                + orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getCompanyContactName() + "\n");
        }
        else {
          if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getQualifier() != null)
            sb.append(" - "
                + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "QUALIFIER",
                    languageEnum)
                + " : "
                + TranslationManager.getCode(eldoradoTranslationPrefix + TRANSLATION_GREETING
                    + orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getQualifier().toString(),
                    languageEnum) + "\n");
          if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getFirstName() != null)
            sb.append(" - "
                + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "FIRSTNAME",
                    languageEnum) + " : "
                + orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getFirstName() + "\n");
          if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getLastName() != null)
            sb.append(" - "
                + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "LASTNAME",
                    languageEnum) + " : "
                + orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getLastName() + "\n");
        }
        if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getMobilePhone() != null) {
          sb.append(" - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "MOBILE", languageEnum)
              + " : ");
          if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getMobileCountryCode() != null)
            sb.append(orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getMobileCountryCode() + " ");
          sb.append(orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getMobilePhone() + "\n");
        }
        if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getPhoneNumber() != null) {
          sb.append(" - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "PHONE", languageEnum)
              + " : ");
          if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getPhoneCountryCode() != null)
            sb.append(orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getPhoneCountryCode() + " ");
          sb.append(orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getPhoneNumber() + "\n");
        }
        if (orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getEmailAddress() != null)
          sb.append(" - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "EMAIL", languageEnum)
              + " : " + orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getEmailAddress() + "\n");
      }

      if (orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress() != null) {
        if (orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getStreet() != null) {
          sb.append(" - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "STREET", languageEnum)
              + " : " + orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getStreet());
          if (orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getStreetNr() != null)
            sb.append(" " + orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getStreetNr());
          if (orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getStreetNrComp() != null)
            sb.append(" " + orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getStreetNrComp()
                + "\n");
        }
        String strZip;
        strZip = "";
        if (orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getCountry() != null)
          if (orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getCountry() == PartnerCountryEnumDto.CH)
            strZip = getSwissZip(orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getZip());
          else
            strZip = orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getZip();

        sb.append(" - "
            + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "CITY", languageEnum)
            + " : " + strZip + " " + orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getCity()
            + "\n");
        if (orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getRegion() != null)
          sb.append(" - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "REGION", languageEnum)
              + " : " + orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getRegion() + "\n");
        if (orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getCountry() != null)
          sb.append(" - "
              + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_CONTACT + "COUNTRY", languageEnum)
              + " : " + orderManager.getOrderDto().getInfo().getUser().getPhysicalAddress().getCountry());
      }
    }
    sb.append("\n\n");

    sb.append(TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "ORDER", languageEnum) + " : "
        + orderManager.getOrderDto().getCodeValue() + "\n");
    sb.append(" - "
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "DESCRIPTION", languageEnum)
        + " : " + summary.getDescription() + "\n");
    sb.append(" - "
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "ELDORADO_HEADING", languageEnum)
        + " : "
        + summary.getEldoradoHeading()
        + "-"
        + TranslationManager.getCode(
            adminEldoradoTranslationPrefix + TRANSLATION_HEADING + summary.getEldoradoHeading(), languageEnum) + "\n");
    if (!StringUtil.isEmpty(orderManager.getBlindBoxText()))
      sb.append(" - "
          + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "BLINDBOX", languageEnum) + "\n");

    sb.append(" - "
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "AMOUNTWITHOUTVAT", languageEnum)
        + " : " + amount2String(summary.getAmountWithoutVAT()) + "\n");
    sb.append(" - "
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "AMOUNTVAT", languageEnum) + " : "
        + amount2String(summary.getAmountVAT()) + "\n");
    sb.append(" - "
        + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "AMOUNTWITHTVAT", languageEnum)
        + " : " + amount2String(summary.getAmountWithVAT()));

    sb.append("\n\n");

    boolean blnIsPrintedRate;
    String strIssueDates;
    String strRate;
    for (EldoradoSummaryOfferDto offers : summary.getOffers()) {
      if (!StringUtil.isEmpty(offers.getBrandName()))
        sb.append(offers.getBrandName() + "\n");
      if (!StringUtil.isEmpty(offers.getMediaName()))
        sb.append(" - " + offers.getMediaName() + "\n");
      if (!StringUtil.isEmpty(offers.getOfferName()))
        sb.append(" - " + offers.getOfferName() + "\n");
      if (!StringUtil.isEmpty(offers.getSubHeadingName()))
        sb.append(" - " + offers.getSubHeadingName() + "\n");
      if (!StringUtil.isEmpty(offers.getSizeName()))
        sb.append(" - " + offers.getSizeName() + "\n");

      blnIsPrintedRate = false;
      strIssueDates = "";
      strRate = "";
      for (EldoradoSummaryOfferPlacementDto placements : offers.getPlacements()) {
        for (EldoradoSummaryOfferInsertionDto insertions : placements.getInsertions()) {
          strIssueDates = strIssueDates + date2SummaryString(insertions.getIssueDate());
          if (insertions.getEndDate() != null)
            strIssueDates = strIssueDates + "/" + date2SummaryString(insertions.getEndDate());
          strIssueDates = strIssueDates + "; ";
          if (!blnIsPrintedRate && insertions.getCalculation() != null) {
            strRate = amount2String(insertions.getCalculation().getPricePerUnit());
            blnIsPrintedRate = true;
          }
        }
      }
      sb.append(" - "
          + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "RATE", languageEnum)
          + " : CHF " + strRate + "\n");
      sb.append(" - "
          + TranslationManager.getLabel(eldoradoTranslationPrefix + TRANSLATION_MAIL + "MEDIAAMOUNT", languageEnum)
          + " : CHF " + amount2String(offers.getAmount()) + "\n");

      sb.append(" - " + strIssueDates + "\n");

      if (!StringUtil.isEmpty(offers.getMaterialCmsUuid())) {
        if (offers.getMediaSupport().equals(MediaSupportEnumDto.ONLINE)) {
          sb.append(" - " + ueditorOnlineEcmsUrlPrefix + offers.getMaterialCmsUuid() + "\n");
        }
        else {
          sb.append(" - " + offers.getMaterialCmsUuid() + "\n");
        }
      }
      else if (!StringUtil.isEmpty(offers.getUploadThumbnailurl())) {
        sb.append(" - " + offers.getUploadThumbnailurl() + "\n");
      }

      if (offers.getPrintPlus() != null) {
        if (!StringUtil.isEmpty(offers.getPrintPlus().getMediaSite())) {
          sb.append(" - Print+ : " + offers.getPrintPlus().getMediaSite());
          if (!StringUtil.isEmpty(offers.getPrintPlus().getMaterialCmsUuid()))
            sb.append(" / " + ueditorOnlineEcmsUrlPrefix + offers.getPrintPlus().getMaterialCmsUuid());
          sb.append("\n");

        }
      }

    }

    return sb;
  }

  private String amount2String(BigDecimal amount) {
    try {
      DecimalFormat dFormat = new DecimalFormat("0.00");
      return dFormat.format(amount);
    }
    catch (Exception ex) {
      return "";
    }
  }

  private String date2SummaryString(Date dateValue) {
    SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
    return formater.format(dateValue.getTime());
  }
}
