package com.publigroupe.eldorado.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.publigroupe.eldorado.dao.Rsgv25Dao;
import com.publigroupe.eldorado.dao.bean.Rsgv25;
import com.publigroupe.eldorado.domain.datatrans.DatatransResponse;
import com.publigroupe.eldorado.domain.datatrans.DatatransTransaction;
import com.publigroupe.eldorado.domain.datatrans.exception.DatatransException;
import com.publigroupe.eldorado.domain.datatrans.exception.TransactionDatatransException;
import com.publigroupe.eldorado.dto.EldoradoPaymentDatatransDto;
import com.publigroupe.eldorado.dto.EldoradoPaymentDto;
import com.publigroupe.eldorado.manager.EldoradoOrderManager;
import com.publigroupe.media.domain.CodeList;
import com.publigroupe.order.dao.p2000.Order01Dao;
import com.publigroupe.order.dao.p2000.bean.Order01;
import com.publigroupe.partner.dto.PartnerDto;

public class EldoradoPayment
{
	
    private static Log log = LogFactory.getLog(EldoradoPayment.class);
	private Rsgv25Dao rsgv25Dao = new Rsgv25Dao();
	private Order01Dao order01RepaymentDao = new Order01Dao("RS7A01");
	private Order01Dao order01Dao = new Order01Dao("RSGV01");

	/**
	 * Launch payment debit process to DataTrans
	 * @throws DatatransException 
	 */
	public void debitPayment(String subsidiaryCode, String orderNr, EldoradoOrderManager orderManager) throws DatatransException
	{
		Rsgv25 order = rsgv25Dao.retrieve(subsidiaryCode, orderNr);
		
		order.utistec = CodeList.extractCodeValue("UTISTE", orderManager.getOrderDto().getAdvertiser().getCodeList(), orderManager.getOrderDto().getAdvertiser().getCodeValue());
		order.utientc = CodeList.extractCodeValue("UTIENTC", orderManager.getOrderDto().getAdvertiser().getCodeList(), orderManager.getOrderDto().getAdvertiser().getCodeValue());
		order.clino = CodeList.extractCodeValue("CLINO", orderManager.getOrderDto().getAdvertiser().getCodeList(), orderManager.getOrderDto().getAdvertiser().getCodeValue());
		order.clicch = CodeList.extractCodeValue("CLICCH", orderManager.getOrderDto().getAdvertiser().getCodeList(), orderManager.getOrderDto().getAdvertiser().getCodeValue());

		
		order.utientop = CodeList.extractCodeValue("UTIENTO", orderManager.getOrderDto().getCodeList(), orderManager.getOrderDto().getCodeValue());
		order.cdenump = CodeList.extractCodeValue("CDENUM", orderManager.getOrderDto().getCodeList(), orderManager.getOrderDto().getCodeValue());
		
		assert ( subsidiaryCode == order.utiento && order.cdenum == orderNr );
		
		debitPayment(order);
	}

	
	private void debitPayment(Rsgv25 order) throws DatatransException {
	    String subsidiaryCode = order.utiento;
	    String orderNr = order.cdenum;
	    
	    EldoradoDatatransProxy datatransProxy = new EldoradoDatatransProxy(order);
        DatatransTransaction<String> debitRequest = datatransProxy.createDebitRequest();
        try {
            debitRequest.execute();
            DatatransResponse paymentResult = debitRequest.getResult();
            if(paymentResult.didFail()) {
                String errMsg = paymentResult.formattedTransactionResultText();
                log.error(errMsg);
                throw new TransactionDatatransException(errMsg);
            }
            else {
                log.info(paymentResult.formattedTransactionResultText());
                
                log.info("Starting update of order "+subsidiaryCode+"-"+orderNr+" in table RSGV25");
                // update rsgv25
                order.stransa = Rsgv25.Stransa.OK;
                order.txordid = orderNr;
                order.vallogdt = new Timestamp(System.currentTimeMillis());
                rsgv25Dao.update(order);
                log.info("Terminated update of order "+subsidiaryCode+"-"+orderNr+" in table RSGV25");

                log.info("debitPayment done / utiento:" + subsidiaryCode + " / cdenum:" + orderNr);
            }
        } catch (DatatransException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
	}

    /**
	 * Save payment authorization Call to DataTrans is outside from this method
	 */
	public void savePaymentAuthorization(String subsidiaryCode, String orderNr, PartnerDto user, EldoradoPaymentDatatransDto paymentDataTrans)
	{
		// Insert into Rsgv25
		boolean isOrderRepayment = false;
		Order01 order01 = order01RepaymentDao.read(subsidiaryCode, orderNr);
		if (order01 == null)
		{
			order01 = order01Dao.read(subsidiaryCode, orderNr);
		}
		else
		{
			isOrderRepayment = true;
		}
		Rsgv25 rsgv25 = new Rsgv25();
		if (order01 != null)
		{
			rsgv25.utiste = order01.utiste;
			rsgv25.libcde = order01.cdecon;
			rsgv25.tvamt = order01.tvamt;
			if (order01.tvainc.equals("1"))
			{
				rsgv25.cdenet = order01.cdeneta;
				rsgv25.cdeneta = order01.cdeneta.subtract(order01.tvamt);
			}
			else
			{
				rsgv25.cdenet = order01.cdeneta.add(order01.tvamt);
				rsgv25.cdeneta = order01.cdeneta;
			}
		}
		rsgv25.utiento = subsidiaryCode;
		rsgv25.cdenum = orderNr;

		rsgv25.tvatxc = "";
		rsgv25.tvatx = new BigDecimal(0);
		rsgv25.inlogdt = new Timestamp(System.currentTimeMillis());
		rsgv25.metpaie = "00";
		rsgv25.modpaie = paymentDataTrans.getPaymentType().getCode();
		rsgv25.stransa = Rsgv25.Stransa.VA;
		rsgv25.dtempaie = new Date();
		rsgv25.orimaj = Rsgv25.Orimaj.EM;
		if ("COMPANY".equals(user.getQualifierInfo().getQualifier().toString())) {
			rsgv25.gvpren = user.getQualifierInfo().getCompanyContactName(); // gvpren
			rsgv25.gvnom = user.getQualifierInfo().getCompanyName(); // gvnom
		} else {
			rsgv25.gvpren = user.getQualifierInfo().getLastName(); // gvpren
			rsgv25.gvnom = user.getQualifierInfo().getFirstName(); // gvnom
		}
		rsgv25.gvrue = user.getPhysicalAddress().getStreet();
		rsgv25.gvnpa = user.getPhysicalAddress().getZip();
		rsgv25.gvloc = getLimitedValue(user.getPhysicalAddress().getCity(), 18);
		rsgv25.gvpays = user.getPhysicalAddress().getCountry().name();
		rsgv25.gvntel = user.getQualifierInfo().getPhoneNumber();
		// rsgv25.gvnfax = user.getPhysicalAddress() // TODO check for fax
		// mapping https://jira.consultas.ch/jira/browse/ELDWEB-58
		rsgv25.gvemail = user.getQualifierInfo().getEmailAddress();
		rsgv25.gvcdlng = user.getLanguage();
		rsgv25.gvtxcon = "0";
		// rsgv25.gvpro = user.getPhysicalAddress() // TODO ask for occupation
		// mapping https://jira.consultas.ch/jira/browse/ELDWEB-58
		rsgv25.adrreg = user.getPhysicalAddress().getRegion();
		rsgv25.adrtele = user.getQualifierInfo().getPhoneCountryCode();
		rsgv25.upptrid = paymentDataTrans.getUppId();
		rsgv25.authcod = paymentDataTrans.getAuthorizationCode();
		rsgv25Dao.insert(rsgv25);

		if (order01 != null && !isOrderRepayment)
		{
			// Update RSGV01 (GVSTPAI='1' + CDEPAMO = '98' + GVSTCD = '1')
			order01.gvstpai = "1";
			order01.cdepamo = "98";
			order01.gvstcd = "1";
			order01Dao.update(order01);
		}

		log.info("savePaymentAuthorization done " + " / utiento:" + subsidiaryCode + " / cdenum:" + orderNr + " / uppId" + paymentDataTrans.getUppId());
	}

	/**
	 * Cancel payment authorization and remove payment
	 */
	public void cancelPaymentAuthorization(String subsidiaryCode, String orderNr) throws DatatransException 
	{
	    
	    Rsgv25 order = rsgv25Dao.retrieve(subsidiaryCode, orderNr);
        
        if (order != null) {

//        	assert ( subsidiaryCode == order.utiento && order.cdenum == orderNr );
            
            cancelPaymentAuthorization(order);
        	
        }
	}
	
	public void cancelPaymentAuthorization(Rsgv25 order) throws DatatransException {
	    String subsidiaryCode = order.utiento;
        String orderNr = order.cdenum;
        
	    EldoradoDatatransProxy datatransProxy = new EldoradoDatatransProxy(order);
        DatatransTransaction<String> cancelRequest = datatransProxy.createCancelRequest();
        try {
            cancelRequest.execute();
            DatatransResponse cancelResult = cancelRequest.getResult();
            if(cancelResult.didFail()) {
                String errMsg = cancelResult.formattedTransactionResultText();
                log.error(errMsg);
                throw new TransactionDatatransException(errMsg);
            }
            else {
                log.info(cancelResult.formattedTransactionResultText());
                
                log.info("Deleting order "+subsidiaryCode+"-"+orderNr+" in table RSGV25");
               
                rsgv25Dao.delete(subsidiaryCode, orderNr);
               
                log.info("Deleted order "+subsidiaryCode+"-"+orderNr+" in table RSGV25");

                log.info("cancelPaymentAuthorization done / utiento:" + subsidiaryCode + " / cdenum:" + orderNr);
            }
        } catch (DatatransException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
	}

	/**
	 * Retrieve a Payment
	 */
	public EldoradoPaymentDto retrieve(String subsidiaryCode, String orderNr)
	{
		Rsgv25 rsgv25 = rsgv25Dao.retrieve(subsidiaryCode, orderNr);
		return EldoradoPaymentMapping.convert(rsgv25);
	}
	
	/**
	 * @param value
	 * @param limit
	 * @return
	 */
	private String getLimitedValue(String value, int limit)
	{
		if (value == null)
			return null;
		return (value.length() <= limit) ? value : value.substring(0, limit);
	}
}
