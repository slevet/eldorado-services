package com.publigroupe.eldorado.domain;

import com.publigroupe.eldorado.dao.bean.Rsgv26;
import com.publigroupe.eldorado.dto.EldoradoFlowStateDto;

public class EldoradoFlowStateMapping {
	
	/**
	 * Convert rsgv26 into PaymentStateDto
	 * @param rsgv26
	 * @return PaymentStateDto
	 */
	public static EldoradoFlowStateDto convert(Rsgv26 rsgv26) {
		EldoradoFlowStateDto state = null;
		if (rsgv26!=null)
		{
			state = new EldoradoFlowStateDto();
			state.setCompanyCode(rsgv26.utiste);
			state.setSubsidiaryCode(rsgv26.utiento);
			state.setOrderNr(rsgv26.cdenum);
			state.setP2000SubsidiaryCode(rsgv26.utient);
			state.setP2000OrderNr(rsgv26.cdenump);
			state.setAuthorizationDate(rsgv26.authdt);
			state.setAuthorizationTime(rsgv26.authhe);
			state.setLoadingDate(rsgv26.p2loaddt);
			state.setLoadingTime(rsgv26.p2loadhe);
			state.setValidationDate(rsgv26.p2valdt);
			state.setValidationTime(rsgv26.p2valhe);
			state.setValidationReference(rsgv26.p2valre);
			state.setDebitDate(rsgv26.debdt);
			state.setDebitTime(rsgv26.debhe);
			state.setConfirmationDate(rsgv26.confmdt);
			state.setConfirmationTime(rsgv26.confmhe);
			state.setSuspendPaimentDate(rsgv26.p2susdt);
			state.setSuspendPaimentTime(rsgv26.p2sushe);
			state.setSuspendPaimentReference(rsgv26.p2susre);
			state.setSuspendMailClientDate(rsgv26.susmdt);
			state.setSuspendMailClientTime(rsgv26.susmhe);
			state.setCancellationDate(rsgv26.p2abddt);
			state.setCancellationTime(rsgv26.p2abdhe);
			state.setCancellationReference(rsgv26.p2abdre);
			state.setCancellationMailClientDate(rsgv26.abdmdt);
			state.setCancellationMailClientTime(rsgv26.abdmhe);
			state.setFeedBackIssueDateMailDate(rsgv26.okparudt);
			state.setFeedBackIssueDateMailTime(rsgv26.okparuhe);
		}
		return state;
	}
	
	/**
	 * Convert rsgv26 into PaymentStateDto
	 * @param rsgv26
	 * @return PaymentStateDto
	 */
	public static Rsgv26 convert(EldoradoFlowStateDto state) {
		Rsgv26 rsgv26 = null;
		if (state!=null)
		{
			rsgv26 = new Rsgv26();
			rsgv26.utiste = state.getCompanyCode();
			rsgv26.utiento = state.getSubsidiaryCode();
			rsgv26.cdenum = state.getOrderNr();
			rsgv26.utient = state.getP2000SubsidiaryCode();
			rsgv26.cdenump = state.getP2000OrderNr();
			rsgv26.authdt = state.getAuthorizationDate();
			rsgv26.authhe = state.getAuthorizationTime();
			rsgv26.p2loaddt = state.getLoadingDate();
			rsgv26.p2loadhe = state.getLoadingTime();
			rsgv26.p2valdt = state.getValidationDate();
			rsgv26.p2valhe = state.getValidationTime();
			rsgv26.p2valre = state.getValidationReference();
			rsgv26.debdt = state.getDebitDate();
			rsgv26.debhe = state.getDebitTime();
			rsgv26.confmdt = state.getConfirmationDate();
			rsgv26.confmhe = state.getConfirmationTime();
			rsgv26.p2susdt = state.getSuspendPaimentDate();
			rsgv26.p2sushe = state.getSuspendPaimentTime();
			rsgv26.p2susre = state.getSuspendPaimentReference();
			rsgv26.susmdt = state.getSuspendMailClientDate();
			rsgv26.susmhe = state.getSuspendMailClientTime();
			rsgv26.p2abddt = state.getCancellationDate();
			rsgv26.p2abdhe = state.getCancellationTime();
			rsgv26.p2abdre = state.getCancellationReference();
			rsgv26.abdmdt = state.getCancellationMailClientDate();
			rsgv26.abdmhe = state.getCancellationMailClientTime();
			rsgv26.okparudt = state.getFeedBackIssueDateMailDate();
			rsgv26.okparuhe = state.getFeedBackIssueDateMailTime();
		}
		return rsgv26;
	}
	
}
