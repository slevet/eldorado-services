package com.publigroupe.eldorado.domain;

import java.sql.Timestamp;

import com.publigroupe.eldorado.dao.bean.Rsgv25;
import com.publigroupe.eldorado.dto.EldoradoPaymentDto;
import com.publigroupe.eldorado.dto.EldoradoPaymentTypeEnumDto;
import com.publigroupe.partner.dto.PartnerCountryEnumDto;

public class EldoradoPaymentMapping
{
	/**
	 * Convert rsgv25 into PaymentDto
	 * 
	 * @param rsgv25
	 * @return PaymentDto
	 */
	public static EldoradoPaymentDto convert(Rsgv25 rsgv25)
	{
		EldoradoPaymentDto payment = null;
		if (rsgv25 != null)
		{
			payment = new EldoradoPaymentDto();
			payment.setCompanyCode(rsgv25.utiste);
			payment.setSubsidiaryCode(rsgv25.utiento);
			payment.setOrderNr(rsgv25.cdenum);
			payment.setCommandLabel(rsgv25.libcde);
			payment.setCommandValueFreeOfVat(rsgv25.cdeneta);
			payment.setCommandValueWithVat(rsgv25.tvamt);
			payment.setCommandValue(rsgv25.cdenet);
			payment.setVatRatioCode(rsgv25.tvatxc);
			payment.setVatRatio(rsgv25.tvatx);
			payment.setCreationDateIn(rsgv25.inlogdt);
			payment.setPaymentMethod(rsgv25.metpaie);
			if (rsgv25.modpaie!=null)
			{
				payment.getPaymentDataTransDto().setPaymentType(EldoradoPaymentTypeEnumDto.get(rsgv25.modpaie));
			}
			payment.setPaymentStatus(rsgv25.stransa.name());
			payment.setDateEMPaymentdtempaie(rsgv25.dtempaie);
			payment.setCreationDateOut(rsgv25.outlogdt);
			payment.setUpdateStatus(rsgv25.orimaj.name());
			payment.getUser().getQualifierInfo().setLastName(rsgv25.gvpren);
			payment.getUser().getQualifierInfo().setFirstName(rsgv25.gvnom);
			payment.getUser().getPhysicalAddress().setStreet(rsgv25.gvrue);
			payment.getUser().getPhysicalAddress().setZip(rsgv25.gvnpa);
			payment.getUser().getPhysicalAddress().setCity(rsgv25.gvloc);
			if (rsgv25.gvpays!=null)
			{
				payment.getUser().getPhysicalAddress().setCountry(PartnerCountryEnumDto.valueOf(rsgv25.gvpays));
			}
			payment.getUser().getQualifierInfo().setPhoneNumber(rsgv25.gvntel);
			payment.setFax(rsgv25.gvnfax);
			payment.getUser().getQualifierInfo().setEmailAddress(rsgv25.gvemail);
			payment.getUser().setLanguage(rsgv25.gvcdlng);
			payment.setP2000SubsidiaryCode(rsgv25.utientop);
			payment.setP2000OrderNr(rsgv25.cdenump);
			payment.setLoadDateP2000(rsgv25.valcdedt);
			payment.setClientCompanyCode(rsgv25.utistec);
			payment.setClienSubsidiaryCode(rsgv25.utientc);
			payment.setClientNumber(rsgv25.clino);
			payment.setClientCchNumber(rsgv25.clicch);
			payment.setClientCategory(rsgv25.cligenr);
			payment.setClientName(rsgv25.adrnom);
			payment.setClientLocation(rsgv25.adrloc);
			payment.setFromCategory(rsgv25.procat);
			payment.setFromCodeGenre(rsgv25.progenr);
			payment.setFromCode(rsgv25.prono);
			payment.setClientValidationDate(rsgv25.valclidt);
			payment.setCommandNumberGv(rsgv25.txordid);
			payment.setCreationDateBatch(rsgv25.vallogdt);
			payment.setTransactionNumberGv(rsgv25.gvtxcon);
			payment.setDateControlBatch(rsgv25.conlogdt);
			payment.setOccupation(rsgv25.gvpro);
			payment.getUser().getPhysicalAddress().setRegion(rsgv25.adrreg);
			payment.getUser().getQualifierInfo().setPhoneCountryCode(rsgv25.adrtele);
			payment.getPaymentDataTransDto().setUppId(rsgv25.upptrid);
			payment.getPaymentDataTransDto().setAuthorizationCode(rsgv25.authcod);
		}
		return payment;
	}

	/**
	 * Convert PaymentDto into rsgv25
	 * 
	 * @param paymentDto
	 * @return rsgv25
	 */
	public static Rsgv25 convert(EldoradoPaymentDto payment)
	{
		Rsgv25 rsgv25 = new Rsgv25();
		if (payment!=null)
		{
			rsgv25.utiste = payment.getCompanyCode(); // utiste
			rsgv25.utiento = payment.getSubsidiaryCode(); // utiento
			rsgv25.cdenum = payment.getOrderNr(); // cdenum
			rsgv25.libcde = payment.getCommandLabel(); // libcde
			rsgv25.cdeneta = payment.getCommandValueFreeOfVat(); // cdeneta
			rsgv25.tvamt = payment.getCommandValueWithVat(); // tvamt
			rsgv25.cdenet = payment.getCommandValue(); // cdenet
			rsgv25.tvatxc = payment.getVatRatioCode(); // tvatxc
			rsgv25.tvatx = payment.getVatRatio(); // tvatx
			if (payment.getCreationDateIn()!=null)
			{
				rsgv25.inlogdt = new Timestamp(payment.getCreationDateIn().getTime()); // inlogdt
			}
			rsgv25.metpaie = payment.getPaymentMethod(); // metpaie
			if (payment.getPaymentDataTransDto()!=null)
			{
				if (payment.getPaymentDataTransDto().getPaymentType()!=null)
				{
					rsgv25.modpaie = payment.getPaymentDataTransDto().getPaymentType().getCode(); // modpaie
				}
				rsgv25.upptrid = payment.getPaymentDataTransDto().getUppId(); // upptrid
				rsgv25.authcod = payment.getPaymentDataTransDto().getAuthorizationCode(); // authcod
			}
			rsgv25.stransa = Rsgv25.Stransa.valueOf(payment.getPaymentStatus()); // stransa
			rsgv25.dtempaie = payment.getDateEMPaymentdtempaie(); // dtempaie
			if (payment.getCreationDateOut()!=null)
			{
				rsgv25.outlogdt = new Timestamp(payment.getCreationDateOut().getTime()); // outlogdt
			}
			rsgv25.orimaj = Rsgv25.Orimaj.valueOf(payment.getUpdateStatus()); // orimaj
			if (payment.getUser()!=null)
			{
				if (payment.getUser().getQualifierInfo()!=null)
				{
					if ("COMPANY".equals(payment.getUser().getQualifierInfo().getQualifier().toString())) {
						rsgv25.gvpren = payment.getUser().getQualifierInfo().getCompanyContactName(); // gvpren
						rsgv25.gvnom = payment.getUser().getQualifierInfo().getCompanyName(); // gvnom
					} else {
						rsgv25.gvpren = payment.getUser().getQualifierInfo().getLastName(); // gvpren
						rsgv25.gvnom = payment.getUser().getQualifierInfo().getFirstName(); // gvnom
					}

					rsgv25.gvntel = payment.getUser().getQualifierInfo().getPhoneNumber(); // gvntel
					rsgv25.gvemail = payment.getUser().getQualifierInfo().getEmailAddress(); // gvemail	
					rsgv25.adrtele = payment.getUser().getQualifierInfo().getPhoneCountryCode(); // adrtele
				}
				if (payment.getUser().getPhysicalAddress()!=null)
				{
					rsgv25.gvrue = payment.getUser().getPhysicalAddress().getStreet(); // gvrue
					rsgv25.gvnpa = payment.getUser().getPhysicalAddress().getZip(); // gvnpa
					rsgv25.gvloc = payment.getUser().getPhysicalAddress().getCity(); // gvloc
					if (payment.getUser().getPhysicalAddress().getCountry()!=null)
					{
						rsgv25.gvpays = payment.getUser().getPhysicalAddress().getCountry().name(); // gvpays	
						rsgv25.adrreg = payment.getUser().getPhysicalAddress().getRegion(); // adrreg
					}
				}
				rsgv25.gvcdlng = payment.getUser().getLanguage(); // gvcdlng
			}
			rsgv25.gvnfax = payment.getFax(); // gvnfax
			rsgv25.utientop = payment.getP2000SubsidiaryCode(); // utientop
			rsgv25.cdenump = payment.getP2000OrderNr(); // cdenump
			if (payment.getLoadDateP2000()!=null)
			{
				rsgv25.valcdedt = new Timestamp(payment.getLoadDateP2000().getTime()); // valcdedt
			}
			rsgv25.utistec = payment.getClientCompanyCode(); // utistec
			rsgv25.utientc = payment.getClienSubsidiaryCode(); // utientc
			rsgv25.clino = payment.getClientNumber(); // clino
			rsgv25.clicch = payment.getClientCchNumber(); // clicch
			rsgv25.cligenr = payment.getClientCategory(); // cligenr
			rsgv25.adrnom = payment.getClientName(); // adrnom
			rsgv25.adrloc = payment.getClientLocation(); // adrloc
			rsgv25.procat = payment.getFromCategory(); // procat
			rsgv25.progenr = payment.getFromCodeGenre(); // progenr
			rsgv25.prono = payment.getFromCode(); // prono
			if (payment.getClientValidationDate()!=null)
			{
				rsgv25.valclidt = new Timestamp(payment.getClientValidationDate().getTime()); // valclidt
			}
			rsgv25.txordid = payment.getCommandNumberGv(); // txordid
			if (payment.getCreationDateBatch()!=null)
			{
				rsgv25.vallogdt = new Timestamp(payment.getCreationDateBatch().getTime()); // vallogdt
			}
			rsgv25.gvtxcon = payment.getTransactionNumberGv(); // gvtxcon
			if (payment.getDateControlBatch()!=null)
			{
				rsgv25.conlogdt = new Timestamp(payment.getDateControlBatch().getTime()); // conlogdt
			}
			rsgv25.gvpro = payment.getOccupation();// gvpro
		}
		return rsgv25;
	}
}
