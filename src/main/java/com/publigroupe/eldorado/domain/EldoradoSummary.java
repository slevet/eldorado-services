package com.publigroupe.eldorado.domain;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.publigroupe.eldorado.dto.EldoradoSummaryDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferInsertionDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferPlacementDto;
import com.publigroupe.eldorado.dto.EldoradoSummarySurchargeDto;
import com.publigroupe.eldorado.manager.EldoradoOrderManager;
import com.publigroupe.javaBase.property.PropertyLoader;
import com.publigroupe.media.dto.MediaSupportEnumDto;
import com.publigroupe.media.dto.SizeMeasurementTypeEnumDto;

public class EldoradoSummary
{
	/* VARIABLES */
	private EldoradoSummaryDto summary;
	private HashMap<String, HashMap<String,String>> labelMap;
	private HashMap<String, HashMap<String,String>> helpMap;
	private String currency = "CHF"; //default value = CHF
	private String baseUrl;
	private String uploadDir;
	private static final NumberFormat DOUBLE_DIGIT = NumberFormat.getInstance();
	private static final NumberFormat SINGLE_DIGIT = NumberFormat.getInstance();

	static
	{
		DOUBLE_DIGIT.setMinimumFractionDigits(2);
		DOUBLE_DIGIT.setMaximumFractionDigits(2);
		
		SINGLE_DIGIT.setMinimumFractionDigits(1);
		SINGLE_DIGIT.setMaximumFractionDigits(1);
	}

	/**
	 * Default constructor of <code>EldoradoSummary</code> bean
	 * @param orderManager
	 * @param i18nService
	 */
	public EldoradoSummary(EldoradoOrderManager orderManager)
	{
		summary = orderManager.retrieveSummary(false, false, true);

		// load values from app.properties
		this.baseUrl = PropertyLoader.getPropertyWithContext(this, "PrintSummary.Eldorado.baseUrl");
		this.uploadDir = PropertyLoader.getPropertyWithContext(this, "PrintSummary.Eldorado.uploadDir");

		// set current currency
		currency = summary.getCurrency();
	}

	/**
	 * Able to produce html representation of a user order summary
	 * @param isForPrint flag to indicate the html will contain js print faculties.
	 * @return html representation of a user order summary
	 */
	public String toHtml(boolean isForPrint)
	{
		StringBuilder result = new StringBuilder();
		StringBuffer header = getHeader();
		StringBuffer beginBody = getBeginBody(isForPrint);
		StringBuffer introduction = getIntroduction();
		StringBuffer orderTitle = getOrderTitle(summary.getDescription(),
				summary.getCommandNumber());

		StringBuffer orderBill = getOrderBill();
		result.append(header);
		result.append(beginBody);
		result.append(introduction);
		result.append(orderTitle);
		for (EldoradoSummaryOfferDto offerItemTmp : summary.getOffers()){
			result.append(getOrderItem(offerItemTmp));
		}
		result.append(orderBill);
		return result.toString();
	}

	/* INTERNAL PRIVATE METHODS */
	private StringBuffer getOrderTitle(String orderTitle, String orderNbr) {
		StringBuffer title = new StringBuffer();
		title.append("</td></tr><tr><td style=\"vertical-align: top;\" align=\"left\">" +
				"<div class=\"screenTitleText\">" + getI18nLabel("ELDORADO_SUMMARY", "ORDER_PREVIEW") + "</div></td></tr><tr>" +
				"<td style=\"vertical-align: top;\" align=\"left\">" +
				"<div class=\"formBoxTitle\">" + orderTitle + "</div>" +
				"</td></tr><tr><td style=\"vertical-align: top;\" align=\"left\"><div>" +
				"<div class=\"orderNumber\">" + getI18nLabel("ELDORADO_THANKS", "ORDER_NUMBER_LBL") + "</div>" +
				"<div class=\"orderNumber\">" + orderNbr + "</div>" +
				"</div></td></tr><tr><td style=\"vertical-align: top;\" align=\"left\"><div>" +
				"<table class=\"summaryOrderFormBox\"><tbody><tr><td class=\"summaryOrderFormBoxBody\">" +
		"<table cellpadding=\"0\" cellspacing=\"0\"><tbody>");
		return title;
	}

	private StringBuffer getBeginBody(boolean isForPrint) {
		StringBuffer beginBody = new StringBuffer();
		beginBody.append("<body id=\"body\"" + 
				(isForPrint?"onload=\"printPage()\"":"")+ ">\n" +
				"<script type=\"text/javascript\">function printPage(){window.print();}</script>\n" +
				"<div id=\"main\">\n<table><tbody>\n<tr><td><img src=\"resources/logo_publicitas.gif\"/>" +
		"</td></tr>\n<tr><td>");
		return beginBody;
	}

	private StringBuffer getOrderBill() {
		StringBuffer bill = new StringBuffer("");
		bill.append("</tbody></table></td><td class=\"summaryOrderFormBoxHelp\">" +
				"<div class=\"screenInfoHelpText\"></div></td></tr></tbody></table></div>" +
				"</td></tr><tr><td style=\"vertical-align: top;\" align=\"left\"><div>" +
		"<table class=\"summaryOrderBillFormBox\"><tbody><tr><td class=\"summaryOrderBillFormBoxBody\">");
		bill.append("<table cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td style=\"vertical-align: top;\" align=\"left\"><div>");
		
		if (summary.getSurcharges() != null && (summary.getAmountSurcharge() != null && summary.getAmountSurcharge().floatValue() > 0)) {
			bill.append("<div class=\"serviceAdditionalCommand\">" + getI18nLabel("ELDORADO_SUMMARY", "SERVICE_ADDITIONAL_COMMAND") + "</div>");
		}

		if (summary.getSurcharges() != null) 
		{
			for (EldoradoSummarySurchargeDto surchargeTmp : summary.getSurcharges()){
				bill.append("<div class=\"subNumber\">" + surchargeTmp.getSurchargeName() + "</div>" +
						"<div class=\"subNumberAmount\">" + currency + " " + DOUBLE_DIGIT.format(surchargeTmp.getAmount()) + "</div>");
			}
		}
		
		if (summary.getAmountSurcharge() != null && summary.getAmountSurcharge().floatValue() > 0) {
		
			bill.append("<div class=\"additionalServiceTotal\">" + getI18nLabel("ELDORADO_SUMMARY", "ADDITIONAL_SERVICES_TOTAL") + "</div>");

			bill.append("<div class=\"additionalServiceTotalAmount\">");
			if (summary.getAmountSurcharge() != null) {
				bill.append(currency + " " + DOUBLE_DIGIT.format(summary.getAmountSurcharge()));
			}
			bill.append("</div>");
		}

		bill.append("<div class=\"commandAmountTotal\">" + getI18nLabel("ELDORADO_SUMMARY", "COMMAND_AMOUNT_TOTAL") + "</div>" +
				"<div class=\"commandAmountTotalWithoutVAT\">" + getI18nLabel("ELDORADO_SUMMARY", "COMMAND_AMOUNT_TOTAL_WITHOUT_VAT") + "</div>");

		bill.append("<div class=\"commandAmountTotalWithoutVATAmount\">");
		if (summary.getAmountWithoutVAT() != null) {
			bill.append(currency + " " + DOUBLE_DIGIT.format(summary.getAmountWithoutVAT()));
		}
		bill.append("</div>");

		bill.append("<div class=\"vat76\">+ " + SINGLE_DIGIT.format(summary.getPercentVAT()) + "% " + getI18nLabel("ELDORADO_SUMMARY", "VAT_NAME") + "</div>");

		bill.append("<div class=\"vatAmount\">");
		if (summary.getAmountVAT() != null) {
			bill.append(currency + " " + DOUBLE_DIGIT.format(summary.getAmountVAT()));
		}
		bill.append("</div>");

		bill.append("<div class=\"commandAmountTotalWithVat\">" + getI18nLabel("ELDORADO_SUMMARY", "COMMAND_AMOUNT_TOTAL_WITH_VAT") + "</div>");

		bill.append("<div class=\"commandAmountTotalWithVatAmount\">");
		if (summary.getAmountWithoutVAT() != null) {
			bill.append(currency + " " + DOUBLE_DIGIT.format(summary.getAmountWithVAT()));
		}
		bill.append("</div>");

		bill.append("<div class=\"cardPaymentInfoTitle\">" + getI18nLabel("ELDORADO_THANKS", "CARD_PAYMENT_INFO_TITLE") + "</div>" +
				"<div class=\"cardInfoValue\"></div>" +  // VISA N� &gt;XXXX-XXXX-XXXX-4242
				"<div class=\"cardInfoValue\"></div>" +  // JEAN DUCHMOL
				"<div class=\"cardInfoValue\"></div>" +  // 12 / 2010
		"</div></td></tr></tbody></table>");
		bill.append("</td><td class=\"summaryOrderBillFormBoxHelp\"><div class=\"screenInfoHelpText\">" +
		"</div></td></tr></tbody></table></div></td></tr></tbody></table></div></body></html>");
		return bill;
	}

	private StringBuffer getOrderItem(EldoradoSummaryOfferDto offer) {
		String insertedDates = "";
		String repeatDiscountLbl = "";
		String repeatDiscountValue = "";
		String extendedMsg = "";
		String priceUnitText = "";
		String priceUnitValue = "";
		String ecmContentUrl = "";
		float discountPercent = -1;

		EldoradoSummaryOfferPlacementDto placement = offer.getPlacements().get(0);
		
		 // A PRINT placement can contains N different insertion periods
        List<EldoradoSummaryOfferInsertionDto> insertionPeriods = placement.getInsertions();

		insertedDates = getDates(placement, offer);

		if (offer.containsExtendedDate()) {
			extendedMsg = buildInfoTextForExtendedPublicationDate();
		}

		if (offer.getMediaSupport().equals(MediaSupportEnumDto.PRINT)) {
			// display detailed information for price of the offer without TVA
			String[] textPriceWithoutTVA =  buildDetailedTextForPriceWithoutTVA(currency, offer, insertionPeriods);
			priceUnitText = textPriceWithoutTVA[0];
			priceUnitValue = textPriceWithoutTVA[1];
		}

		// display discount (if any)
        float discountAmount = getTotalDiscountAmountForInsertions(insertionPeriods);
        
        if (discountAmount != 0) {
            discountPercent = getDiscountPercentForInsertions(insertionPeriods);
            // display discount information only if a discount for the offer exist!
            repeatDiscountLbl = buildTextForDiscountPercent(discountPercent);
            
            repeatDiscountValue = buildTextForPrice(discountAmount);
        }		
        
		if (offer.getMediaSupport().equals(MediaSupportEnumDto.PRINT)) {
			try {
				ecmContentUrl = PropertyLoader.getPropertyWithContext(this, "UEditorPrint.ecmsurlprefix");
				if (ecmContentUrl != null && ecmContentUrl.length() > 0) {
					ecmContentUrl += offer.getMaterialCmsUuid() + PropertyLoader.getPropertyWithContext(this, "UEditorPrint.ecmsurlsuffix");
				}
			} catch (Exception e) {
				// Ignore
			}
		}
		StringBuffer orderItem = new StringBuffer("");
		orderItem.append("<tr><td style=\"vertical-align: top;\" align=\"left\">" +
				"<div><table class=\"orderItemTable\" id=\"orderItemTable\" cellpadding=\"0\" cellspacing=\"0\">" +
				"<tbody><tr class=\"orderItemEvenRow\" id=\"orderItemTableTd1\">" +
				"<td class=\"orderItemFirstColumn\" id=\"orderItemFirstColumn\">" +
				"<img src=\"" + this.baseUrl + this.uploadDir + offer.getBrandLogo() + "\" class=\"brandImage\"/>" +
				"<div class=\"mediaTitle\">" + offer.getMediaName() + "</div>" +
				"<div class=\"mediaCategory\">&gt; " + summary.getEldoradoHeading() + " &gt; " + 
				offer.getSubHeadingName() + "</div>" +
				"</td><td class=\"orderItemSecondColumn\" id=\"orderItemSecondColumn\">" +
				"<img src=\"resources/spacer.gif\" class=\"gwt-Image\"/>" +
				"</td><td class=\"orderItemThirdColumn\" id=\"orderItemThirdColumn\">" +
				"<img src=\"resources/spacer.gif\" class=\"gwt-Image\"/>" +
				"</td></tr><tr class=\"orderItemOddRow\" id=\"orderItemTableTd2\">" +
				"<td class=\"orderItemFirstColumn\" id=\"orderItemFirstColumn\">" +
				"<div class=\"parutionDate\">" + getI18nLabel("ELDORADO_SUMMARY", "PARUTION_DATE") + "</div>" +
				"<div class=\"parutionDatesList\">" + insertedDates + "</div>" +
				"<div class=\"parutionDatesList\">" + extendedMsg + "</div>" +
				"</td><td class=\"orderItemSecondColumn\" id=\"orderItemSecondColumn\">" +
				"<img src=\"resources/spacer.gif\" class=\"gwt-Image\"/>" +
				"</td><td class=\"orderItemThirdColumn\" id=\"orderItemThirdColumn\">" +
				"<img src=\"resources/spacer.gif\" class=\"gwt-Image\"/>" +
				"</td></tr><tr class=\"orderItemEvenRow\" id=\"orderItemTableTd3\">" +
				"<td class=\"orderItemFirstColumn\" id=\"orderItemFirstColumn\">" +
				"<div class=\"adLbl\">" + getI18nLabel("ELDORADO_SUMMARY", "AD_LBL") + 
				"</div><img src=\"" + ecmContentUrl + "\" class=\"adPreviewImage\"/>" +
				"<div class=\"adSize\">" + getI18nLabel("ELDORADO_SUMMARY", "AD_SIZE") + "</div><div class=\"adSizeValue\">" + offer.getSizeName() + "</div>" +
				"<div class=\"clickOnImageMsg\">&nbsp;</div>" +
				"</td><td class=\"orderItemSecondColumn\" id=\"orderItemSecondColumn\">" +
				"<img src=\"resources/spacer.gif\" class=\"gwt-Image\"/>" +
				"</td><td class=\"orderItemThirdColumn\" id=\"orderItemThirdColumn\">" +
				"<img src=\"resources/spacer.gif\" class=\"gwt-Image\"/>" +
				"</td></tr><tr class=\"orderItemOddRow\" id=\"orderItemTableTd4\">" +
				"<td class=\"orderItemFirstColumn\" id=\"orderItemFirstColumn\">" +
				"<div class=\"priceWithoutVat\">" + getI18nLabel("ELDORADO_SUMMARY", "PRICE_WITHOUT_VAT") + "</div>" +
				"<div class=\"unitPrice\">" + priceUnitText + "</div>" +
				"<div class=\"repeatDiscount\">" + repeatDiscountLbl + "</div>" +
				"<div class=\"priceIsSubjectToMutation\">" + getI18nLabel("ELDORADO_SUMMARY", "PRICE_OFFER_CAN_MUTATE") + "</div>" +
				"</td><td class=\"orderItemSecondColumn\" id=\"orderItemSecondColumn\">" +
				"<img src=\"resources/spacer.gif\" class=\"gwt-Image\"/>" +
				"</td><td class=\"orderItemThirdColumn\" id=\"orderItemThirdColumn\">" +
				"<div class=\"unitPriceValue\">" + priceUnitValue + "</div>" +
				"<div class=\"repeatDiscountValue\">" + repeatDiscountValue + "</div>" +
				"</td></tr><tr class=\"orderItemOddRow\" id=\"orderItemTableTd5\">" +
				"<td class=\"orderItemFirstColumn\" id=\"orderItemFirstColumn\">" +
				"<div class=\"netAmountWithoutVat\">" + getI18nLabel("ELDORADO_SUMMARY", "NET_AMOUNT_WITHOUT_VAT") + "</div>" +
				"</td><td class=\"orderItemSecondColumn\" id=\"orderItemSecondColumn\">" +
				"<img src=\"resources/spacer.gif\" class=\"gwt-Image\"/>" +
				"</td><td class=\"orderItemThirdColumn\" id=\"orderItemThirdColumn\">");

		orderItem.append("<div class=\"netAmountWithoutVatValue\">");
		if (offer.getAmount() != null) 
		{
			orderItem.append(currency + " " + DOUBLE_DIGIT.format(offer.getAmount()));
		}
		orderItem.append("</div>");
		orderItem.append("</td></tr></tbody></table></div></td></tr>");
		return orderItem;
	}

	private StringBuffer getIntroduction() {
		StringBuffer introduction = new StringBuffer();
		introduction.append("<div class=\"alertBox\">" +
				"<span class=\"topRow-alertBox\"><div class=\"leftTop-alertBox\"></div><div class=\"middleTop-alertBox\"></div>" +
				"<div class=\"rightTop-alertBox\"></div></span><span class=\"middleRow-alertBox\"><div id=\"content-alertBox\">" +
				"<div class=\"title-alertBox\">" + getI18nLabel("ELDORADO_THANKS", "INFORMATION_TITLE") + "</div><div class=\"body-alertBox\"><p>" + getI18nHelp("ELDORADO_THANKS", "THANKS_TITLE_HELP") + "</p></div>" +
				"</div></span><span class=\"bottomRow-alertBox\"><div class=\"leftBottom-alertBox\"></div><div class=\"middleBottom-alertBox\"></div>" +
		"<div class=\"rightBottom-alertBox\"></div></span></div>");
		return introduction;
	}

	private StringBuffer getHeader(){
		StringBuffer header = new StringBuffer();
		header.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" " +
				"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
				"<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n" +
				"<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"/>\n" +
				"<title>Publicitas - Eldorado impression commande</title>\n" +
				"<link href=\"resources/favicon.ico\" rel=\"shortcut icon\"/>\n" +
				"<style>\n" +
				".summaryOrderFormBox {	width: 100%;}\n" +
				".summaryOrderFormBoxBody {	width: 611px; background-color: #f1f0f0;}\n" +
				".summaryOrderFormBoxHelp {vertical-align: top;}\n" +
				".summaryOrderBillFormBox {width: 100%;}\n" +
				".summaryOrderBillFormBoxBody {width: 611px;background-color: #f1f0f0;}\n" +
				".summaryOrderBillFormBoxHelp {	vertical-align: top;}\n" +
				".serviceAdditionalCommand {font-family: verdana !important;font-size: 11px;font-weight: bold;color: #00426e; float: left;margin: 5px 0px 5px 15px;width: 570px;}\n" +
				".subNumber{font-family: verdana !important;font-size: 11px;color: #666666;	float: left; margin: 5px 0px 5px 15px;	width: 490px;}\n" +
				".subNumberAmount{font-family: verdana !important;font-size: 11px;color: #666666;float: right;  margin: 5px 5px 5px 15px;}\n" +
				".additionalServiceTotal{  font-family: verdana !important;  font-size: 11px;  font-weight: bold;  color: #00426e;  float: left;  margin: 5px 0px 5px 15px;  width: 490px;}\n" +
				".additionalServiceTotalAmount{  font-family: verdana !important;  font-size: 11px;  font-weight: bold;  color: #00426e;  float: right;  margin: 5px 5px 5px 15px;}\n" +
				".commandAmountTotal{  font-family: verdana !important;  font-size: 11px;  font-weight: bold;  color: #00426e;  float: left;  margin: 5px 0px 5px 15px;  width: 570px;}\n" +
				".commandAmountTotalWithoutVAT{  font-family: verdana !important;  font-size: 11px;  color: #666666;  float: left;  margin: 5px 0px 5px 15px;  width: 490px;}\n" +
				".commandAmountTotalWithVatAmount{  font-family: verdana !important;  font-size: 11px;  color: #E66647;  float: right;  margin: 5px 5px 5px 15px;}\n" +
				".commandAmountTotalWithoutVATAmount{  font-family: verdana !important;  font-size: 11px;  color: #666666;  float: right;  margin: 5px 5px 5px 15px; }\n" +
				".vat76{  font-family: verdana !important;  font-size: 11px;  color: #666666;  float: left;  margin: 5px 0px 5px 15px;  width: 490px;}\n" +
				".vatAmount{  font-family: verdana !important;  font-size: 11px;  color: #666666;  float: right;  margin: 5px 5px 5px 15px; }\n" +
				".priceIsSubjectToMutation{  font-family: verdana !important;  font-size: 11px;	color: #606160;	margin: 10px 0 8px 20px; font-style: italic; }\n" +
				".commandAmountTotalWithVat{  font-family: verdana !important;  font-size: 12px;  font-weight: bold;  color: #e66647;  float: left;  margin: 5px 0px 5px 15px;  width: 490px; }\n" +
				".commandAmountTotalWithVatAmount{  font-family: verdana !important;  font-size: 12px;  font-weight: bold;  color: #e66647;  float: right;  margin: 5px 5px 5px 15px; }\n" +
				".orderNumber{  color: #000000;  font-family: verdana !important;  font-size: 14px;  margin-bottom: 10px;  margin-right: 5px;  position: relative;  float: left; }\n" +
				".cardPaymentInfoTitle{  font-family: verdana !important;  font-size: 11px;  font-weight: bold;  color: #00426e;  float: left;  margin: 5px 0px 5px 15px;  width: 570px; }\n" +
				".cardInfoValue{  font-family: verdana !important;  font-size: 11px;  color: #666666;  float: left;  margin: 1px 0px 1px 15px;  width: 570px;  height: 13px; }\n" +
				".orderItemTable{  width: 600px;  border: 1px solid #e5e5e5;  margin: 5px; } .orderItemEvenRow{  background-color: #FFFFFF;  border-left: 1px solid #e5e5e5;  border-top: 1px solid #e5e5e5;  border-right: 1px solid #e5e5e5; }\n" +
				".orderItemOddRow{  background-color: #F1F0F0;  border-left: 1px solid #e5e5e5;  border-right: 1px solid #e5e5e5; } .orderItemFirstColumn{  width: 430px;  position: relative; }\n" +
				".orderItemSecondColumn{  width: 70px;  border-left: 1px solid #e5e5e5;  border-right: 1px solid #e5e5e5;  text-align: center;  vertical-align: top; }\n" +
				".orderItemThirdColumn{  width: 100px;  vertical-align: top;  text-align: right; }\n" +
				".brandImage{  height: 30px;  width: 120px;  position: relative;  float: left;  margin: 5px; }\n" +
				".mediaTitle{  font-family: verdana !important;  font-size: 12px;  font-weight: bold;  color: #444444;  position: relative;  float: left;  margin: 10px 0 0 0;  width: 290px; }\n" +
				".mediaCategory{  font-family: verdana !important;  font-size: 11px;  color: #00426e;  position: relative;  float: left;  margin: 3px 0 5px 0; }\n" +
				".parutionDate{  font-family: verdana !important;  font-size: 11px;  font-weight: bold;  color: #00426e;  margin: 8px 0 0 20px; }\n" +
				".parutionDatesList{  font-family: verdana !important;  font-size: 11px;  color: #606160;  margin: 5px 0 8px 20px; }\n" +
				".adLbl{  font-family: verdana !important;  font-size: 11px;  font-weight: bold;  color: #00426e;  position: relative;  float : left;  margin: 8px 0 0 20px;  width: 300px;  height: 13px; }\n" +
				".adPreviewImage{  height: 116px;  width: 153px;  position: relative;  float : left;  margin: 5px 10px 10px 20px; }\n" +
				".adSize{  font-family: verdana !important;  font-size: 11px;  color: #606160;  position: relative;  float: left;  width: 230px; }\n" +
				".adSizeValue{  font-family: verdana !important;  font-size: 11px;  color: #606160;  float: left;  margin-bottom: 60px;  position: relative;  width: 230px; }\n" +
				".clickOnImageMsg{  font-family: verdana !important;  font-size: 10px;  color: #999999;  position: relative;  float: left;  width: 140px;  height: 42px; }\n" +
				".priceWithoutVat{  font-family: verdana !important;  font-size: 11px;  font-weight: bold;  color: #00426e;  margin: 8px 0 0 20px; }\n" +
				".unitPrice{  font-family: verdana !important;  font-size: 11px;  color: #606160;  margin: 5px 0 0 21px; }\n" +
				".repeatDiscount{  	font-family: verdana !important; font-size: 11px; color: #e66647; margin: 5px 0 0 21px; }\n" +
				".netAmountWithoutVat{  font-family: verdana !important;  font-size: 11px;  font-weight: bold;  color: #00426e;  margin: 5px 0 12px 21px; }\n" +
				".unitPriceValue{  font-family: verdana !important;  font-size: 11px;  color: #606160;  margin: 26px 3px 0 0; }\n" +
				".repeatDiscountValue{  font-family: verdana !important; font-size: 11px; color: #e66647; margin: 5px 3px 0 0;}\n" +
				".netAmountWithoutVatValue{  font-family: verdana !important;  font-size: 11px;  font-weight: bold;  color: #00426e;  margin: 26px 3px 0 0;  margin: 5px 3px 0 0; }\n" +
				".alertBox{  width: 611px;  position: relative;  float: left;  margin: 10px 0px; }\n" +
				".topRow-alertBox{  height: 6px;  width: 611px;  position: relative;  float: left; }\n" +
				".middleRow-alertBox{  width: 611px;  position: relative;  float: left;  background: transparent url(\"../img/alertBox_blue_04.gif\") repeat-y scroll left top; }\n" +
				".bottomRow-alertBox{  height: 8px;  width: 611px;  position: relative;  float: left; } #content-alertBox{  position: relative;  float: left;  width: 598; }\n" +
				".leftTop-alertBox{  width: 6px;  height: 6px;  background: transparent url(\"../img/alertBox_blue_01.gif\") no-repeat scroll 0 bottom;  position: relative;  float: left; }\n" +
				".middleTop-alertBox{  width: 598px;  height: 6px;  background: transparent url(\"../img/alertBox_blue_02.gif\") no-repeat scroll 0 bottom;  position: relative;  float: left; }\n" +
				".rightTop-alertBox{  width: 7px;  height: 6px;  background: transparent url(\"../img/alertBox_blue_03.gif\") no-repeat scroll 0 bottom;  position: relative;  float: left; }\n" +
				".title-alertBox{  position: relative;  float: left;  width: 120px;  height: 34px;  margin: 10px;  color: #00426e;  font-family: arial !important;  font-weight: bold;  font-size: 10px;  text-align: right;  padding-top: 12px;  background: transparent url(\"../img/info_logo.gif\") no-repeat scroll left top; }\n" +
				".body-alertBox{  position: relative;  float: right; width: 440px;  margin: 10px;  color: #666666;  font-family: arial !important;  font-style: italic;  font-size: 11px; }\n" +
				".leftBottom-alertBox{  width: 6px;  height: 8px;  background: transparent url(\"../img/alertBox_blue_07.gif\") no-repeat scroll 0 bottom;  position: relative;  float: left; }\n" +
				".middleBottom-alertBox{  width: 597px;  height: 8px;  background: transparent url(\"../img/alertBox_blue_08.gif\") no-repeat scroll 0 bottom;  position: relative;  float: left; }\n" +
				".rightBottom-alertBox{  width: 7px;  height: 8px;  background: transparent url(\"../img/alertBox_blue_09.gif\") no-repeat scroll 0 bottom;  position: relative;  float: left; }\n" +
				".screenTitleText {  font-family: verdana !important;  font-size: 14px;  color: #005482;  width: 438px; }\n" +
				".screenTitleHelpText {  color: #666666;  font-family: verdana !important;  font-size: 12px;  margin-top: 10px;  width: 611px; }\n" +
				".screenTitleHelpText strong {  font-size: 10px;  font-weight: bold;  text-transform: uppercase;  color: #666666;  margin-bottom: 20px; }\n" +
				".screenTitleHelpText p {  margin: 5px 0px; }\n" +
				".formBoxTitle {  color: #000000;  font-family: verdana !important;  font-size: 14px;  margin-top: 30px;  margin-bottom: 10px; }\n" +
		"</style>\n</head>\n");
		return header;
	}

	private String getI18nLabel(String parent, String key)
	{
		String NOT_FOUND = "???";
		String result = NOT_FOUND + key + NOT_FOUND;
		if (this.labelMap.containsKey(parent)) {
			if (this.labelMap.get(parent).containsKey(key)) {
				result = this.labelMap.get(parent).get(key);
			}
		}
		return result;
	}

	public String getI18nHelp(String parent, String key){
		String NOT_FOUND = "???";
		String result = NOT_FOUND + key + NOT_FOUND;
		if (this.helpMap.containsKey(parent)) {
			if (this.helpMap.get(parent).containsKey(key)) {
				result = this.helpMap.get(parent).get(key);
			}
		}
		return result;
	}

	public void setLabelMap(HashMap<String, HashMap<String, String>> labelMap) 
	{
		this.labelMap = labelMap;
	}

	public void setHelpMap(HashMap<String, HashMap<String, String>> helpMap) 
	{
		this.helpMap = helpMap;
	}	
	
	
	//================== Copied from FrontEnd. Will be removed shortly and refactored ========================

	private static final String EXTENDED_PUBLICATION_DATE_SYMBOL = "*";
	private static final String ONLINE_PUBLICATION_DATE_SEPARATOR_SYMBOL = "-";

	private List<String> buildFormattedListOfSelectedInsertionDatesForPrintOffer(List<EldoradoSummaryOfferInsertionDto> insertionPeriods) {
		// Build the list of selected dates for this offer.
		List<String> result = new ArrayList<String>();
		for(EldoradoSummaryOfferInsertionDto insertion : insertionPeriods) {
			String date = date2SummaryString(insertion.getIssueDate());
			if(insertion.isExtendedCirculation()) {
				date += EXTENDED_PUBLICATION_DATE_SYMBOL; 
			}
			result.add(date);
		}
		return result;
	}

	private String date2SummaryString(Date dateValue)
	{
		StringBuffer date = new StringBuffer();
		date.append(getDayById(dateValue).substring(0, 2).toLowerCase() + " ");
		date.append(dateValue.getDate() + " ");
		date.append(getMonthById(dateValue).toLowerCase() + " ");
		date.append((dateValue.getYear() + 1900));
		return date.toString();
	}

	private String buildPublicationDatesTextForPrintOffer(List<String> selectedDatesAsString) {
		return asString(selectedDatesAsString, ", ") + ";<br/>";
	}

	private static String[] monthNames = new String[] { "MONTH_01", "MONTH_02",
		"MONTH_03", "MONTH_04", "MONTH_05", "MONTH_06", "MONTH_07", "MONTH_08", "MONTH_09",
		"MONTH_10", "MONTH_11", "MONTH_12" };

	private static String[] dayNames = new String[] { "DAY_01", "DAY_02",
		"DAY_03", "DAY_04", "DAY_05", "DAY_06", "DAY_07" };

	private String getMonthById(int id){
		return getI18nLabel("ELDORADO_CALENDAR", monthNames[id]);
	}

	private String getDayById(int id){
		return getI18nLabel("ELDORADO_CALENDAR", dayNames[id]);
	}

	@SuppressWarnings("deprecation")
	private String getDayById(Date day) {
		int date = day.getDay();
		if(date == 0) {
			date += 7; // sunday is 7
		}

		return getDayById(date-1);
	}

	@SuppressWarnings("deprecation")
	private String getMonthById(Date day) {
		int month = day.getMonth();

		return getMonthById(month);
	}
	
	private String asString(List<?> words, String separator) {
        if(words == null || words.isEmpty()) {
            return "";
        }
        // here size must be at least 1
        int size = words.size() - 1; 
        if(size == 0) {
            return words.get(0).toString();
        }
        else {
            StringBuffer buf = new StringBuffer();
            for(int i = 0; i < size; i++) {
                buf.append(words.get(i)+separator);
            }
            //this is ok 'cause we did (size-1) above 
            buf.append(words.get(size)); 
            
            return buf.toString();
        }
    }
	
	private String buildFormattedTextOfInsertionDatesForPrintPlusOffer(EldoradoSummaryOfferDto offer) {
        StringBuffer result = new StringBuffer();
        result.append(" " + getI18nLabel("ELDORADO_SUMMARY", "PRINT_PLUS_MEDIA"));
        result.append(" " + offer.getPrintPlus().getMediaSite());
        result.append(" " + getI18nLabel("ELDORADO_SUMMARY", "PRINT_PLUS_SURCHARGE"));
        
        EldoradoSummaryOfferInsertionDto firstInsertion = offer.getFirstInsertionInFirstPlacement();
        result.append(" " + buildFormattedTextOfInsertionDatesForOnlineOffer(firstInsertion));
        
        return result.toString();
    }

	private String buildFormattedTextOfInsertionDatesForOnlineOffer(EldoradoSummaryOfferInsertionDto firstInsertion) {
		Date startDate = firstInsertion.getIssueDate();
		Date endDate = copyDate(startDate);
		addDaysToDate(endDate, firstInsertion.getDuration());

		return date2SummaryString(startDate) + " " + ONLINE_PUBLICATION_DATE_SEPARATOR_SYMBOL + " " + date2SummaryString(endDate);
	}

	private void addDaysToDate(Date date, int days) {
		date.setDate(date.getDate() + days);
	}

	private Date copyDate(Date date) {
		if (date == null) {
			return null;
		}
		Date newDate = new Date();
		newDate.setTime(date.getTime());
		return newDate;
	}
	
	private String getLocalizedMeasurementType(SizeMeasurementTypeEnumDto sizeMeasurementType) 
	{
		String type = "";
		if (sizeMeasurementType == null) 
		{
			return type;
		}

		switch (sizeMeasurementType) {
		case MILLIMETERS:
			type = "MM_UNIT_TEXT";
			break;
		case CHARS:
			type = "CHARS_UNIT_TEXT";
			break;
		case WORDS:
			type = "WORDS_UNIT_TEXT";
			break;
		case LINES:
			type = "LINES_UNIT_TEXT";
			break;
		default:
			return type;
		}

		return getI18nLabel("ELDORADO_SUMMARY", type);
	}
	
	private String price2String(Float priceValue)
	{
		if (priceValue != null) 
		{
			return DOUBLE_DIGIT.format(priceValue);
		}
		return "NaN";
	}

	private float getTotalDiscountAmountForInsertions(List<EldoradoSummaryOfferInsertionDto> insertions) {
		float result = 0;
		for (EldoradoSummaryOfferInsertionDto insertion : insertions) {
			result += insertion.getDiscount().floatValue();
		}

		//always negative or 0
		return result;
	}
	
    private String buildInfoTextForExtendedPublicationDate() {
        return EXTENDED_PUBLICATION_DATE_SYMBOL + " " + getI18nLabel("ELDORADO_SUMMARY", "EXTENDEDUNIT_PRICE");
    }
    
    private float getDiscountPercentForInsertions (List<EldoradoSummaryOfferInsertionDto> insertions) {
        float result = 0;
        for (EldoradoSummaryOfferInsertionDto insertion : insertions) {
            if (result == 0 && insertion.getDiscountCalculation() != null) {
                result = insertion.getDiscountCalculation().getPercent().floatValue();
            }
            if(result != 0) {
                return result;
            }
        }
        // always positive or 0!
        return result;
    }

	private String[] buildDetailedTextForPriceWithoutTVA(String currency, 
			EldoradoSummaryOfferDto currentOffer, List<EldoradoSummaryOfferInsertionDto> insertions) 
	{
        // this tells whether it is mm/col, ...
		String unitType = getLocalizedMeasurementType(currentOffer.getSizeMeasurementType());
        
        StringBuffer insertionPriceText = new StringBuffer();
        StringBuffer insertionPriceValue = new StringBuffer();

        for (EldoradoSummaryOfferInsertionDto insertion : insertions) {
            if(insertionPriceText.length() > 0 || insertionPriceValue.length() > 0) {
                insertionPriceText.append("<br>");
                insertionPriceValue.append("<br>");
            }
            
            String textForCurrentInsertionPrice = date2SummaryString(insertion.getIssueDate()) + ", ";
            if (insertion.isExtendedCirculation()) {
                textForCurrentInsertionPrice += getI18nLabel("ELDORADO_SUMMARY", "EXTENDEDUNIT_PRICE");
            }
            else {
                textForCurrentInsertionPrice += getI18nLabel("ELDORADO_SUMMARY", "UNIT_PRICE");
            }
            textForCurrentInsertionPrice += ", " + currency + " " + price2String(insertion.getCalculation().getPricePerUnit().floatValue()) + " / " + unitType;
            insertionPriceText.append(textForCurrentInsertionPrice);
                    
            float price = insertion.getAmount().floatValue(); 
            insertionPriceValue.append(currency + " " + price2String(price));         
        }

        return new String[]{insertionPriceText.toString(), insertionPriceValue.toString()};
    }

	private String getDates (EldoradoSummaryOfferPlacementDto placement, EldoradoSummaryOfferDto offer) 
	{
		final List<EldoradoSummaryOfferInsertionDto> insertionPeriods = placement.getInsertions();

		// Build the list of selected dates for this offer.
		final List<String> selectedDatesAsString;
		String dates = "";
		if (offer.getMediaSupport() == MediaSupportEnumDto.PRINT)
		{
			selectedDatesAsString = buildFormattedListOfSelectedInsertionDatesForPrintOffer(insertionPeriods);
			dates = buildPublicationDatesTextForPrintOffer(selectedDatesAsString);
		} 
		else 
		{
			EldoradoSummaryOfferInsertionDto firstInsertion = insertionPeriods.get(0);
	        dates = buildFormattedTextOfInsertionDatesForOnlineOffer(firstInsertion);
		}

		// if print plus, then update the view accordingly
		if (offer.getPrintPlus() != null && offer.getPrintPlus().getMediaSite() != null) {
			dates += buildFormattedTextOfInsertionDatesForPrintPlusOffer(offer);
		}

		return dates;
	}
	
	private String buildTextForDiscountPercent(float discountPercent) {
        return getI18nLabel("ELDORADO_SUMMARY", "OFFER_DISCOUNT") + " " + price2String(discountPercent) + "%";
    }
	
	private String buildTextForPrice(float price) {
        return currency + " " + price2String(price);
    }

}
