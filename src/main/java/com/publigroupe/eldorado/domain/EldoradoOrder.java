package com.publigroupe.eldorado.domain;

import com.publigroupe.eldorado.exception.EldoradoException;
import com.publigroupe.javaBase.base.StringUtil;
import com.publigroupe.media.domain.CodeList;
import com.publigroupe.order.dao.p2000.Order01Dao;
import com.publigroupe.order.dao.p2000.SPMypInsert7ADao;
import com.publigroupe.order.dao.p2000.bean.Order01;
import com.publigroupe.order.domain.OrderCriteria;
import com.publigroupe.order.domain.p2000.OrderP2000Persist;
import com.publigroupe.order.dto.OrderDto;

public class EldoradoOrder {
  /**
   * Store the user into P2000
   */
  public void saveUserInfo(String subsidiaryCode, String orderNr, String user) {
    Order01Dao order01Dao = new Order01Dao("RSGV01");
    Order01 order01 = order01Dao.read(subsidiaryCode, orderNr);
    order01.gvprofid = user;
    order01.gvst01 = "1";
    order01.gvst02 = "1";
    order01Dao.update(order01);
  }

  /**
   * Store the command into P2000
   */
  public void insertP2000(OrderDto orderDto) {
    OrderCriteria orderCriteria = new OrderCriteria(null);
    OrderP2000Persist orderPersist = new OrderP2000Persist(orderDto, orderCriteria);
    orderPersist.persistMaterials();

    callProcedureInsert7A(orderDto, "V1");
  }

  /**
   * Revert suspension of the command and revalidate P2000 command
   */
  public void sendP2000Validation(OrderDto orderDto) {
    callProcedureInsert7A(orderDto, "VE");
  }

  /**
   * Ask P2000 to cancel of the command
   */
  public void sendP2000Cancelation(OrderDto orderDto) {
    callProcedureInsert7A(orderDto, "AB");
  }

  private void callProcedureInsert7A(OrderDto orderDto, String trtCode) {
    String utiento = CodeList.extractCodeValue("UTIENTO", orderDto.getCodeList(), orderDto.getCodeValue());
    String cdenum = CodeList.extractCodeValue("CDENUM", orderDto.getCodeList(), orderDto.getCodeValue());
    SPMypInsert7ADao spMypInsert7ADao = new SPMypInsert7ADao();
    spMypInsert7ADao.setTrtcod2(trtCode);
    spMypInsert7ADao.setUtiento(utiento);
    spMypInsert7ADao.setCdenum(cdenum);
    spMypInsert7ADao.setCdvalid("0"); // 1 pour Online - 0 pour Print/Mixed
    spMypInsert7ADao.call();
    String message = spMypInsert7ADao.checkReturn();
    if (!StringUtil.isEmpty(message)) {
      throw new EldoradoException(message);
    }
  }
}
