package com.publigroupe.eldorado.domain.datatrans;

public interface DatatransResponseParameters extends DatatransRequestParameters {
    /**
     * @return Settlement response code.
     */
    String getResponseCode();
    
    /**
     * @return Settlement response message text.
     */
    String getResponseMessage();
    
    /**
     * @return In case of error, this provide more information about the cause.
     */
    String getResponseDetail();
}
