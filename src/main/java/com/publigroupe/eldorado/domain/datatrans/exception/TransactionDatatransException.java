package com.publigroupe.eldorado.domain.datatrans.exception;


/**
 * Errors reported by Datatrans are always mapped to {@link TransactionDatatransException}.
 * @author mdt
 *
 */
@SuppressWarnings("serial")
public class TransactionDatatransException extends DatatransException {

    public TransactionDatatransException(String msg) {
        super(msg);
    }
}
