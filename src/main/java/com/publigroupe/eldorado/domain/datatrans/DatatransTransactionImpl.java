package com.publigroupe.eldorado.domain.datatrans;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.publigroupe.eldorado.domain.datatrans.exception.DatatransException;
import com.publigroupe.eldorado.domain.datatrans.exception.GenericDatatransException;
import com.publigroupe.javaBase.property.PropertyLoader;

/**
 * Datatrans debit transaction.
 * @author mdt
 *
 */
public class DatatransTransactionImpl implements DatatransTransaction<String> {

    private final String request;
    private DatatransResponse result;
    
    private boolean executed;
    
    public DatatransTransactionImpl(String request) {
        this.request = request;
        this.result = null;
        this.executed = false;
    }
    
    /**
     * Create the debit request to send to Datatrans for the provided order.
     */
    @Override
    public synchronized void execute() throws DatatransException {
        checkTransactionExecutionState();
        
        PropertyLoader pl = new PropertyLoader();
        String DATATRANS_PAYMENT_SERVICE_URL = pl.getProperty("datatrans.paymentServiceUrl");
        String DATATRANS_XML_ENCODING = pl.getProperty("datatrans.encoding");
    
        StringEntity strent;
        try {
            strent = new StringEntity(this.getRequest());
        } catch (UnsupportedEncodingException e) {
            throw GenericDatatransException.encodingError(e);
        }

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost debitService = new HttpPost(DATATRANS_PAYMENT_SERVICE_URL);
        strent.setContentType("text/xml; charset="+DATATRANS_XML_ENCODING.toLowerCase());
        debitService.setEntity(strent); 
        
        HttpResponse response = null;
        Document transactionResult = null;
        
        try {
            response = httpclient.execute(debitService);
            transactionResult = parseXmlFile(response.getEntity().getContent());
            setResult(transactionResult);
        } catch (ClientProtocolException e) {
            throw GenericDatatransException.communicationError(e);
        } catch (IllegalStateException e) {
            throw GenericDatatransException.communicationError(e);
        } catch (IOException e) {
            throw GenericDatatransException.communicationError(e);
        } finally {
            this.executed = true;
        }
    }

    private void checkTransactionExecutionState() throws GenericDatatransException {
        if(executed) {
            throw GenericDatatransException.transactionAlreadyExecuted(this);
        }
    }

    @Override
    public String getRequest() {
        return this.request;
    }

    @Override
    public DatatransResponse getResult() {
        return this.result;
    }
    
    private void setResult(Document dom) throws DatatransException {
        this.result = DatatransResponse.create(dom);
    }
    
    private Document parseXmlFile(InputStream is) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            return db.parse(is);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    
}
