package com.publigroupe.eldorado.domain.datatrans;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

/**
 * Encapsulates the result of an executed Datatrans transaction.
 * 
 * @author mdt
 */
public class DatatransResponse implements DatatransResponseParameters {
    private static Log log = LogFactory.getLog(DatatransResponse.class);

    private final String amount;
    private final String authorizationCode;
    private final String currency;
    private final String merchantId;
    private final String refno;

    private final String responseCode;
    private final String responseMessage;
    private final String responseDetail;

    private final String trxStatus;
    private final String status;

    private DatatransResponse(String amount, String authorizationCode,
            String currency, String merchantId, String refno,
            String responseCode, String responseMessage, String responseDetail,
            String trxStatus, String status) {
        this.amount = amount;
        this.authorizationCode = authorizationCode;
        this.currency = currency;
        this.merchantId = merchantId;
        this.refno = refno;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.responseDetail = responseDetail;
        this.trxStatus = trxStatus;
        this.status = status;
    }

    /**
     * Factory method for building a {@link DatatransResponse}. The response is
     * built from the passed XML document, which is assumed to be the document
     * sent by Datatrans in response to the previously sent transaction request.
     * 
     * @param doc The XML document returned by Datatrans for the requested
     *            transaction.
     * @return An easy-to-manipulate object that encapsulates the XML document
     *         returned by Datatrans.
     */
    public static DatatransResponse create(Document doc) {
        try {
            OutputFormat format = new OutputFormat(doc);
            format.setLineWidth(65);
            format.setIndenting(true);
            format.setIndent(2);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(doc);
            log.debug("Received XML from Datatrans for transition is: \n"
                    + out.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        String amount = getTextContentForNode(doc,
                "paymentService/body/transaction/request/amount");
        String authorizationCode = getTextContentForNode(doc,
                "paymentService/body/transaction/request/authorizationCode");
        String currency = getTextContentForNode(doc,
                "paymentService/body/transaction/request/currency");
        String merchantId = getTextContentForNodeAttr(doc,
                "paymentService/body", "merchantId");
        String status = getTextContentForNodeAttr(doc,
                "paymentService/body", "status");
        String refno = getTextContentForNodeAttr(doc,
                "paymentService/body/transaction", "refno");
        String trxStatus = getTextContentForNodeAttr(doc,
                "paymentService/body/transaction", "trxStatus");
        
        String responseCode;
        String responseMessage;
        String responseDetail;
        if("error".equals(status)) {
            responseCode = getTextContentForNode(doc,
                "paymentService/body/error/errorCode");
            responseMessage = getTextContentForNode(doc,
                "paymentService/body/error/errorMessage");
            responseDetail = getTextContentForNode(doc,
                "paymentService/body/error/errorDetail");
        }
        else {
            responseCode = getTextContentForNode(doc,
                "paymentService/body/transaction/error/errorCode");
            responseMessage = getTextContentForNode(doc,
                "paymentService/body/transaction/error/errorMessage");
            responseDetail = getTextContentForNode(doc,
                "paymentService/body/transaction/error/errorDetail");
        }
        return new DatatransResponse(amount, authorizationCode, currency,
                merchantId, refno, responseCode, responseMessage,
                responseDetail, trxStatus, status);
    }

    private static String getTextContentForNodeAttr(Document doc, String expr,
            String attr) {
        XPath xpath = XPathFactory.newInstance().newXPath();
        try {
            Node node = (Node) xpath.evaluate(expr, doc, XPathConstants.NODE);
            NamedNodeMap attrs = node.getAttributes();
            Node attrNode = attrs.getNamedItem(attr);
            return attrNode.getTextContent();
        } catch (XPathExpressionException xpe) {
            return null;
        } catch (NullPointerException npe) {
            return null;
        }
    }

    private static String getTextContentForNode(Document doc, String elementExpr) {
        XPath xpath = XPathFactory.newInstance().newXPath();
        try {
            Node node = (Node) xpath.evaluate(elementExpr, doc,
                    XPathConstants.NODE);
            return node.getTextContent();
        } catch (XPathExpressionException xpe) {
            return null;
        } catch (NullPointerException npe) {
            return null;
        }
    }

    public boolean didFail() {
        return "error".equals(trxStatus) || "error".equals(status);
    }

    @Override
    public String getMerchantId() {
        return this.merchantId;
    }

    @Override
    public String getAmount() {
        return this.amount;
    }

    @Override
    public String getCurrency() {
        return this.currency;
    }

    @Override
    public String getRefno() {
        return this.refno;
    }

    @Override
    public String getAuthorizationCode() {
        return this.authorizationCode;
    }

    @Override
    public String getPMethod() {
        return "";
    }

    @Override
    public String getResponseCode() {
        return this.responseCode;
    }

    @Override
    public String getResponseMessage() {
        return this.responseMessage;
    }

    @Override
    public String getResponseDetail() {
        return this.responseDetail;
    }

    public String formattedTransactionResultText() {
        String msg = "Transaction refno=" + getRefno();
        if (didFail()) {
            msg += " failed because of the following error: ["
                    + getResponseCode() + "] " + getResponseMessage() + " ("
                    + getResponseDetail() + " ).";
        } else {
            msg += " succeeded.";
        }

        return msg;
    }

}
