package com.publigroupe.eldorado.domain.datatrans;

import com.publigroupe.eldorado.domain.datatrans.exception.DatatransException;

/**
 * <p>
 * Proxy class that defines the interface for executing a transaction with Datatrans.
 * </p>
 * 
 * @author mdt
 *
 * @param <Input> The input's type of the transaction that has to be carried out. 
 */
public abstract class DatatransProxy<Input> {

    public DatatransTransaction<Input> createCancelRequest() throws DatatransException {
        Input cancelInput = createInputCancelTransaction();
        return createDatatransCancelTransaction(cancelInput);
    }
    
    public DatatransTransaction<Input> createDebitRequest() throws DatatransException {
        Input debitInput = createInputDebitTransaction();
        return createDatatransDebitTransaction(debitInput);
    }

    abstract protected Input createInputCancelTransaction() throws DatatransException;
    
    abstract protected Input createInputDebitTransaction() throws DatatransException;

    abstract protected DatatransTransaction<Input> createDatatransDebitTransaction(Input debitInput);
    
    abstract protected DatatransTransaction<Input> createDatatransCancelTransaction(Input cancelInput);
}
