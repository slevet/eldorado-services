package com.publigroupe.eldorado.domain.datatrans;

/**
 * Mandatory parameters that have to be sent to Datatrans for 
 * correctly carrying out a transaction.
 * 
 * @author mdt
 *
 */
public interface DatatransRequestParameters {
// MANDATORY PARAMETERS 
    
    /**
     * Unique Merchant Identifier (assigned by Datatrans).
     */
    String getMerchantId();
    
    /**
     * Transaction amount in cents or smallest available unit of the
     * currency (e.g. 123.50 = 12350) Must not exceed the authorised amount!
     */
    String getAmount();
    
    /**
     * Transaction currency – ISO Character Code (CHF, USD, EUR etc.).
     */
    String getCurrency();
    
    /**
     * Merchant reference number; note: unique value for PayPal; can be
     * defined as unique value for all other payment methods, too. Please refer
     * to support@datatrans.ch for details.
     */
    String getRefno();
    
    /**
     * Unique authorization code returned by the Datatrans
     * payment application during authorization procedure.
     * Has been returned after the authorization request!
     */
    String getAuthorizationCode();
    
    /**
     * Required for PostFinance (value POS) and PayPal (value PAP). 
     * Default is EMPTY String.
     */
    String getPMethod();
    
// OPTIONAL PARAMETERS 
    
    //TODO: Add if needed
}
