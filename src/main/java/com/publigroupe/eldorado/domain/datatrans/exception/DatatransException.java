package com.publigroupe.eldorado.domain.datatrans.exception;

/**
 * Top-class for all exceptions occurring while processing a Datatrans 
 * transaction.
 * 
 * @author mdt
 */
@SuppressWarnings("serial")
public abstract class DatatransException extends Exception {
    
    public DatatransException(String msg) {
        super(msg);
    }
    
    public DatatransException(String msg, Throwable e) {
        super(msg, e);
    }
    
}
