package com.publigroupe.eldorado.domain.datatrans.util;

public class DatatransXmlBuilder {

    private final StringBuffer xml;
    
    private DatatransXmlBuilder(String xmlEncoding) {
        this.xml = new StringBuffer();
        this.xml.append("<?xml version=\"1.0\" encoding=\"" + xmlEncoding + "\"?>");
    }

    public static DatatransXmlBuilder build(String xmlEncoding) {
        return new DatatransXmlBuilder(xmlEncoding);
    }

    public void insertPaymentServiceTag(String version) {
        this.xml.append("<paymentService version=\"" + version + "\">");
    }

    public void insertBodyTag(String merchantid, String testOnly) {
        this.xml.append("<body merchantId=\"" + merchantid + "\" testOnly=\"" + testOnly + "\">");
    }

    public void insertTransactionTag(String refno) {
        this.xml.append("<transaction refno=\"" + refno + "\">");
    }

    public void insertRequestTag() {
        this.xml.append("<request>");
    }

    public void insertAmount(int priceInCents) {
        this.xml.append("<amount>" + priceInCents + "</amount>");
    }

    public void insertCurrency(String currency) {
        this.xml.append("<currency>" + currency + "</currency>");
    }

    public void insertAuthorizationCode(String authorizationCode) {
        this.xml.append("<authorizationCode>" + authorizationCode + "</authorizationCode>");
    }

    public void closeRequestTag() {
        this.xml.append("</request>");
    }

    public void closeTransactionTag() {
        this.xml.append("</transaction>");
    }

    public void closeBodyTag() {
        this.xml.append("</body>");
    }

    public void closePaymentServiceTag() {
        this.xml.append("</paymentService>");
    }
    
    public String getXml() {
        return this.xml.toString();
    }

    public void insertReqType() {
        this.xml.append("<reqtype>DOA</reqtype>");
    }
}
