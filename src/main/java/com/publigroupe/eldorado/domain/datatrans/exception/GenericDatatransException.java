package com.publigroupe.eldorado.domain.datatrans.exception;

import com.publigroupe.eldorado.domain.datatrans.DatatransTransaction;

/**
 * <p>
 * Generic Datatrans Exception used to report generic errors occuring while processing 
 * a Datatrans transaction. 
 * </p>
 * <p>
 * It should be used to report wrongly configured clients (tipically missing keys in app.properties, wrong 
 * encoding, and such).  
 * </p>
 * <p>
 * For transaction errors reported by Datatrans (e.g., -80 - UPP record not found), you should always use 
 * the {@link TransactionDatatransException}.
 * </p>
 * 
 * @author mdt
 */
@SuppressWarnings("serial")
public class GenericDatatransException extends DatatransException {
    
    private static String ENCODING_ERROR = "Could not carry out Datatrans transaction because of a character encoding issue.";
    private static String GENERIC_COMUNICATION_ERROR = "A connection error occurred.";
    private static String REQUESTED_CONTENT_ERROR = "The requested content couln't be supplied by Datatrans.";
    
    private GenericDatatransException(String msg) {
        super(msg);
    }
    
    private GenericDatatransException(String msg, Throwable t) {
        super(msg, t);
    }

    public static GenericDatatransException encodingError(Throwable t) {
        return new GenericDatatransException(GenericDatatransException.ENCODING_ERROR, t);
    }
    
    public static GenericDatatransException communicationError(Throwable t) {
        return new GenericDatatransException(GenericDatatransException.GENERIC_COMUNICATION_ERROR, t);
    }
    
    public static GenericDatatransException requestedContentError(Throwable t) {
        return new GenericDatatransException(GenericDatatransException.REQUESTED_CONTENT_ERROR, t);
    }
    
    public static GenericDatatransException transactionAlreadyExecuted(DatatransTransaction<?> transaction) {
        return new GenericDatatransException("The transaction '"+transaction+"' has already been executed");
    }

}
