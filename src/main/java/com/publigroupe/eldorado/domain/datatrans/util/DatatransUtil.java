package com.publigroupe.eldorado.domain.datatrans.util;

import java.math.BigDecimal;

public class DatatransUtil {

    private DatatransUtil() {}
    
    /**
     * Datatrans expects the price to be an integer number where the unit is the cent. 
     * Convert the provided {@code price} into the expected format. If the {@code price}
     * can't be converted into an exact integer (where the unit is the cent), throw
     * a {@link RuntimeException}.
     * 
     * 
     * @param price The price to convert into a Datatrans valid natural number.
     * @return The {@code price} as an integer number where the unit is the cent.
     */
    public static int convertPrice(BigDecimal price) {
        BigDecimal result = price; 
        // build-up the amount as an integer, unit is cents 
        result = result.multiply(new BigDecimal(100)); 

        // This might throw an exception if the amount cannot be converted in an
        // integer. If this is the case,
        // it means that the price's order stored in the DB is wrong and must be
        // corrected.
        return result.toBigIntegerExact().intValue(); 
    }
}
