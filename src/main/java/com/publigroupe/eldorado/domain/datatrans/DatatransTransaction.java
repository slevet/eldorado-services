package com.publigroupe.eldorado.domain.datatrans;

import com.publigroupe.eldorado.domain.datatrans.exception.DatatransException;

/**
 * Simple interface for any Datatrans transaction.
 * 
 * @author mdt
 *
 * @param <Input> The input's type of the transaction that has to be carried out. 
 */
public interface DatatransTransaction<Input> {
    /**
     * Execute the Datatrans transaction.
     * @throws DatatransException if an error occurs while processing the transaction request.
     */
    void execute() throws DatatransException;
    
    /**
     * @return the request transaction object that is sent to Datatrans. 
     */
    Input getRequest();
    
    /**
     * @return The result of executing the {@code Input} Datatrans transaction.
     */
    DatatransResponse getResult();
}
