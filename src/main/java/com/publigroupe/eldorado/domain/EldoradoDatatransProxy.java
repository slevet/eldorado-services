package com.publigroupe.eldorado.domain;

import com.publigroupe.eldorado.dao.bean.Rsgv25;
import com.publigroupe.eldorado.dao.bean.Rsgv25.Stransa;
import com.publigroupe.eldorado.domain.datatrans.DatatransProxy;
import com.publigroupe.eldorado.domain.datatrans.DatatransTransaction;
import com.publigroupe.eldorado.domain.datatrans.DatatransTransactionImpl;
import com.publigroupe.eldorado.domain.datatrans.exception.DatatransException;
import com.publigroupe.eldorado.domain.datatrans.exception.TransactionDatatransException;
import com.publigroupe.eldorado.domain.datatrans.util.DatatransUtil;
import com.publigroupe.eldorado.domain.datatrans.util.DatatransXmlBuilder;
import com.publigroupe.javaBase.property.PropertyLoader;

/**
 * Proxy for interfacing Eldorado to Datatrans.
 * 
 * @author mdt
 */
public class EldoradoDatatransProxy extends DatatransProxy<String> {

  private final Rsgv25 order;

  private String DATATRANS_XML_ENCODING;
  private String DATATRANS_XML_PAYMENT_VERSION;
  private String DATATRANS_XML_TESTONLY;
  private String DATATRANS_XML_ORDER_PREFIX;
  private String DATATRANS_XML_ORDER_CURRENCY;
  private String DATATRANS_XML_MERCHANTID;

  private String getBpoVariable(String bpoPrefix) {
    if (bpoPrefix == null || bpoPrefix.equals("") || bpoPrefix.equals("10")) {
      return "";
    }
    return "." + bpoPrefix;
  }

  public void initVariables(String bpoPrefix) {
    String bpoVariable = getBpoVariable(bpoPrefix);
    PropertyLoader pl = new PropertyLoader();
    DATATRANS_XML_ENCODING = pl.getProperty("datatrans.encoding" + bpoVariable);
    DATATRANS_XML_PAYMENT_VERSION = pl.getProperty("datatrans.paymentVersion" + bpoVariable);
    DATATRANS_XML_TESTONLY = pl.getProperty("datatrans.testOnly" + bpoVariable);
    DATATRANS_XML_ORDER_PREFIX = pl.getProperty("datatrans.refnoPrefix" + bpoVariable);
    DATATRANS_XML_ORDER_CURRENCY = pl.getProperty("datatrans.currency" + bpoVariable);
    DATATRANS_XML_MERCHANTID = pl.getProperty("datatrans.merchantId" + bpoVariable);
  }

  public EldoradoDatatransProxy(Rsgv25 order) {
    this.order = order;
    initVariables(order.utiento.substring(0, 2));
  }

  private void checkDebitTransactionPrecondition() throws TransactionDatatransException {
    if (this.order == null) {
      throw new TransactionDatatransException(
          "Serious Error! Order is NULL! Cannot create XML for executing the debit transaction.");
    }

    if (this.order.stransa != Stransa.VA) {
      throw new TransactionDatatransException("Expected order'" + order.utiento + "-" + order.cdenum
          + "' to be in state 'order.stransa=VA'. Found  'order.stransa=" + order.stransa
          + "'. Because of this inconsistency it is impossible " + "to create XML for executing the debit transaction.");
    }
  }

  @Override
  protected DatatransTransaction<String> createDatatransCancelTransaction(String cancelInput) {
    return new DatatransTransactionImpl(cancelInput);
  }

  @Override
  protected DatatransTransaction<String> createDatatransDebitTransaction(String debitInput) {
    return new DatatransTransactionImpl(debitInput);
  }

  /**
   * {@example} <paymentService version="1">
   * <body merchantId="1000011011" testOnly="yes">
   * <transaction refno="628a069429b4453bb2">
   * <request>
   * <amount>100</amount>
   * <currency>CHF</currency>
   * <authorizationCode>800247117</authorizationCode>
   * </request>
   * </transaction>
   * </body>
   * </paymentService>
   */
  @Override
  protected String createInputDebitTransaction() throws TransactionDatatransException {
    checkDebitTransactionPrecondition();

    String refno = DATATRANS_XML_ORDER_PREFIX + order.utiento + "-" + order.cdenum;

    // prepare Datatrans XML Debit request
    DatatransXmlBuilder xml = DatatransXmlBuilder.build(DATATRANS_XML_ENCODING);

    xml.insertPaymentServiceTag(DATATRANS_XML_PAYMENT_VERSION);
    xml.insertBodyTag(DATATRANS_XML_MERCHANTID, DATATRANS_XML_TESTONLY);
    xml.insertTransactionTag(refno);
    xml.insertRequestTag();
    xml.insertAmount(DatatransUtil.convertPrice(order.cdenet));
    xml.insertCurrency(DATATRANS_XML_ORDER_CURRENCY);
    xml.insertAuthorizationCode(order.authcod);
    xml.closeRequestTag();
    xml.closeTransactionTag();
    xml.closeBodyTag();
    xml.closePaymentServiceTag();

    return xml.getXml();
  }

  /**
   * {@example} <paymentService version="1">
   * <body merchantId="1000011011" testOnly="yes">
   * <transaction refno="628a069429b4453bb2">
   * <request>
   * <amount>100</amount>
   * <currency>CHF</currency>
   * <authorizationCode>800247117</authorizationCode>
   * <reqtype>DOA</reqtype>
   * </request>
   * </transaction>
   * </body>
   * </paymentService>
   */
  @Override
  protected String createInputCancelTransaction() throws DatatransException {

    String refno = DATATRANS_XML_ORDER_PREFIX + order.utiento + "-" + order.cdenum;

    // prepare Datatrans XML Debit request
    DatatransXmlBuilder xml = DatatransXmlBuilder.build(DATATRANS_XML_ENCODING);

    xml.insertPaymentServiceTag(DATATRANS_XML_PAYMENT_VERSION);
    xml.insertBodyTag(DATATRANS_XML_MERCHANTID, DATATRANS_XML_TESTONLY);
    xml.insertTransactionTag(refno);
    xml.insertRequestTag();
    xml.insertAmount(DatatransUtil.convertPrice(order.cdenet));
    xml.insertCurrency(DATATRANS_XML_ORDER_CURRENCY);
    xml.insertAuthorizationCode(order.authcod);
    xml.insertReqType();
    xml.closeRequestTag();
    xml.closeTransactionTag();
    xml.closeBodyTag();
    xml.closePaymentServiceTag();

    return xml.getXml();
  }

}
