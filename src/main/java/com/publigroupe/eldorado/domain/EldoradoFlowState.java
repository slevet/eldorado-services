package com.publigroupe.eldorado.domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.publigroupe.eldorado.dao.Rsgv26Dao;
import com.publigroupe.eldorado.dao.bean.Rsgv26;
import com.publigroupe.eldorado.dto.EldoradoFlowStateDto;
import com.publigroupe.javaBase.property.PropertyLoader;

public class EldoradoFlowState {
  private static Log log = LogFactory.getLog(EldoradoFlowState.class);

  /** For each day we have 24 * 60 * 60 * 1000 milliseconds. */
  private static final long MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000;

  /**
   * Number of days that an offer is allowed to stay in a SUSPENDED state.
   * When an offer is set to SUSPENDED, then the customer must edit its command
   * with the SUSPENSION_EXPIRATION_DAYS period, or the command will get
   * canceled.
   */
  private static int SUSPENSION_EXPIRATION_DAYS;
  private static final int DEFAULT_ORDER_SUSPENSION_EXPIRATION_DAYS = 6;

  /**
   * Number of days granted to the CAP to process an offer. If the order is not
   * process within this time frame, then a reminder is sent. Note that only
   * working days are considered here.
   */
  private static int ORDER_PROCESSING_DELAY;
  private static final int DEFAULT_ORDER_PROCESSING_DELAY = 1;

  static {
    PropertyLoader pl = new PropertyLoader();
    try {
      ORDER_PROCESSING_DELAY = Integer.parseInt(pl.getProperty("Order.ProcessingDelay"));
    }
    catch (NumberFormatException nfe) {
      ORDER_PROCESSING_DELAY = DEFAULT_ORDER_PROCESSING_DELAY;
      log.error("Cannot convert properties Order.ProcessingDelay in app.properties file. " + "Set to default value "
          + ORDER_PROCESSING_DELAY + ".", nfe);
    }

    try {
      SUSPENSION_EXPIRATION_DAYS = Integer.parseInt(pl.getProperty("Order.SuspensionExpirationDays"));
    }
    catch (NumberFormatException nfe) {
      SUSPENSION_EXPIRATION_DAYS = DEFAULT_ORDER_SUSPENSION_EXPIRATION_DAYS;
      log.error("Cannot convert properties Order.SuspensionExpirationDays in app.properties file. "
          + "Set to default value " + SUSPENSION_EXPIRATION_DAYS + ".", nfe);
    }
  }

  private Rsgv26Dao rsgv26Dao = new Rsgv26Dao();

  /**
   * Check if a payment is validated
   * 
   * @param paymentStateDto
   *          provided state to check
   * @return true if validated
   */
  public static boolean isValidated(EldoradoFlowStateDto paymentStateDto) {
    return (paymentStateDto.getValidationDate() != null);
  }

  /**
   * Check if an order is validated by the end user (repayment)
   * 
   * @param paymentStateDto
   *          provided state to check
   * @return true if repayment
   */
  public static boolean isValidatedByEndUser(EldoradoFlowStateDto paymentStateDto) {
    if (paymentStateDto.getValidationReference() != null && paymentStateDto.getValidationReference() != "WWL")
      return false;
    else
      return true;
  }

  /**
   * Check if a payment is debited
   * 
   * @param paymentStateDto
   *          provided state to check
   * @return true if debited
   */
  public static boolean isDebited(EldoradoFlowStateDto paymentStateDto) {
    return (paymentStateDto.getDebitDate() != null);
  }

  /**
   * Check if a payment is confirmed
   * 
   * @param paymentStateDto
   *          provided state to check
   * @return true if confirmed
   */
  public static boolean isConfirmed(EldoradoFlowStateDto paymentStateDto) {
    return paymentStateDto.getConfirmationDate() != null;
  }

  /**
   * check if the issue date is confirmed
   * 
   * @param paymentStateDto
   * @return true if confirmed
   */
  public static boolean isFeedBackIssueDateConfirmed(EldoradoFlowStateDto paymentStateDto) {
    return paymentStateDto.getFeedBackIssueDateMailDate() != null;
  }

  /**
   * Check if a payment is suspend
   * 
   * @param paymentStateDto
   *          provided state to check
   * @return true if suspended
   */
  public static boolean isSuspended(EldoradoFlowStateDto paymentStateDto) {
    return paymentStateDto.getSuspendPaimentDate() != null;
  }

  /**
   * Check if a payment suspension is notified
   * 
   * @param paymentStateDto
   *          provided state to check
   * @return true if suspension notified
   */
  public static boolean isSuspensionNotifiedToClient(EldoradoFlowStateDto paymentStateDto) {
    assert (isSuspended(paymentStateDto));
    return paymentStateDto.getSuspendMailClientDate() != null;
  }

  /**
   * Check if the suspension period for the order has expired.
   * 
   * @param paymentStateDto
   *          Holds all information about the order's state.
   * @return true if the suspension period has expired, false otherwise.
   */
  public static boolean isSuspensionTimeExpired(EldoradoFlowStateDto paymentStateDto) {
    assert (isSuspended(paymentStateDto));

    Date supensionDate = paymentStateDto.getSuspendMailClientDate();

    if (supensionDate == null) {
      return false;
    }
    else {
      long delta = System.currentTimeMillis() - supensionDate.getTime();
      return delta >= MILLISECONDS_IN_DAY * SUSPENSION_EXPIRATION_DAYS;
    }
  }

  /**
   * Check if a payment is canceled
   * 
   * @param paymentStateDto
   *          provided state to check
   * @return true if canceled
   */
  public static boolean isCancelled(EldoradoFlowStateDto paymentStateDto) {
    return paymentStateDto.getCancellationDate() != null;
  }

  /**
   * Check if a payment cancellation is notified to the client
   * 
   * @param paymentStateDto
   *          provided state to check
   * @return true if cancellation notified
   */
  public static boolean isCancellationNotifiedToClient(EldoradoFlowStateDto paymentStateDto) {
    return paymentStateDto.getCancellationMailClientDate() != null;
  }

  /**
   * Check if the provided order exist in Pub2000.
   * 
   * @param paymentStateDto
   *          The order's state
   * @return true if the order exists.
   */
  public static boolean hasOrderBeenLoadedInPub2000(EldoradoFlowStateDto paymentStateDto) {
    return paymentStateDto.getLoadingDate() != null;
  }

  /**
   * Insert entry (UTISTE+UTIENTO+CDENUM) given in parameters and
   * AUTHDT+AUTHHE to current into rsgv26
   * 
   * @param subsidiary
   *          selector of the rsgv26 entry
   * @param commandNumber
   *          selector of the rsgv26 entry
   * @param commandNumber
   */
  public EldoradoFlowStateDto create(String subsidiaryCode, String orderNr) {
    Rsgv26 rsgv26 = new Rsgv26();
    rsgv26.utiste = subsidiaryCode.substring(0, 2);
    rsgv26.utiento = subsidiaryCode;
    rsgv26.cdenum = orderNr;
    rsgv26.authdt = new Date();
    rsgv26.authhe = new Date();
    rsgv26Dao.insert(rsgv26);
    return EldoradoFlowStateMapping.convert(rsgv26);
  }

  /**
   * Set authorization date to current in the selected rsgv26 entry
   * 
   * @param subsidiary
   *          selector of the rsgv26 entry
   * @param commandNumber
   *          selector of the rsgv26 entry
   */
  public void setAuthorized(String subsidiary, String commandNumber) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiary, commandNumber);
    rsgv26.authdt = new Date();
    rsgv26.authhe = new Date();
    rsgv26Dao.update(rsgv26);
  }

  /**
   * Set debit date to current in the selected rsgv26 entry
   * 
   * @param subsidiary
   *          selector of the rsgv26 entry
   * @param commandNumber
   *          selector of the rsgv26 entry
   */
  public void setDebited(String subsidiary, String commandNumber) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiary, commandNumber);
    rsgv26.debdt = new Date();
    rsgv26.debhe = new Date();
    rsgv26Dao.update(rsgv26);
    log.info("Debit date for order=" + commandNumber + " has been stored " + "in the rsgv26 DB table");
  }

  /**
   * Set cancellation of the authorization date to 0 in the selected rsgv26
   * entry
   * 
   * @param subsidiary
   *          selector of the rsgv26 entry
   * @param commandNumber
   *          selector of the rsgv26 entry
   */
  public void cancelAuthorization(String subsidiary, String commandNumber) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiary, commandNumber);
    rsgv26.authdt = null;
    rsgv26.authhe = null;
    rsgv26Dao.update(rsgv26);
  }

  /**
   * Set P2000 loading date to current date in the selected rsgv26 entry and
   * add P2000 command information given in parameters
   * 
   * @param subsidiary
   *          selector of the rsgv26 entry
   * @param commandNumber
   *          selector of the rsgv26 entry
   * @param p2000Subsidiary
   *          P2000 utiente code
   * @param p2000CommandNumber
   *          P2000 cdenump code
   */
  public void setLoaded(String subsidiary, String commandNumber, String p2000Subsidiary, String p2000CommandNumber) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiary, commandNumber);
    rsgv26.p2loaddt = new Date();
    rsgv26.p2loadhe = new Date();
    rsgv26.utient = p2000Subsidiary;
    rsgv26.cdenump = p2000CommandNumber;
    rsgv26.p2valdt = null;
    rsgv26.p2valhe = null;
    // rsgv26.debdt = null;
    // rsgv26.debhe = null;
    rsgv26.confmdt = null;
    rsgv26.confmhe = null;
    rsgv26.p2susdt = null;
    rsgv26.p2sushe = null;
    rsgv26.susmdt = null;
    rsgv26.susmhe = null;
    rsgv26.p2abddt = null;
    rsgv26.p2abdhe = null;
    rsgv26Dao.update(rsgv26);
  }

  /**
   * Set P2000 abandoned date to current date in the selected rsgv26 entry and
   * p2abdre to current user.
   * 
   * @param subsidiaryCode
   *          selector of the rsgv26 entry
   * @param orderNr
   *          selector of the rsgv26 entry
   * @param user
   *          user that update the field
   */
  public void setAbanbonned(String subsidiaryCode, String orderNr, String user) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiaryCode, orderNr);
    rsgv26.p2susdt = null;
    rsgv26.p2sushe = null;
    rsgv26.p2susre = "";
    rsgv26.p2abddt = new Date();
    rsgv26.p2abdhe = new Date();
    rsgv26.p2abdre = user;
    rsgv26Dao.update(rsgv26);
  }

  /**
   * Reset suspension notification date in the selected rsgv26 entry
   * 
   * @param subsidiaryCode
   *          selector of the rsgv26 entry
   * @param orderNr
   *          selector of the rsgv26 entry
   */
  public void resetClientSuspensionNotification(String subsidiaryCode, String orderNr) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiaryCode, orderNr);
    rsgv26.susmdt = null;
    rsgv26.susmhe = null;
    rsgv26Dao.update(rsgv26);
  }

  /**
   * Set suspension notification date to current date in the selected rsgv26
   * entry
   * 
   * @param subsidiaryCode
   *          selector of the rsgv26 entry
   * @param orderNr
   *          selector of the rsgv26 entry
   */
  public void setClientSuspensionNotification(String subsidiaryCode, String orderNr) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiaryCode, orderNr);
    rsgv26.susmdt = new Date();
    rsgv26.susmhe = new Date();
    rsgv26Dao.update(rsgv26);
    log.info("Suspend date for order=" + orderNr + " has been stored " + "in the rsgv26 DB table");
  }

  /**
   * Set cancellation date to current date in the selected rsgv26 entry
   * 
   * @param subsidiary
   *          selector of the rsgv26 entry
   * @param commandNumber
   *          selector of the rsgv26 entry
   */
  public void setCancelled(String subsidiary, String commandNumber, String user) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiary, commandNumber);
    rsgv26.p2abddt = new Date();
    rsgv26.p2abdhe = new Date();
    rsgv26.p2abdre = user;
    rsgv26Dao.update(rsgv26);
    log.info("Cancel date for order=" + commandNumber + " has been saved in the rsgv26 DB table");
  }

  public void setCancellationNotified(String subsidiary, String commandNumber) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiary, commandNumber);
    Date now = new Date();
    rsgv26.abdmdt = now;
    rsgv26.abdmhe = now;
    rsgv26Dao.update(rsgv26);
    log.info("Cancellation Notification date for order=" + subsidiary + "-" + commandNumber
        + " has been saved in the rsgv26 DB table");
  }

  /**
   * Set confirmation-to-client date to current date in the selected rsgv26
   * entry
   * 
   * @param subsidiary
   * @param commandNumber
   */
  public void setConfirmationNotified(String subsidiary, String commandNumber) {
    // update set CONFMDT and CONFMHE of RSGV26 to current Date/Hour
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiary, commandNumber);
    rsgv26.confmdt = new Date();
    rsgv26.confmhe = new Date();
    rsgv26Dao.update(rsgv26);
    log.info("Notification of the confirmation date for order=" + commandNumber
        + " has been saved in the rsgv26 DB table");
  }

  /**
   * Set feedback-to-client date to current date in the selected rsgv26 entry
   * 
   * @param subsidiary
   * @param commandNumber
   */
  public void setFeedBackIssueDateNotified(String subsidiary, String commandNumber) {
    // update set OKPARUDT and OKPARUHE of RSGV26 to current Date/Hour
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiary, commandNumber);
    Date date = new Date();
    rsgv26.okparudt = date;
    rsgv26.okparuhe = date;
    rsgv26Dao.update(rsgv26);
    log.info("Notification of the feeddBackIssueDate date for order=" + commandNumber
        + " has been saved in the rsgv26 DB table");
  }

  @SuppressWarnings("deprecation")
  public void setFeedBackIssueDateDeleted(String subsidiary, String commandNumber) {
    // update set OKPARUDT and OKPARUHE of RSGV26 to current Date/Hour
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiary, commandNumber);
    Date date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    rsgv26.okparudt = date;
    rsgv26.okparuhe = date;
    rsgv26Dao.update(rsgv26);
    log.info("Notification of the feeddBackIssueDate date for order=" + commandNumber
        + " has been saved in the rsgv26 DB table");
  }

  /**
   * Retrieve States of a payment
   * 
   * @param susbidiary
   *          subsidiary entity
   * @param orderNr
   *          Command number
   * @return current state of the associated payment (return null is not
   *         present in db)
   */
  public EldoradoFlowStateDto retrieve(String subsidiaryCode, String orderNr) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiaryCode, orderNr);
    EldoradoFlowStateDto result = null;
    if (rsgv26 != null) {
      result = EldoradoFlowStateMapping.convert(rsgv26);
    }
    return result;
  }

  /**
   * Retrieve full list of payment with all their states
   * 
   * @return list of Payment
   */
  public List<EldoradoFlowStateDto> findAll() {
    List<EldoradoFlowStateDto> result = new ArrayList<EldoradoFlowStateDto>();
    List<Rsgv26> rsgv26s = rsgv26Dao.findAll();
    for (Rsgv26 rsgv26 : rsgv26s) {
      result.add(EldoradoFlowStateMapping.convert(rsgv26));
    }
    return result;
  }

  /**
   * Check if the provided order has already been handled, i.e., it's in either
   * a validated, suspended or canceled state.
   * 
   * @param paymentStateDto
   *          The order's state.
   * @return true if the order has already been handled.
   */
  public static boolean hasOrderBeenHandled(EldoradoFlowStateDto paymentStateDto) {
    return isValidated(paymentStateDto) || isSuspended(paymentStateDto) || isCancelled(paymentStateDto);
  }

  @SuppressWarnings("deprecation")
  public static boolean shouldRemindCapToProcessThisOrder(EldoradoFlowStateDto paymentStateDto) {
    assert (!hasOrderBeenHandled(paymentStateDto));

    Date awaitingOrderDate = paymentStateDto.getLoadingDate();
    assert (awaitingOrderDate != null);

    Calendar cal = Calendar.getInstance();
    Date now = cal.getTime();

    long delta = now.getTime() - awaitingOrderDate.getTime();

    // if day of the command is Friday/Saturday/Sunday,
    // then we should extend the cap processing period
    if (awaitingOrderDate.getDay() == Calendar.FRIDAY) {
      // skip the weekend. Therefore, add 2 (Saturday + Sunday) to the
      // ORDER_PROCESSING_DELAY
      return delta >= MILLISECONDS_IN_DAY * (ORDER_PROCESSING_DELAY + 2);
    }
    else if (awaitingOrderDate.getDay() == Calendar.SATURDAY) {
      Date temp = (Date) awaitingOrderDate.clone();
      long additionalTimeToEndOfDay = 0;
      if (temp.getHours() < 18) {
        temp.setHours(18);
        additionalTimeToEndOfDay = temp.getTime() - awaitingOrderDate.getTime();
      }
      // order should be processed by MONDAY @ 18:00
      return delta >= ((MILLISECONDS_IN_DAY * (ORDER_PROCESSING_DELAY + 1)) + additionalTimeToEndOfDay);
    }
    else if (awaitingOrderDate.getDay() == Calendar.SUNDAY) {
      Date temp = (Date) awaitingOrderDate.clone();
      long additionalTimeToEndOfDay = 0;

      if (temp.getHours() < 18) {
        temp.setHours(18);
        additionalTimeToEndOfDay = temp.getTime() - awaitingOrderDate.getTime();
      }

      // order should be processed by MONDAY @ 18:00
      return delta >= ((MILLISECONDS_IN_DAY * (ORDER_PROCESSING_DELAY)) + additionalTimeToEndOfDay);
    }
    else {
      // It is OK not to consider holidays (discussed with blaise).
      return delta >= MILLISECONDS_IN_DAY * ORDER_PROCESSING_DELAY;
    }
  }

  public boolean hasOrderBeenCancelledByUser(String subsidiaryCode, String orderNr) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiaryCode, orderNr);
    return rsgv26.p2abdre.equals("WWL");
  }

  public boolean hasOrderBeenCancelledByBatch(String subsidiaryCode, String orderNr, String p2abdreBatchName) {
    Rsgv26 rsgv26 = rsgv26Dao.retrieve(subsidiaryCode, orderNr);
    return rsgv26.p2abdre.equals(p2abdreBatchName);
  }

}
