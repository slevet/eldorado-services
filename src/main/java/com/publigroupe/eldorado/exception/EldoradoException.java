package com.publigroupe.eldorado.exception;

/**
 * This class represents the Exception for the Order services. 
 * 
 * @author Barman Dominique
 */
public class EldoradoException extends RuntimeException
{
	private static final long serialVersionUID = 1567863123503445409L;

	/**
	 * Default constructor. 
	 */
	public EldoradoException()
	{
		super();
	}

	/**
	 * Constructor with a message string.
	 * 
	 * @param message the message string
	 */
	public EldoradoException(String message)
	{
		super(message);
	}
	
	/**
	 * Constructor with a cause.
	 * 
	 * @param cause the cause of Exception
	 */
	public EldoradoException(Throwable cause)
	{
		super(cause);
	}
	
	/**
	 * Constructor with a cause and a message string.
	 * 
	 * @param message the message String
	 * @param cause the cause of Exception
	 */
	public EldoradoException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
