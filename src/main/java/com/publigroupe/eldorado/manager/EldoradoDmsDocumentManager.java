package com.publigroupe.eldorado.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import com.publigroupe.archiving.dto.DocumentDto;
import com.publigroupe.archiving.dto.DocumentMetadataDto;
import com.publigroupe.archiving.dto.RequestDownloadDto;
import com.publigroupe.archiving.dto.RequestSearchDto;
import com.publigroupe.archiving.dto.ResponseDownloadDto;
import com.publigroupe.archiving.dto.ResponseSearchDto;
import com.publigroupe.archiving.manager.ArchivingManager;
import com.publigroupe.archiving.manager.ArchivingManagerFactory;
import com.publigroupe.eldorado.dto.EldoradoInvoiceDto;
import com.publigroupe.eldorado.exception.EldoradoException;
import com.publigroupe.javaBase.property.PropertyLoader;

/**
 * Manager class to search and retrieve invoices.
 * 
 * @author Gon�alo Rodrigues - Consultant IT
 */
public class EldoradoDmsDocumentManager {

  private ArchivingManager manager;

  public EldoradoDmsDocumentManager() {
    String host = PropertyLoader.getPropertyWithContext(this, "dms.host");
    int port = Integer.parseInt(PropertyLoader.getPropertyWithContext(this, "dms.port"));
    int nbMaxSockets = Integer.parseInt(PropertyLoader.getPropertyWithContext(this, "dms.nbMaxSockets"));
    int nbMaxTries = Integer.parseInt(PropertyLoader.getPropertyWithContext(this, "dms.nbMaxTries"));

    manager = ArchivingManagerFactory.createManager(host, port, nbMaxSockets, nbMaxTries);
  }

  public EldoradoDmsDocumentManager(String host, int port, int nbMaxSockets, int nbMaxTries) {
    manager = ArchivingManagerFactory.createManager(host, port, nbMaxSockets, nbMaxTries);
  }

  public EldoradoInvoiceDto retrieveDmsInvoice(String agencyCode, String invoiceNr, Date invoiceDate) {

    List<DocumentDto> foundDocuments = searchDmsInvoices(agencyCode, invoiceNr, invoiceDate);

    if (foundDocuments != null && !foundDocuments.isEmpty()) {
      DocumentDto foundDoc = foundDocuments.get(0);

      RequestDownloadDto dowloadRequest = new RequestDownloadDto();
      dowloadRequest.setId(foundDoc.getId());
      ResponseDownloadDto downloadResponse = manager.download(dowloadRequest);
      if (downloadResponse.getCode().equals("99")) {
        throw new EldoradoException("DMS document (id=" + foundDoc.getId() + ") download failed: "
            + downloadResponse.getMessage());
      }

      EldoradoInvoiceDto invoice = new EldoradoInvoiceDto();
      invoice.setDocumentId(foundDoc.getId());
      invoice.setId(invoiceNr);
      invoice.setDate(invoiceDate);
      invoice.setDocumentName(foundDoc.getDocumentName());
      invoice.setDocumentContent(downloadResponse.getDocumentContent());
      return invoice;
    }

    return null;
  }

  public List<DocumentDto> searchDmsInvoices(String agencyCode, String invoiceNr, Date invoiceDate) {

    RequestSearchDto searchRequest = new RequestSearchDto();
    searchRequest.setRowCount(10);

    DocumentDto searchDocument = new DocumentDto();
    searchDocument.setId(StringUtils.EMPTY);
    searchDocument.setDocumentName(StringUtils.EMPTY);
    searchDocument.setFileName(StringUtils.EMPTY);
    searchDocument.setDocumentType("Invoice");

    List<DocumentMetadataDto> searchMetaList = new ArrayList<DocumentMetadataDto>();
    searchMetaList.add(createRequestMedatata("AgencyCode", agencyCode));
    searchMetaList.add(createRequestMedatata("InvoiceNR", invoiceNr));
    searchMetaList.add(createRequestMedatata("InvoiceDate", DateFormatUtils.format(invoiceDate, "yyyy-MM-dd")));

    searchDocument.setMetadatas(searchMetaList);

    searchRequest.setDocument(searchDocument);

    ResponseSearchDto searchResponse = manager.search(searchRequest);
    if (searchResponse.getCode().equals("99")) {
      throw new EldoradoException("DMS document search failed: " + searchResponse.getMessage());
    }

    return searchResponse.getDocuments();
  }

  private DocumentMetadataDto createRequestMedatata(String key, String value) {
    DocumentMetadataDto meta = new DocumentMetadataDto();
    meta.setKey(key);
    meta.setValue(value);
    return meta;
  }

}
