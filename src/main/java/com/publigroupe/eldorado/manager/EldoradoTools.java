package com.publigroupe.eldorado.manager;

import com.publigroupe.javaBase.encryption.blowfish.Blowfish;

public class EldoradoTools {
  private static final String CRYPT_KEY = "ELDORADO";

  private EldoradoTools() {
  }

  public static String encrypt(String value) {
    Blowfish blowfish = new Blowfish(CRYPT_KEY, false);
    return blowfish.encrypt(value);
  }

  public static String decrypt(String value) {
    Blowfish blowfish = new Blowfish(CRYPT_KEY, false);
    return blowfish.decrypt(value);
  }

  public static String addBPOPrefix(String prefix, String userId) {
    if (prefix == null || prefix.equals("") || prefix.startsWith("10")) {
      return userId;
    }
    if (userId.contains(prefix + "|")) {
      return userId;
    }
    return prefix + "|" + userId;
  }

  public static String removeBPOPrefix(String prefix, String userId) {
    if (prefix == null || prefix.equals("") || prefix.startsWith("10")) {
      return userId;
    }
    if (userId.contains(prefix + "|")) {
      return userId.replace(prefix + "|", "");
    }
    return userId;
  }

}
