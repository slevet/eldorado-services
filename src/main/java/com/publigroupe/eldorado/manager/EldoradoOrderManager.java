package com.publigroupe.eldorado.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.attachment.AttachmentDataSource;
import org.apache.xerces.jaxp.DocumentBuilderFactoryImpl;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.publigroupe.basis.dto.LanguageEnumDto;
import com.publigroupe.basis.manager.TranslationManager;
import com.publigroupe.eldorado.domain.EldoradoFlowState;
import com.publigroupe.eldorado.dto.EldoradoBrandDto;
import com.publigroupe.eldorado.dto.EldoradoCategoryEnumDto;
import com.publigroupe.eldorado.dto.EldoradoFlowStateDto;
import com.publigroupe.eldorado.dto.EldoradoInvoiceDto;
import com.publigroupe.eldorado.dto.EldoradoMediaDto;
import com.publigroupe.eldorado.dto.EldoradoPaymentDatatransDto;
import com.publigroupe.eldorado.dto.EldoradoPaymentModeEnumDto;
import com.publigroupe.eldorado.dto.EldoradoPdfErrorEnumDto;
import com.publigroupe.eldorado.dto.EldoradoPdfValidationDto;
import com.publigroupe.eldorado.dto.EldoradoRateDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryInvoiceDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryListDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferInsertionDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferLocalPointDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferPlacementDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferPrintPlusDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryOfferStatusDto;
import com.publigroupe.eldorado.dto.EldoradoSummarySurchargeDto;
import com.publigroupe.eldorado.service.EldoradoFlowService;
import com.publigroupe.eldorado.utils.EldoradoUtils;
import com.publigroupe.javaBase.base.DateUtil;
import com.publigroupe.javaBase.base.StringUtil;
import com.publigroupe.javaBase.languageId.LanguageId;
import com.publigroupe.javaBase.property.PropertyLoader;
import com.publigroupe.media.domain.CodeList;
import com.publigroupe.media.domain.p2000.MediaP2000;
import com.publigroupe.media.dto.CriteriaDto;
import com.publigroupe.media.dto.CriteriaKeyEnumDto;
import com.publigroupe.media.dto.MediaDto;
import com.publigroupe.media.dto.MediaIdDto;
import com.publigroupe.media.dto.MediaNameDto;
import com.publigroupe.media.dto.MediaSupportEnumDto;
import com.publigroupe.media.dto.MediaTypeEnumDto;
import com.publigroupe.media.dto.MediaUpsellingDto;
import com.publigroupe.media.dto.MediaUpsellingTreatmentTypeEnumDto;
import com.publigroupe.media.dto.OptionColorDto;
import com.publigroupe.media.dto.OptionDto;
import com.publigroupe.media.dto.OptionIdDto;
import com.publigroupe.media.dto.OptionIssueDateDto;
import com.publigroupe.media.dto.OptionOnlinePeriodDto;
import com.publigroupe.media.dto.OptionSizeDto;
import com.publigroupe.media.dto.OptionTypeEnumDto;
import com.publigroupe.media.dto.RateDiscountScaleDto;
import com.publigroupe.media.dto.RateDiscountScaleVolumeTypeEnumDto;
import com.publigroupe.media.dto.RateDto;
import com.publigroupe.media.dto.SizeMeasurementTypeEnumDto;
import com.publigroupe.media.dto.StandardClientStatusEnumDto;
import com.publigroupe.order.domain.OrderConstant;
import com.publigroupe.order.domain.asp.OrderConstantASP;
import com.publigroupe.order.domain.p2000.OrderConstantP2000;
import com.publigroupe.order.dto.OrderApplicationEnumDto;
import com.publigroupe.order.dto.OrderCriteriaDto;
import com.publigroupe.order.dto.OrderCriteriaKeyEnumDto;
import com.publigroupe.order.dto.OrderDto;
import com.publigroupe.order.dto.OrderIdDto;
import com.publigroupe.order.dto.OrderInfoDto;
import com.publigroupe.order.dto.OrderInsertionDto;
import com.publigroupe.order.dto.OrderListDto;
import com.publigroupe.order.dto.OrderMaterialDto;
import com.publigroupe.order.dto.OrderMaterialUEditorDto;
import com.publigroupe.order.dto.OrderMaterialUEditorPrintDto;
import com.publigroupe.order.dto.OrderMaterialUploadDto;
import com.publigroupe.order.dto.OrderMediaPrintPlusDto;
import com.publigroupe.order.dto.OrderMediaProductionDto;
import com.publigroupe.order.dto.OrderPlacementDto;
import com.publigroupe.order.dto.OrderPlacementGroupDto;
import com.publigroupe.order.dto.OrderQuotationMessageDto;
import com.publigroupe.order.dto.OrderQuotationMessageTypeEnumDto;
import com.publigroupe.order.dto.OrderQuotationStatusEnumDto;
import com.publigroupe.order.dto.OrderStatusEnumDto;
import com.publigroupe.order.dto.OrderValorizationCalculationDto;
import com.publigroupe.order.dto.OrderValorizationCalculationPriceTypeEnumDto;
import com.publigroupe.order.dto.OrderValorizationDto;
import com.publigroupe.order.dto.OrderValorizationPriceComponentDto;
import com.publigroupe.order.dto.OrderValorizationPriceGroupDto;
import com.publigroupe.order.dto.OrderValorizationPriceItemEnumDto;
import com.publigroupe.order.dto.OrderValorizationPriceStepEnumDto;
import com.publigroupe.order.exception.OrderNotFoundException;
import com.publigroupe.order.manager.OrderManager;
import com.publigroupe.order.manager.OrderMaterialManager;
import com.publigroupe.order.manager.OrderPlacementGroupManager;
import com.publigroupe.order.manager.OrderPlacementManager;
import com.publigroupe.partner.domain.p2000.PartnerConstantP2000;
import com.publigroupe.partner.domain.p2000.PartnerP2000;
import com.publigroupe.partner.dto.CityFindCriteriaDto;
import com.publigroupe.partner.dto.CityFindResultDto;
import com.publigroupe.partner.dto.CoordinatesPerformanceDto;
import com.publigroupe.partner.dto.CoordinatesResultDto;
import com.publigroupe.partner.dto.PartnerCountryEnumDto;
import com.publigroupe.partner.dto.PartnerDto;
import com.publigroupe.partner.dto.PartnerIdDto;
import com.publigroupe.partner.dto.PartnerLinkDto;
import com.publigroupe.partner.dto.PartnerLinkTypeEnumDto;
import com.publigroupe.partner.dto.PartnerQualifierEnumDto;
import com.publigroupe.partner.dto.PartnerTypeEnumDto;
import com.publigroupe.partner.service.LocalizationService;
import com.publigroupe.pdfcheckservice._1_0.CheckAndConvertPdfRequest;
import com.publigroupe.pdfcheckservice._1_0.CheckAndConvertPdfResponse;
import com.publigroupe.pdfcheckservice._1_0.ImageType;
import com.publigroupe.pdfcheckservice._1_0.PdfCheckService;
import com.publigroupe.pdfcheckservice._1_0.PdfThumbnailGenerationType;
import com.publigroupe.pdfcheckservice._1_0.ProfileParameterType;
import com.publigroupe.pdfcheckservice._1_0.ProfileParametersType;
import com.publigroupe.pdfcheckservice._1_0.RuleType;
import com.thoughtworks.xstream.XStream;

public class EldoradoOrderManager extends OrderManager {

  private static final Log LOGGER = LogFactory.getLog(EldoradoOrderManager.class);

  private static final String SPECIAL_CURRENT_OFFERID = "CURRENT_OFFERID";
  private static final String SPECIAL_QUERY_OFFERID = "QUERY_OFFERID";
  private static final String USER_ANONYMOUS = "ANONYMOUS";

  private LocalizationService localizationService;
  private EldoradoFlowService eldoradoFlowService;
  private PdfCheckService pdfCheckService;
  private EldoradoDmsDocumentManager dmsDocumentManager;
  private String pdfUploadBaseDir;

  private String state;
  private String currentUeditoPrintJobNumber;
  private boolean isPdfUploadChecked;
  private EldoradoPaymentModeEnumDto paymentMode;

  private String pdfCheckThumbnailQuality;
  private String pdfCheckThumbnailWidth;
  private String pdfCheckThumbnailHeight;
  private String pdfCheckProfileBw;
  private String pdfCheckProfileColor;
  private String pdfCheckProfileResize;

  private static final String PDF_SUFFIX_UPLOADED = "V01";
  private static final String PDF_SUFFIX_VALIDATED = "V01_checked";

  private EldoradoPdfValidationDto pdfValidation;

  private String eldoradoTranslationPrefix = "ELDORADO.WEB";

  public enum DisplayablePdfUpload {
    NOT_DISPLAYABLE,
    DISPLAYABLE_UNBLOCKED,
    DISPLAYABLE_BLOCKED_CHECKED,
    DISPLAYABLE_UNBLOCKED_CHECKED;
  }

  public EldoradoOrderManager() {
    super();
    this.eldoradoTranslationPrefix = PropertyLoader.getPropertyWithContext(this, "Application.translation");
    OrderIdDto orderIdDto = new OrderIdDto();
    orderIdDto.setOwnerId(OrderConstantASP.ORDER_ASP);
    orderIdDto.setId(OrderManager.generateUUID());
    getOrderDto().setId(orderIdDto);
    getOrderDto().setDescription("");

    PartnerDto advertiser = new PartnerDto();
    PartnerIdDto advertiserId = new PartnerIdDto();
    advertiserId.setOwnerId(PartnerConstantP2000.PARTNER_P2000);
    // TODO param�trable par site whitelabel
    String partnerIdAdvertiser = PropertyLoader.getPropertyWithContext(this, "Application.defaultPartner");
    if (partnerIdAdvertiser.isEmpty()) {
      partnerIdAdvertiser = "10-10012-500000-000";
    }
    advertiserId.setId(PartnerP2000.constructPartnerIdAdvertiser(
        CodeList.extractCodeValue("UTISTE", PartnerConstantP2000.PARTNER_ADVERTISER_CODELIST, partnerIdAdvertiser),
        CodeList.extractCodeValue("UTIENTC", PartnerConstantP2000.PARTNER_ADVERTISER_CODELIST, partnerIdAdvertiser),
        CodeList.extractCodeValue("CLINO", PartnerConstantP2000.PARTNER_ADVERTISER_CODELIST, partnerIdAdvertiser),
        CodeList.extractCodeValue("CLICCH", PartnerConstantP2000.PARTNER_ADVERTISER_CODELIST, partnerIdAdvertiser)));
    advertiserId.setType(PartnerTypeEnumDto.ADVERTISER);
    advertiser.setId(advertiserId);
    advertiser.setCodeList(PartnerConstantP2000.PARTNER_ADVERTISER_CODELIST);
    advertiser.setCodeValue(partnerIdAdvertiser);
    getOrderDto().setAdvertiser(advertiser);

    getOrderDto().setLanguage(LanguageId.French.getIsoCode());

    OrderInfoDto info = new OrderInfoDto();
    getOrderDto().setInfo(info);
    info.setApplication(OrderApplicationEnumDto.ELDORADO);
    initUserInfo();
    info.setCookieId("NONE");
  }

  /**
   * Check if an initialized order already exists in the session. If draft =
   * true, the method will return true only if the order is in a draft status
   * 
   * @param draft
   *          set to true to restrict the search to drafts only
   * @return true if EntryDateTime is not null, false otherwise.
   */
  public boolean isOrderInSession(boolean draft) {
    final boolean isOrderInSession = getOrderDto().getInfo().getEntryDateTime() != null;
    if (draft) {
      final OrderStatusEnumDto status = getOrderDto().getStatus();
      return isOrderInSession && status == OrderStatusEnumDto.DRAFT;
    }

    return isOrderInSession;
  }

  public void init() {
    init(true);
  }

  public void init(boolean deleteDraft) {
    super.init();
    setCurrentOfferId(null);
    getOrderDto().getInfo().setEntryDateTime(new Date());
    if (deleteDraft) {
      deleteOrderDraft(getOrderDto().getId().getId());
    }
    getOrderDto().getId().setId(OrderManager.generateUUID());
    getOrderDto().setDescription("");
    pdfValidation = null;
    isPdfUploadChecked = false;

  }

  public DisplayablePdfUpload getPdfDisplayable(String offerId) {

    if (!PropertyLoader.getPropertyWithContext(this, "PdfUpload.Enabled").equals("true"))
      return DisplayablePdfUpload.NOT_DISPLAYABLE;

    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);

    OrderMediaProductionDto orderMediaProduction = orderPlacement.retrieveMediaProduction();
    MediaSupportEnumDto support = orderPlacement.getOrderPlacementDto().getMedia().getMediaId().getMediaType().getSupport();

    boolean existOnePlacementUpload = false;
    boolean existOnePlacementUE = false;
    boolean isCurrentPlacementUpload = false;
    boolean isCurrentPlacementUE = false;

    if (support.equals(MediaSupportEnumDto.ONLINE))
      return DisplayablePdfUpload.NOT_DISPLAYABLE;
    if ("01".equals(getOrderMediaProductionTypeCode(orderMediaProduction)))
      return DisplayablePdfUpload.NOT_DISPLAYABLE;

    for (OrderPlacementGroupDto placementGroups : getOrderDto().getPlacementGroups()) {
      if (placementGroups.getPlacements() != null) {
        for (OrderPlacementDto placements : placementGroups.getPlacements()) {
          if (!placements.getMedia().getMediaId().getMediaType().getSupport().equals(MediaSupportEnumDto.ONLINE)) {
            OrderMaterialDto material = retrieveBasePrintMaterial(placements.getMedia().getMediaId().getId());
            if (material instanceof OrderMaterialUEditorPrintDto) {
              OrderMaterialUEditorPrintDto orderMaterialUEditorPrintDto = (OrderMaterialUEditorPrintDto) material;
              if (!StringUtil.isEmpty(orderMaterialUEditorPrintDto.getCmsUuid())) {
                if (placementGroups.getId().equals(orderPlacementGroup.getOrderPlacementGroupDto().getId())) {
                  isCurrentPlacementUpload = false;
                  isCurrentPlacementUE = true;
                }
                else {
                  existOnePlacementUE = true;
                }
              }
            }
            else {
              if (placementGroups.getId().equals(orderPlacementGroup.getOrderPlacementGroupDto().getId())) {
                isCurrentPlacementUpload = true;
              }
              else {
                existOnePlacementUpload = true;
              }
            }
          }
        }
      }
    }

    if (existOnePlacementUE) {
      return DisplayablePdfUpload.NOT_DISPLAYABLE;
    }
    if (existOnePlacementUpload) {
      return DisplayablePdfUpload.DISPLAYABLE_BLOCKED_CHECKED;
    }
    if (isCurrentPlacementUpload) {
      return DisplayablePdfUpload.DISPLAYABLE_UNBLOCKED_CHECKED;
    }
    if (isCurrentPlacementUE) {
      return DisplayablePdfUpload.DISPLAYABLE_UNBLOCKED;
    }
    // recherche si material Upload non attribu� existe (duplication)
    List<OrderMaterialDto> materials = this.getOrderDto().getMaterials();
    if (materials != null) {
      for (OrderMaterialDto material : materials) {
        if (material instanceof OrderMaterialUploadDto) {
          return DisplayablePdfUpload.DISPLAYABLE_UNBLOCKED_CHECKED;
        }
      }
    }

    return isPDFUpload() ? DisplayablePdfUpload.DISPLAYABLE_UNBLOCKED_CHECKED
        : DisplayablePdfUpload.DISPLAYABLE_UNBLOCKED;
  }

  public void uploadPDFMaterial(String offerId, InputStream pdfAsStream) {
    this.pdfValidation = null;
    this.pdfValidation = validateUploadMaterial(offerId, pdfAsStream);
  }

  public boolean existOneUploadMaterial(String offerId) {
    OrderMaterialDto material = retrieveBasePrintMaterial(offerId);
    if (material != null && material instanceof OrderMaterialUploadDto) {
      return true;
    }
    List<OrderMaterialDto> materials = this.getOrderDto().getMaterials();
    if (materials != null) {
      for (OrderMaterialDto materialall : materials) {
        if (materialall instanceof OrderMaterialUploadDto) {
          return true;
        }
      }
    }
    return false;
  }

  public EldoradoPdfValidationDto getLastPDFValidation() {
    if (this.pdfValidation != null) {
      return this.pdfValidation;
    }
    else {
      return searchValidateExistingUploadMaterial(getCurrentOfferId().getId());
    }
  }

  private EldoradoPdfValidationDto searchValidateExistingUploadMaterial(String offerId) {
    EldoradoPdfValidationDto pdfValidation = new EldoradoPdfValidationDto();
    pdfValidation.setValid(false);
    Map<EldoradoPdfErrorEnumDto, EldoradoPdfErrorEnumDto> errorMap = new HashMap<EldoradoPdfErrorEnumDto, EldoradoPdfErrorEnumDto>();
    String inputFilePath = pdfUploadBaseDir + File.separator;
    try {
      OrderMaterialUploadDto materialUpload = null;
      // recherche si current material = Upload
      OrderMaterialDto material = retrieveBasePrintMaterial(getCurrentOfferId().getId());
      if (material != null) {
        if (material instanceof OrderMaterialUploadDto) {
          materialUpload = (OrderMaterialUploadDto) material;
        }
      }
      // recherche si material Upload non attribu� existe (duplication)
      if (materialUpload == null) {
        List<OrderMaterialDto> materials = this.getOrderDto().getMaterials();
        if (materials != null) {
          for (OrderMaterialDto materialall : materials) {
            if (materialall instanceof OrderMaterialUploadDto) {
              materialUpload = (OrderMaterialUploadDto) materialall;
            }
          }
        }
      }
      if (materialUpload != null) {
        if (!StringUtil.isEmpty(materialUpload.getHighresUrl())) {
          URL url = new URL(materialUpload.getHighresUrl());
          URLConnection urlConn = url.openConnection();
          urlConn.setConnectTimeout(5000);
          this.pdfValidation = validateUploadMaterial(offerId, urlConn.getInputStream());
          return this.pdfValidation;
        }
        else if (!StringUtil.isEmpty(materialUpload.getFileName())) {
          File file = new File(inputFilePath + materialUpload.getFileName());
          this.pdfValidation = validateUploadMaterial(offerId, FileUtils.openInputStream(file));
          return this.pdfValidation;
        }
      }
    }
    catch (Exception e) {
      LOGGER.error("Error occured during pdf search", e);
      e.printStackTrace();
      errorMap.put(EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM, EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM);
      pdfValidation.setMessage(e.getMessage());
    }
    LOGGER.error("Problem with PDF Material Not Found");
    errorMap.put(EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM, EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM);
    pdfValidation.setMessage("Problem with PDF Material Not Found");
    if (errorMap != null) {
      for (EldoradoPdfErrorEnumDto error : errorMap.values()) {
        pdfValidation.addError(error);
      }
    }
    return pdfValidation;
  }

  /**
   * Validates the given PDF stream against for given offer ID.
   * 
   * @param offerId
   *          The offer ID to validate the PDF for.
   * @param pdfAsStream
   *          The PDF stream to validate.
   * @return The DTO containing the PDF validation information.
   */
  private CheckAndConvertPdfResponse callCheckService(String inputFilePath, String inputFilename,
      String checkedFilename, String profileName, int width, int height, InputStream pdfAsStream) throws Exception {
    CheckAndConvertPdfResponse response = null;

    File file = new File(inputFilePath + inputFilename);
    if (!pdfCheckProfileResize.equals(profileName)) {
      LOGGER.error("savepdf:" + inputFilePath + inputFilename);
      FileUtils.copyInputStreamToFile(pdfAsStream, file);
    }
    CheckAndConvertPdfRequest request = new CheckAndConvertPdfRequest();
    DataHandler dh = new DataHandler(new AttachmentDataSource("application/pdf", new FileInputStream(file)));
    request.setPdfStream(dh);
    request.setFileName(checkedFilename);
    request.setProfileName(profileName);
    if (pdfCheckProfileResize.equals(profileName)) {
      ProfileParametersType parmList = new ProfileParametersType();
      request.setProfileParameters(parmList);
      ProfileParameterType typeWidth = new ProfileParameterType();
      typeWidth.setName("mediabox_width");
      typeWidth.setValue(String.valueOf(width));
      request.getProfileParameters().getProfileParameter().add(typeWidth);
      ProfileParameterType typeHeight = new ProfileParameterType();
      typeHeight.setName("mediabox_height");
      typeHeight.setValue(String.valueOf(height));
      request.getProfileParameters().getProfileParameter().add(typeHeight);
      LOGGER.info("ResizePDF New size : width:" + String.valueOf(width) + "/height:" + String.valueOf(height));
    }
    PdfThumbnailGenerationType thumbnailGeneration = new PdfThumbnailGenerationType();
    thumbnailGeneration.setCompressionQuality(Integer.valueOf(pdfCheckThumbnailQuality));
    thumbnailGeneration.setType(ImageType.JPEG);
    thumbnailGeneration.setWidth(Integer.valueOf(pdfCheckThumbnailWidth));
    thumbnailGeneration.setHeight(Integer.valueOf(pdfCheckThumbnailHeight));
    request.setThumbnailGeneration(thumbnailGeneration);
    response = pdfCheckService.checkAndConvertPdf(request);

    return response;
  }

  public EldoradoPdfValidationDto validateUploadMaterial(String offerId, InputStream pdfAsStream) {

    EldoradoPdfValidationDto pdfVal = new EldoradoPdfValidationDto();
    pdfVal.setValid(false);
    CheckAndConvertPdfResponse response = null;
    InputStream pdfStream = pdfAsStream;

    String uuid = OrderManager.generateUUID();
    String inputFilename = generatePdfFilename(uuid, offerId, PDF_SUFFIX_UPLOADED);
    String checkedFilename = generatePdfFilename(uuid, offerId, PDF_SUFFIX_VALIDATED);
    String inputFilePath = pdfUploadBaseDir + File.separator;
    String profileName = getCallasProfileNameForOffer(offerId);

    Map<EldoradoPdfErrorEnumDto, EldoradoPdfErrorEnumDto> errorMap = new HashMap<EldoradoPdfErrorEnumDto, EldoradoPdfErrorEnumDto>();

    try {
      response = callCheckService(inputFilePath, inputFilename, checkedFilename, profileName, 0, 0, pdfAsStream);
    }
    catch (Exception e) {
      LOGGER.error("Error occured during pdf check", e);
      e.printStackTrace();
      errorMap.put(EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM, EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM);
      pdfVal.setMessage(e.getMessage());
    }

    if (response != null) {
      pdfVal.setValid(response.getPdfCheckInfo().isValid());
      pdfVal.setInColor(response.getPdfInfo().getNbOfColors() > 1);
      pdfVal.setPdfWidth(EldoradoUtils.roundUp(response.getPdfInfo().getMediaBoxSize().getWidthMm().doubleValue()));
      pdfVal.setPdfHeight(EldoradoUtils.roundUp(response.getPdfInfo().getMediaBoxSize().getHeightMm().doubleValue()));
      LOGGER.info("CheckPDF size : width:" + pdfVal.getPdfWidth() + "/height:" + pdfVal.getPdfHeight());
      OptionSizeDto optionSizeDto = findPrintSizes(offerId, pdfVal.getPdfWidth(), pdfVal.getPdfHeight());
      pdfVal.setOptionSize(optionSizeDto);
      if (optionSizeDto != null) {
        if (optionSizeDto.getMeasurementType().equals(SizeMeasurementTypeEnumDto.MILLIMETERS)) {
          optionSizeDto.setHeight(EldoradoUtils.roundUp(response.getPdfInfo().getMediaBoxSize().getHeightMm().doubleValue())
              + optionSizeDto.getGutterHeight());
        }
        fillCallasErrors(profileName, errorMap, response, pdfVal);
        pdfVal.setMessage(response.getPdfCheckInfo().getValidationMessage());

        int adminHeight = optionSizeDto.getHeightMaximumInMm();
        if (optionSizeDto.getMeasurementType().equals(SizeMeasurementTypeEnumDto.MILLIMETERS)) {
          adminHeight = optionSizeDto.getHeight() - optionSizeDto.getGutterHeight();
        }
        double adminWidthMin = ((double) optionSizeDto.getWidthMinimumInMm()) / 100 * 95;
        double adminWidthMax = ((double) optionSizeDto.getWidthMinimumInMm()) / 100 * 105;
        double adminHeightMin = ((double) adminHeight) / 100 * 95;
        double adminHeightMax = ((double) adminHeight) / 100 * 105;
        if (pdfVal.isValid()
            && (pdfVal.getPdfWidth() > adminWidthMax || pdfVal.getPdfWidth() < adminWidthMin
                || pdfVal.getPdfHeight() > adminHeightMax || pdfVal.getPdfHeight() < adminHeightMin)) {
          CheckAndConvertPdfResponse responseResize = null;
          try {
            responseResize = callCheckService(inputFilePath, inputFilename, checkedFilename, pdfCheckProfileResize,
                optionSizeDto.getWidthMinimumInMm(), adminHeight, null);
            if (responseResize != null && responseResize.getPdfCheckInfo().isValid()) {
              if (optionSizeDto.getMeasurementType().equals(SizeMeasurementTypeEnumDto.MILLIMETERS)) {
                optionSizeDto.setHeight(EldoradoUtils.roundUp(responseResize.getPdfInfo().getMediaBoxSize().getHeightMm().doubleValue())
                    + optionSizeDto.getGutterHeight());
              }
              errorMap.put(EldoradoPdfErrorEnumDto.WARNING_RESIZED_WITH_BLANK,
                  EldoradoPdfErrorEnumDto.WARNING_RESIZED_WITH_BLANK);
              response = responseResize;
              LOGGER.info("SizeAfterResizing : width:"
                  + EldoradoUtils.roundUp(response.getPdfInfo().getMediaBoxSize().getWidthMm().doubleValue())
                  + "/height:"
                  + EldoradoUtils.roundUp(response.getPdfInfo().getMediaBoxSize().getHeightMm().doubleValue()));

            }
            else {
              LOGGER.error("No Valid Pdf after resizing");
              errorMap.put(EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM,
                  EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM);
              pdfVal.setMessage("No Valid Pdf after resizing");
            }
          }
          catch (Exception e) {
            LOGGER.error("Exception occured during pdf adjustment", e);
            e.printStackTrace();
            errorMap.put(EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM,
                EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM);
            pdfVal.setMessage(e.getMessage());
          }
        }
      }
      else {
        // OptionSize not found
        pdfVal.setValid(false);
        errorMap.put(EldoradoPdfErrorEnumDto.BLOCKER_WRONG_SIZE, EldoradoPdfErrorEnumDto.BLOCKER_WRONG_SIZE);
        pdfVal.setMessage(response.getPdfCheckInfo().getValidationMessage());

      }
      // Save files
      try {
        if (response.getConvertedPdfStream() != null) {
          pdfStream = response.getConvertedPdfStream().getInputStream();
          File checkedPdf = new File(inputFilePath + checkedFilename);
          FileUtils.copyInputStreamToFile(pdfStream, checkedPdf);
        }
        else {
          copyFile(inputFilePath + inputFilename, inputFilePath + checkedFilename, false);
        }
        pdfVal.setFilename(checkedFilename);
        if (response.getPdfThumbnail().getThumbnailStream() != null) {
          File thumbnail = new File(inputFilePath + response.getPdfThumbnail().getFileName());
          FileUtils.copyInputStreamToFile(response.getPdfThumbnail().getThumbnailStream().getInputStream(), thumbnail);
          pdfVal.setThumbnailPath(inputFilePath);
          pdfVal.setThumbnailName(response.getPdfThumbnail().getFileName());
        }
      }
      catch (IOException e) {
        String message = "Failed to copy stream to: '" + pdfUploadBaseDir + "'";
        LOGGER.error(message, e);
        e.printStackTrace();
        errorMap.put(EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM,
            EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM);
        pdfVal.setMessage(message);
      }
    }
    else {
      // Response == null
      errorMap.put(EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM, EldoradoPdfErrorEnumDto.BLOCKER_TECHNICAL_PROBLEM);
    }

    if (errorMap != null) {
      for (EldoradoPdfErrorEnumDto error : errorMap.values()) {
        pdfVal.addError(error);
      }
    }
    return pdfVal;
  }

  private void fillCallasErrors(String profileName, Map<EldoradoPdfErrorEnumDto, EldoradoPdfErrorEnumDto> errorMap,
      CheckAndConvertPdfResponse response, EldoradoPdfValidationDto pdfVal) {
    boolean asError = false;
    if (profileName.equals(pdfCheckProfileColor) && !pdfVal.isInColor()) {
      errorMap.put(EldoradoPdfErrorEnumDto.WARNING_COLOR_BW, EldoradoPdfErrorEnumDto.WARNING_COLOR_BW);
    }
    if (!profileName.equals(pdfCheckProfileColor) && pdfVal.isInColor()) {
      errorMap.put(EldoradoPdfErrorEnumDto.WARNING_COLOR_CONVERSION_BW,
          EldoradoPdfErrorEnumDto.WARNING_COLOR_CONVERSION_BW);
    }
    if (response.getPdfCheckInfo().getHits() != null) {
      for (RuleType hit : response.getPdfCheckInfo().getHits().getHit()) {
        // We have to check the display name and not the rule id as a same
        // rule can have a different id in another profile
        if (StringUtils.containsIgnoreCase(hit.getRuleDisplayName(), "resolution")) {
          asError = true;
          errorMap.put(EldoradoPdfErrorEnumDto.ERROR_PICTURE_RESOLUTION,
              EldoradoPdfErrorEnumDto.ERROR_PICTURE_RESOLUTION);
        }
        else if (StringUtils.containsIgnoreCase(hit.getRuleDisplayName(), "pages")) {
          asError = true;
          errorMap.put(EldoradoPdfErrorEnumDto.BLOCKER_MORE_THAN_ONE_PAGE,
              EldoradoPdfErrorEnumDto.BLOCKER_MORE_THAN_ONE_PAGE);
        }
        else if (StringUtils.containsIgnoreCase(hit.getRuleDisplayName(), "font")) {
          asError = true;
          errorMap.put(EldoradoPdfErrorEnumDto.ERROR_FONT_EMBEDEDDED, EldoradoPdfErrorEnumDto.ERROR_FONT_EMBEDEDDED);
        }
        else if (!profileName.equals(pdfCheckProfileColor)
            && (StringUtils.containsIgnoreCase(hit.getRuleDisplayName(), "color")
                || StringUtils.containsIgnoreCase(hit.getRuleDisplayName(), "cyan")
                || StringUtils.containsIgnoreCase(hit.getRuleDisplayName(), "magenta") || StringUtils.containsIgnoreCase(
                hit.getRuleDisplayName(), "yellow"))) {
          errorMap.put(EldoradoPdfErrorEnumDto.WARNING_COLOR_CONVERSION_BW,
              EldoradoPdfErrorEnumDto.WARNING_COLOR_CONVERSION_BW);
        }
        else {
          asError = true;
          errorMap.put(EldoradoPdfErrorEnumDto.BLOCKER_DOCUMENT_PROBLEM,
              EldoradoPdfErrorEnumDto.BLOCKER_DOCUMENT_PROBLEM);
        }
      }
    }
    pdfVal.setValid(!asError);
  }

  // Test only
  public void getGrandeurMaterial(File file) {

    CheckAndConvertPdfResponse response = null;

    String profileName = pdfCheckProfileBw;

    try {
      CheckAndConvertPdfRequest request = new CheckAndConvertPdfRequest();
      DataHandler dh = new DataHandler(new AttachmentDataSource("application/pdf", FileUtils.openInputStream(file)));
      request.setPdfStream(dh);
      request.setFileName(file.getName());
      request.setProfileName(profileName);
      PdfThumbnailGenerationType thumbnailGeneration = new PdfThumbnailGenerationType();
      thumbnailGeneration.setCompressionQuality(Integer.valueOf(pdfCheckThumbnailQuality));
      thumbnailGeneration.setType(ImageType.JPEG);
      thumbnailGeneration.setWidth(Integer.valueOf(pdfCheckThumbnailWidth));
      thumbnailGeneration.setHeight(Integer.valueOf(pdfCheckThumbnailHeight));
      request.setThumbnailGeneration(thumbnailGeneration);
      response = pdfCheckService.checkAndConvertPdf(request);
    }
    catch (Exception e) {
      e.printStackTrace();
      LOGGER.error("TEST Error occured during pdf check", e);
    }

    if (response != null) {
      System.out.println("--> File:" + file.getName() + " size : "
          + EldoradoUtils.roundUp(response.getPdfInfo().getMediaBoxSize().getWidthMm().doubleValue()) + " X "
          + EldoradoUtils.roundUp(response.getPdfInfo().getMediaBoxSize().getHeightMm().doubleValue()));
    }
  }

  /**
   * Gets the Callas profile for the given offer ID (B/W or color).
   * 
   * @param offerId
   *          The offer ID.
   * @return The Callas profile ID.
   */
  private String getCallasProfileNameForOffer(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    OptionColorDto colorDto = (OptionColorDto) orderPlacement.retrieveOption(OptionTypeEnumDto.COLOR);
    if (colorDto == null || colorDto.getOptionId().getId().contains("00")) {
      return pdfCheckProfileBw;
    }
    else {
      return pdfCheckProfileColor;
    }
  }

  /**
   * Generates the PDF filename from the given offer ID.
   * 
   * @param offerId
   *          The offer ID to generate the filename from.
   * @param suffix
   *          filename suffix.
   * @return The generated filename.
   */
  private String generatePdfFilename(String prefix, String offerId, String suffix) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    String fileName;
    // if (getOrderDto().getCodeValue() != null) {
    // fileName = getOrderDto().getCodeValue();
    // }
    // else {
    // fileName = getOrderDto().getId().getId();
    // }
    if (prefix.equals("")) {
      fileName = OrderManager.generateUUID();
    }
    else {
      fileName = prefix;
    }
    fileName += "-";
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    if (orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel2() != null
        && orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel2().getMediaId() != null) {
      fileName += orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel2().getName(getLanguage()).getName();
    }
    else if (orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel1() != null
        && orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel1().getMediaId() != null) {
      fileName += orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel1().getName(getLanguage()).getName();
    }
    else {
      fileName += orderPlacementGroup.getOrderPlacementGroupDto().getMedia().getName(getLanguage()).getName();

    }

    fileName = org.apache.commons.lang3.StringUtils.stripAccents(fileName);
    fileName = StringUtils.replace(fileName, " ", "_");
    fileName = StringUtils.replace(fileName, "/", "_");
    return fileName + "_" + suffix + ".pdf";
  }

  public void setLocalizationService(LocalizationService localizationService) {
    this.localizationService = localizationService;
  }

  public void setEldoradoFlowService(EldoradoFlowService eldoradoFlowService) {
    this.eldoradoFlowService = eldoradoFlowService;
  }

  public String getEldoradoHeading() {
    if (getOrderDto().getSpecialCodes() == null) {
      return null;
    }
    return getSpecialCode(OrderConstant.SPECIAL_CODE_ELDORADO_HEADING);
  }

  public void setEldoradoHeading(String eldoradoHeading) {
    setSpecialCode(OrderConstant.SPECIAL_CODE_ELDORADO_HEADING, eldoradoHeading);
  }

  public void setExternalSource(String externalSource) {
    getOrderDto().getInfo().setExternalSource(externalSource);
  }

  public String getClientStatus() {
    return getSpecialCode(OrderConstant.SPECIAL_CODE_CLIENT_STATUS);
  }

  public void setClientStatus(String clientStatus) {
    setSpecialCode(OrderConstant.SPECIAL_CODE_CLIENT_STATUS, clientStatus);
  }

  public String getCountryCode() {
    return getSpecialCode(OrderConstant.SPECIAL_CODE_COUNTRY_CODE);
  }

  public void setCountryCode(String countryCode) {
    setSpecialCode(OrderConstant.SPECIAL_CODE_COUNTRY_CODE, countryCode);
  }

  public String getZipCode() {
    return getSpecialCode(OrderConstant.SPECIAL_CODE_POSTAL_CODE);
  }

  public void setZipCode(String zipCode) {
    setSpecialCode(OrderConstant.SPECIAL_CODE_POSTAL_CODE, zipCode);
  }

  public String getCity() {
    return getSpecialCode(OrderConstant.SPECIAL_CODE_CITY);
  }

  public void setCity(String city) {
    setSpecialCode(OrderConstant.SPECIAL_CODE_CITY, city);
  }

  public List<String> getOfferIds() {
    List<String> offerIds = new ArrayList<String>();

    List<OrderPlacementGroupManager> orderPlacementGroups = findPlacementGroups();
    for (OrderPlacementGroupManager orderPlacementGroup : orderPlacementGroups) {
      offerIds.add(orderPlacementGroup.getOrderPlacementGroupDto().getMedia().getMediaId().getId());
    }

    return offerIds;
  }

  public MediaIdDto getCurrentOfferId() {
    String offerId = getSpecialCode(SPECIAL_CURRENT_OFFERID);
    if (offerId == null) {
      return null;
    }
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    if (orderPlacementGroup.getOrderPlacementGroupDto() == null) {
      setCurrentOfferId(null);
      return null;
    }
    return orderPlacementGroup.getOrderPlacementGroupDto().getMedia().getMediaId();
  }

  public void setCurrentOfferId(String offerId) {
    setSpecialCode(SPECIAL_CURRENT_OFFERID, offerId);
  }

  public String getQueryOfferId() {
    return getSpecialCode(SPECIAL_QUERY_OFFERID);
  }

  public void setQueryOfferId(String offerId) {
    setSpecialCode(SPECIAL_QUERY_OFFERID, offerId);
  }

  public String retrieveAdType() {
    String txttyp = null;

    List<OrderPlacementGroupManager> orderPlacementGroups = findPlacementGroups();
    for (OrderPlacementGroupManager orderPlacementGroup : orderPlacementGroups) {
      MediaDto mediaDto = orderPlacementGroup.getOrderPlacementGroupDto().getMedia();
      if (mediaDto.getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
        List<OrderPlacementManager> orderPlacements = orderPlacementGroup.findPlacements();
        for (OrderPlacementManager orderPlacement : orderPlacements) {
          OrderMediaProductionDto orderMediaProductionDto = orderPlacement.retrieveMediaProduction();
          txttyp = getOrderMediaProductionTypeCode(orderMediaProductionDto);
          break;
        }
        break;
      }
    }

    return txttyp;
  }

  private String getOrderMediaProductionTypeCode(OrderMediaProductionDto orderMediaProductionDto) {
    return CodeList.extractCodeValue("TXTTYP", orderMediaProductionDto.getCodeList(),
        orderMediaProductionDto.getCodeValue());
  }

  public List<CityFindResultDto> findCities(String city, int limit) {
    CityFindCriteriaDto criteria = new CityFindCriteriaDto();
    criteria.setCountry("CH");
    criteria.setName(city);
    criteria.setLimit(limit);
    return localizationService.findCitiesMedia(criteria);
  }

  public int retrieveBestRange(CoordinatesResultDto zipCodeCoordinates, int menage) {
    List<CoordinatesPerformanceDto> coordinates = new ArrayList<CoordinatesPerformanceDto>();
    CoordinatesPerformanceDto coordinate = new CoordinatesPerformanceDto();
    coordinate.setRange(10);
    coordinates.add(coordinate);
    coordinate = new CoordinatesPerformanceDto();
    coordinate.setRange(20);
    coordinates.add(coordinate);
    coordinate = new CoordinatesPerformanceDto();
    coordinate.setRange(50);
    coordinates.add(coordinate);
    CoordinatesPerformanceDto performance = localizationService.findPerformance(zipCodeCoordinates, coordinates, menage);
    return performance.getRange();
  }

  public List<EldoradoBrandDto> findOffersByEditor(String editor) {
    List<CriteriaDto> criterias = fillPrintStandardCriteria(null);
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_EDITOR, editor));
    List<EldoradoBrandDto> printOffers = fillBrandTree(criterias, false, false);

    criterias = fillOnlineStandardCriteria();
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_EDITOR, editor));
    List<EldoradoBrandDto> onlineOffers = fillBrandTree(criterias, false, false);

    // Mixed the print offers and the online offers
    for (EldoradoBrandDto brandPrint : printOffers) {
      for (EldoradoBrandDto brandOnline : onlineOffers) {
        if (brandOnline.getBrand().getMediaId().getId().equals(brandPrint.getBrand().getMediaId().getId())) {
          for (EldoradoMediaDto mediaOnline : brandOnline.getMedias()) {
            brandPrint.getMedias().add(mediaOnline);
          }
        }
      }
    }
    for (EldoradoBrandDto brandOnline : onlineOffers) {
      boolean isBrandPrintFound = false;
      for (EldoradoBrandDto brandPrint : printOffers) {
        if (brandPrint.getBrand().getMediaId().getId().equals(brandOnline.getBrand().getMediaId().getId())) {
          isBrandPrintFound = true;
          break;
        }
      }
      if (!isBrandPrintFound) {
        printOffers.add(brandOnline);
      }
    }
    return printOffers;
  }

  public List<EldoradoBrandDto> findPrintsByGeoloc(double latitudeMin, double latitudeMax, double longitudeMin,
      double longitudeMax) {
    return findPrintsByGeoloc(latitudeMin, latitudeMax, longitudeMin, longitudeMax, 10);
  }

  public List<EldoradoBrandDto> findPrintsByGeoloc(double latitudeMin, double latitudeMax, double longitudeMin,
      double longitudeMax, int percentCirculation) {
    List<CriteriaDto> criterias = fillPrintStandardCriteria(null);
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_GEOLOCALISATION_LATITUDE_MIN,
        Double.valueOf(latitudeMin).toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_GEOLOCALISATION_LATITUDE_MAX,
        Double.valueOf(latitudeMax).toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_GEOLOCALISATION_LONGITUDE_MIN,
        Double.valueOf(longitudeMin).toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_GEOLOCALISATION_LONGITUDE_MAX,
        Double.valueOf(longitudeMax).toString()));
    if (percentCirculation != 0) {
      criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_GEOLOCALISATION_PERCENT_CIRCULATION, Integer.valueOf(
          percentCirculation).toString()));
    }
    return fillBrandTree(criterias, true, true);
  }

  public List<EldoradoBrandDto> findPrintsByName(String name) {
    List<CriteriaDto> criterias = fillPrintStandardCriteria(null);
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_NAME, name));
    return fillBrandTree(criterias, true, true);
  }

  private List<CriteriaDto> fillPrintStandardCriteria(OrderPlacementManager orderPlacement) {
    List<CriteriaDto> criterias = new ArrayList<CriteriaDto>();
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.MEDIA_SUPPORT, MediaSupportEnumDto.PRINT.toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.MEDIA_TYPE, MediaTypeEnumDto.ELDORADO.toString()));
    Date referenceDate = getReferenceDate();
    if (orderPlacement != null) {
      if (orderPlacement.getOrderPlacementDto().getMedia().getValidityFrom().after(referenceDate)) {
        referenceDate = orderPlacement.getOrderPlacementDto().getMedia().getValidityFrom();
      }
    }
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.VALIDITY, new DateUtil("yyyyMMdd", referenceDate).toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.STANDARD_HEADING_ELDORADO, getEldoradoHeading()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.STANDARD_CLIENT_STATUS, StandardClientStatusEnumDto.getByName(
        getClientStatus()).toString()));
    if (getZipCode() != null) {
      criterias.add(new CriteriaDto(CriteriaKeyEnumDto.ADVERTISER_POSTAL_CODE, getZipCode()));
    }
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.PLACEMENT_REMOVE_DUPLICATE_SIZE, "true"));
    return criterias;
  }

  private List<EldoradoBrandDto> fillBrandTree(List<CriteriaDto> criterias, boolean addOnline, boolean addSponsored) {
    List<EldoradoBrandDto> brands = new ArrayList<EldoradoBrandDto>();

    String orderId = null;
    if (getCurrentOfferId() != null) {
      orderId = getCurrentOfferId().getId();
    }

    List<MediaDto> brandDtos = getMediaService().findMedias(criterias);
    for (MediaDto media : brandDtos) {
      EldoradoBrandDto brand = new EldoradoBrandDto();
      brands.add(brand);
      brand.setBrand(media);
    }
    if (addSponsored) {
      criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_SPONSORED, "true"));
      List<MediaDto> sponsoredBrandDtos = getMediaService().findMedias(criterias);
      int sponsoredSequence = 1;
      for (MediaDto media : sponsoredBrandDtos) {
        boolean found = false;
        for (EldoradoBrandDto brand : brands) {
          if (brand.getBrand().getMediaId().getExternalReference().equals(media.getMediaId().getExternalReference())) {
            brand.setSponsored(true);
            brand.setSponsoredSequence(sponsoredSequence);
            found = true;
            break;
          }
        }
        if (!found) {
          EldoradoBrandDto brand = new EldoradoBrandDto();
          brands.add(0, brand);
          brand.setBrand(media);
          brand.setSponsored(true);
          brand.setSponsoredSequence(sponsoredSequence);
        }
        sponsoredSequence++;
      }
    }
    for (EldoradoBrandDto brand : brands) {
      List<EldoradoMediaDto> medias = new ArrayList<EldoradoMediaDto>();
      brand.setMedias(medias);
      List<MediaDto> mediaDtos = getMediaService().findChilds(brand.getBrand().getMediaId(), criterias);
      for (MediaDto mediaDto : mediaDtos) {
        EldoradoMediaDto media = new EldoradoMediaDto();
        media.setMedia(mediaDto);
        List<MediaDto> offers = getMediaService().findChilds(mediaDto.getMediaId(), criterias);
        offers = excludeNoValidOffers(orderId, offers);
        if (!offers.isEmpty()) {
          media.setOffers(offers);
          medias.add(media);
        }
      }
      if (addOnline) {
        List<CriteriaDto> onlineCriterias = fillOnlineStandardCriteria();
        List<MediaDto> onlineMediaDtos = getMediaService().findChilds(brand.getBrand().getMediaId(), onlineCriterias);
        for (MediaDto mediaDto : onlineMediaDtos) {
          EldoradoMediaDto media = new EldoradoMediaDto();
          media.setMedia(mediaDto);
          List<MediaDto> offers = getMediaService().findChilds(mediaDto.getMediaId(), criterias);
          offers = excludeNoValidOffers(orderId, offers);
          if (!offers.isEmpty()) {
            media.setOffers(offers);
            medias.add(media);
          }
        }
      }
    }

    // clean the tree (the offers could be empty).
    // - clean media without offer
    for (EldoradoBrandDto brandDto : brands) {
      int n = brandDto.getMedias().size();
      for (int i = 0; i < n; i++) {
        EldoradoMediaDto mediaDto = brandDto.getMedias().get(i);
        if (mediaDto.getOffers().size() == 0) {
          brandDto.getMedias().remove(i);
          i--;
          n--;
        }
      }
    }
    // - clean brand without media
    int n = brands.size();
    for (int i = 0; i < n; i++) {
      EldoradoBrandDto brandDto = brands.get(i);
      if (brandDto.getMedias().size() == 0) {
        brands.remove(i);
        i--;
        n--;
      }
    }

    // Fill for the brand/media the minimum price
    // Fill the default gif for the offer
    for (EldoradoBrandDto brandDto : brands) {
      for (EldoradoMediaDto mediaDto : brandDto.getMedias()) {
        for (MediaDto offerDto : mediaDto.getOffers()) {
          if (offerDto.getBasicPrice() != null) {
            if (mediaDto.getMedia().getBasicPrice() == null
                || mediaDto.getMedia().getBasicPrice().compareTo(offerDto.getBasicPrice()) > 0) {
              mediaDto.getMedia().setBasicPrice(offerDto.getBasicPrice());
              mediaDto.getMedia().setBasicCurrency(offerDto.getBasicCurrency());
            }
            if (brandDto.getBrand().getBasicPrice() == null
                || brandDto.getBrand().getBasicPrice().compareTo(offerDto.getBasicPrice()) > 0) {
              brandDto.getBrand().setBasicPrice(offerDto.getBasicPrice());
              brandDto.getBrand().setBasicCurrency(offerDto.getBasicCurrency());
            }
          }
          for (MediaNameDto mediaNameDto : offerDto.getNames()) {
            if (StringUtil.isEmpty(mediaNameDto.getGif())) {
              if (offerDto.getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
                String group = "";
                String eldoradoHeading = getEldoradoHeading();
                if (eldoradoHeading != null) {
                  if (eldoradoHeading.equals("20") || eldoradoHeading.equals("21") || eldoradoHeading.equals("22")) {
                    group = "job_";
                  }
                  else if (eldoradoHeading.equals("10") || eldoradoHeading.equals("11") || eldoradoHeading.equals("12")) {
                    group = "realestate_";
                  }
                  else if (eldoradoHeading.equals("30")) {
                    group = "car_";
                  }
                  else if (eldoradoHeading.equals("31")) {
                    group = "private_";
                  }
                }
                if (StringUtil.isEmpty(mediaNameDto.getSpecial()) || mediaNameDto.getSpecial().contains("00")) {
                  mediaNameDto.setGif("default_print_" + group + mediaNameDto.getPackageName() + "_"
                      + mediaNameDto.getLanguage() + ".gif");
                }
                else {
                  mediaNameDto.setGif("default_print_" + group + mediaNameDto.getPackageName() + "_"
                      + mediaNameDto.getLanguage() + "_colored" + ".gif");
                }
              }
              else if (offerDto.getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
                mediaNameDto.setGif("default_online.gif");

              }
              else {
                // ADSCREEN
              }
            }
          }
        }
      }
    }

    return brands;
  }

  private List<MediaDto> excludeNoValidOffers(String offerId, List<MediaDto> offers) {
    // clean the offers with problem (offtyp - jnlcod/jnlgrc)
    List<OrderPlacementGroupManager> orderPlacementGroups = findPlacementGroups();
    if (orderPlacementGroups.isEmpty()) {
      return offers;
    }

    String offtyp = null;
    Map<String, Boolean> offerMap = new HashMap<String, Boolean>();
    for (OrderPlacementGroupManager orderPlacementGroup : orderPlacementGroups) {
      MediaDto mediaDto = orderPlacementGroup.getOrderPlacementGroupDto().getMedia();
      if (offerId == null || !offerId.equals(mediaDto.getMediaId().getId())) {
        if (mediaDto.getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
          offtyp = mediaDto.getNames().get(0).getPackageName();
          String jnlcod = CodeList.extractCodeValue("JNLCOD", mediaDto.getCodeList(), mediaDto.getCodeValue());
          String jnlgrc = CodeList.extractCodeValue("JNLGRC", mediaDto.getCodeList(), mediaDto.getCodeValue());
          offerMap.put(jnlcod + jnlgrc, true);
        }
        else {
          String editno = CodeList.extractCodeValue("EDITNO", mediaDto.getCodeList(), mediaDto.getCodeValue());
          offerMap.put(editno, true);
        }
      }
    }

    int n = offers.size();
    for (int i = 0; i < n; i++) {
      MediaDto mediaDto = offers.get(i);
      boolean isValid = true;
      if (mediaDto.getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
        if (offtyp != null && !mediaDto.getNames().get(0).getPackageName().equals(offtyp)) {
          isValid = false;
        }
        else {
          String jnlcod = CodeList.extractCodeValue("JNLCOD", mediaDto.getCodeList(), mediaDto.getCodeValue());
          String jnlgrc = CodeList.extractCodeValue("JNLGRC", mediaDto.getCodeList(), mediaDto.getCodeValue());
          if (offerMap.containsKey(jnlcod + jnlgrc)) {
            isValid = false;
          }
        }
      }
      else {
        String editno = CodeList.extractCodeValue("EDITNO", mediaDto.getCodeList(), mediaDto.getCodeValue());
        if (offerMap.containsKey(editno)) {
          isValid = false;
        }
      }
      if (!isValid) {
        offers.remove(i);
        i--;
        n--;
      }
    }
    return offers;
  }

  private EldoradoBrandDto fillBrand(String offerId) {
    EldoradoBrandDto brand = new EldoradoBrandDto();

    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementGroupDto orderPlacementGroupDto = orderPlacementGroup.getOrderPlacementGroupDto();
    brand.setBrand(orderPlacementGroupDto.getMediaLevel1());

    List<EldoradoMediaDto> medias = new ArrayList<EldoradoMediaDto>();
    brand.setMedias(medias);

    List<CriteriaDto> criterias = fillPrintStandardCriteria(null);
    List<MediaDto> printMediaDtos = getMediaService().findChilds(brand.getBrand().getMediaId(), criterias);
    for (MediaDto mediaDto : printMediaDtos) {
      EldoradoMediaDto media = new EldoradoMediaDto();
      media.setMedia(mediaDto);
      List<MediaDto> offers = getMediaService().findChilds(mediaDto.getMediaId(), criterias);
      offers = excludeNoValidOffers(offerId, offers);
      if (!offers.isEmpty()) {
        media.setOffers(offers);
        medias.add(media);
      }
    }

    criterias = fillOnlineStandardCriteria();
    List<MediaDto> onlineMediaDtos = getMediaService().findChilds(brand.getBrand().getMediaId(), criterias);
    for (MediaDto mediaDto : onlineMediaDtos) {
      EldoradoMediaDto media = new EldoradoMediaDto();
      media.setMedia(mediaDto);
      List<MediaDto> offers = getMediaService().findChilds(mediaDto.getMediaId(), criterias);
      offers = excludeNoValidOffers(offerId, offers);
      if (!offers.isEmpty()) {
        media.setOffers(offers);
        medias.add(media);
      }
    }

    fillBrandInfo(brand);

    return brand;
  }

  public boolean placeBrand(String brandId, String mediaId, String offerId) {
    MediaIdDto brandIdDto = fillMediaIdDto(brandId);

    // Print
    List<CriteriaDto> criterias = fillPrintStandardCriteria(null);
    List<MediaDto> printMediaDtos = getMediaService().findChilds(brandIdDto, criterias);
    for (MediaDto mediaDto : printMediaDtos) {
      if (StringUtil.isEmpty(mediaId) || mediaId.equals(mediaDto.getMediaId().getId())) {
        List<MediaDto> offers = getMediaService().findChilds(mediaDto.getMediaId(), criterias);
        for (MediaDto offerDto : offers) {
          if (StringUtil.isEmpty(offerId) || offerId.equals(offerDto.getMediaId().getId())) {
            placePrint(offers.get(0).getMediaId().getId());
            setCurrentOfferId(offers.get(0).getMediaId().getId());
            return true;
          }
        }
      }
    }
    // Online
    criterias = fillOnlineStandardCriteria();
    List<MediaDto> onlineMediaDtos = getMediaService().findChilds(brandIdDto, criterias);
    for (MediaDto mediaDto : onlineMediaDtos) {
      if (StringUtil.isEmpty(mediaId) || mediaId.equals(mediaDto.getMediaId().getId())) {
        List<MediaDto> offers = getMediaService().findChilds(mediaDto.getMediaId(), criterias);
        for (MediaDto offerDto : offers) {
          if (StringUtil.isEmpty(offerId) || offerId.equals(offerDto.getMediaId().getId())) {
            placeOnline(offers.get(0).getMediaId().getId());
            setCurrentOfferId(offers.get(0).getMediaId().getId());
            return true;
          }
        }
      }
    }

    return false;
  }

  private void fillBrandInfo(EldoradoBrandDto brand) {
    // Fill for the brand/media the minimum price
    // Fill the default gif for the offer
    for (EldoradoMediaDto mediaDto : brand.getMedias()) {
      for (MediaDto offerDto : mediaDto.getOffers()) {
        if (offerDto.getBasicPrice() != null) {
          if (mediaDto.getMedia().getBasicPrice() == null
              || mediaDto.getMedia().getBasicPrice().compareTo(offerDto.getBasicPrice()) > 0) {
            mediaDto.getMedia().setBasicPrice(offerDto.getBasicPrice());
            mediaDto.getMedia().setBasicCurrency(offerDto.getBasicCurrency());
          }
          if (brand.getBrand().getBasicPrice() == null
              || brand.getBrand().getBasicPrice().compareTo(offerDto.getBasicPrice()) > 0) {
            brand.getBrand().setBasicPrice(offerDto.getBasicPrice());
            brand.getBrand().setBasicCurrency(offerDto.getBasicCurrency());
          }
        }
        for (MediaNameDto mediaNameDto : offerDto.getNames()) {
          if (StringUtil.isEmpty(mediaNameDto.getGif())) {
            if (offerDto.getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
              String group = "";
              String eldoradoHeading = getEldoradoHeading();
              if (eldoradoHeading != null) {
                if (eldoradoHeading.equals("20") || eldoradoHeading.equals("21") || eldoradoHeading.equals("22")) {
                  group = "job_";
                }
                else if (eldoradoHeading.equals("10") || eldoradoHeading.equals("11") || eldoradoHeading.equals("12")) {
                  group = "realestate_";
                }
                else if (eldoradoHeading.equals("30")) {
                  group = "car_";
                }
                else if (eldoradoHeading.equals("31")) {
                  group = "private_";
                }
              }
              if (StringUtil.isEmpty(mediaNameDto.getSpecial()) || mediaNameDto.getSpecial().contains("00")) {
                mediaNameDto.setGif("default_print_" + group + mediaNameDto.getPackageName() + "_"
                    + mediaNameDto.getLanguage() + ".gif");
              }
              else {
                mediaNameDto.setGif("default_print_" + group + mediaNameDto.getPackageName() + "_"
                    + mediaNameDto.getLanguage() + "_colored" + ".gif");
              }
            }
            else {
              mediaNameDto.setGif("default_online.gif");
            }
          }
        }
      }
    }
  }

  public void removePrintOrOnline(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    removePlacementGroup(offerIdDto);
  }

  public void placePrintUpselling(MediaUpsellingDto upsellingDto, String parentId) {
    OrderPlacementGroupManager orderPlacementGroup = addPlacementGroup(upsellingDto);
    orderPlacementGroup.getOrderPlacementGroupDto().getUpselling().setCodeList(
        OrderConstantP2000.ORDER_UPSELLING_CODELIST);
    orderPlacementGroup.getOrderPlacementGroupDto().getUpselling().setCodeValue(parentId);

    OrderMaterialUEditorDto orderMaterialDto = new OrderMaterialUEditorDto();
    OrderMaterialManager orderMaterial = addMaterial(orderMaterialDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(upsellingDto.getMedia().getMediaId());
    orderPlacement.getOrderPlacementDto().setMaterialId(orderMaterial.getOrderMaterialDto().getId());
    orderPlacement.changeMediaProductionAdType("11");
  }

  public void placePrint(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = addPlacementGroup(offerIdDto);
    OrderPlacementGroupDto orderPlacementGroupDto = orderPlacementGroup.getOrderPlacementGroupDto();

    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    orderPlacement.placeDefaultOptions();

    // place first size (if first is 1 col, select next)
    List<OptionSizeDto> sizeDtos = findPrintSizes(offerId);
    OptionSizeDto choosedOptionDto = null;
    for (OptionSizeDto optionDto : sizeDtos) {
      choosedOptionDto = optionDto;
      if (optionDto.getMeasurementType() != SizeMeasurementTypeEnumDto.MILLIMETERS || optionDto.getWidthMaximum() != 1) {
        break;
      }
    }
    orderPlacement.placeOption(choosedOptionDto);

    orderPlacement.retrieveMediaProduction();

    List<CriteriaDto> criterias = fillPrintStandardCriteria(orderPlacement);
    MediaIdDto mediaIdDto = orderPlacementGroupDto.getMedia().getParentMediaId();
    MediaDto mediaDto = getMediaService().retrieveMedia(mediaIdDto, criterias);
    orderPlacementGroupDto.setMediaLevel2(mediaDto);

    MediaIdDto brandIdDto = mediaDto.getParentMediaId();
    MediaDto brandDto = getMediaService().retrieveMedia(brandIdDto, criterias);
    orderPlacementGroupDto.setMediaLevel1(brandDto);

    OrderMaterialUEditorPrintDto orderMaterialDto = new OrderMaterialUEditorPrintDto();
    OrderMaterialManager orderMaterial = addMaterial(orderMaterialDto);
    orderPlacement.getOrderPlacementDto().setMaterialId(orderMaterial.getOrderMaterialDto().getId());
  }

  public void placeUploadMaterialSurcharge() {

    boolean doesExistsSurcharge = false;

    for (OrderPlacementGroupManager placementGroupManager : findPlacementGroups()) {
      String offerId = placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getId();
      if (placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
        OrderMaterialDto material = retrieveBasePrintMaterial(offerId);
        if (material instanceof OrderMaterialUploadDto) {
          OrderMaterialUploadDto materialUpload = (OrderMaterialUploadDto) material;
          if (materialUpload.getAdminjobType().equals("Gini")) {
            doesExistsSurcharge = true;
            break;
          }
        }
      }
    }

    boolean existsSurcharge = false;
    OrderValorizationDto forcedValorization = this.getOrderDto().getForcedValorization();
    if (forcedValorization != null) {
      List<OrderValorizationPriceGroupDto> priceGroups = forcedValorization.getPriceGroups();
      if (priceGroups != null) {
        for (OrderValorizationPriceGroupDto priceGroup : priceGroups) {
          if (priceGroup.getCodeValue().equals(
              PropertyLoader.getPropertyWithContext(this, "placeUploadMaterialSurcharge.id"))) {
            if (!doesExistsSurcharge) {
              priceGroups.remove(priceGroup);
            }
            else {
              existsSurcharge = true;
            }
            break;
          }
        }
      }
    }

    if (!existsSurcharge && doesExistsSurcharge) {
      LanguageEnumDto languageEnum = LanguageEnumDto.getByValue(getOrderDto().getLanguage());
      OrderValorizationCalculationDto valorizationCalculation = new OrderValorizationCalculationDto();
      valorizationCalculation.setPricePerUnit(new BigDecimal(PropertyLoader.getPropertyWithContext(this,
          "placeUploadMaterialSurcharge.amount")));
      placeOrderSurcharge("SUPCAT-SUPNO",
          PropertyLoader.getPropertyWithContext(this, "placeUploadMaterialSurcharge.id"), TranslationManager.getLabel(
              this.eldoradoTranslationPrefix + ".ELDORADO_AD_EDITION.AD_UPLOAD_SURCHARGE", languageEnum),
          valorizationCalculation);
    }
  }

  public EldoradoBrandDto retrievePrint(String offerId) {
    return fillBrand(offerId);
  }

  // ##EV appel� au d�but de la page donn�es de parution
  public List<OptionDto> findPrintSubHeadings(String offerId) {
    LOGGER.warn("findPrintSubHeadings:" + offerId);
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    List<OptionIdDto> existingOptionIdDtos = orderPlacement.fillExistingOptionIdDtos(OptionTypeEnumDto.SUB_HEADING);
    List<CriteriaDto> criterias = fillPrintStandardCriteria(orderPlacement);
    return orderPlacement.findOptions(OptionTypeEnumDto.SUB_HEADING, existingOptionIdDtos, criterias);
  }

  private boolean mediaExistInOrder(String upsellingMediaId) {
    for (OrderPlacementGroupDto placementGroupDto : getOrderDto().getPlacementGroups()) {
      String mediaId = CodeList.extractCodeValue("JNLCOD", placementGroupDto.getMedia().getCodeList(),
          placementGroupDto.getMedia().getCodeValue())
          + "-"
          + CodeList.extractCodeValue("JNLCODS", placementGroupDto.getMedia().getCodeList(),
              placementGroupDto.getMedia().getCodeValue());
      if (mediaId.equals(upsellingMediaId)) {
        return true;
      }
    }

    return false;
  }

  // set selected subHeading and Print+ in back-end
  // ##EV appel� juste avant le retrievePrintPlus
  public void placePrintSubHeading(String offerId, String subHeadingId) {
    LOGGER.warn("placePrintSubHeading:" + offerId + ":" + subHeadingId);
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    OptionIdDto optionIdDto = new OptionIdDto();
    optionIdDto.setOwnerId("Pub2000");
    optionIdDto.setId(subHeadingId);
    optionIdDto.setOptionType(OptionTypeEnumDto.SUB_HEADING);
    orderPlacement.placeOption(optionIdDto);

    // Print plus
    if (orderPlacement.getOrderPlacementDto().getMediaPrintPlus() != null) {
      orderPlacement.getOrderPlacementDto().setMediaPrintPlus(null);
    }
    // old version OrderMediaPrintPlusDto orderMediaPrintPlusDto =
    // orderPlacement.retrievePrintPlus();
    MediaUpsellingDto upsellingDto = orderPlacement.retrievePrintPlus(fillPrintStandardCriteria(orderPlacement));

    if (orderPlacement.getOrderPlacementDto().getMediaPrintPlus() != null
        && orderPlacement.getOrderPlacementDto().getMediaPrintPlus().isPrintPlus()) {
      if (!mediaExistInOrder(CodeList.extractCodeValue("JNLCOD", upsellingDto.getMedia().getCodeList(),
          upsellingDto.getMedia().getCodeValue())
          + "-"
          + CodeList.extractCodeValue("JNLCODS", upsellingDto.getMedia().getCodeList(),
              upsellingDto.getMedia().getCodeValue()))) {

        placePrintUpselling(
            upsellingDto,
            generateParentCodeValue(orderPlacementGroup.getOrderPlacementGroupDto().getMedia().getCodeList(),
                orderPlacementGroup.getOrderPlacementGroupDto().getMedia().getCodeValue()));
        OrderPlacementGroupManager orderPlacementGroupUpselling = retrievePlacementGroup(upsellingDto.getMedia().getMediaId());
        OrderPlacementManager orderPlacementUpselling = orderPlacementGroupUpselling.retrievePlacement(upsellingDto.getMedia().getMediaId());

        orderPlacement.getOrderPlacementDto().setOtherMaterialId(
            orderPlacementUpselling.getOrderPlacementDto().getMaterialId());
      }
    }
    // normalement material upselling fait dans placePrintUpselling
    // if
    // (orderPlacement.getOrderPlacementDto().getMediaPrintPlus().isPrintPlus()
    // &&
    // StringUtil.isEmpty(orderPlacement.getOrderPlacementDto().getOtherMaterialId()))
    // {
    // OrderMaterialUEditorDto orderMaterialDto = new OrderMaterialUEditorDto();
    // OrderMaterialManager orderMaterial = addMaterial(orderMaterialDto);
    // orderPlacement.getOrderPlacementDto().setOtherMaterialId(orderMaterial.getOrderMaterialDto().getId());
    // }
  }

  private String generateParentCodeValue(String codeList, String codeValue) {
    return CodeList.extractCodeValue("JNLCOD", codeList, codeValue) + "-"
        + CodeList.extractCodeValue("JNLCODS", codeList, codeValue) + "-"
        + CodeList.extractCodeValue("JNLGRC", codeList, codeValue);
  }

  public void setWithoutPrintPlus(String offerId, boolean isWithoutPrintPlus) {
    LOGGER.warn("setWithoutPrintPlus:" + offerId + ":" + isWithoutPrintPlus);
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    if (orderPlacement.getOrderPlacementDto().getMediaPrintPlus() != null) {
      getOrderDto().setQuotationStatus(OrderQuotationStatusEnumDto.TODO);
      getOrderDto().setWithoutPrintPlus(isWithoutPrintPlus);
    }
    if (isWithoutPrintPlus) {
      String upJNLCOD = null;
      String upJNLCODS = null;
      if (orderPlacement.getOrderPlacementDto().getMediaPrintPlus() == null) {
        OrderMediaPrintPlusDto orderMediaPrintPlusDto = new OrderMediaPrintPlusDto();
        orderPlacement.getOrderPlacementDto().setMediaPrintPlus(orderMediaPrintPlusDto);
      }
      orderPlacement.getOrderPlacementDto().getMediaPrintPlus().setPrintPlus(false);
      orderPlacement.getOrderPlacementDto().getMediaPrintPlus().setMandatory(false);
      try {
        upJNLCOD = CodeList.extractCodeValue("JNLCOD", "JNLCOD-JNLCODS",
            orderPlacement.getOrderPlacementDto().getMediaPrintPlus().getMediaId().getId());
        upJNLCODS = CodeList.extractCodeValue("JNLCODS", "JNLCOD-JNLCODS",
            orderPlacement.getOrderPlacementDto().getMediaPrintPlus().getMediaId().getId());
      }
      catch (Exception e) {
      }

      if (upJNLCOD != null) {
        for (OrderPlacementGroupManager pg : findPlacementGroups()) {
          String up1JNLCOD = CodeList.extractCodeValue("JNLCOD",
              pg.getOrderPlacementGroupDto().getMedia().getCodeList(),
              pg.getOrderPlacementGroupDto().getMedia().getCodeValue());
          String up1JNLCODS = CodeList.extractCodeValue("JNLCODS",
              pg.getOrderPlacementGroupDto().getMedia().getCodeList(),
              pg.getOrderPlacementGroupDto().getMedia().getCodeValue());

          if (upJNLCOD != null && upJNLCODS != null && upJNLCOD.equals(up1JNLCOD) && upJNLCODS.equals(up1JNLCODS)) {
            removePlacementGroup(pg.getOrderPlacementGroupDto().getMedia().getMediaId());
          }
        }
      }
    }
  }

  public boolean isPrintPlusOfferSelected(String offerId) {
    LOGGER.warn("isPrintPlusOfferSelected:" + offerId);
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    if (orderPlacement.getOrderPlacementDto().getMediaPrintPlus() != null && !getOrderDto().isWithoutPrintPlus()) {
      return true;
    }
    return false;
  }

  public String retrievePrintSubHeading(String offerId) {
    LOGGER.warn("retrievePrintSubHeading:" + offerId);
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    OptionDto optionDto = orderPlacement.retrieveOption(OptionTypeEnumDto.SUB_HEADING);
    if (optionDto == null) {
      return null;
    }
    else {
      return optionDto.getOptionId().getId();
    }
  }

  // retrieve Print+ infos to display check-box
  // ##EV appel� au d�but de la page donn�es de parution
  public OrderMediaPrintPlusDto retrievePrintPlus(String offerId) {
    LOGGER.warn("retrievePrintPlus:" + offerId);
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    MediaUpsellingDto upsellingDto = orderPlacement.retrievePrintPlus(fillPrintStandardCriteria(orderPlacement));

    if (orderPlacement.getOrderPlacementDto().getMediaPrintPlus() != null
        && orderPlacement.getOrderPlacementDto().getMediaPrintPlus().isPrintPlus()) {
      if (!mediaExistInOrder(CodeList.extractCodeValue("JNLCOD", upsellingDto.getMedia().getCodeList(),
          upsellingDto.getMedia().getCodeValue())
          + "-"
          + CodeList.extractCodeValue("JNLCODS", upsellingDto.getMedia().getCodeList(),
              upsellingDto.getMedia().getCodeValue()))) {
        placePrintUpselling(
            upsellingDto,
            generateParentCodeValue(orderPlacementGroup.getOrderPlacementGroupDto().getMedia().getCodeList(),
                orderPlacementGroup.getOrderPlacementGroupDto().getMedia().getCodeValue()));
        OrderPlacementGroupManager orderPlacementGroupUpselling = retrievePlacementGroup(upsellingDto.getMedia().getMediaId());
        OrderPlacementManager orderPlacementUpselling = orderPlacementGroupUpselling.retrievePlacement(upsellingDto.getMedia().getMediaId());

        orderPlacement.getOrderPlacementDto().setOtherMaterialId(
            orderPlacementUpselling.getOrderPlacementDto().getMaterialId());
      }
    }
    // old version OrderMediaPrintPlusDto mediaPrintPlusDto =
    // orderPlacement.retrievePrintPlus();

    // if
    // (StringUtil.isEmpty(orderPlacement.getOrderPlacementDto().getOtherMaterialId()))
    // {
    // OrderMaterialUEditorDto orderMaterialDto = new OrderMaterialUEditorDto();
    // OrderMaterialManager orderMaterial = addMaterial(orderMaterialDto);
    // orderPlacement.getOrderPlacementDto().setOtherMaterialId(orderMaterial.getOrderMaterialDto().getId());
    // }

    // return orderPlacement.retrievePrintPlus();
    return orderPlacement.getOrderPlacementDto().getMediaPrintPlus();
  }

  public List<OptionSizeDto> findPrintSizes(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    List<OptionIdDto> existingOptionIdDtos = orderPlacement.fillExistingOptionIdDtos(OptionTypeEnumDto.SIZE);
    List<CriteriaDto> criterias = fillPrintStandardCriteria(orderPlacement);
    List<OptionDto> optionDtos = orderPlacement.findOptions(OptionTypeEnumDto.SIZE, existingOptionIdDtos, criterias);
    List<OptionSizeDto> optionSizeDtos = new ArrayList<OptionSizeDto>();
    for (OptionDto optionDto : optionDtos) {
      optionSizeDtos.add((OptionSizeDto) optionDto);
    }
    return optionSizeDtos;
  }

  public OptionSizeDto findPrintSizes(String offerId, int width, int height) {
    double widthWithMinTolerance = ((double) width) / 100 * 95;
    double heigthWithMinTolerance = ((double) height) / 100 * 95;
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    List<OptionIdDto> existingOptionIdDtos = orderPlacement.fillExistingOptionIdDtos(OptionTypeEnumDto.SIZE);
    // List<CriteriaDto> criterias = fillPrintStandardCriteria(orderPlacement);
    // criterias.add(new CriteriaDto(CriteriaKeyEnumDto.STANDARD_SIZE_WIDTH, ""
    // + width));
    // criterias.add(new CriteriaDto(CriteriaKeyEnumDto.STANDARD_SIZE_HEIGHT, ""
    // + height));
    // criterias.add(new CriteriaDto(CriteriaKeyEnumDto.STANDARD_SIZE_TOLERANCE,
    // "0"));
    List<OptionDto> optionDtos = orderPlacement.findOptions(OptionTypeEnumDto.SIZE, existingOptionIdDtos,
        new ArrayList<CriteriaDto>());
    for (OptionDto optionDto : optionDtos) {
      OptionSizeDto optionSizeDto = (OptionSizeDto) optionDto;

      switch (optionSizeDto.getMeasurementType()) {
        case MILLIMETERS:
          if ((double) optionSizeDto.getWidthMinimumInMm() >= widthWithMinTolerance
              && (double) optionSizeDto.getHeightMaximumInMm() >= heigthWithMinTolerance
                  + (double) optionSizeDto.getGutterHeight()) {
            return optionSizeDto;
          }
          break;
        case FRACTIONS:
        case MODULES:
        case FIELDS:
          if ((double) optionSizeDto.getWidthMinimumInMm() >= widthWithMinTolerance
              && (double) optionSizeDto.getHeightMaximumInMm() >= heigthWithMinTolerance) {
            return optionSizeDto;
          }
          break;
        default:
          return optionSizeDto;
      }
    }
    return null;
  }

  public void placePrintSize(String offerId, String sizeId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    OptionIdDto optionIdDto = new OptionIdDto();
    optionIdDto.setOwnerId("Pub2000");
    optionIdDto.setId(sizeId);
    optionIdDto.setOptionType(OptionTypeEnumDto.SIZE);
    orderPlacement.placeOption(optionIdDto);
  }

  public void changeMediaProductionUeditorAdType(String offerId, boolean containsImage) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    if (!getOrderMediaProductionTypeCode(orderPlacement.getOrderPlacementDto().getMediaProduction()).equals("01")) {
      if (containsImage) {
        orderPlacement.changeMediaProductionAdType("03");
      }
      else {
        orderPlacement.changeMediaProductionAdType("02");
      }
    }
  }

  public void changeMediaProductionUploadAdType(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    orderPlacement.changeMediaProductionAdType("08");
  }

  public void placePrintSizeHeightTotal(String offerId, int height, int total) {
    getOrderDto().setQuotationStatus(OrderQuotationStatusEnumDto.TODO);
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    OptionSizeDto optionDto = (OptionSizeDto) orderPlacement.retrieveOption(OptionTypeEnumDto.SIZE);
    if (optionDto != null) {
      if (height != 0) {
        optionDto.setHeight(height);
        optionDto.setTotal(optionDto.getWidthMaximum() * height);
      }
      if (total != 0) {
        optionDto.setTotal(total);
      }
    }
  }

  public OptionSizeDto retrievePrintSize(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    return (OptionSizeDto) orderPlacement.retrieveOption(OptionTypeEnumDto.SIZE);
  }

  public String retrievePrintAdType(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    OrderMediaProductionDto orderMediaProductionDto = orderPlacement.retrieveMediaProduction();

    String txttyp = CodeList.extractCodeValue("TXTTYP", orderMediaProductionDto.getCodeList(),
        orderMediaProductionDto.getCodeValue());
    return txttyp;
  }

  public Date retrievePrintInsertionValidityFrom(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    return orderPlacement.getOrderPlacementDto().getMedia().getValidityFrom();
  }

  public Date retrievePrintInsertionValidityUntil(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    return orderPlacement.getOrderPlacementDto().getMedia().getValidityUntil();
  }

  public List<OptionIssueDateDto> findPrintInsertions(String offerId, Date initialDate, Date finalDate) {
    CheckDelay checkDelay = fillCheckDelay();

    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    List<OptionIssueDateDto> insertions = orderPlacement.findInsertions(initialDate, finalDate);
    for (OptionIssueDateDto item : insertions) {
      item.setDelayDate(calculateDelay(item.getDelayDate(), checkDelay));
    }
    return insertions;
  }

  public void placePrintInsertions(String offerId, List<Date> dates) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    orderPlacement.removeInsertion();
    orderPlacement.placeInsertion(dates);
  }

  public ArrayList<Date> retrievePrintInsertions(String offerId) {
    ArrayList<Date> dates = new ArrayList<Date>();

    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    if (orderPlacement.retrieveInsertions() != null) {
      for (OrderInsertionDto insertionDto : orderPlacement.retrieveInsertions()) {
        dates.add(insertionDto.getIssueDate());
      }
    }

    return dates;
  }

  public RateDiscountScaleDto retrievePrintDiscountScale(String offerId) {
    RateDiscountScaleDto eldoradoDiscountScale = null;

    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);

    List<OptionIdDto> existingOptionIdDtos = orderPlacement.fillExistingOptionIdDtos(null);

    List<CriteriaDto> criterias = new ArrayList<CriteriaDto>();
    List<OrderInsertionDto> insertions = orderPlacement.retrieveInsertions();
    Date validity;
    if (insertions != null && insertions.size() > 0) {
      validity = insertions.get(0).getIssueDate();
    }
    else {
      validity = new Date();
      if (validity.before(orderPlacement.getOrderPlacementDto().getMedia().getValidityFrom())) {
        validity = orderPlacement.getOrderPlacementDto().getMedia().getValidityFrom();
      }
    }
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.VALIDITY, new DateUtil("yyyyMMdd", validity).toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.PLACEMENT_EXTRACT_DISCOUNT_SCALE, "true"));

    RateDto rate = getOptionService().retrieveRate(offerIdDto, existingOptionIdDtos, criterias);
    for (RateDiscountScaleDto item : rate.getDiscountScales()) {
      if (item.getVolumeType() == RateDiscountScaleVolumeTypeEnumDto.REPETITION) {
        eldoradoDiscountScale = item;
        break;
      }
    }

    return eldoradoDiscountScale;
  }

  public void removePrintInsertions(String offerId, List<Date> dates) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    orderPlacement.removeInsertion(dates);
  }

  private String getFlashHelpFileName(String txttyp, String language) {
    // String flashFile = "";
    // String heading = getEldoradoHeading();
    //
    // if (heading == "20" || heading == "21" || heading == "22")
    // flashFile = "JOB";
    // else if (heading == "10" || heading == "11" || heading == "12")
    // flashFile = "IMMO";
    // else if (heading == "30")
    // flashFile = "AUTO";
    // else if (heading == "31")
    // flashFile = "PRIVATE";
    // else
    // flashFile = "OTHER";
    // return flashFile + "_" + txttyp + "_" + language.toUpperCase() + ".swf";
    return PropertyLoader.getPropertyWithContext(this, "UEditorPrint.help.Url." + language.toUpperCase());
  }

  protected String getFormatedSize(OptionSizeDto size) {
    final StringBuffer buff = new StringBuffer();
    LanguageEnumDto languageEnum = LanguageEnumDto.getByValue(getOrderDto().getLanguage());

    switch (size.getMeasurementType()) {
      case MILLIMETERS:
        buff.append(TranslationManager.getLabel(this.eldoradoTranslationPrefix + ".ELDORADO_AD_EDITION.AD_WIDTH",
            languageEnum));
        buff.append(" ");
        buff.append(size.getWidthEquivalenceInMm());
        buff.append(" mm (");
        buff.append(size.getWidth());
        buff.append(" ");
        buff.append(TranslationManager.getLabel(this.eldoradoTranslationPrefix + ".ELDORADO_AD_EDITION.COLUMNS",
            languageEnum));
        buff.append(")");
        break;

      case FRACTIONS:
        buff.append(size.getWidth());
        buff.append("/");
        buff.append(size.getHeight());
        buff.append(" ");
        buff.append(TranslationManager.getLabel(this.eldoradoTranslationPrefix + ".ELDORADO_AD_EDITION.PAGE",
            languageEnum));
        buff.append(" (");
        buff.append(size.getWidthEquivalenceInMm());
        buff.append(" x ");
        buff.append(size.getHeightEquivalenceInMm());
        buff.append(" mm) ");
        break;

      case MODULES:
        buff.append(size.getWidth());
        buff.append(" x ");
        buff.append(size.getHeight());
        buff.append(" ");
        buff.append(TranslationManager.getLabel(this.eldoradoTranslationPrefix + ".ELDORADO_AD_EDITION.MODULE",
            languageEnum));
        buff.append(" (");
        buff.append(size.getWidthEquivalenceInMm());
        buff.append(" x ");
        buff.append(size.getHeightEquivalenceInMm());
        buff.append(" mm) ");
        break;

      case FIELDS:
        buff.append(size.getTotal());
        buff.append(" ");
        buff.append(TranslationManager.getLabel(this.eldoradoTranslationPrefix + ".ELDORADO_AD_EDITION.FIELDS",
            languageEnum));
        buff.append(" (");
        buff.append(size.getWidthEquivalenceInMm());
        buff.append(" x ");
        buff.append(size.getHeightEquivalenceInMm());
        buff.append(" mm) ");
        break;

      default:
        buff.append(size.getTotalMinimum());
        buff.append(" ");
        buff.append(size.getName(getOrderDto().getLanguage()));
        buff.append(" ");
        break;
    }

    return buff.toString();
  }

  private void addElement(Document doc, String tagName, String value, Element element) {
    Element tmpElement = doc.createElement(tagName);
    element.appendChild(tmpElement);
    if (value != null)
      tmpElement.setTextContent(value);

  }

  private void addContactElement(Document doc, String ref, String id, String value, Element element) {
    Element el = doc.createElement("criteria");
    element.appendChild(el);
    el.setAttribute("ref", ref);
    el.setAttribute("id", id);
    el.setAttribute("value", value);
  }

  public Document retrieveContactOnlineAdmin(PartnerDto partner) {
    Document doc = null;
    try {
      doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    }
    catch (Exception ex) {
    }
    Element eContents = doc.createElement("contents");
    doc.appendChild(eContents);

    Element els = doc.createElement("criterias");
    eContents.appendChild(els);
    els.setAttribute("id", "1");
    els.setAttribute("default", "yes");
    els.setAttribute("name", "contact");
    els.setAttribute("type", "contact");

    if (partner.getQualifierInfo().getQualifier() != null
        && partner.getQualifierInfo().getQualifier() == PartnerQualifierEnumDto.COMPANY) {
      addContactElement(doc, "firm", "CONTACT_COMPANY_COMPANYNAME", partner.getQualifierInfo().getCompanyName(), els);
      addContactElement(doc, "additionalName", "CONTACT_PERSON_ADDITIONALNAME",
          partner.getQualifierInfo().getCompanyContactName(), els);
    }
    else {
      if (partner.getQualifierInfo().getQualifier() != null) {
        String salutation = partner.getQualifierInfo().getQualifier().name();
        salutation = salutation.charAt(0) + salutation.substring(1).toLowerCase();
        addContactElement(doc, "qualifier", "CONTACT_PERSON_SALUTATIONID", salutation, els);
      }
      if (partner.getQualifierInfo().getQualifier() != null
          && partner.getQualifierInfo().getQualifier() == PartnerQualifierEnumDto.NONE) {
        addContactElement(doc, "firm", "CONTACT_COMPANY_COMPANYNAME", partner.getQualifierInfo().getLastName(), els);
        addContactElement(doc, "additionalName", "CONTACT_PERSON_ADDITIONALNAME",
            partner.getQualifierInfo().getComplement(), els);
      }
      else {
        addContactElement(doc, "familyName", "CONTACT_PERSON_LASTNAME", partner.getQualifierInfo().getLastName(), els);
        addContactElement(doc, "name", "CONTACT_PERSON_FIRSTNAME", partner.getQualifierInfo().getFirstName(), els);
        addContactElement(doc, "additionalName", "CONTACT_PERSON_ADDITIONALNAME",
            partner.getQualifierInfo().getComplement(), els);
      }
    }
    addContactElement(doc, "mobilePhone", "CONTACT_MOBILEPHONE",
        makePhone(partner.getQualifierInfo().getMobileCountryCode(), partner.getQualifierInfo().getMobilePhone()), els);
    addContactElement(doc, "email", "CONTACT_EMAIL", partner.getQualifierInfo().getEmailAddress(), els);
    addContactElement(doc, "professionalPhone", "CONTACT_FIXEDPHONESPROFESSIONAL",
        makePhone(partner.getQualifierInfo().getPhoneCountryCode(), partner.getQualifierInfo().getPhoneNumber()), els);

    addContactElement(doc, "street", "CONTACT_PHYSICALADDRESS_STREETNAME", partner.getPhysicalAddress().getStreet(),
        els);
    addContactElement(doc, "streetNr", "CONTACT_PHYSICALADDRESS_STREETNUMBER",
        partner.getPhysicalAddress().getStreetNr(), els);
    addContactElement(doc, "additionalStreet", "CONTACT_PHYSICALADDRESS_ADDITIONALSTREETNAME",
        partner.getPhysicalAddress().getStreetNrComp(), els);
    addContactElement(doc, "city", "CONTACT_PHYSICALADDRESS_CITY", partner.getPhysicalAddress().getCity(), els);
    if (partner.getPhysicalAddress().getCountry() != null
        || partner.getPhysicalAddress().getCountry().equals(PartnerCountryEnumDto.CH)) {
      addContactElement(doc, "zipcode", "CONTACT_PHYSICALADDRESS_ZIPPOSTALCODE",
          partner.getPhysicalAddress().getZip().substring(0, 4), els);
    }
    else {
      addContactElement(doc, "zipcode", "CONTACT_PHYSICALADDRESS_ZIPPOSTALCODE", partner.getPhysicalAddress().getZip(),
          els);
    }
    if (partner.getPhysicalAddress().getCountry() != null) {
      addContactElement(doc, "land", "CONTACT_PHYSICALADDRESS_COUNTRYCODE",
          partner.getPhysicalAddress().getCountry().name(), els);
    }

    return doc;
  }

  public Document retrieveContactBlindBox(PartnerDto partner) {
    Document doc = null;
    try {
      doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    }
    catch (Exception ex) {
    }
    Element eContents = doc.createElement("contents");
    doc.appendChild(eContents);

    Element els = doc.createElement("criterias");
    eContents.appendChild(els);
    els.setAttribute("id", "1");
    els.setAttribute("default", "yes");
    els.setAttribute("name", "contact");
    els.setAttribute("type", "contact");
    LanguageEnumDto languageEnum = LanguageEnumDto.getByValue(getOrderDto().getLanguage());

    String blindBox = TranslationManager.getLabel(this.eldoradoTranslationPrefix + ".BLINDBOX.CHIFFRE", languageEnum)
        + " "
        + CodeList.extractCodeValue("CDESCHL", getOrderDto().getBlindBoxInfo().getCodeList(),
            getOrderDto().getBlindBoxInfo().getCodeValue()) + " "
        + CodeList.extractCodeValue("UTIENTO", getOrderDto().getCodeList(), getOrderDto().getCodeValue()).substring(2)
        + "-" + CodeList.extractCodeValue("CDESCHN", getBlindBox().getCodeList(), getBlindBox().getCodeValue());

    addContactElement(doc, "firm", "CONTACT_COMPANY_COMPANYNAME", blindBox, els);
    addContactElement(doc, "street", "CONTACT_PHYSICALADDRESS_STREETNAME", partner.getQualifierInfo().getLastName(),
        els);
    addContactElement(doc, "additionalStreetName", "CONTACT_PHYSICALADDRESS_ADDITIONALSTREETNAME",
        TranslationManager.getLabel(this.eldoradoTranslationPrefix + ".BLINDBOX.CASE", languageEnum) + " "
            + partner.getPhysicalAddress().getPoBoxNr(), els);
    addContactElement(doc, "city", "CONTACT_PHYSICALADDRESS_CITY", partner.getPhysicalAddress().getPoBoxCity(), els);
    addContactElement(doc, "zipcode", "CONTACT_PHYSICALADDRESS_ZIPPOSTALCODE",
        partner.getPhysicalAddress().getPoBoxZip(), els);
    return doc;
  }

  private String makePhone(String countryCode, String phone) {
    if (phone == null || StringUtil.isEmpty(phone)) {
      return null;
    }
    if (countryCode != null && !StringUtil.isEmpty(countryCode)) {
      return countryCode + phone;
    }
    else {
      return phone;
    }
  }

  public Document retrieveOnlineMaterialAdmin(String offerId, String adminFileUrl) {
    Document doc = null;
    try {
      doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    }
    catch (Exception ex) {
    }

    // Retrieve information for metadata
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);

    Element eAdmin = doc.createElement("admin");
    doc.appendChild(eAdmin);

    String adTypeId = EldoradoCategoryEnumDto.getCategoryForCode(getEldoradoHeading()).getAdType().toString();
    addElement(doc, "heading", adTypeId, eAdmin);

    if (EldoradoCategoryEnumDto.getCategoryForCode(getEldoradoHeading()).getAdOperationId() != null) {
      addElement(doc, "adOperation",
          EldoradoCategoryEnumDto.getCategoryForCode(getEldoradoHeading()).getAdOperationId().toString(), eAdmin);
    }
    if (EldoradoCategoryEnumDto.getCategoryForCode(getEldoradoHeading()).getAdTransactionId() != null) {
      addElement(doc, "adTransaction",
          EldoradoCategoryEnumDto.getCategoryForCode(getEldoradoHeading()).getAdTransactionId().toString(), eAdmin);
    }

    addElement(doc, "sourceId", "MYPEASY", eAdmin);
    addElement(doc, "contentLanguage", getLanguage(), eAdmin);

    Element eMedia = doc.createElement("media");
    eAdmin.appendChild(eMedia);

    addElement(doc, "ownerId", orderPlacementGroup.getOrderPlacementGroupDto().getMedia().getMediaId().getOwnerId(),
        eMedia);

    String mediaId = CodeList.extractCodeValue("JNLCOD",
        orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel2().getCodeList(),
        orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel2().getCodeValue())
        + CodeList.extractCodeValue("JNLCODS",
            orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel2().getCodeList(),
            orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel2().getCodeValue());
    addElement(doc, "mediaId", mediaId, eMedia);
    addElement(doc, "offerId", offerId, eMedia);

    boolean isPrintPlus = false;
    String otherOnlineMaterial = null;
    String currentExistingMaterial = null;
    OrderMaterialUEditorDto material = retrievePrintPlusMaterial(offerId);
    if (material != null) {
      currentExistingMaterial = (material.getCmsUuid());
      isPrintPlus = true;
    }
    else {
      /**
       * We are in normal online mode, check if existing material
       */
      material = retrieveOnlineMaterial(offerId);
      if (material != null && material.getCmsUuid() != null) {
        currentExistingMaterial = material.getCmsUuid();
      }
    }
    if (StringUtil.isEmpty(currentExistingMaterial)) {
      otherOnlineMaterial = retrieveOnlineMaterialCmsUuidAlreadyDone(offerId);
      if (StringUtil.isEmpty(otherOnlineMaterial)) {
        otherOnlineMaterial = retrievePrintPlusMaterialCmsUuidAlreadyDone(offerId);
      }
      if (StringUtil.isEmpty(otherOnlineMaterial)) {
        addElement(doc, "adContentIdentifier", null, eAdmin);
        addElement(doc, "duplicateAd", "no", eAdmin);

        // Find other print material
        List<OrderMaterialManager> orderMaterials = findMaterials();
        for (OrderMaterialManager orderMaterial : orderMaterials) {
          OrderMaterialDto orderMaterialDto = orderMaterial.getOrderMaterialDto();
          if (orderMaterialDto instanceof OrderMaterialUEditorPrintDto) {
            OrderMaterialUEditorPrintDto orderMaterialUEditorPrintDto = (OrderMaterialUEditorPrintDto) orderMaterialDto;
            if (!StringUtil.isEmpty(orderMaterialUEditorPrintDto.getCmsUuid())) {
              addElement(doc, "printAdContentIdentifier", orderMaterialUEditorPrintDto.getCmsUuid(), eAdmin);
              break;
            }
          }
        }
      }
      else {
        addElement(doc, "adContentIdentifier", otherOnlineMaterial, eAdmin);
        addElement(doc, "duplicateAd", "yes", eAdmin);
      }
    }
    else {
      addElement(doc, "adContentIdentifier", currentExistingMaterial, eAdmin);
      addElement(doc, "duplicateAd", "no", eAdmin);
    }

    addElement(doc, "objectTitle", getOrderDto().getDescription(), eAdmin);
    addElement(doc, "businessValidation", "all", eAdmin);
    addElement(doc, "multiPagesForm", PropertyLoader.getPropertyWithContext(this, "UEditorOnline.multiplePage"), eAdmin);
    addElement(doc, "extract", "no", eAdmin);

    if (getOrderDto().isBlindBox()) {
      addElement(doc, "contactBlindBox", "yes", eAdmin);
    }
    else {
      addElement(doc, "contactBlindBox", "no", eAdmin);
    }

    if (getOrderDto().isBlindBox() || isUserRegistred()) {
      addElement(doc, "urlContent", adminFileUrl, eAdmin);
    }
    else {
      addElement(doc, "urlContent", null, eAdmin);
    }

    addElement(doc, "preview", "no", eAdmin);

    addElement(doc, "urlOk", PropertyLoader.getPropertyWithContext(this, "UEditorOnline.urlok") + "&isPRintPlus="
        + isPrintPlus, eAdmin);
    addElement(doc, "urlCancel", PropertyLoader.getPropertyWithContext(this, "UEditorOnline.urlko"), eAdmin);
    addElement(doc, "proxyBaseUrl", PropertyLoader.getPropertyWithContext(this, "UEditorOnline.proxy"), eAdmin);

    return doc;

  }

  private Element fillBlindBoxAdress(Document doc) {
    // Element eAdressBlindBox = new Element("adressBlindBox");
    return null;
  }

  public Document retrievePrintMaterialAdmin(String offerId, String baseHref, String guiVariation) {
    Document doc = null;
    try {
      doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    }
    catch (Exception ex) {
    }

    boolean isUeV2 = true;

    // Retrieve information for metadata
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    OrderMediaProductionDto orderMediaProductionDto = orderPlacement.retrieveMediaProduction();
    OptionSizeDto sizeDto = (OptionSizeDto) orderPlacement.retrieveOption(OptionTypeEnumDto.SIZE);
    OptionColorDto colorDto = (OptionColorDto) orderPlacement.retrieveOption(OptionTypeEnumDto.COLOR);
    OptionDto subHeadingDto = orderPlacement.retrieveOption(OptionTypeEnumDto.SUB_HEADING);
    OptionDto clientStatusDto = orderPlacement.retrieveOption(OptionTypeEnumDto.CLIENT_STATUS);
    String txttyp = CodeList.extractCodeValue("TXTTYP", orderMediaProductionDto.getCodeList(),
        orderMediaProductionDto.getCodeValue());

    Element eAdmin = doc.createElement("admin");
    doc.appendChild(eAdmin);

    Element eBaseHref = doc.createElement("baseHref");
    eAdmin.appendChild(eBaseHref);
    eBaseHref.setTextContent(baseHref);

    Element eUrlContent = doc.createElement("urlContent");
    eAdmin.appendChild(eUrlContent);

    Element eGuiParameters = doc.createElement("guiParameters");
    eAdmin.appendChild(eGuiParameters);

    Element eFlashFile = doc.createElement("helpFlashFileName");
    eAdmin.appendChild(eFlashFile);
    eFlashFile.setTextContent(getFlashHelpFileName(txttyp, getLanguage()));

    Element eLanguage = doc.createElement("language");
    eGuiParameters.appendChild(eLanguage);
    eLanguage.setTextContent(getLanguage());

    Element eWindowSize = doc.createElement("windowSize");
    eGuiParameters.appendChild(eWindowSize);

    if (isUeV2) {
      Element eWindowsHeight = doc.createElement("height");
      eWindowSize.appendChild(eWindowsHeight);
      eWindowsHeight.setTextContent("600");

      Element eWindowsWidth = doc.createElement("width");
      eWindowSize.appendChild(eWindowsWidth);
      eWindowsWidth.setTextContent("910");
    }
    else {
      Element eWindowsHeight = doc.createElement("height");
      eWindowSize.appendChild(eWindowsHeight);
      eWindowsHeight.setTextContent("415");

      Element eWindowsWidth = doc.createElement("width");
      eWindowSize.appendChild(eWindowsWidth);
      eWindowsWidth.setTextContent("775");
    }

    Element eEditorWindowName = doc.createElement("editorWindowName");
    eGuiParameters.appendChild(eEditorWindowName);
    eEditorWindowName.setTextContent(orderPlacementGroup.getOrderPlacementGroupDto().getMediaLevel1().getName(
        getLanguage()).getName());

    Element eExpertiseLevel = doc.createElement("expertiseLevel");
    eGuiParameters.appendChild(eExpertiseLevel);
    eExpertiseLevel.setTextContent("10");

    Element eGuiLabelFileURL = doc.createElement("guiLabelFileURL");
    eGuiParameters.appendChild(eGuiLabelFileURL);

    Element eGuiVariation = doc.createElement("guiVariation");
    eGuiParameters.appendChild(eGuiVariation);
    eGuiVariation.setTextContent(guiVariation);

    Element eUrlOk = doc.createElement("urlOk");
    eAdmin.appendChild(eUrlOk);

    Element eUrlCancel = doc.createElement("urlCancel");
    eAdmin.appendChild(eUrlCancel);

    // Generate uuid, unique reference by transaction
    // Reprise existant si touche retour press�e
    boolean newContent = false;
    if (this.currentUeditoPrintJobNumber == null || StringUtil.isEmpty(this.currentUeditoPrintJobNumber)) {
      this.currentUeditoPrintJobNumber = OrderManager.generateUUID();
      newContent = true;
    }
    Element eJobNumber = doc.createElement("jobNumber");
    eAdmin.appendChild(eJobNumber);
    eJobNumber.setTextContent(this.currentUeditoPrintJobNumber);

    boolean isExistingContent = false;
    boolean isDuplication = false;
    Element eExistingContentID = doc.createElement("existingContentID");
    eAdmin.appendChild(eExistingContentID);
    OrderMaterialDto material = retrieveBasePrintMaterial(offerId);
    if (material != null && material instanceof OrderMaterialUEditorPrintDto) {
      OrderMaterialUEditorPrintDto materialUEditorPrintDto = (OrderMaterialUEditorPrintDto) material;
      if (!StringUtil.isEmpty(materialUEditorPrintDto.getCmsUuid())) {
        isExistingContent = true;
        eExistingContentID.setTextContent(materialUEditorPrintDto.getCmsUuid());
      }
      else if (!newContent) {
        eExistingContentID.setTextContent(this.currentUeditoPrintJobNumber);
      }
      else {
        // Find other print material
        List<OrderMaterialManager> orderMaterials = findMaterials();
        for (OrderMaterialManager orderMaterial : orderMaterials) {
          OrderMaterialDto orderMaterialDto = orderMaterial.getOrderMaterialDto();
          if (orderMaterialDto instanceof OrderMaterialUEditorPrintDto) {
            OrderMaterialUEditorPrintDto orderMaterialUEditorPrintDto = (OrderMaterialUEditorPrintDto) orderMaterialDto;
            if (!StringUtil.isEmpty(orderMaterialUEditorPrintDto.getCmsUuid())) {
              isDuplication = true;
              eExistingContentID.setTextContent(orderMaterialUEditorPrintDto.getCmsUuid());
              break;
            }
          }
        }

      }
    }

    Element eMetadata = doc.createElement("metadata");
    eAdmin.appendChild(eMetadata);

    String jnlcod = CodeList.extractCodeValue("JNLCOD", orderMediaProductionDto.getCodeList(),
        orderMediaProductionDto.getCodeValue());
    String jnlcods = CodeList.extractCodeValue("JNLCODS", orderMediaProductionDto.getCodeList(),
        orderMediaProductionDto.getCodeValue());
    retrievePrintMaterialAdminMetadata(doc, eMetadata, "template_mediareference", "=", "\"" + jnlcod + "-" + jnlcods
        + "\"", "media");

    if (sizeDto != null) {
      String grdgenr = CodeList.extractCodeValue("GRDGENR", sizeDto.getCodeList(), sizeDto.getCodeValue());
      retrievePrintMaterialAdminMetadata(doc, eMetadata, "template_sizetypecode", "=", "\"" + grdgenr + "\"",
          "measurement type");
    }

    if (sizeDto != null && sizeDto.getWidthMaximumInMm() != 0) {
      retrievePrintMaterialAdminMetadata(doc, eMetadata, "template_widthmax", ">=",
          Integer.valueOf(sizeDto.getWidthMaximumInMm()).toString(), "max width");
      retrievePrintMaterialAdminMetadata(doc, eMetadata, "template_widthmin", "<=",
          Integer.valueOf(sizeDto.getWidthMaximumInMm()).toString(), "min width");
    }

    if (subHeadingDto != null) {
      String ctarubga = CodeList.extractCodeValue("CTARUBGA", subHeadingDto.getCodeList(), subHeadingDto.getCodeValue());
      String ctarubsa = CodeList.extractCodeValue("CTARUBSA", subHeadingDto.getCodeList(), subHeadingDto.getCodeValue());
      retrievePrintMaterialAdminMetadata(doc, eMetadata, "template_rubrique", "=", "\"" + ctarubga + "\"", "heading");
      retrievePrintMaterialAdminMetadata(doc, eMetadata, "template_sousrubrique", "=", "\"" + ctarubga + "." + ctarubsa
          + "\"", "sub heading");
    }

    if (clientStatusDto != null) {
      String ctarubt = CodeList.extractCodeValue("CTARUBT", clientStatusDto.getCodeList(),
          clientStatusDto.getCodeValue());
      retrievePrintMaterialAdminMetadata(doc, eMetadata, "template_rubriquetarif", "=", "\"" + ctarubt + "\"",
          "client status");
    }

    String adtype = txttyp;
    if (isUeV2) {
      if (adtype.equals("01")) {
        adtype = "(\"01\")";
      }
      else {
        adtype = "(\"02\", \"03\")";
      }
      retrievePrintMaterialAdminMetadata(doc, eMetadata, "template_adtype", " in ", adtype, "text type");
    }
    else {
      if (!adtype.equals("01")) {
        adtype = "02";
      }
      retrievePrintMaterialAdminMetadata(doc, eMetadata, "template_adtype", "=", "\"" + adtype + "\"", "text type");
    }
    Element eTextLanguage = doc.createElement("textLanguage");
    eAdmin.appendChild(eTextLanguage);
    String utilng = CodeList.extractCodeValue("UTILNG", orderMediaProductionDto.getCodeList(),
        orderMediaProductionDto.getCodeValue());
    String textLanguage = LanguageId.chooseInstanceByName(utilng).getIsoCode();
    eTextLanguage.setTextContent(textLanguage);

    Element eAdSize = doc.createElement("adSize");
    eAdmin.appendChild(eAdSize);

    if (sizeDto != null) {
      if (sizeDto.getWidthMaximumInMm() != 0) {
        Element eWidth = doc.createElement("width");
        eAdSize.appendChild(eWidth);
        eWidth.setTextContent(Integer.valueOf(sizeDto.getWidthMaximumInMm()).toString());
      }

      Element eFixedHeight = doc.createElement("fixedHeight");
      eAdSize.appendChild(eFixedHeight);
      if (sizeDto.getMeasurementType() == SizeMeasurementTypeEnumDto.FRACTIONS
          || sizeDto.getMeasurementType() == SizeMeasurementTypeEnumDto.FIELDS
          || sizeDto.getMeasurementType() == SizeMeasurementTypeEnumDto.ENTRIES
          || sizeDto.getMeasurementType() == SizeMeasurementTypeEnumDto.MODULES) {
        eFixedHeight.setTextContent("True");

        Element eHeight = doc.createElement("height");
        eAdSize.appendChild(eHeight);
        eHeight.setTextContent(Integer.valueOf(sizeDto.getHeightMaximumInMm()).toString());
      }
      else {
        eFixedHeight.setTextContent("False");
      }
    }
    if (sizeDto.getMeasurementType() != SizeMeasurementTypeEnumDto.WORDS
        && sizeDto.getMeasurementType() != SizeMeasurementTypeEnumDto.LINES) {
      List<OptionSizeDto> sizes = findPrintSizes(offerId);
      if (sizes != null) {
        Element eOptionSizes = doc.createElement("selectableAdSizes");
        eAdmin.appendChild(eOptionSizes);

        for (OptionSizeDto size : sizes) {
          Element eOptionSize = doc.createElement("adSize");
          eOptionSizes.appendChild(eOptionSize);

          Element eFixedHeight = doc.createElement("fixedHeight");
          eOptionSize.appendChild(eFixedHeight);
          if (size.getMeasurementType() == SizeMeasurementTypeEnumDto.FRACTIONS
              || size.getMeasurementType() == SizeMeasurementTypeEnumDto.FIELDS
              || size.getMeasurementType() == SizeMeasurementTypeEnumDto.ENTRIES
              || size.getMeasurementType() == SizeMeasurementTypeEnumDto.MODULES) {
            eFixedHeight.setTextContent("True");

            Element eHeight = doc.createElement("height");
            eOptionSize.appendChild(eHeight);
            eHeight.setTextContent(Integer.valueOf(size.getHeightMaximumInMm()).toString());
          }
          else {
            eFixedHeight.setTextContent("False");
          }
          if (size.getWidthMaximumInMm() != 0) {
            Element eWidth = doc.createElement("width");
            eOptionSize.appendChild(eWidth);
            eWidth.setTextContent(Integer.valueOf(size.getWidthMaximumInMm()).toString());
          }
          Element eAdminWidth = doc.createElement("adminWidth");
          eOptionSize.appendChild(eAdminWidth);
          Element eAdminWidthUnit = doc.createElement("unit");
          eAdminWidth.appendChild(eAdminWidthUnit);
          eAdminWidthUnit.setTextContent(size.getWidthUnitOfMeasurement().toString());
          Element eAdminWidthValue = doc.createElement("value");
          eAdminWidth.appendChild(eAdminWidthValue);
          eAdminWidthValue.setTextContent(Integer.valueOf(size.getWidthMaximum()).toString());

          Element eAdminHeigth = doc.createElement("adminHeigth");
          eOptionSize.appendChild(eAdminHeigth);
          Element eAdminHeigthUnit = doc.createElement("unit");
          eAdminHeigth.appendChild(eAdminHeigthUnit);
          eAdminHeigthUnit.setTextContent(size.getHeightUnitOfMeasurement().toString());
          Element eAdminHeigtValue = doc.createElement("value");
          eAdminHeigth.appendChild(eAdminHeigtValue);
          eAdminHeigtValue.setTextContent(Integer.valueOf(size.getHeightMaximum()).toString());
          if (size.getHeightMinimumInMm() != 0) {
            Element eMinHeight = doc.createElement("minHeight");
            eOptionSize.appendChild(eMinHeight);
            eMinHeight.setTextContent(Integer.valueOf(size.getHeightMinimumInMm()).toString());
          }
          if (size.getHeightMaximumInMm() != 0) {
            Element eMaxHeight = doc.createElement("maxHeight");
            eOptionSize.appendChild(eMaxHeight);
            eMaxHeight.setTextContent(Integer.valueOf(size.getHeightMaximumInMm()).toString());
          }
          Element eName = doc.createElement("name");
          eOptionSize.appendChild(eName);
          eName.setTextContent(getFormatedSize(size));
        }
      }

    }

    Element eSaveNewVersionIfModified = doc.createElement("saveNewVersionIfModified");
    eAdmin.appendChild(eSaveNewVersionIfModified);
    if (isExistingContent) {
      eSaveNewVersionIfModified.setTextContent("False");
    }
    else {
      eSaveNewVersionIfModified.setTextContent("True");
    }

    Element eSaveAsNewContent = doc.createElement("saveAsNewContent");
    eAdmin.appendChild(eSaveAsNewContent);
    if (isExistingContent && !isDuplication) {
      eSaveAsNewContent.setTextContent("False");
    }
    else {
      eSaveAsNewContent.setTextContent("True");
    }
    Element eSsChiffre = doc.createElement("ssChiffre");
    eAdmin.appendChild(eSsChiffre);
    eSsChiffre.setTextContent(getBlindBoxText());

    Element eSsChiffrePrefix = doc.createElement("ssChiffrePrefix");
    eAdmin.appendChild(eSsChiffrePrefix);
    eSsChiffrePrefix.setTextContent(getBlindBoxPrefix());

    Element eCmdNr = doc.createElement("cmdNr");
    eAdmin.appendChild(eCmdNr);
    eCmdNr.setTextContent("");

    if (isUeV2) {
      Element eColor = doc.createElement("colorType");
      eAdmin.appendChild(eColor);
      if (colorDto == null || colorDto.getOptionId().getId().contains("00")) {
        eColor.setTextContent("GREY_SCALE");
      }
      else {
        eColor.setTextContent("CMYK");
      }
    }

    Element eInfos = doc.createElement("infos");
    eAdmin.appendChild(eInfos);

    Element eUserid = doc.createElement("userID");
    eInfos.appendChild(eUserid);
    eUserid.setTextContent(OrderApplicationEnumDto.ELDORADO.toString().toLowerCase());

    Element eUtiento = doc.createElement("utiento");
    eInfos.appendChild(eUtiento);
    eUtiento.setTextContent(CodeList.extractCodeValue("UTIENTC", PartnerConstantP2000.PARTNER_ADVERTISER_CODELIST,
        PropertyLoader.getPropertyWithContext(this, "Application.defaultPartner")));

    Element eApplication = doc.createElement("application");
    eInfos.appendChild(eApplication);
    eApplication.setTextContent(OrderApplicationEnumDto.ELDORADO.toString().toLowerCase());

    return doc;
  }

  private String getBlindBoxPrefix() {
    if (!getOrderDto().isBlindBox()) {
      return "";
    }
    LanguageEnumDto languageEnum = LanguageEnumDto.getByValue(getOrderDto().getLanguage());
    return (TranslationManager.getLabel(this.eldoradoTranslationPrefix + ".BLINDBOX.PREFIX", languageEnum));
  }

  private void retrievePrintMaterialAdminMetadata(Document doc, Element eMetadata, String data, String operator,
      String value, String description) {
    Element eMetadatum = doc.createElement("metadatum");
    eMetadata.appendChild(eMetadatum);

    Element eData = doc.createElement("data");
    eMetadatum.appendChild(eData);
    eData.setTextContent(data);

    Element eOperator = doc.createElement("operator");
    eMetadatum.appendChild(eOperator);

    CDATASection eOperatorValue = doc.createCDATASection("operatorValue");
    eOperator.appendChild(eOperatorValue);
    eOperatorValue.setTextContent(operator);

    Element eValue = doc.createElement("value");
    eMetadatum.appendChild(eValue);
    eValue.setTextContent(value);

    Element eDescription = doc.createElement("description");
    eMetadatum.appendChild(eDescription);
    eDescription.setTextContent(description);
  }

  public OrderMaterialDto retrieveBasePrintMaterial(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    OrderMaterialManager orderMaterial = retrieveMaterial(orderPlacement.getOrderPlacementDto().getMaterialId());
    return orderMaterial.getOrderMaterialDto();
  }

  public OrderMaterialUEditorPrintDto retrievePrintMaterial(String offerId) {
    return (OrderMaterialUEditorPrintDto) retrieveBasePrintMaterial(offerId);
  }

  public void updatePrintMaterial(String offerId, OrderMaterialUEditorPrintDto orderMaterialDto) {
    OrderMaterialUEditorPrintDto materialUeditor = null;

    OrderMaterialDto material = retrieveBasePrintMaterial(offerId);
    if (material instanceof OrderMaterialUEditorPrintDto) {
      materialUeditor = (OrderMaterialUEditorPrintDto) material;
    }
    else {
      materialUeditor = new OrderMaterialUEditorPrintDto();
      OrderMaterialManager materialManager = addMaterial(materialUeditor);
      materialManager.getOrderMaterialDto().setId(material.getId());
      material.setId("to_delete");
    }

    materialUeditor.setCmsVersion(orderMaterialDto.getCmsVersion());
    materialUeditor.setCmsUuid(orderMaterialDto.getCmsUuid());
    if (StringUtil.isEmpty(orderMaterialDto.getDescription())) {
      orderMaterialDto.setDescription("Print Content");
    }
    // EV en principe concernant saisi sur page calendrier
    if (!StringUtil.isEmpty(getOrderDto().getDescription())) {
      materialUeditor.setDescription(getOrderDto().getDescription());
    }
    else {
      if (orderMaterialDto.getDescription().length() > 30) {
        materialUeditor.setDescription(orderMaterialDto.getDescription().substring(0, 30));
        getOrderDto().setDescription(orderMaterialDto.getDescription().substring(0, 30));
      }
      else {
        materialUeditor.setDescription(orderMaterialDto.getDescription());
        getOrderDto().setDescription(orderMaterialDto.getDescription());
      }
    }

    changeMediaProductionUeditorAdType(offerId, orderMaterialDto.isContainsImage());

    materialUeditor.setWithInMm(orderMaterialDto.getWithInMm());
    materialUeditor.setHeightInMm(orderMaterialDto.getHeightInMm());
    materialUeditor.setNbLines(orderMaterialDto.getNbLines());
    materialUeditor.setNbWords(orderMaterialDto.getNbWords());
    materialUeditor.setNbChars(orderMaterialDto.getNbChars());
    OptionSizeDto optionSizeDto = retrievePrintSize(offerId);

    switch (optionSizeDto.getMeasurementType()) {
      case MILLIMETERS:
        int heightInMm = Math.max(orderMaterialDto.getHeightInMm() + optionSizeDto.getGutterHeight(),
            optionSizeDto.getHeightMinimumInMm());
        placePrintSizeHeightTotal(offerId, heightInMm, 0);
        break;
      case WORDS:
        int totalWords = Math.max(orderMaterialDto.getNbWords(), optionSizeDto.getTotalMinimum());
        placePrintSizeHeightTotal(offerId, 0, totalWords);
        break;
      case LINES:
        int totalLines = Math.max(orderMaterialDto.getNbLines(), optionSizeDto.getTotalMinimum());
        placePrintSizeHeightTotal(offerId, 0, totalLines);
        break;
      case CHARS:
        int totalChars = Math.max(orderMaterialDto.getNbChars(), optionSizeDto.getTotalMinimum());
        placePrintSizeHeightTotal(offerId, 0, totalChars);
        break;
    }
    placeUploadMaterialSurcharge();

  }

  public void updatePrintMaterialUpload(String offerId, String sizeId, OrderMaterialUploadDto orderMaterialDto) {
    this.pdfValidation = null;
    OrderMaterialUploadDto materialUpload = null;

    OrderMaterialDto material = retrieveBasePrintMaterial(offerId);
    if (material instanceof OrderMaterialUploadDto) {
      materialUpload = (OrderMaterialUploadDto) material;
    }
    else {
      materialUpload = new OrderMaterialUploadDto();
      OrderMaterialManager materialManager = addMaterial(materialUpload);
      materialManager.getOrderMaterialDto().setId(material.getId());
      material.setId("to_delete");
    }

    materialUpload.setFileName(orderMaterialDto.getFileName());
    materialUpload.setFileUrl(orderMaterialDto.getFileUrl());
    materialUpload.setWidthInMm(orderMaterialDto.getWidthInMm());
    materialUpload.setHeightInMm(orderMaterialDto.getHeightInMm());
    materialUpload.setInColor(orderMaterialDto.isInColor());
    materialUpload.setAdminjobType(orderMaterialDto.getAdminjobType());

    placePrintSize(offerId, sizeId);

    OptionSizeDto optionSizeDto = retrievePrintSize(offerId);

    switch (optionSizeDto.getMeasurementType()) {
      case MILLIMETERS:
        int heightInMm = Math.max(materialUpload.getHeightInMm() + optionSizeDto.getGutterHeight(),
            optionSizeDto.getHeightMinimumInMm());
        placePrintSizeHeightTotal(offerId, heightInMm, 0);
        break;
    }

    changeMediaProductionUploadAdType(offerId);
    placeUploadMaterialSurcharge();

  }

  public void resetPrintMaterial(String offerId) {
    OrderMaterialDto material = retrieveBasePrintMaterial(offerId);
    if (material != null && material instanceof OrderMaterialUEditorPrintDto) {
      OrderMaterialUEditorPrintDto materialUEditorPrintDto = (OrderMaterialUEditorPrintDto) material;
      materialUEditorPrintDto.setCmsUuid(null);
    }
    else if (material != null && material instanceof OrderMaterialUploadDto) {
      OrderMaterialUploadDto materialUploadPrintDto = (OrderMaterialUploadDto) material;
      materialUploadPrintDto.setFileName(null);
    }
  }

  public EldoradoRateDto retrievePrintMaterialRate(String offerId, String sizeId,
      OrderMaterialUEditorPrintDto orderMaterialDto) {
    EldoradoRateDto eldoradoRate = new EldoradoRateDto();

    if (sizeId == null || sizeId.trim().equals("")) {
      sizeId = findPrintSizes(offerId, orderMaterialDto.getWithInMm(), orderMaterialDto.getHeightInMm()).getOptionId().getId();
    }
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);

    List<OptionIdDto> existingOptionIdDtos = orderPlacement.fillExistingOptionIdDtos(OptionTypeEnumDto.SIZE);
    OptionIdDto optionIdDto = new OptionIdDto();
    optionIdDto.setOwnerId("Pub2000");
    optionIdDto.setId(sizeId);
    optionIdDto.setOptionType(OptionTypeEnumDto.SIZE);
    OptionSizeDto optionSizeDto = (OptionSizeDto) orderPlacement.retrieveOption(optionIdDto);
    existingOptionIdDtos.add(optionSizeDto.getOptionId());
    eldoradoRate.setOptionSize(optionSizeDto);

    List<CriteriaDto> criterias = new ArrayList<CriteriaDto>();
    List<OrderInsertionDto> insertions = orderPlacement.retrieveInsertions();
    Date validity;
    if (insertions.size() > 0) {
      validity = insertions.get(0).getIssueDate();
    }
    else {
      validity = new Date();
    }
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.VALIDITY, new DateUtil("yyyyMMdd", validity).toString()));
    switch (optionSizeDto.getMeasurementType()) {
      case MILLIMETERS:
        int heightInMm = orderMaterialDto.getHeightInMm() + optionSizeDto.getGutterHeight();
        eldoradoRate.setHeightInMmWithGutter(heightInMm);
        if (heightInMm < optionSizeDto.getHeightMinimumInMm()) {
          heightInMm = optionSizeDto.getHeightMinimumInMm();
          eldoradoRate.setForcedSize(heightInMm);
        }
        criterias.add(new CriteriaDto(CriteriaKeyEnumDto.RATE_SIZE_HEIGHT, Integer.valueOf(heightInMm).toString()));
        break;
      case WORDS:
        int totalWords = orderMaterialDto.getNbWords();
        if (totalWords < optionSizeDto.getTotalMinimum()) {
          totalWords = optionSizeDto.getTotalMinimum();
          eldoradoRate.setForcedSize(totalWords);
        }
        criterias.add(new CriteriaDto(CriteriaKeyEnumDto.RATE_SIZE_TOTAL, Integer.valueOf(totalWords).toString()));
        break;
      case LINES:
        int totalLines = orderMaterialDto.getNbLines();
        if (totalLines < optionSizeDto.getTotalMinimum()) {
          totalLines = optionSizeDto.getTotalMinimum();
          eldoradoRate.setForcedSize(totalLines);
        }
        criterias.add(new CriteriaDto(CriteriaKeyEnumDto.RATE_SIZE_TOTAL, Integer.valueOf(totalLines).toString()));
        break;
      case CHARS:
        int totalChars = orderMaterialDto.getNbChars();
        if (totalChars < optionSizeDto.getTotalMinimum()) {
          totalChars = optionSizeDto.getTotalMinimum();
          eldoradoRate.setForcedSize(totalChars);
        }
        criterias.add(new CriteriaDto(CriteriaKeyEnumDto.RATE_SIZE_TOTAL, Integer.valueOf(totalChars).toString()));
        break;
    }

    RateDto rate = getOptionService().retrieveRate(offerIdDto, existingOptionIdDtos, criterias);
    eldoradoRate.setCurrency(rate.getCurrency());
    if (rate.getPrice().compareTo(rate.getPriceMinimum()) < 0) {
      eldoradoRate.setAmount(rate.getPriceMinimum());
    }
    else {
      eldoradoRate.setAmount(rate.getPriceWithSurcharges());
    }

    return eldoradoRate;
  }

  public OrderMaterialUEditorDto retrievePrintPlusMaterial(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OrderPlacementManager orderPlacement = orderPlacementGroup.retrievePlacement(offerIdDto);
    if (getOrderDto().isWithoutPrintPlus() || orderPlacement == null || orderPlacement.getOrderPlacementDto() == null
        || orderPlacement.getOrderPlacementDto().getMediaPrintPlus() == null
        || !orderPlacement.getOrderPlacementDto().getMediaPrintPlus().isPrintPlus()) {
      return null;
    }
    // OLD OrderMaterialManager orderMaterial =
    // retrieveMaterial(orderPlacement.getOrderPlacementDto().getOtherMaterialId());

    OrderMaterialManager orderMaterial = null;
    OrderPlacementGroupManager orderPlacementGroupPrintPlus = retrievePlacementGroup(orderPlacement.getOrderPlacementDto().getMediaPrintPlus().getMediaId());
    if (orderPlacementGroupPrintPlus.getOrderPlacementGroupDto() != null) {
      OrderPlacementManager orderPlacementPrintPlus = orderPlacementGroupPrintPlus.retrievePlacement(orderPlacement.getOrderPlacementDto().getMediaPrintPlus().getMediaId());
      orderMaterial = retrieveMaterial(orderPlacementPrintPlus.getOrderPlacementDto().getMaterialId());
    }
    else {
      orderMaterial = retrieveMaterial(orderPlacement.getOrderPlacementDto().getOtherMaterialId());
    }
    return (OrderMaterialUEditorDto) orderMaterial.getOrderMaterialDto();
  }

  public String retrievePrintPlusMaterialCmsUuidAlreadyDone(String offerId) {
    OrderMaterialUEditorDto material = retrievePrintPlusMaterial(offerId);
    if (material != null && StringUtil.isEmpty(material.getCmsUuid())) {
      List<OrderMaterialManager> orderMaterials = findMaterials();
      for (OrderMaterialManager orderMaterial : orderMaterials) {
        OrderMaterialDto orderMaterialDto = orderMaterial.getOrderMaterialDto();
        if (orderMaterialDto instanceof OrderMaterialUEditorDto
            && !(orderMaterialDto instanceof OrderMaterialUEditorPrintDto)) {
          OrderMaterialUEditorDto orderMaterialUEditorDto = (OrderMaterialUEditorDto) orderMaterialDto;
          if (!StringUtil.isEmpty(orderMaterialUEditorDto.getCmsUuid())) {
            return orderMaterialUEditorDto.getCmsUuid();
          }
        }
      }
    }
    return null;
  }

  public void updatePrintPlusMaterial(String offerId, OrderMaterialUEditorDto orderMaterialDto) {
    OrderMaterialUEditorDto orderMaterialUEditorDto = retrievePrintPlusMaterial(offerId);
    orderMaterialUEditorDto.setCmsVersion(orderMaterialDto.getCmsVersion());
    orderMaterialUEditorDto.setCmsUuid(orderMaterialDto.getCmsUuid());
    orderMaterialUEditorDto.setDescription(orderMaterialDto.getDescription());
  }

  public List<EldoradoBrandDto> findOnlines() {
    List<CriteriaDto> criterias = fillOnlineStandardCriteria();
    return fillBrandTree(criterias, false, true);
  }

  public List<EldoradoBrandDto> findOnlinesByName(String name) {
    List<CriteriaDto> criterias = fillOnlineStandardCriteria();
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_NAME, name));
    return fillBrandTree(criterias, false, true);
  }

  private List<CriteriaDto> fillOnlineStandardCriteria() {
    List<CriteriaDto> criterias = new ArrayList<CriteriaDto>();
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.MEDIA_SUPPORT, MediaSupportEnumDto.ONLINE.toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.MEDIA_TYPE, MediaTypeEnumDto.ELDORADO.toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.VALIDITY, new DateUtil("yyyyMMdd", getReferenceDate()).toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.STANDARD_HEADING_ELDORADO, getEldoradoHeading()));
    return criterias;
  }

  public void placeOnline(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = addPlacementGroup(offerIdDto);
    OrderPlacementGroupDto orderPlacementGroupDto = orderPlacementGroup.getOrderPlacementGroupDto();

    List<OptionOnlinePeriodDto> periodDtos = findOnlinePeriods(offerId);

    List<OrderPlacementManager> orderPlacements = orderPlacementGroup.findPlacements();
    for (OrderPlacementManager orderPlacement : orderPlacements) {
      if (orderPlacement.getOrderPlacementDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
        // place first period
        for (OptionOnlinePeriodDto optionDto : periodDtos) {
          orderPlacement.placeOption(optionDto);
          break;
        }
      }
      // place default option (depending of RSHM02)
      orderPlacement.placeDefaultOptions();
      // place default size (depending of RSHM02)
      List<OptionIdDto> existingOptionIdDtos = orderPlacement.fillExistingOptionIdDtos(OptionTypeEnumDto.SIZE);
      List<CriteriaDto> criterias = fillOnlineStandardCriteria();
      List<OptionDto> sizeDtos = orderPlacement.findOptions(OptionTypeEnumDto.SIZE, existingOptionIdDtos, criterias);
      for (OptionDto optionDto : sizeDtos) {
        orderPlacement.placeOption(optionDto);
        break;
      }

      orderPlacement.retrieveMediaProduction();
    }

    List<CriteriaDto> criterias = fillOnlineStandardCriteria();
    MediaIdDto mediaIdDto = orderPlacementGroupDto.getMedia().getParentMediaId();
    MediaDto mediaDto = getMediaService().retrieveMedia(mediaIdDto, criterias);
    orderPlacementGroupDto.setMediaLevel2(mediaDto);

    MediaIdDto brandIdDto = mediaDto.getParentMediaId();
    MediaDto brandDto = getMediaService().retrieveMedia(brandIdDto, criterias);
    orderPlacementGroupDto.setMediaLevel1(brandDto);

    OrderMaterialUEditorDto orderMaterialDto = new OrderMaterialUEditorDto();
    OrderMaterialManager orderMaterial = addMaterial(orderMaterialDto);
    for (OrderPlacementManager orderPlacement : orderPlacements) {
      if (orderPlacement.getOrderPlacementDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
        orderPlacement.getOrderPlacementDto().setMaterialId(orderMaterial.getOrderMaterialDto().getId());
      }
    }
  }

  public EldoradoBrandDto retrieveOnline(String offerId) {
    return fillBrand(offerId);
  }

  public Date retrieveOnlinePeriodValidityFrom(String offerId) {
    CheckDelay checkDelay = fillCheckDelay();

    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    Date initialDate = orderPlacementGroup.getOrderPlacementGroupDto().getMedia().getValidityFrom();
    List<OrderPlacementManager> orderPlacements = orderPlacementGroup.findPlacements();
    for (OrderPlacementManager orderPlacement : orderPlacements) {
      if (orderPlacement.getOrderPlacementDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
        if (initialDate.compareTo(checkDelay.from) <= 0) {
          initialDate = checkDelay.from;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(initialDate);
        cal.add(Calendar.MONTH, 1);
        Date finalDate = cal.getTime();
        List<OptionIssueDateDto> insertions = orderPlacement.findInsertions(initialDate, finalDate);
        for (OptionIssueDateDto item : insertions) {
          item.setDelayDate(calculateDelay(item.getDelayDate(), checkDelay));
          if (item.getDelayDate().compareTo(checkDelay.from) >= 0) {
            initialDate = item.getIssueDate();
            break;
          }
        }
      }
    }
    return initialDate;
  }

  public Date retrieveOnlinePeriodValidityUntil(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    return orderPlacementGroup.getOrderPlacementGroupDto().getMedia().getValidityUntil();
  }

  public List<OptionOnlinePeriodDto> findOnlinePeriods(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    List<CriteriaDto> criterias = fillOnlineStandardCriteria();
    List<OptionDto> optionDtos = orderPlacementGroup.findOptions(OptionTypeEnumDto.ONLINE_PERIOD, criterias);
    List<OptionOnlinePeriodDto> optionOnlinePeriodDtos = new ArrayList<OptionOnlinePeriodDto>();
    for (OptionDto optionDto : optionDtos) {
      optionOnlinePeriodDtos.add((OptionOnlinePeriodDto) optionDto);
    }
    return optionOnlinePeriodDtos;
  }

  public void placeOnlinePeriod(String offerId, String periodId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    OptionIdDto optionIdDto = new OptionIdDto();
    optionIdDto.setOwnerId("Pub2000");
    optionIdDto.setId(periodId);
    optionIdDto.setOptionType(OptionTypeEnumDto.ONLINE_PERIOD);
    List<OrderPlacementManager> orderPlacements = orderPlacementGroup.findPlacements();
    for (OrderPlacementManager orderPlacement : orderPlacements) {
      if (orderPlacement.getOrderPlacementDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
        orderPlacement.placeOption(optionIdDto);
      }
    }
  }

  public void placeOnlinePeriodDate(String offerId, Date startDate, Date endDate) {
    getOrderDto().setQuotationStatus(OrderQuotationStatusEnumDto.TODO);
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    List<OrderPlacementManager> orderPlacements = orderPlacementGroup.findPlacements();
    for (OrderPlacementManager orderPlacement : orderPlacements) {
      if (orderPlacement.getOrderPlacementDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
        OptionOnlinePeriodDto optionDto = (OptionOnlinePeriodDto) orderPlacement.retrieveOption(OptionTypeEnumDto.ONLINE_PERIOD);
        if (optionDto != null) {
          orderPlacement.removeInsertion();
          orderPlacement.placeInsertion(startDate);
          if (orderPlacement.retrieveInsertions() != null) {
            for (OrderInsertionDto insertionDto : orderPlacement.retrieveInsertions()) {
              insertionDto.setEndDate(endDate);
              insertionDto.setDuration(optionDto.getDuration());
            }
          }
        }
      }
    }
  }

  public OptionOnlinePeriodDto retrieveOnlinePeriod(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    List<OrderPlacementManager> orderPlacements = orderPlacementGroup.findPlacements();
    for (OrderPlacementManager orderPlacement : orderPlacements) {
      if (orderPlacement.getOrderPlacementDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
        OptionOnlinePeriodDto optionDto = (OptionOnlinePeriodDto) orderPlacement.retrieveOption(OptionTypeEnumDto.ONLINE_PERIOD);
        if (optionDto != null) {
          optionDto.setStartDate(retrieveOnlinePeriodStartDate(offerId));
          optionDto.setEndDate(retrieveOnlinePeriodEndDate(offerId));
          return optionDto;
        }
      }
    }
    return null;
  }

  public Date retrieveOnlinePeriodStartDate(String offerId) {
    OrderInsertionDto insertion = retrieveOnlinePeriodDate(offerId);
    if (insertion == null) {
      return null;
    }
    else {
      return insertion.getIssueDate();
    }
  }

  public Date retrieveOnlinePeriodEndDate(String offerId) {
    OrderInsertionDto insertion = retrieveOnlinePeriodDate(offerId);
    if (insertion == null) {
      return null;
    }
    else {
      return insertion.getEndDate();
    }
  }

  public OrderInsertionDto retrieveOnlinePeriodDate(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    List<OrderPlacementManager> orderPlacements = orderPlacementGroup.findPlacements();
    for (OrderPlacementManager orderPlacement : orderPlacements) {
      if (orderPlacement.getOrderPlacementDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
        if (orderPlacement.retrieveInsertions() != null) {
          for (OrderInsertionDto insertionDto : orderPlacement.retrieveInsertions()) {
            return insertionDto;
          }
        }
      }
    }
    return null;
  }

  public OrderMaterialUEditorDto retrieveOnlineMaterial(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    List<OrderPlacementManager> orderPlacements = orderPlacementGroup.findPlacements();
    for (OrderPlacementManager orderPlacement : orderPlacements) {
      if (orderPlacement.getOrderPlacementDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
        OrderMaterialManager orderMaterial = retrieveMaterial(orderPlacement.getOrderPlacementDto().getMaterialId());
        return (OrderMaterialUEditorDto) orderMaterial.getOrderMaterialDto();
      }
    }
    return null;
  }

  public String retrieveOnlineMaterialMediaId(String offerId) {
    MediaIdDto offerIdDto = fillMediaIdDto(offerId);
    OrderPlacementGroupManager orderPlacementGroup = retrievePlacementGroup(offerIdDto);
    List<OrderPlacementManager> orderPlacements = orderPlacementGroup.findPlacements();
    for (OrderPlacementManager orderPlacement : orderPlacements) {
      if (orderPlacement.getOrderPlacementDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
        String jnlcod = CodeList.extractCodeValue("JNLCOD",
            orderPlacement.getOrderPlacementDto().getMedia().getCodeList(),
            orderPlacement.getOrderPlacementDto().getMedia().getCodeValue());
        String jnlcods = CodeList.extractCodeValue("JNLCODS",
            orderPlacement.getOrderPlacementDto().getMedia().getCodeList(),
            orderPlacement.getOrderPlacementDto().getMedia().getCodeValue());
        return jnlcod + jnlcods;
      }
    }
    return null;
  }

  public String retrieveOnlineMaterialCmsUuidAlreadyDone(String offerId) {
    OrderMaterialUEditorDto material = retrieveOnlineMaterial(offerId);
    if (material != null && StringUtil.isEmpty(material.getCmsUuid())) {
      List<OrderMaterialManager> orderMaterials = findMaterials();
      for (OrderMaterialManager orderMaterial : orderMaterials) {
        OrderMaterialDto orderMaterialDto = orderMaterial.getOrderMaterialDto();
        if (orderMaterialDto instanceof OrderMaterialUEditorDto
            && !(orderMaterialDto instanceof OrderMaterialUEditorPrintDto)) {
          OrderMaterialUEditorDto orderMaterialUEditorDto = (OrderMaterialUEditorDto) orderMaterialDto;
          if (!StringUtil.isEmpty(orderMaterialUEditorDto.getCmsUuid())) {
            return orderMaterialUEditorDto.getCmsUuid();
          }
        }
      }
    }
    return null;
  }

  public void updateOnlineMaterial(String offerId, OrderMaterialUEditorDto orderMaterialDto) {
    OrderMaterialUEditorDto orderMaterialUEditorDto = retrieveOnlineMaterial(offerId);
    orderMaterialUEditorDto.setCmsVersion(orderMaterialDto.getCmsVersion());
    orderMaterialUEditorDto.setCmsUuid(orderMaterialDto.getCmsUuid());
    if (StringUtil.isEmpty(orderMaterialDto.getDescription())) {
      orderMaterialDto.setDescription("Online Content");
    }
    // EV en principe concernant saisi sur page calendrier
    if (!StringUtil.isEmpty(getOrderDto().getDescription())) {
      orderMaterialUEditorDto.setDescription(getOrderDto().getDescription());
    }
    else {
      if (orderMaterialDto.getDescription().length() > 30) {
        orderMaterialUEditorDto.setDescription(orderMaterialDto.getDescription().substring(0, 30));
      }
      else {
        orderMaterialUEditorDto.setDescription(orderMaterialDto.getDescription());
      }
      getOrderDto().setDescription(orderMaterialUEditorDto.getDescription());
    }
  }

  private MediaIdDto fillMediaIdDto(String offerId) {
    MediaIdDto offerIdDto = new MediaIdDto();
    offerIdDto.setOwnerId(MediaP2000.MEDIA_P2000);
    offerIdDto.setId(offerId);
    return offerIdDto;
  }

  /**
   * @param zipCode
   * @return
   */
  public CoordinatesResultDto findZipCodeCoordinates(String zipCode) {
    return localizationService.findZipCodeCoordinates(zipCode);
  }

  private List<EldoradoSummaryInvoiceDto> fillInvoices(OrderDto orderDto) {
    List<EldoradoSummaryInvoiceDto> summaryInvoices = (new ArrayList<EldoradoSummaryInvoiceDto>());
    EldoradoSummaryInvoiceDto summaryInvoice;
    Map<String, String> invoiceMap = new HashMap<String, String>();

    for (OrderPlacementGroupDto placementGroups : orderDto.getPlacementGroups()) {
      if (placementGroups.getPlacements() != null) {
        for (OrderPlacementDto placements : placementGroups.getPlacements()) {
          if (placements.getInsertions() != null) {
            for (OrderInsertionDto insertions : placements.getInsertions()) {
              if (insertions.getStatus() != null && insertions.getStatus().getInvoice() != null
                  && !StringUtil.isEmpty(insertions.getStatus().getInvoice().getId())) {
                if (!invoiceMap.containsKey(insertions.getStatus().getInvoice().getId()
                    + insertions.getStatus().getInvoice().getDate())) {
                  invoiceMap.put(insertions.getStatus().getInvoice().getId()
                      + insertions.getStatus().getInvoice().getDate(), "true");
                  summaryInvoice = new EldoradoSummaryInvoiceDto();
                  summaryInvoice.setId(insertions.getStatus().getInvoice().getId());
                  summaryInvoice.setDate(insertions.getStatus().getInvoice().getDate());
                  summaryInvoices.add(summaryInvoice);
                }
              }
            }
          }
        }
      }
    }
    return summaryInvoices;

  }

  private boolean isUpsellingPlacementGroupWithoutContent(OrderPlacementGroupDto placementGroup) {
    if (placementGroup.getUpselling() == null) {
      return false;
    }
    if (placementGroup.getUpselling().getTreatmentType().equals(MediaUpsellingTreatmentTypeEnumDto.LOCALPOINT)
        || placementGroup.getUpselling().getTreatmentType().equals(
            MediaUpsellingTreatmentTypeEnumDto.PRINTPLUS_WITHOUT_ONLINE_CONTENT)) {
      return true;
    }
    return false;
  }

  public EldoradoSummaryDto retrieveSummary(boolean isShortView, boolean isWithQuotate, boolean isWithUpSelling) {
    EldoradoSummaryDto summary = new EldoradoSummaryDto();

    OrderDto orderDto = getOrderDto();
    OrderValorizationDto valorization;
    OrderValorizationPriceComponentDto priceComponent;

    summary.setStatus(orderDto.getStatus());
    summary.setAllPublicationDone(getIsAllPublicationsDone(summary.getStatus()));
    summary.setDescription(orderDto.getDescription());
    summary.setEldoradoHeading(getEldoradoHeading());

    boolean isValid = true;
    summary.setQuotationMessages(new ArrayList<OrderQuotationMessageDto>());

    summary.setApplication(orderDto.getInfo().getApplication());
    if (orderDto.getStatus() != OrderStatusEnumDto.DRAFT) {
      summary.setInvoices(fillInvoices(orderDto));
    }

    if (orderDto.getStatus() == OrderStatusEnumDto.DRAFT) {
      List<OrderQuotationMessageDto> messages = checkMaterials();
      // reset currentUeditoPrintJobNumber
      this.currentUeditoPrintJobNumber = "";
      if (!messages.isEmpty()) {
        isValid = false;
        for (OrderQuotationMessageDto quotationMessage : messages) {
          orderDto.setQuotationStatus(OrderQuotationStatusEnumDto.TODO);
          summary.getQuotationMessages().add(quotationMessage);
        }
      }
    }

    if (orderDto.getStatus() == OrderStatusEnumDto.DRAFT || orderDto.getStatus() == OrderStatusEnumDto.IN_MODIFICATION) {
      boolean isToDelete = false;
      if (orderDto.getStatus() == OrderStatusEnumDto.DRAFT) {
        isToDelete = true;
      }
      List<OrderQuotationMessageDto> messages = checkInsertions(isToDelete);
      if (!messages.isEmpty()) {
        if (orderDto.getStatus() == OrderStatusEnumDto.IN_MODIFICATION) {
          isValid = false;
        }
        for (OrderQuotationMessageDto quotationMessage : messages) {
          orderDto.setQuotationStatus(OrderQuotationStatusEnumDto.TODO);
          summary.getQuotationMessages().add(quotationMessage);
        }
      }
    }

    for (OrderPlacementGroupManager placementGroupManager : findPlacementGroups()) {
      if (placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getMediaType().getType() == MediaTypeEnumDto.ELDORADO
          || placementGroupManager.getOrderPlacementGroupDto().getUpselling() != null) {
        String offerId = placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getId();
        for (OrderPlacementManager placementManager : placementGroupManager.findPlacements()) {
          // check material
          if (placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
            OrderMaterialDto material = retrieveBasePrintMaterial(offerId);
            if (material == null) {
              isValid = false;
              break;
            }
            if (material instanceof OrderMaterialUEditorPrintDto) {
              OrderMaterialUEditorPrintDto materialUEditorPrintDto = (OrderMaterialUEditorPrintDto) material;
              if (StringUtil.isEmpty(materialUEditorPrintDto.getCmsUuid())) {
                isValid = false;
                break;
              }
            }
            if (material instanceof OrderMaterialUploadDto) {
              OrderMaterialUploadDto materialUploadPrintDto = (OrderMaterialUploadDto) material;
              if (StringUtil.isEmpty(materialUploadPrintDto.getFileName())) {
                isValid = false;
                break;
              }
              if (orderDto.getStatus() == OrderStatusEnumDto.DRAFT && !existPdf(materialUploadPrintDto.getFileName())) {
                materialUploadPrintDto.setFileName("");
                isValid = false;
                break;

              }
            }
            // if (retrievePrintPlusMaterial(offerId) != null
            // &&
            // StringUtil.isEmpty(retrievePrintPlusMaterial(offerId).getCmsUuid()))
            // {
            // isValid = false;
            // break;
            // }
          }
          else if (isUpsellingPlacementGroupWithoutContent(placementGroupManager.getOrderPlacementGroupDto())) {
            // TODO: Traitement AdScreen (Localpoint)
          }
          else {
            if (retrieveOnlineMaterial(offerId) == null
                || StringUtil.isEmpty(retrieveOnlineMaterial(offerId).getCmsUuid())) {
              isValid = false;
              break;
            }
          }
          // check insertions
          if (placementManager.getOrderPlacementDto().getInsertions() == null
              || placementManager.getOrderPlacementDto().getInsertions().isEmpty()) {
            // P+????????
            if (placementGroupManager.getOrderPlacementGroupDto().getUpselling() == null) {
              isValid = false;
              break;
            }
          }
        }
      }
      else {
        isValid = false;
      }
      if (!isValid) {
        break;
      }
    }

    if (isWithQuotate && summary.getStatus() == OrderStatusEnumDto.DRAFT) {
      if (isValid) {
        quotateOrder(isWithUpSelling);
        if (orderDto.getQuotationMessages() != null && !orderDto.getQuotationMessages().isEmpty()) {
          isValid = false;
          for (OrderQuotationMessageDto quotationMessage : orderDto.getQuotationMessages()) {
            summary.getQuotationMessages().add(quotationMessage);
          }
        }
        else {
          if (!orderDto.getId().getId().equals(orderDto.getCodeValue())) {
            deleteOrderDraft(getOrderDto().getId().getId());
          }
          orderDto.getId().setId(orderDto.getCodeValue());
          saveOrderDraft();
        }
      }
    }
    summary.setValid(isValid);

    if (orderDto.getStatus() != OrderStatusEnumDto.DRAFT && orderDto.getStatus() != OrderStatusEnumDto.TO_RUN) {
      summary.setCommandNumber(orderDto.getId().getId());
      LOGGER.info("Id for not draft :" + orderDto.getId().getId());
    }
    else {
      if (orderDto.getCodeValue() != null) {
        summary.setCommandNumber(orderDto.getCodeValue());
        LOGGER.info("Id for draft :" + orderDto.getCodeValue());
      }
      else {
        summary.setCommandNumber(orderDto.getId().getId());
        LOGGER.info("Id for not draft :" + orderDto.getId().getId());
      }
    }

    if (orderDto.getStatus() != OrderStatusEnumDto.DRAFT) {
      // To show the amount
      isValid = true;
    }

    valorization = orderDto.getValorization();
    if (isValid && valorization != null) {
      priceComponent = valorization.retrievePriceComponent(OrderValorizationPriceItemEnumDto.TOTAL_WITHOUT_VAT,
          OrderValorizationPriceStepEnumDto.NET);
      if (priceComponent != null) {
        summary.setCurrency(priceComponent.getCurrency());
        summary.setAmountWithoutVAT(priceComponent.getAmount());
      }
      priceComponent = valorization.retrievePriceComponent(OrderValorizationPriceItemEnumDto.VAT,
          OrderValorizationPriceStepEnumDto.NET);
      if (priceComponent != null) {
        summary.setAmountVAT(priceComponent.getAmount());
        summary.setPercentVAT(priceComponent.getCalculation().getPercent());
        if (priceComponent.getCalculation().getPriceType() != null
            && priceComponent.getCalculation().getPriceType() == OrderValorizationCalculationPriceTypeEnumDto.VAT_INCLUDED) {
          summary.setIncludedVAT(true);
        }
        else {
          summary.setIncludedVAT(false);
        }
      }
      priceComponent = valorization.retrievePriceComponent(OrderValorizationPriceItemEnumDto.TOTAL_WITH_VAT,
          OrderValorizationPriceStepEnumDto.NET);
      if (priceComponent != null) {
        summary.setAmountWithVAT(priceComponent.getAmount());
      }
    }

    if (isShortView) {
      return summary;
    }

    if (isValid && valorization != null) {
      priceComponent = valorization.retrievePriceComponent(OrderValorizationPriceItemEnumDto.TOTAL_SURCHARGE,
          OrderValorizationPriceStepEnumDto.NET);
      if (priceComponent == null) {
        summary.setAmountSurcharge(BigDecimal.ZERO);
      }
      else {
        summary.setAmountSurcharge(priceComponent.getAmount());
      }
      summary.setSurcharges(retrieveSummarySurcharges(valorization));
    }

    List<EldoradoSummaryOfferDto> summaryOffers = new ArrayList<EldoradoSummaryOfferDto>();
    summary.setOffers(summaryOffers);
    for (OrderPlacementGroupManager placementGroupManager : findPlacementGroups()) {
      OrderPlacementGroupDto placementGroup = placementGroupManager.getOrderPlacementGroupDto();
      if (placementGroup.getUpselling() != null) {
        continue;
      }

      EldoradoSummaryOfferDto summaryOffer = new EldoradoSummaryOfferDto();
      summaryOffers.add(summaryOffer);

      summaryOffer.setMediaSupport(placementGroup.getMedia().getMediaId().getMediaType().getSupport());
      summaryOffer.setOfferId(placementGroup.getMedia().getMediaId().getId());
      summaryOffer.setOfferName(placementGroup.getMedia().getName(getLanguage()).getName());
      if (placementGroup.getMediaLevel1() != null) {
        summaryOffer.setBrandId(placementGroup.getMediaLevel1().getMediaId().getId());
        summaryOffer.setBrandName(placementGroup.getMediaLevel1().getName(getLanguage()).getName());
        summaryOffer.setBrandLogo(placementGroup.getMediaLevel1().getName(getLanguage()).getGif());
      }
      else {
        summaryOffer.setBrandName(placementGroup.getMedia().getName(getLanguage()).getName());
      }
      if (placementGroup.getMediaLevel2() != null && placementGroup.getMediaLevel2().getMediaId() != null) {
        summaryOffer.setMediaId(placementGroup.getMediaLevel2().getMediaId().getId());
        summaryOffer.setMediaName(placementGroup.getMediaLevel2().getName(getLanguage()).getName());
      }
      else {
        summaryOffer.setMediaName(placementGroup.getMedia().getName(getLanguage()).getName());
      }
      String subHeadingName = "";
      OrderMaterialDto material;
      if (summaryOffer.getMediaSupport() == MediaSupportEnumDto.PRINT) {
        OrderPlacementManager orderPlacementManager = placementGroupManager.retrievePlacement(placementGroup.getMedia().getMediaId());
        if (orderPlacementManager.getOrderPlacementDto().getOptions() != null) {
          // EV Rajout rubrique PUB2000 pour les commandes non-eldorado
          if (summary.getEldoradoHeading() == null) {
            OptionDto optionDto = orderPlacementManager.retrieveOption(OptionTypeEnumDto.HEADING);
            if (optionDto != null && optionDto.getNames() != null && optionDto.getNames().size() > 0) {
              summary.setEldoradoHeading(optionDto.getName(getLanguage()).getName());
            }
          }
          OptionDto optionDto = orderPlacementManager.retrieveOption(OptionTypeEnumDto.SUB_HEADING);
          if (optionDto != null && optionDto.getNames() != null && optionDto.getNames().size() > 0) {
            subHeadingName = optionDto.getName(getLanguage()).getName();
          }
        }
        material = retrieveBasePrintMaterial(summaryOffer.getOfferId());
        if (material != null) {
          if (material instanceof OrderMaterialUEditorPrintDto) {
            OrderMaterialUEditorPrintDto materialUEditorPrintDto = (OrderMaterialUEditorPrintDto) material;
            summaryOffer.setMaterialCmsUuid(materialUEditorPrintDto.getCmsUuid());
            summaryOffer.setUeditor(true);
          }
          if (material instanceof OrderMaterialUploadDto) {
            OrderMaterialUploadDto materialUploadDto = (OrderMaterialUploadDto) material;
            if (orderDto.getCodeValue() != null && orderDto.getStatus() == OrderStatusEnumDto.DRAFT) {
              checkRenameFile(orderDto.getCodeValue(), summaryOffer.getOfferId(), materialUploadDto);
            }
            summaryOffer.setUploadThumbnailurl(materialUploadDto.getFileUrl()
                + materialUploadDto.getFileName().replace(".pdf", ".jpg"));
            summaryOffer.setHighresUrl(materialUploadDto.getHighresUrl());
            summaryOffer.setUeditor(false);
          }
        }
      }
      else if (summaryOffer.getMediaSupport() == MediaSupportEnumDto.ONLINE) {
        OrderMaterialUEditorDto materialUEditorDto = retrieveOnlineMaterial(summaryOffer.getOfferId());
        if (materialUEditorDto != null) {
          summaryOffer.setMaterialCmsUuid(materialUEditorDto.getCmsUuid());
          summaryOffer.setUeditor(true);
        }
      }
      summaryOffer.setSubHeadingName(subHeadingName);

      String sizeName = "";
      if (summaryOffer.getMediaSupport() == MediaSupportEnumDto.PRINT) {
        OrderPlacementManager orderPlacementManager = placementGroupManager.retrievePlacement(placementGroup.getMedia().getMediaId());
        if (orderPlacementManager.getOrderPlacementDto().getOptions() != null) {
          OptionSizeDto optionSizeDto = (OptionSizeDto) orderPlacementManager.retrieveOption(OptionTypeEnumDto.SIZE);
          if (optionSizeDto != null && optionSizeDto.getNames() != null && optionSizeDto.getNames().size() > 0) {
            sizeName = optionSizeDto.getName(getLanguage()).getNameVariable();
            summaryOffer.setSizeMeasurementType(optionSizeDto.getMeasurementType());
            switch (optionSizeDto.getMeasurementType()) {
              case MILLIMETERS:
                sizeName = String.format(sizeName, optionSizeDto.getHeight());
                break;
              case WORDS:
              case LINES:
              case CHARS:
                sizeName = String.format(sizeName, optionSizeDto.getTotal());
                break;
            }
          }
        }
      }
      summaryOffer.setSizeName(sizeName);

      valorization = placementGroup.getValorization();
      if (isValid && valorization != null) {
        priceComponent = valorization.retrievePriceComponent(OrderValorizationPriceItemEnumDto.TOTAL_GROUP,
            OrderValorizationPriceStepEnumDto.NET);
        summaryOffer.setAmount(priceComponent.getAmount());
      }
      List<EldoradoSummaryOfferPlacementDto> summaryPlacements = new ArrayList<EldoradoSummaryOfferPlacementDto>();
      summaryOffer.setPlacements(summaryPlacements);
      for (OrderPlacementManager placementManager : placementGroupManager.findPlacements()) {
        OrderPlacementDto placement = placementManager.getOrderPlacementDto();
        EldoradoSummaryOfferPlacementDto summaryPlacement = new EldoradoSummaryOfferPlacementDto();
        summaryPlacements.add(summaryPlacement);

        summaryPlacement.setMedia(placement.getMedia());
        summaryPlacement.setPlacementName(placement.getMedia().getName(getLanguage()).getName());
        List<EldoradoSummaryOfferInsertionDto> summaryInsertions = new ArrayList<EldoradoSummaryOfferInsertionDto>();
        summaryPlacement.setInsertions(summaryInsertions);

        if (placementManager.getOrderPlacementDto().getInsertions() != null) {
          for (OrderInsertionDto insertion : placementManager.getOrderPlacementDto().getInsertions()) {
            EldoradoSummaryOfferInsertionDto summaryInsertion = new EldoradoSummaryOfferInsertionDto();
            summaryInsertions.add(summaryInsertion);

            summaryInsertion.setIssueDate(insertion.getIssueDate());
            summaryInsertion.setEndDate(insertion.getEndDate());
            summaryInsertion.setDuration(insertion.getDuration());
            if (!StringUtil.isEmpty(insertion.getCodeList())) {
              String jnlcodc = CodeList.extractCodeValue("JNLCODC", insertion.getCodeList(), insertion.getCodeValue());
              if (MediaP2000.isExtendedCirculation(jnlcodc)) {
                summaryInsertion.setExtendedCirculation(true);
              }
            }

            valorization = insertion.getValorization();
            if (isValid
                && valorization != null
                && valorization.retrievePriceComponent(OrderValorizationPriceItemEnumDto.INSERTION,
                    OrderValorizationPriceStepEnumDto.GROSS) != null) {
              priceComponent = valorization.retrievePriceComponent(OrderValorizationPriceItemEnumDto.INSERTION,
                  OrderValorizationPriceStepEnumDto.GROSS);
              summaryInsertion.setAmount(priceComponent.getAmount());
              summaryInsertion.setCalculation(priceComponent.getCalculation());
              priceComponent = valorization.retrievePriceComponent(OrderValorizationPriceItemEnumDto.INSERTION,
                  OrderValorizationPriceStepEnumDto.DISCOUNT);
              summaryInsertion.setDiscount(priceComponent.getAmount());
              summaryInsertion.setDiscountCalculation(priceComponent.getCalculation());

              summaryInsertion.setSurcharges(retrieveSummarySurcharges(valorization));
            }
          }
        }

        if (isValid && placementManager.getOrderPlacementDto().getValorization() != null) {
          summaryPlacement.setSurcharges(retrieveSummarySurcharges(placementManager.getOrderPlacementDto().getValorization()));
        }
      }

      List<EldoradoSummarySurchargeDto> summaryOfferGroupSurcharges = new ArrayList<EldoradoSummarySurchargeDto>();
      for (EldoradoSummaryOfferPlacementDto summaryPlacement : summaryOffer.getPlacements()) {
        if (summaryPlacement.getSurcharges() != null && !summaryPlacement.getSurcharges().isEmpty()) {
          summaryOfferGroupSurcharges = retrieveSummaryGroupSurcharges(summaryOfferGroupSurcharges,
              summaryPlacement.getSurcharges());
        }
        for (EldoradoSummaryOfferInsertionDto summaryInsertion : summaryPlacement.getInsertions()) {
          if (summaryInsertion.getSurcharges() != null && !summaryInsertion.getSurcharges().isEmpty()) {
            summaryOfferGroupSurcharges = retrieveSummaryGroupSurcharges(summaryOfferGroupSurcharges,
                summaryInsertion.getSurcharges());
          }
        }
      }
      summaryOffer.setGroupSurcharges(summaryOfferGroupSurcharges);

      if (summaryOffer.getMediaSupport() == MediaSupportEnumDto.PRINT) {
        Collection<OrderPlacementGroupDto> placementsLocalPoint = retrieveAssociatedPlacementUpselling(orderDto,
            placementGroup);
        if (placementsLocalPoint != null && !placementsLocalPoint.isEmpty()) {
          summaryOffer.setLocalPoints(new ArrayList<EldoradoSummaryOfferLocalPointDto>());
          for (OrderPlacementGroupDto group : placementsLocalPoint) {
            EldoradoSummaryOfferLocalPointDto summaryOfferLocalPoint = new EldoradoSummaryOfferLocalPointDto();
            summaryOfferLocalPoint.setOfferId(group.getMedia().getMediaId().getId());
            summaryOfferLocalPoint.setMediaSite(group.getMediaLevel2().getName(getLanguage()).getName());
            summaryOfferLocalPoint.setUpSellingGenerationType(group.getUpselling().getGenerationType());
            summaryOfferLocalPoint.setUpSellingTreatmentType(group.getUpselling().getTreatmentType());

            if (group.getUpselling().getTreatmentType().equals(
                MediaUpsellingTreatmentTypeEnumDto.PRINTPLUS_WITH_ONLINE_CONTENT)) {
              // old OrderMaterialUEditorDto materialPrintPlus =
              // retrievePrintPlusMaterial(summaryOffer.getOfferId());
              for (OrderPlacementDto placement : group.getPlacements()) {
                if (placement.getMedia().getMediaId().equals(group.getMedia().getMediaId())) {
                  OrderMaterialUEditorDto materialPrintPlus = (OrderMaterialUEditorDto) retrieveMaterial(
                      placement.getMaterialId()).getOrderMaterialDto();
                  if (materialPrintPlus != null) {

                    // OrderPlacementGroupManager orderPlacementGroup =
                    // retrievePlacementGroup(fillMediaIdDto(summaryOffer.getOfferId()));
                    // OrderPlacementManager orderPlacement =
                    // orderPlacementGroup.retrievePlacement(fillMediaIdDto(summaryOffer.getOfferId()));
                    OrderMediaPrintPlusDto printPlus = retrievePrintPlus(summaryOffer.getOfferId());
                    EldoradoSummaryOfferPrintPlusDto summaryOfferPrintPlus = new EldoradoSummaryOfferPrintPlusDto();
                    summaryOfferPrintPlus.setDuration(printPlus.getDuration());
                    summaryOfferPrintPlus.setMediaSite(printPlus.getName());
                    summaryOfferPrintPlus.setMaterialCmsUuid(materialPrintPlus.getCmsUuid());
                    summaryOffer.setPrintPlus(summaryOfferPrintPlus);

                    summaryOfferLocalPoint.setMaterialCmsUuid(materialPrintPlus.getCmsUuid());
                    summaryOfferLocalPoint.setOnlineduration(printPlus.getDuration());

                  }
                }
              }
            }
            summaryOffer.getLocalPoints().add(summaryOfferLocalPoint);

            valorization = group.getValorization();
            EldoradoSummarySurchargeDto localPointSurcharge = new EldoradoSummarySurchargeDto();
            localPointSurcharge.setSurchargeName(group.getMediaLevel2().getName(getLanguage()).getName());
            BigDecimal totalGroup = new BigDecimal(0);
            if (summaryOffer.getAmount() != null) {
              totalGroup = summaryOffer.getAmount();
            }
            if (valorization != null) {
              priceComponent = valorization.retrievePriceComponent(OrderValorizationPriceItemEnumDto.TOTAL_GROUP,
                  OrderValorizationPriceStepEnumDto.NET);
              localPointSurcharge.setAmount(priceComponent.getAmount());
              totalGroup = totalGroup.add(priceComponent.getAmount());
            }
            summaryOffer.setAmount(totalGroup);
            summaryOffer.getGroupSurcharges().add(localPointSurcharge);
          }
        }
      }
    }

    for (EldoradoSummaryOfferDto summaryOffer : summary.getOffers()) {
      EldoradoSummaryOfferStatusDto status = new EldoradoSummaryOfferStatusDto();
      summaryOffer.setStatus(status);

      status.setValid(true);
      status.setInsertionValid(true);
      for (EldoradoSummaryOfferPlacementDto summaryPlacement : summaryOffer.getPlacements()) {
        if (summaryPlacement.getInsertions().isEmpty()) {
          status.setValid(false);
          status.setInsertionValid(false);
          break;
        }
      }
      if (summaryOffer.getMediaSupport() == MediaSupportEnumDto.PRINT) {
        status.setMaterialPrintValid(true);
        if (summaryOffer.getMaterialCmsUuid() == null && summaryOffer.getUploadThumbnailurl() == null) {
          status.setValid(false);
          status.setMaterialPrintValid(false);
        }
        status.setMaterialOnlineValid(true);
        if (summaryOffer.getPrintPlus() != null && summaryOffer.getPrintPlus().getMaterialCmsUuid() == null) {
          status.setValid(false);
          status.setMaterialOnlineValid(false);
        }
      }
      else if (summaryOffer.getMediaSupport() == MediaSupportEnumDto.ADSCREEN) {
        status.setValid(true);
      }
      else {
        status.setMaterialOnlineValid(true);
        if (summaryOffer.getMaterialCmsUuid() == null) {
          status.setValid(false);
          status.setMaterialOnlineValid(false);
        }
      }
    }

    return summary;
  }

  private void checkRenameFile(String orderID, String offerId, OrderMaterialUploadDto materialUploadDto) {

    if (!materialUploadDto.getFileName().contains(orderID)) {
      String inputFilePath = pdfUploadBaseDir + File.separator;
      String newFileName;
      String oldFileName;
      String saveOldFileName = materialUploadDto.getFileName();
      if (materialUploadDto.getFileName().contains(PDF_SUFFIX_VALIDATED)) {
        // rename 2 fichiers
        oldFileName = (materialUploadDto.getFileName());
        newFileName = generatePdfFilename(orderID, offerId, PDF_SUFFIX_VALIDATED);
        if (copyFile(inputFilePath + oldFileName, inputFilePath + newFileName, true)) {
          materialUploadDto.setFileName(newFileName);
        }
        oldFileName = (saveOldFileName.replace(PDF_SUFFIX_VALIDATED, PDF_SUFFIX_UPLOADED));
        newFileName = generatePdfFilename(orderID, offerId, PDF_SUFFIX_UPLOADED);
        copyFile(inputFilePath + oldFileName, inputFilePath + newFileName, true);
        File thumbnailFile = new File(inputFilePath + saveOldFileName.replace(".pdf", ".jpg"));
        if (thumbnailFile.exists()) {
          newFileName = generatePdfFilename(orderID, offerId, PDF_SUFFIX_VALIDATED);
          copyFile(inputFilePath + saveOldFileName.replace(".pdf", ".jpg"),
              inputFilePath + newFileName.replace(".pdf", ".jpg"), true);
        }
      }
    }
  }

  private boolean existPdf(String fileName) {

    String inputFilePath = pdfUploadBaseDir + File.separator;
    File file = new File(inputFilePath + fileName);
    return file.exists();
  }

  private boolean copyFile(String oldFileName, String newFileName, boolean deleteOld) {
    try {
      File oldFile = new File(oldFileName);
      File newFile = new File(newFileName);
      FileUtils.copyFile(oldFile, newFile);
      if (deleteOld) {
        FileUtils.deleteQuietly(oldFile);
      }
    }
    catch (Exception e) {
      LOGGER.error("Error occured during copy File", e);
      e.printStackTrace();
      return false;
    }
    return true;
  }

  private Collection<OrderPlacementGroupDto> retrieveAssociatedPlacementUpselling(OrderDto orderDto,
      OrderPlacementGroupDto placementGroup) {

    Collection<OrderPlacementGroupDto> groups = new ArrayList<OrderPlacementGroupDto>();
    String refJNLCOD = CodeList.extractCodeValue("JNLCOD", placementGroup.getMedia().getCodeList(),
        placementGroup.getMedia().getCodeValue());
    String refJNLCODS = CodeList.extractCodeValue("JNLCODS", placementGroup.getMedia().getCodeList(),
        placementGroup.getMedia().getCodeValue());
    String refJNLGRC = CodeList.extractCodeValue("JNLGRC", placementGroup.getMedia().getCodeList(),
        placementGroup.getMedia().getCodeValue());

    for (OrderPlacementGroupDto pg : orderDto.getPlacementGroups()) {
      if (pg.getUpselling() != null) {
        try {
          String upJNLCOD = CodeList.extractCodeValue("JNLCOD", pg.getUpselling().getCodeList(),
              pg.getUpselling().getCodeValue());
          String upJNLCODS = CodeList.extractCodeValue("JNLCODS", pg.getUpselling().getCodeList(),
              pg.getUpselling().getCodeValue());
          String upJNLGRC = CodeList.extractCodeValue("JNLGRC", pg.getUpselling().getCodeList(),
              pg.getUpselling().getCodeValue());

          if (upJNLCOD != null && upJNLGRC != null && upJNLCOD.equals(refJNLCOD) && upJNLGRC.equals(refJNLGRC)) {
            groups.add(pg);
          }
        }
        catch (Exception e) {
          return null;
        }
      }
    }
    return groups;
  }

  private List<EldoradoSummarySurchargeDto> retrieveSummarySurcharges(OrderValorizationDto valorization) {
    List<EldoradoSummarySurchargeDto> surcharges = new ArrayList<EldoradoSummarySurchargeDto>();

    for (OrderValorizationPriceGroupDto priceGroup : valorization.getPriceGroups()) {
      if (priceGroup.getPriceItem() == OrderValorizationPriceItemEnumDto.SURCHARGE) {
        EldoradoSummarySurchargeDto summarySurcharge = new EldoradoSummarySurchargeDto();
        surcharges.add(summarySurcharge);
        summarySurcharge.setSurchargeName(priceGroup.getDescription());
        OrderValorizationPriceComponentDto priceComponent = priceGroup.retrievePriceComponent(OrderValorizationPriceStepEnumDto.NET);
        summarySurcharge.setAmount(priceComponent.getAmount());
      }
    }

    return surcharges;
  }

  private List<EldoradoSummarySurchargeDto> retrieveSummaryGroupSurcharges(
      List<EldoradoSummarySurchargeDto> groupSurcharges, List<EldoradoSummarySurchargeDto> surcharges) {
    for (EldoradoSummarySurchargeDto surcharge : surcharges) {
      boolean isSurchargeFound = false;
      for (EldoradoSummarySurchargeDto groupSurcharge : groupSurcharges) {
        if (groupSurcharge.getSurchargeName().equals(surcharge.getSurchargeName())) {
          groupSurcharge.setAmount(groupSurcharge.getAmount().add(surcharge.getAmount()));
          isSurchargeFound = true;
          break;
        }
      }
      if (!isSurchargeFound) {
        groupSurcharges.add(surcharge);
      }
    }
    return groupSurcharges;
  }

  public void setUserCookie(String cookie) {
    getOrderDto().getInfo().setCookieId(cookie);
  }

  public boolean isInvoicePayment() {
    if (getOrderDto().getInfo().getUser().getCodeValue().contains(USER_ANONYMOUS)) {
      return false;
    }
    return this.paymentMode != null && this.paymentMode.equals(EldoradoPaymentModeEnumDto.INVOICE_PAYMENT);
  }

  public boolean isUserRegistred() {
    if (getOrderDto().getInfo().getUser().getCodeValue().contains(USER_ANONYMOUS)) {
      return false;
    }
    return true;
  }

  public boolean loadOrderDraft(String orderId) {
    List<OrderCriteriaDto> criteriaDtos = new ArrayList<OrderCriteriaDto>();
    String user = addBpoPrefix(getOrderDto().getInfo().getUser().getCodeValue());
    if (!user.contains(USER_ANONYMOUS)) {
      criteriaDtos.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.USER, user));
    }
    if (orderId != null && !orderId.trim().isEmpty()) {
      criteriaDtos.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.ORDER_UUID, orderId));
    }
    else {
      criteriaDtos.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.COOKIE, getOrderDto().getInfo().getCookieId()));
    }
    final List<OrderDto> orderDtos = getOrderService().findOrderDrafts(criteriaDtos);
    int n = orderDtos.size();
    for (int i = 0; i < n; i++) {
      if (orderDtos.get(i).getStatus() != OrderStatusEnumDto.DRAFT) {
        orderDtos.remove(i);
        i--;
        n--;
      }
    }
    if (!orderDtos.isEmpty()) {
      setOrderDto(orderDtos.get(0));
      initUserInfo();
      checkInsertions(true);
      return true;
    }
    return false;
  }

  public PartnerDto getUserInfo() {
    return getOrderDto().getInfo().getUser();
  }

  private String removeBpoPrefix(String userId) {
    return EldoradoTools.removeBPOPrefix(
        PropertyLoader.getPropertyWithContext(this, "Application.defaultPartner").substring(0, 2), userId);
  }

  private String addBpoPrefix(String userId) {
    return EldoradoTools.addBPOPrefix(
        PropertyLoader.getPropertyWithContext(this, "Application.defaultPartner").substring(0, 2), userId);
  }

  public void setUserInfo(PartnerDto user) {
    user.getId().setId(user.getId().getId().toUpperCase());
    user.setCodeValue(user.getCodeValue().toUpperCase());
    getOrderDto().getInfo().setUser(user);
    getOrderDto().setQuotationStatus(OrderQuotationStatusEnumDto.TODO);
    if (user.getCodeValue().contains(USER_ANONYMOUS)) {
      this.paymentMode = EldoradoPaymentModeEnumDto.ONLINE_PAYMENT;
    }
    else {
      List<PartnerLinkDto> subLinks = new ArrayList<PartnerLinkDto>();
      subLinks = user.getLinksByType(PartnerLinkTypeEnumDto.ADVERTISER);
      if (subLinks != null && !subLinks.isEmpty()) {
        getOrderDto().getAdvertiser().setCodeList(subLinks.get(0).getPartner().getCodeList());
        getOrderDto().getAdvertiser().setCodeValue(subLinks.get(0).getPartner().getCodeValue());
      }
      this.paymentMode = eldoradoFlowService.retrievePaymentMode(removeBpoPrefix(PartnerP2000.extractPartnerIdUser(user.getId().getId())));
      if (this.paymentMode == null) {
        this.paymentMode = EldoradoPaymentModeEnumDto.ONLINE_PAYMENT;
      }
    }
  }

  public void initUserInfo() {
    PartnerDto user = new PartnerDto();
    PartnerIdDto userId = new PartnerIdDto();
    userId.setOwnerId(PartnerConstantP2000.PARTNER_P2000);
    userId.setId(PartnerP2000.constructPartnerIdUser(addBpoPrefix(USER_ANONYMOUS)));
    userId.setType(PartnerTypeEnumDto.USER);
    user.setId(userId);
    user.setCodeList(PartnerConstantP2000.PARTNER_USER_CODELIST);
    user.setCodeValue(addBpoPrefix(USER_ANONYMOUS));
    getOrderDto().getInfo().setUser(user);
    this.paymentMode = EldoradoPaymentModeEnumDto.ONLINE_PAYMENT;
  }

  /**
   * Retrieve the flow state of the order
   */
  public EldoradoFlowStateDto retrieveFlowState() {
    return eldoradoFlowService.retrieveFlowState(this);
  }

  /**
   * Store the user in the command
   */
  public void saveUserInfo() {
    eldoradoFlowService.saveUserInfo(this);
  }

  private void generateUniquePdfFlow() {

    boolean existGini = false;
    boolean existJames = false;
    for (OrderPlacementGroupManager placementGroupManager : findPlacementGroups()) {
      String offerId = placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getId();
      if (placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
        OrderMaterialDto material = retrieveBasePrintMaterial(offerId);
        if (material instanceof OrderMaterialUploadDto) {
          OrderMaterialUploadDto materialUpload = (OrderMaterialUploadDto) material;
          materialUpload.setAdmincontactMail(PropertyLoader.getPropertyWithContext(this, "Notification.Email.Cap"));
          if (materialUpload.getAdminjobType().equals("Gini")) {
            existGini = true;
          }
          else {
            existJames = true;
          }
        }
      }
    }
    // if (existGini && existJames) {
    // EV changement tout pass� en James
    if (existGini) {
      for (OrderMaterialDto material : this.getOrderDto().getMaterials()) {
        if (material instanceof OrderMaterialUploadDto) {
          OrderMaterialUploadDto materialUpload = (OrderMaterialUploadDto) material;
          materialUpload.setAdminjobType("James");
        }
      }
    }
  }

  /**
   * Store the payment authorization information related to the user command The
   * payment authorization process (connection to DataTrans) is a prerequisite
   * of this service call
   */
  public void savePaymentAuthorization(EldoradoPaymentDatatransDto paymentData) {
    if (getOrderDto().getStatus() != OrderStatusEnumDto.IN_MODIFICATION) {
      generateUniquePdfFlow();
    }
    eldoradoFlowService.savePaymentAuthorization(this, paymentData);
    getOrderDto().setStatus(OrderStatusEnumDto.TO_RUN);
  }

  /**
   * Store the payment authorization information related to the user command The
   * payment authorization process (connection to DataTrans) is a prerequisite
   * of this service call
   */
  public void savePaymentInvoice() {
    generateUniquePdfFlow();
    eldoradoFlowService.savePaymentAuthorization(this, null);
    getOrderDto().setStatus(OrderStatusEnumDto.TO_RUN);
  }

  /**
   * retrieve the DataTrans transaction Id for cancellation purpose The
   * cancellation of payment authorization process (connection to DataTrans) is
   * a prerequisite of this service call cancel the command
   */
  public void cancelPaymentAuthorization() {
    eldoradoFlowService.cancelPaymentAuthorization(this);
  }

  /**
   * Send user command into P2000 System
   */
  // Mrt
  // public void insertP2000()
  // {
  // eldoradoFlowService.insertP2000(this);
  // deleteOrderDraft(getOrderDto().getId().getId());
  // }
  /**
   * Call P2000 back-end to start the validation of the command
   */
  // Mrt
  // public void sendP2000Validation()
  // {
  // eldoradoFlowService.sendP2000Validation(this);
  // }
  /**
   * Call P2000 back-end to start cancellation of the command
   */
  public void sendP2000Cancelation() {
    eldoradoFlowService.sendP2000Cancelation(this);
  }

  public void retrieveP2000(String orderId) throws OrderNotFoundException {
    String cookieId = getOrderDto().getInfo().getCookieId();

    OrderIdDto orderIdDto = new OrderIdDto();
    orderIdDto.setOwnerId(OrderConstantP2000.ORDER_P2000);
    orderIdDto.setId(orderId);
    retrieveOrder(orderIdDto);
    fillMediaPrintPlus();

    getOrderDto().getInfo().setCookieId(cookieId);
    setEldoradoStatus();

  }

  private void fillMediaPrintPlus() {

    List<CriteriaDto> criterias = fillPrintStandardCriteria(null);
    for (OrderPlacementGroupManager placementGroupManager : findPlacementGroups()) {
      for (OrderPlacementManager placementManager : placementGroupManager.findPlacements()) {
        List<OptionIdDto> existingOptionIdDtos = placementManager.fillExistingOptionIdDtos(null);
        String upsellingLevel = CodeList.extractCodeValue("JNLML",
            placementManager.getOrderPlacementDto().getMedia().getUpsellingInfo().getCodeList(),
            placementManager.getOrderPlacementDto().getMedia().getUpsellingInfo().getCodeValue());
        if ("1".equals(CodeList.extractCodeValue("JNLML",
            placementManager.getOrderPlacementDto().getMedia().getUpsellingInfo().getCodeList(),
            placementManager.getOrderPlacementDto().getMedia().getUpsellingInfo().getCodeValue()))) {
          MediaUpsellingDto upsellingDtos = placementManager.retrievePrintPlus(criterias);
        }
      }
    }
  }

  private void setEldoradoStatus() {

    if (!checkP2000Order()) {
      getOrderDto().setStatus(OrderStatusEnumDto.CANCELED);
    }
    else if (getOrderDto().getStatus().equals(OrderStatusEnumDto.IN_VALIDATION)) {
      // retrieveFlowState fait uniquement pour les commande in_validation P2000
      EldoradoFlowStateDto flowState = retrieveFlowState();
      // repaiement demand�
      if (EldoradoFlowState.isSuspended(flowState)) {
        getOrderDto().setStatus(OrderStatusEnumDto.IN_MODIFICATION);
      }
      else {
        getOrderDto().setStatus(OrderStatusEnumDto.IN_VALIDATION);
      }
    }
    else if (getOrderDto().getStatus().equals(OrderStatusEnumDto.CANCELED)) {
      getOrderDto().setStatus(OrderStatusEnumDto.CANCELED);
    }
    else if (getOrderDto().getStatus().equals(OrderStatusEnumDto.IN_MODIFICATION)) {
      getOrderDto().setStatus(OrderStatusEnumDto.IN_VALIDATION);
    }
    else if (getOrderDto().getStatus().equals(OrderStatusEnumDto.IN_RUN)) {
      getOrderDto().setStatus(OrderStatusEnumDto.IN_RUN);

    }
    else {
      getOrderDto().setStatus(OrderStatusEnumDto.IN_ERROR);
    }

  }

  private boolean getIsAllPublicationsDone(OrderStatusEnumDto status) {

    if (status.equals(OrderStatusEnumDto.CANCELED) || status.equals(OrderStatusEnumDto.IN_ERROR)) {
      return true;
    }

    if (status.equals(OrderStatusEnumDto.IN_RUN)) {
      for (OrderPlacementGroupDto placementGroups : getOrderDto().getPlacementGroups()) {
        if (placementGroups.getPlacements() != null) {
          for (OrderPlacementDto placements : placementGroups.getPlacements()) {
            if (placements.getInsertions() != null) {
              for (OrderInsertionDto insertions : placements.getInsertions()) {
                if (placements.getMedia().getMediaId().getMediaType().getSupport().equals(MediaSupportEnumDto.ONLINE)) {
                  if (insertions.getEndDate().after(new Date())) {
                    return false;
                  }
                }
                else {
                  if (insertions.getIssueDate().after(new Date())) {
                    return false;
                  }
                }
              }
            }
          }
        }
      }
      return true;
    }
    return false;
  }

  private boolean checkP2000Order() {
    // check that order can be see in summary
    // first check : order should have at least one insertion
    boolean insertionFound = false;
    if (findPlacementGroups() == null) {
      return false;
    }
    for (OrderPlacementGroupManager placementGroupManager : findPlacementGroups()) {
      if (placementGroupManager.findPlacements() == null) {
        return false;
      }
      for (OrderPlacementManager placementManager : placementGroupManager.findPlacements()) {
        if (placementManager.getOrderPlacementDto().getInsertions() != null
            && !placementManager.getOrderPlacementDto().getInsertions().isEmpty()) {
          insertionFound = true;
        }
      }
    }
    return insertionFound;
  }

  public void duplicate(boolean keepMaterial) {
    getOrderDto().setCodeList(null);
    getOrderDto().setCodeValue(null);
    getOrderDto().setReferenceDate(new Date());
    getOrderDto().setStatus(OrderStatusEnumDto.DRAFT);
    getOrderDto().setQuotationStatus(OrderQuotationStatusEnumDto.TODO);
    getOrderDto().setValorization(null);
    getOrderDto().getId().setId(OrderManager.generateUUID());
    // Remove placement group no valid (change in P2000)
    int n = getOrderDto().getPlacementGroups().size();
    for (int i = 0; i < n; i++) {
      OrderPlacementGroupDto groupDto = getOrderDto().getPlacementGroups().get(i);
      if (!keepMaterial) {
        for (OrderPlacementDto placement : groupDto.getPlacements()) {
          OrderMaterialDto orderMaterialDto = null;
          if (placement.getMedia().getMediaId().getMediaType().getSupport().equals(MediaSupportEnumDto.PRINT)) {
            orderMaterialDto = new OrderMaterialUEditorPrintDto();
          }
          else if (placement.getMedia().getMediaId().getMediaType().getSupport().equals(MediaSupportEnumDto.ONLINE)) {
            orderMaterialDto = new OrderMaterialUEditorDto();
          }
          if (orderMaterialDto != null) {
            OrderMaterialManager orderMaterial = addMaterial(orderMaterialDto);
            placement.setMaterialId(orderMaterial.getOrderMaterialDto().getId());

          }
          placement.setOtherMaterialId(null);
          placement.setInsertions(null);
        }
      }
      if (groupDto.getMediaLevel2().getMediaId().getMediaType().getType() != MediaTypeEnumDto.ELDORADO) {
        getOrderDto().getPlacementGroups().remove(i);
        i--;
        n--;
      }
    }
    checkInsertions(true);
  }

  private List<OrderQuotationMessageDto> checkMaterials() {
    List<OrderQuotationMessageDto> messages = new ArrayList<OrderQuotationMessageDto>();

    String ecmsUrl = PropertyLoader.getPropertyWithContext(this, "UEditorPrint.ecmsurlprefix");
    for (OrderPlacementGroupManager placementGroupManager : findPlacementGroups()) {
      if (placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getMediaType().getType() == MediaTypeEnumDto.ELDORADO) {
        String offerId = placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getId();
        for (OrderPlacementManager placementManager : placementGroupManager.findPlacements()) {
          if (placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
            OrderMaterialDto material = retrieveBasePrintMaterial(offerId);
            if (material != null && material instanceof OrderMaterialUEditorPrintDto) {
              OrderMaterialUEditorPrintDto materialUEditorPrintDto = (OrderMaterialUEditorPrintDto) material;
              String cmsUuid = null;
              if (material != null) {
                cmsUuid = materialUEditorPrintDto.getCmsUuid();
              }
              if (!StringUtil.isEmpty(cmsUuid)) {
                boolean isMaterialToSupress = false;
                boolean isMaterialValid = true;
                String url = ecmsUrl + cmsUuid + ".result.xml";
                // http://content.ecms.publicitas.com/ECMSSearchServlet/ECMSGetContentServlet?filename=90f7307f-ce05-4d28-8c27-ea008554b508.result.xml
                try {
                  InputStream is = new URL(url).openStream();
                  DocumentBuilderFactory dbf = DocumentBuilderFactoryImpl.newInstance();
                  DocumentBuilder db = dbf.newDocumentBuilder();
                  Document doc = db.parse(is);
                  is.close();

                  OrderMaterialUEditorPrintDto contentMaterial = new OrderMaterialUEditorPrintDto();
                  Element eContents = doc.getDocumentElement();
                  NodeList listOfContents = eContents.getElementsByTagName("size");
                  Element contentNode = (Element) listOfContents.item(0);
                  String height = (((Element) contentNode.getElementsByTagName("height").item(0)).getAttribute("value"));
                  String width = (((Element) contentNode.getElementsByTagName("width").item(0)).getAttribute("value"));
                  String lines = (((Element) contentNode.getElementsByTagName("lines").item(0)).getAttribute("value"));
                  String words = (((Element) contentNode.getElementsByTagName("words").item(0)).getAttribute("value"));
                  String chars = (((Element) contentNode.getElementsByTagName("chars").item(0)).getAttribute("value"));
                  contentMaterial.setHeightInMm(Integer.valueOf(height));
                  contentMaterial.setWithInMm(Integer.valueOf(width));
                  contentMaterial.setNbLines(Integer.valueOf(lines));
                  contentMaterial.setNbWords(Integer.valueOf(words));
                  contentMaterial.setNbChars(Integer.valueOf(chars));

                  // Check material
                  OptionSizeDto optionSizeDto = retrievePrintSize(offerId);
                  switch (optionSizeDto.getMeasurementType()) {
                    case MILLIMETERS:
                      int heightInMm = Math.max(contentMaterial.getHeightInMm() + optionSizeDto.getGutterHeight(),
                          optionSizeDto.getHeightMinimumInMm());
                      if (optionSizeDto.getHeight() != heightInMm) {
                        isMaterialValid = false;
                      }
                      break;
                    case WORDS:
                      int totalWords = Math.max(contentMaterial.getNbWords(), optionSizeDto.getTotalMinimum());
                      if (optionSizeDto.getTotal() != totalWords) {
                        isMaterialValid = false;
                      }
                      break;
                    case LINES:
                      int totalLines = Math.max(contentMaterial.getNbLines(), optionSizeDto.getTotalMinimum());
                      if (optionSizeDto.getTotal() != totalLines) {
                        isMaterialValid = false;
                      }
                      break;
                    case CHARS:
                      int totalChars = Math.max(contentMaterial.getNbChars(), optionSizeDto.getTotalMinimum());
                      if (optionSizeDto.getTotal() != totalChars) {
                        isMaterialValid = false;
                      }
                      break;
                  }
                }
                catch (Exception ex) {
                  LOGGER.error("Error occured during checkMaterials", ex);
                  ex.printStackTrace();
                  isMaterialToSupress = true;
                  isMaterialValid = false;
                }
                if (isMaterialToSupress) {
                  OrderMaterialUEditorPrintDto orderMaterialDto = new OrderMaterialUEditorPrintDto();
                  OrderMaterialManager orderMaterial = addMaterial(orderMaterialDto);
                  placementManager.getOrderPlacementDto().setMaterialId(orderMaterial.getOrderMaterialDto().getId());
                }
                if (!isMaterialValid) {
                  OrderQuotationMessageDto message = new OrderQuotationMessageDto();
                  message.setMedia(placementManager.getOrderPlacementDto().getMedia());
                  message.setType(OrderQuotationMessageTypeEnumDto.MATERIAL_INVALID);
                  messages.add(message);
                }
              }
            }
          }
        }
      }
    }

    return messages;
  }

  private List<OrderQuotationMessageDto> checkInsertions(boolean isToDelete) {
    List<OrderQuotationMessageDto> messages = new ArrayList<OrderQuotationMessageDto>();

    CheckDelay checkDelay = fillCheckDelay();
    for (OrderPlacementGroupManager placementGroupManager : findPlacementGroups()) {
      for (OrderPlacementManager placementManager : placementGroupManager.findPlacements()) {
        OrderPlacementDto placement = placementManager.getOrderPlacementDto();
        boolean isInsertionDeleted = false;
        if (placement == null || placement.getInsertions() == null)
          break;
        int n = placement.getInsertions().size();
        for (int i = 0; i < n; i++) {
          OrderInsertionDto insertion = placementManager.getOrderPlacementDto().getInsertions().get(i);
          boolean isValid = true;
          Date delayDate = insertion.getDelayDate();
          if (delayDate == null) {
            delayDate = insertion.getIssueDate();
          }
          if (isToDelete) {
            delayDate = calculateDelay(delayDate, checkDelay);
          }
          if (delayDate.compareTo(checkDelay.from) < 0) {
            isValid = false;
          }
          if (!isValid) {
            isInsertionDeleted = true;
            if (isToDelete) {
              placementManager.getOrderPlacementDto().getInsertions().remove(i);
              i--;
              n--;
            }
          }
        }
        if (isInsertionDeleted) {
          OrderQuotationMessageDto message = new OrderQuotationMessageDto();
          message.setMedia(placementManager.getOrderPlacementDto().getMedia());
          if (isToDelete) {
            message.setType(OrderQuotationMessageTypeEnumDto.INSERTION_DELETED);
          }
          else {
            message.setType(OrderQuotationMessageTypeEnumDto.INSERTION_INVALID);
          }
          messages.add(message);
        }
      }
    }

    return messages;
  }

  public boolean isBlindBoxUpdatable() {
    boolean isUpdatable = true;

    // for (OrderPlacementGroupManager placementGroupManager :
    // findPlacementGroups())
    // {
    // String offerId =
    // placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getId();
    // if
    // (placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getMediaType().getSupport()
    // ==
    // MediaSupportEnumDto.PRINT)
    // {
    // if (!StringUtil.isEmpty(retrievePrintMaterial(offerId).getCmsUuid()))
    // {
    // isUpdatable = false;
    // break;
    // }
    // }
    // }

    return isUpdatable;
  }

  public void setBlindBox(boolean isBlindBox) {
    boolean isOldBlindBox = isBlindBox();
    if (isBlindBox != isOldBlindBox) {
      super.setBlindBox(isBlindBox);

      // Suppress the linked materialId
      for (OrderPlacementGroupManager placementGroupManager : findPlacementGroups()) {
        String offerId = placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getId();
        if (placementGroupManager.getOrderPlacementGroupDto().getMedia().getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
          OrderMaterialDto material = retrieveBasePrintMaterial(offerId);
          if (material != null && material instanceof OrderMaterialUEditorPrintDto) {
            OrderMaterialUEditorPrintDto materialUEditorPrintDto = (OrderMaterialUEditorPrintDto) material;
            if (!StringUtil.isEmpty(materialUEditorPrintDto.getCmsUuid())) {
              MediaIdDto offerIdDto = fillMediaIdDto(offerId);
              OrderPlacementManager orderPlacement = placementGroupManager.retrievePlacement(offerIdDto);
              OrderMaterialUEditorPrintDto orderMaterialDto = new OrderMaterialUEditorPrintDto();
              OrderMaterialManager orderMaterial = addMaterial(orderMaterialDto);
              orderPlacement.getOrderPlacementDto().setMaterialId(orderMaterial.getOrderMaterialDto().getId());
            }
          }
        }
      }
    }
  }

  public String interceptorLog() {
    try {
      if (getOrderDto() != null) {
        XStream xStream = new XStream();
        return xStream.toXML(getOrderDto());
      }
      return null;
    }
    catch (Exception ex) {
    }
    return null;
  }

  private CheckDelay fillCheckDelay() {
    CheckDelay checkDelay = new CheckDelay();

    Date now = new Date();
    Calendar cal = Calendar.getInstance();
    cal.setTime(now);
    checkDelay.from = now;

    cal.add(Calendar.DATE, 1);
    cal.set(Calendar.HOUR_OF_DAY, 10);
    cal.set(Calendar.MINUTE, 30);
    if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
      cal.add(Calendar.DATE, 1);
    }
    if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
      cal.add(Calendar.DATE, 1);
    }
    checkDelay.to = cal.getTime();

    cal.setTime(now);
    cal.set(Calendar.HOUR_OF_DAY, 7);
    cal.set(Calendar.MINUTE, 30);
    checkDelay.limitFrom = cal.getTime();

    cal.set(Calendar.HOUR_OF_DAY, 15);
    cal.set(Calendar.MINUTE, 0);
    checkDelay.limitTo = cal.getTime();

    cal.add(Calendar.DATE, -1);
    checkDelay.previousTo = cal.getTime();

    return checkDelay;
  }

  private Date calculateDelay(Date delay, CheckDelay checkDelay) {
    if (delay.compareTo(checkDelay.from) >= 0 && delay.compareTo(checkDelay.to) <= 0) {
      Calendar cal = Calendar.getInstance();
      cal.setTime(delay);
      cal.add(Calendar.HOUR_OF_DAY, -3);
      delay = cal.getTime();
      if (delay.before(checkDelay.limitFrom)) {
        delay = checkDelay.previousTo;
      }
      else if (delay.after(checkDelay.limitTo)) {
        delay = checkDelay.limitTo;
      }
    }
    return delay;
  }

  private class CheckDelay {
    // current Date
    public Date from;
    // current Date + 1 at 10:30 (if day is saturday or sunday --> force monday)
    public Date to;

    // current Date - 1 at 15:00
    public Date previousTo;

    // current Date at 07:30
    public Date limitFrom;
    // current Date at 15:00
    public Date limitTo;
  }

  /**
   * @return the state
   */
  public String getState() {
    String city = getCity();
    if (this.state == null && !StringUtil.isEmptyOrOnlyWhitespaces(city)) {
      CityFindCriteriaDto criteria = new CityFindCriteriaDto();
      criteria.setCountry("CH");
      criteria.setName(city);
      criteria.setLimit(1);
      List<CityFindResultDto> result = localizationService.findCitiesMedia(criteria);
      if (result != null && !result.isEmpty()) {
        this.state = result.get(0).getState();
      }
    }
    return this.state;
  }

  public String getState(String city) {
    if (!StringUtil.isEmptyOrOnlyWhitespaces(city)) {
      CityFindCriteriaDto criteria = new CityFindCriteriaDto();
      criteria.setCountry("CH");
      criteria.setName(city);
      criteria.setLimit(1);
      List<CityFindResultDto> result = localizationService.findCitiesMedia(criteria);
      if (result != null && !result.isEmpty()) {
        return result.get(0).getState();
      }
    }
    return "";
  }

  /**
   * @param state
   *          the state to set
   */
  public void setState(String state) {
    this.state = state;
  }

  public EldoradoSummaryListDto findOrdersForUser(PartnerDto user, int first, int limit) {
    // save current order
    OrderDto currentOrder = this.getOrderDto();
    ArrayList<EldoradoSummaryDto> summaries = new ArrayList<EldoradoSummaryDto>();

    // retrieve DraftOrders
    // List<OrderCriteriaDto> criteriaDtos = new ArrayList<OrderCriteriaDto>();
    // criteriaDtos.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.USER,
    // PartnerP2000.extractPartnerIdUser(user.getId().getId())));
    // final List<OrderDto> orderDtos =
    // getOrderService().findOrderDrafts(criteriaDtos);

    // retrieve P2000 Orders
    List<OrderCriteriaDto> criterias = new ArrayList<OrderCriteriaDto>();
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.ORDER_OWNERID, OrderConstantP2000.ORDER_P2000));
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.ORDER_APPLICATION,
        OrderApplicationEnumDto.ELDORADO.toString()));
    // criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.USER_ID,
    // user.getId().getId()));
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.USER_ID,
        addBpoPrefix(PartnerP2000.extractPartnerIdUser(user.getId().getId()))));
    List<PartnerLinkDto> subLinks = new ArrayList<PartnerLinkDto>();
    subLinks = user.getLinksByType(PartnerLinkTypeEnumDto.ADVERTISER);
    if (subLinks != null && !subLinks.isEmpty()) {
      criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.ADVERTISER_ID,
          subLinks.get(0).getPartner().getId().getId()));
    }
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.EXTRACT_ARCHIVE, "true"));
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.DATE_START, "20100101"));
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.DATE_END, "99991231"));
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.ORDER_EXCLUDE_ADVERTISER, "true"));
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.ORDER_EXCLUDE_OBJECT_INFO, "true"));
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.ORDER_EXCLUDE_SPECIAL_REQUIREMENT, "true"));
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.MEDIA_EXCLUDE_EXTENDED_CIRCULATION, "true"));
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.MEDIA_EXCLUDE_INFORMATION, "true"));
    // OrderCriteriaDto(OrderCriteriaKeyEnumDto.LANGUAGE_TO_EXTRACT, "de"));
    criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.SEARCH_PAGINATION_ORDER_DESCENDING, "true"));

    if (first > 0 && limit > 0) {
      criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.SEARCH_PAGINATION_FIRST, Integer.toString(first)));
      criterias.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.SEARCH_PAGINATION_LIMIT, Integer.toString(limit)));
    }

    final OrderListDto list = getOrderService().findOrders(criterias);
    int realTotal = list.getPagination().getTotal();
    for (OrderDto orderDto : list.getOrders()) {
      this.setOrderDto(orderDto);
      setEldoradoStatus();
      summaries.add(retrieveSummary(false, true, false));
    }

    // reset current order
    this.setOrderDto(currentOrder);
    return new EldoradoSummaryListDto(summaries, list.getPagination().getFirst(), list.getPagination().getFirst()
        + summaries.size(), realTotal);
  }

  public EldoradoSummaryListDto findDraftOrdersForUser(PartnerDto user, int first, int limit) {
    // save current order
    OrderDto currentOrder = this.getOrderDto();
    ArrayList<EldoradoSummaryDto> summaries = new ArrayList<EldoradoSummaryDto>();

    // retrieve DraftOrders
    List<OrderCriteriaDto> criteriaDtos = new ArrayList<OrderCriteriaDto>();
    criteriaDtos.add(new OrderCriteriaDto(OrderCriteriaKeyEnumDto.USER,
        addBpoPrefix(PartnerP2000.extractPartnerIdUser(user.getId().getId().toUpperCase()))));
    final List<OrderDto> orderDtos = getOrderService().findOrderDrafts(criteriaDtos);

    for (int i = 0; i < orderDtos.size(); i++) {
      this.setOrderDto(orderDtos.get(i));
      summaries.add(retrieveSummary(false, true, false));
    }

    int realTotal = orderDtos.size();

    // reset current order
    this.setOrderDto(currentOrder);
    return new EldoradoSummaryListDto(summaries, first, realTotal, realTotal);
  }

  public EldoradoInvoiceDto retrieveDmsInvoice(String commandNumber, EldoradoSummaryInvoiceDto summaryInvoice) {
    Validate.isTrue(commandNumber != null && commandNumber.length() >= 5);
    Validate.notNull(summaryInvoice);
    String agencyCode = commandNumber.substring(0, 5);
    return dmsDocumentManager.retrieveDmsInvoice(agencyCode, summaryInvoice.getId(), summaryInvoice.getDate());
  }

  public EldoradoPaymentModeEnumDto getPaymentMode() {
    return paymentMode;
  }

  public boolean isPDFUpload() {
    return this.isPdfUploadChecked;
  }

  public void setPDFUpload(boolean isPDFUPload) {
    this.isPdfUploadChecked = isPDFUPload;
  }

  public void saveOrderDraft() {
    if (!getOrderDto().getPlacementGroups().isEmpty() && getOrderDto().getStatus().equals(OrderStatusEnumDto.DRAFT)) {
      getOrderDto().getInfo().getUser().setCodeValue(addBpoPrefix(getOrderDto().getInfo().getUser().getCodeValue()));
      super.saveOrderDraft();
    }
  }

  public void setPdfCheckService(PdfCheckService pdfCheckService) {
    this.pdfCheckService = pdfCheckService;
  }

  public void setDmsDocumentManager(EldoradoDmsDocumentManager dmsDocumentManager) {
    this.dmsDocumentManager = dmsDocumentManager;
  }

  public void setPdfUploadBaseDir(String pdfUploadBaseDir) {
    this.pdfUploadBaseDir = pdfUploadBaseDir;
  }

  public void setDescription(String description) {
    this.getOrderDto().setDescription(description);

  }

  public String getDescription() {
    return this.getOrderDto().getDescription();
  }

  public void setPdfCheckThumbnailQuality(String pdfCheckThumbnailQuality) {
    this.pdfCheckThumbnailQuality = pdfCheckThumbnailQuality;
  }

  public void setPdfCheckThumbnailWidth(String pdfCheckThumbnailWidth) {
    this.pdfCheckThumbnailWidth = pdfCheckThumbnailWidth;
  }

  public void setPdfCheckThumbnailHeight(String pdfCheckThumbnailHeight) {
    this.pdfCheckThumbnailHeight = pdfCheckThumbnailHeight;
  }

  public void setPdfCheckProfileBw(String pdfCheckProfileBw) {
    this.pdfCheckProfileBw = pdfCheckProfileBw;
  }

  public void setPdfCheckProfileColor(String pdfCheckProfileColor) {
    this.pdfCheckProfileColor = pdfCheckProfileColor;
  }

  public void setPdfCheckProfileResize(String pdfCheckProfileResize) {
    this.pdfCheckProfileResize = pdfCheckProfileResize;
  }

  public EldoradoPdfValidationDto getPdfValidation() {
    return pdfValidation;
  }

  public void setPdfValidation(EldoradoPdfValidationDto pdfValidation) {
    this.pdfValidation = pdfValidation;
  }

}
