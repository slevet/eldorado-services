package com.publigroupe.eldorado.manager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.publigroupe.basis.domain.PropertyLoader;
import com.publigroupe.eldorado.domain.EldoradoNotification;
import com.publigroupe.eldorado.dto.EldoradoPaymentModeEnumDto;
import com.publigroupe.eldorado.service.EldoradoFlowService;
import com.publigroupe.eldorado.utils.EldoradoActiveDirectoryUtil;
import com.publigroupe.javaBase.base.StringUtil;
import com.publigroupe.partner.domain.p2000.PartnerConstantP2000;
import com.publigroupe.partner.domain.p2000.PartnerP2000;
import com.publigroupe.partner.dto.PartnerAttributeDto;
import com.publigroupe.partner.dto.PartnerCriteriaDto;
import com.publigroupe.partner.dto.PartnerCriteriaKeyEnumDto;
import com.publigroupe.partner.dto.PartnerDto;
import com.publigroupe.partner.dto.PartnerIdDto;
import com.publigroupe.partner.dto.PartnerLevelEnumDto;
import com.publigroupe.partner.dto.PartnerLinkDto;
import com.publigroupe.partner.dto.PartnerLinkTypeEnumDto;
import com.publigroupe.partner.dto.PartnerSearchTypeEnumDto;
import com.publigroupe.partner.dto.PartnerSpecialDto;
import com.publigroupe.partner.dto.PartnerTypeEnumDto;
import com.publigroupe.partner.exception.PartnerAlreadyBoundException;
import com.publigroupe.partner.exception.PartnerNotFoundException;
import com.publigroupe.partner.service.ActiveDirectoryService;
import com.publigroupe.partner.service.PartnerService;

public class EldoradoPartnerManager {

  private PartnerService partnerService;
  private ActiveDirectoryService activeDirectoryService;

  private PartnerDto user;
  private EldoradoNotification eldoradoNotification;
  private EldoradoFlowService eldoradoFlowService;
  private String logoutUrl;

  public EldoradoPartnerManager() {
    initUser();
  }

  public void setPartnerService(PartnerService partnerService) {
    this.partnerService = partnerService;
  }

  public void setActiveDirectoryService(ActiveDirectoryService activeDirectoryService) {
    this.activeDirectoryService = activeDirectoryService;
  }

  public void initUser() {
    user = new PartnerDto();
    PartnerIdDto userId = new PartnerIdDto();
    userId.setOwnerId(PartnerConstantP2000.PARTNER_P2000);
    userId.setId(PartnerP2000.constructPartnerIdUser(addBpoPrefix(EldoradoActiveDirectoryUtil.USER_ANONYMOUS)));
    userId.setType(PartnerTypeEnumDto.USER);
    user.setId(userId);
    user.setCodeList(PartnerConstantP2000.PARTNER_USER_CODELIST);
    user.setCodeValue(addBpoPrefix(EldoradoActiveDirectoryUtil.USER_ANONYMOUS));
  }

  public PartnerDto getUser() {
    return user;
  }

  public void setUser(PartnerDto user) {
    this.user = user;
  }

  public boolean isUserAnonymous() {
    return user.getCodeValue().contains(EldoradoActiveDirectoryUtil.USER_ANONYMOUS);
  }

  private PartnerDto fillLinksAndRemarks(PartnerDto partner, List<PartnerAttributeDto> attributes) {
    Map<String, String> mapAttributes = EldoradoActiveDirectoryUtil.convertListToMap(attributes);
    String advId = EldoradoActiveDirectoryUtil.getMapValue(mapAttributes,
        EldoradoActiveDirectoryUtil.getAttributeNameExternalPartnerId(getLdapType()));

    if (!advId.equals("")) {
      PartnerIdDto id = new PartnerIdDto();
      id.setId(advId);
      id.setOwnerId(PartnerConstantP2000.PARTNER_P2000);
      id.setType(PartnerTypeEnumDto.ADVERTISER);
      List<PartnerLinkDto> links = new ArrayList<PartnerLinkDto>();
      partner.setLinks(links);
      partner.getLinks().add(setLinkAdvertiser(partner, id));
    }
    String remark = EldoradoActiveDirectoryUtil.texteToTextArea(EldoradoActiveDirectoryUtil.getMapValue(mapAttributes,
        EldoradoActiveDirectoryUtil.getAttributeNameDescription(getLdapType())));
    if (!remark.equals("")) {
      PartnerSpecialDto special = new PartnerSpecialDto();
      special.setApplication("ELDORADO");
      special.setName("REMARK");
      special.setValue(remark);
      List<PartnerSpecialDto> specials = new ArrayList<PartnerSpecialDto>();
      partner.setSpecials(specials);
      partner.getSpecials().add(special);
    }
    return partner;
  }

  private PartnerLinkDto setLinkAdvertiser(PartnerDto partner, PartnerIdDto advId) {
    Calendar cal = Calendar.getInstance();
    String validity = cal.get(Calendar.YEAR) + 1900 + "" + cal.get(Calendar.MONTH) + 1 + ""
        + cal.get(Calendar.DAY_OF_YEAR);
    List<PartnerCriteriaDto> criterias = new ArrayList<PartnerCriteriaDto>();
    criterias.add(new PartnerCriteriaDto(PartnerCriteriaKeyEnumDto.VALIDITY, validity));
    criterias.add(new PartnerCriteriaDto(PartnerCriteriaKeyEnumDto.PARTNER_TYPE,
        PartnerTypeEnumDto.ADVERTISER.toString()));
    criterias.add(new PartnerCriteriaDto(PartnerCriteriaKeyEnumDto.PARTNER_OWNERID, "Pub2000"));
    criterias.add(new PartnerCriteriaDto(PartnerCriteriaKeyEnumDto.SEARCH_TYPE,
        PartnerSearchTypeEnumDto.NORMAL.toString()));
    PartnerDto advertizer = partnerService.retrievePartner(advId, criterias);
    PartnerLinkDto link = new PartnerLinkDto();
    link.setPartner(advertizer);
    link.setType(PartnerLinkTypeEnumDto.ADVERTISER);
    return link;
  }

  private String removeBpoPrefix(String userId) {
    return EldoradoTools.removeBPOPrefix(
        PropertyLoader.getProperty(this, "Application.defaultPartner").substring(0, 2), userId);
  }

  private String addBpoPrefix(String userId) {
    return EldoradoTools.addBPOPrefix(PropertyLoader.getProperty(this, "Application.defaultPartner").substring(0, 2),
        userId);
  }

  private String getBpoCodePrefix() {
    return PropertyLoader.getProperty(this, "Application.defaultPartner").substring(0, 2);
  }

  private String getLdapType() {
    return PropertyLoader.getProperty(this, "ActiveDirectory.type");
  }

  public PartnerDto retrieveUser(String userId) {
    // userId = userId.toUpperCase();
    // PartnerIdDto partnerId = new PartnerIdDto();
    // partnerId.setOwnerId(PartnerConstantP2000.PARTNER_P2000);
    // partnerId.setId(PartnerP2000.constructPartnerIdUser(userId));
    // partnerId.setType(PartnerTypeEnumDto.USER);
    // List<PartnerCriteriaDto> criterias = new ArrayList<PartnerCriteriaDto>();
    // criterias.add(new
    // PartnerCriteriaDto(PartnerCriteriaKeyEnumDto.EXTRACT_LINK_ADVERTISER,
    // "TRUE"));
    // user = partnerService.retrievePartner(partnerId, criterias);
    // return user;
    try {
      List<String> retrieveAttributes = EldoradoActiveDirectoryUtil.fillAddressAttribute(getLdapType());
      retrieveAttributes.add(EldoradoActiveDirectoryUtil.getAttributeNamePaymentMode(getLdapType()));
      retrieveAttributes.add(EldoradoActiveDirectoryUtil.getAttributeNameExternalPartnerId(getLdapType()));
      List<PartnerAttributeDto> attributes = activeDirectoryService.readUser(removeBpoPrefix(userId),
          getRegisterdeOu(), retrieveAttributes);
      PartnerDto partner = EldoradoActiveDirectoryUtil.fillPartnerFromActiveDirectory(getLdapType(), attributes,
          addBpoPrefix(""));
      partner = fillLinksAndRemarks(partner, attributes);

      return partner;
    }
    catch (PartnerNotFoundException ex) {
      return null;
    }
  }

  public void createUser(PartnerDto user, String userId) {
    saveUser(user, userId);
  }

  public void modifyUser(PartnerDto user, String userId) {
    saveUser(user, userId);
    this.user = retrieveUserInActiveDirectory(userId);

    if (EldoradoPaymentModeEnumDto.INVOICE_PAYMENT.equals(eldoradoFlowService.retrievePaymentMode(userId))
        && !this.user.getLinksByType(PartnerLinkTypeEnumDto.ADVERTISER).isEmpty()) {
      this.eldoradoNotification.initVariables(getBpoCodePrefix());
      this.eldoradoNotification.notifyUserModificationToCap(user);
    }
  }

  private void saveUser(PartnerDto user, String userId) {
    PartnerIdDto partnerId = new PartnerIdDto();
    partnerId.setOwnerId(PartnerConstantP2000.PARTNER_P2000);
    if (StringUtil.isEmpty(userId)) {
      userId = user.getQualifierInfo().getEmailAddress();
    }
    userId = addBpoPrefix(userId.toUpperCase());
    partnerId.setId(PartnerP2000.constructPartnerIdUser(userId));
    partnerId.setType(PartnerTypeEnumDto.USER);
    user.setId(partnerId);
    PartnerIdDto parentId = new PartnerIdDto();
    parentId.setOwnerId(PartnerConstantP2000.PARTNER_P2000);
    parentId.setId(PartnerP2000.constructPartnerIdUser("ELDORADO"));
    parentId.setType(PartnerTypeEnumDto.USER);
    user.setParentId(parentId);
    user.setLevel(PartnerLevelEnumDto.USER);
    user.setApplication("ELDORADO");
    user.setCodeList(PartnerConstantP2000.PARTNER_USER_CODELIST);
    user.setCodeValue(addBpoPrefix(userId));
    List<PartnerCriteriaDto> criterias = new ArrayList<PartnerCriteriaDto>();
    partnerService.savePartner(user, criterias);
  }

  // @deprecated
  public boolean createUserInActiveDirectory(String user, String password) {
    // List<PartnerAttributeDto> attributes = new
    // ArrayList<PartnerAttributeDto>();
    // EldoradoActiveDirectoryUtil.fillStandardInsertAttributes(getLdapType(),
    // password);
    // attributes.add(new
    // PartnerAttributeDto(EldoradoActiveDirectoryUtil.getAttributeNamePassword(getLdapType()),
    // password));
    // try {
    // activeDirectoryService.addUser(user, getRegisterdeOu(), attributes);
    // }
    // catch (PartnerAlreadyBoundException pabEx) {
    return false;
    // }
    // return true;
  }

  private String getRegisterdeOu() {
    return PropertyLoader.getProperty(this, "ActiveDirectory.registeredUsers");
  }

  private String getOu(String password) {
    if (password == null) {
      return PropertyLoader.getProperty(this, "ActiveDirectory.unRegisteredUsers");
    }
    else {
      return getRegisterdeOu();
    }
  }

  public boolean createUserInActiveDirectory(String user, String password, PartnerDto userDto) {

    List<PartnerAttributeDto> attributes = EldoradoActiveDirectoryUtil.fillStandardInsertAttributes(getLdapType(),
        password);
    attributes.add(new PartnerAttributeDto(EldoradoActiveDirectoryUtil.getAttributeNamePaymentMode(getLdapType()),
        EldoradoPaymentModeEnumDto.ONLINE_PAYMENT.toString()));

    if (password != null && !EldoradoActiveDirectoryUtil.isUtf16PAssword(getLdapType())) {
      attributes.add(new PartnerAttributeDto(EldoradoActiveDirectoryUtil.getAttributeNamePassword(getLdapType()),
          password));
    }
    try {
      activeDirectoryService.addUser(user, getOu(password),
          EldoradoActiveDirectoryUtil.fillAdUserAttributes(getLdapType(), userDto, attributes));
      if (password != null && EldoradoActiveDirectoryUtil.isUtf16PAssword(getLdapType())) {
        activeDirectoryService.updateUserPassword(user, getOu(password), password);
      }
    }
    catch (PartnerAlreadyBoundException pabEx) {
      return false;
    }
    return true;
  }

  public boolean modifyUserPasswordInActiveDirectory(String user, String password) {
    List<PartnerAttributeDto> attributes = new ArrayList<PartnerAttributeDto>();
    if (EldoradoActiveDirectoryUtil.isUtf16PAssword(getLdapType())) {
      return activeDirectoryService.updateUserPassword(user, getOu(password), password);
    }
    else {
      attributes.add(new PartnerAttributeDto(EldoradoActiveDirectoryUtil.getAttributeNamePassword(getLdapType()),
          password));
      activeDirectoryService.updateUser(user, getRegisterdeOu(), attributes);
      return true;
    }
  }

  public boolean modifyUserInActiveDirectory(String user, PartnerDto userDto) {
    List<PartnerAttributeDto> attributes = new ArrayList<PartnerAttributeDto>();
    // attributes.add(new PartnerAttributeDto("userpassword", password));
    activeDirectoryService.updateUser(user, getRegisterdeOu(),
        EldoradoActiveDirectoryUtil.fillAdUserAttributes(getLdapType(), userDto, attributes));
    return true;
  }

  public boolean isUserInActiveDirectory(String user) {
    List<String> attributes = new ArrayList<String>();
    try {
      activeDirectoryService.readUser(user, getRegisterdeOu(), attributes);
    }
    catch (PartnerNotFoundException ex) {
      return false;
    }
    return true;
  }

  public PartnerDto retrieveUserInActiveDirectory(String user) {
    List<String> attributeKeys = EldoradoActiveDirectoryUtil.fillAddressAttribute(getLdapType());
    attributeKeys.add(EldoradoActiveDirectoryUtil.getAttributeNamePaymentMode(getLdapType()));
    attributeKeys.add(EldoradoActiveDirectoryUtil.getAttributeNameExternalPartnerId(getLdapType()));
    try {
      List<PartnerAttributeDto> attributes = activeDirectoryService.readUser(user, getRegisterdeOu(), attributeKeys);
      PartnerDto partner = EldoradoActiveDirectoryUtil.fillPartnerFromActiveDirectory(getLdapType(), attributes,
          addBpoPrefix(user));
      return fillLinksAndRemarks(partner, attributes);
    }
    catch (PartnerNotFoundException ex) {
      return null;
    }
  }

  public boolean isUserInActiveDirectoryActivated(String user) {
    List<String> attributeKeys = new ArrayList<String>();
    attributeKeys.add(EldoradoActiveDirectoryUtil.getAttributeAccountDisable(getLdapType()));
    try {
      List<PartnerAttributeDto> attributes = activeDirectoryService.readUser(user, getRegisterdeOu(), attributeKeys);
      for (PartnerAttributeDto attribute : attributes) {
        if (attribute.getKey().equals(EldoradoActiveDirectoryUtil.getAttributeAccountDisable(getLdapType()))) {
          if (attribute.getValue().equals(EldoradoActiveDirectoryUtil.getValueAccountDisable(getLdapType()))) {
            return false;
          }
        }
      }
    }
    catch (PartnerNotFoundException ex) {
      return false;
    }
    return true;
  }

  public boolean isUserInActiveDirectoryPasswordValid(String user, String password) {
    return activeDirectoryService.isUserPasswordValid(user, getRegisterdeOu(), password);
  }

  public void activateUserInActiveDirectory(String user) {
    List<PartnerAttributeDto> attributes = new ArrayList<PartnerAttributeDto>();
    attributes.add(new PartnerAttributeDto(EldoradoActiveDirectoryUtil.getAttributeAccountDisable(getLdapType()),
        EldoradoActiveDirectoryUtil.getValueAccountEnable(getLdapType())));
    activeDirectoryService.updateUser(user, getRegisterdeOu(), attributes);
  }

  public void resetUserPasswordInActiveDirectory(String user, String language) {
    String uuid = UUID.randomUUID().toString();
    final String newPassword = "E" + uuid.substring(9, 18) + "7";
    modifyUserPasswordInActiveDirectory(user, newPassword);

    this.eldoradoNotification.initVariables(getBpoCodePrefix());
    this.eldoradoNotification.notifyUserResetPassword(user, newPassword, language);
  }

  // public void findUsersInActiveDirectory(String criteria) {
  // try {
  // List<PartnerGroupAttributeDto> result =
  // activeDirectoryService.findUsers(criteria, null,
  // EldoradoActiveDirectoryUtil.fillSearchAttribute());
  // }
  // catch (Exception ex) {
  // return;
  // }
  // }

  public void deleteUserInActiveDirectory(String user) {
    activeDirectoryService.deleteUser(user, getRegisterdeOu());
  }

  public void notifyUserActivation(String user, String language) {
    this.eldoradoNotification.initVariables(getBpoCodePrefix());
    this.eldoradoNotification.notifyUserActivation(user, language);
  }

  public void sendContactFormToCap(String problem, String firstName, String lastName, String firmName, String country,
      String email, String phone, String remarks, String language) {
    this.eldoradoNotification.initVariables(getBpoCodePrefix());
    this.eldoradoNotification.sendContactFormToCap(problem, firstName, lastName, firmName, country, email, phone,
        remarks, language);
  }

  public void requestInvoicePayment(String userId) {
    // Modify the payment mode for this user
    eldoradoFlowService.modifyPaymentMode(removeBpoPrefix(userId), addBpoPrefix(userId),
        EldoradoPaymentModeEnumDto.INVOICE_REQUESTED);

    // Gets the user
    PartnerDto user = retrieveUserInActiveDirectory(EldoradoTools.removeBPOPrefix(
        PropertyLoader.getProperty(this, "Application.defaultPartner").substring(0, 2), userId));

    // Notify the CAP that the user requested invoice payment mode
    this.eldoradoNotification.initVariables(getBpoCodePrefix());
    this.eldoradoNotification.sendInvoicePaymentRequestToCap(user);
    this.eldoradoNotification.sendInvoicePaymentRequestToUser(user);
  }

  public void setEldoradoFlowService(EldoradoFlowService eldoradoFlowService) {
    this.eldoradoFlowService = eldoradoFlowService;
  }

  public void setEldoradoNotification(EldoradoNotification eldoradoNotification) {
    this.eldoradoNotification = eldoradoNotification;
  }

  public String getLogoutUrl() {
    return logoutUrl;
  }

  public void setLogoutUrl(String logoutUrl) {
    this.logoutUrl = logoutUrl;
  }
}
