package com.publigroupe.eldorado.batch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.publigroupe.basis.domain.PropertyLoader;
import com.publigroupe.eldorado.dto.EldoradoPaymentModeEnumDto;
import com.publigroupe.eldorado.service.EldoradoFlowService;
import com.publigroupe.eldorado.utils.EldoradoActiveDirectoryUtil;
import com.publigroupe.order.dao.p2000.OrderAccessor;
import com.publigroupe.partner.domain.p2000.PartnerConstantP2000;
import com.publigroupe.partner.domain.p2000.PartnerP2000;
import com.publigroupe.partner.dto.PartnerAttributeDto;
import com.publigroupe.partner.dto.PartnerCriteriaDto;
import com.publigroupe.partner.dto.PartnerCriteriaKeyEnumDto;
import com.publigroupe.partner.dto.PartnerDto;
import com.publigroupe.partner.dto.PartnerGroupAttributeDto;
import com.publigroupe.partner.dto.PartnerIdDto;
import com.publigroupe.partner.dto.PartnerLinkTypeEnumDto;
import com.publigroupe.partner.dto.PartnerTypeEnumDto;
import com.publigroupe.partner.exception.PartnerNotFoundException;
import com.publigroupe.partner.service.ActiveDirectoryService;
import com.publigroupe.partner.service.PartnerService;

public class UpdateEldoradoUsers {

  private static Log log = LogFactory.getLog(UpdateEldoradoUsers.class);

  private static ClassPathXmlApplicationContext APPCONTEXT;

  private PartnerService partnerService;
  private ActiveDirectoryService activeDirectoryService;
  private EldoradoFlowService eldoradoFlowService;
  private Map<String, String> criteria = new HashMap<String, String>();

  private static String extractedDate = "2014.12.02";

  private static final ReportGenerator reportGenerator = new ReportGenerator();

  public void setPartnerService(PartnerService partnerService) {
    this.partnerService = partnerService;
  }

  public void setActiveDirectoryService(ActiveDirectoryService activeDirectoryService) {
    this.activeDirectoryService = activeDirectoryService;
  }

  public void setEldoradoFlowService(EldoradoFlowService eldoradoFlowService) {
    this.eldoradoFlowService = eldoradoFlowService;
  }

  public static void main(String[] args) {
    // UpdateEldoradoUsers updateEldoradoUsers = new UpdateEldoradoUsers();
    try {
      reportGenerator.addLine("<span style=\"color:#92D050\">Begin Upste </span> : ", ReportLineType.OK);
      log.info("*** Start updating Eldorado users");
      APPCONTEXT = new ClassPathXmlApplicationContext(new String[] {"eldoradoServicesContext.xml"});

      UpdateEldoradoUsers main = (UpdateEldoradoUsers) APPCONTEXT.getBean("updateEldoradoUsers");

      main.run();

      OrderAccessor.commit();
      reportGenerator.addLine("<span style=\"color:#92D050\">End UpdateLoad </span> : ", ReportLineType.OK);
      log.info("*** End successfully updating Eldorado users");
      reportGenerator.sendByMail("Update Users ADAM" + extractedDate);
    }
    catch (Exception ex) {
      OrderAccessor.rollBack();
      log.error(ex);
    }

  }

  private String getRegisterdeOu() {
    return PropertyLoader.getProperty(this, "ActiveDirectory.registeredUsers");
  }

  private String getUnRegisterdeOu() {
    return PropertyLoader.getProperty(this, "ActiveDirectory.unRegisteredUsers");
  }

  private void fillMap() {
    criteria.put("INFO@", "INFO@A");
    criteria.put("INFO@Z", "INFP");
    // criteria.put("", "a");
    // criteria.put("a", "b");
    // criteria.put("b", "c");
    // criteria.put("c", "d");
    // criteria.put("d", "e");
    // criteria.put("e", "f");
    // criteria.put("f", "g");
    // criteria.put("g", "h");
    // criteria.put("h", "i");
    // criteria.put("i", "j");
    // criteria.put("j", "k");
    // criteria.put("k", "l");
    // criteria.put("l", "m");
    // criteria.put("m", "n");
    // criteria.put("n", "o");
    // criteria.put("o", "p");
    // criteria.put("p", "q");
    // criteria.put("q", "r");
    // criteria.put("r", "s");
    // criteria.put("s", "t");
    // criteria.put("t", "u");
    // criteria.put("u", "v");
    // criteria.put("v", "w");
    // criteria.put("w", "x");
    // criteria.put("x", "y");
    // criteria.put("y", "z");
    // criteria.put("z", "");
  }

  private String fillFilter(String fromF, String toF) {
    if (fromF.trim().isEmpty()) {
      return "cn<=" + toF;
    }
    if (toF.trim().isEmpty()) {
      return "cn>=" + fromF;
    }
    return "(&(cn>=" + fromF + ")(cn<=" + toF + "))";
  }

  public void run() {
    fillMap();
    int nbre = 0;
    int nbre_tot = 0;
    for (String from : criteria.keySet()) {
      log.info(from + "/" + criteria.get(from) + "/Filter:" + fillFilter(from, criteria.get(from)) + "/Begin");
      reportGenerator.addLine("<span style=\"color:#92D050\">Begin Criteria </span> : " + criteria.get(from)
          + "/Filter:" + fillFilter(from, criteria.get(from)), ReportLineType.OK);
      try {
        List<PartnerGroupAttributeDto> result = activeDirectoryService.findUsers(fillFilter(from, criteria.get(from)),
            getRegisterdeOu(), EldoradoActiveDirectoryUtil.fillSearchAttribute("ADAM"));
        PartnerDto user;
        nbre = 0;
        for (PartnerGroupAttributeDto item : result) {
          nbre++;
          Map<String, String> mapAttributes = EldoradoActiveDirectoryUtil.convertListToMap(item.getAttributes());
          String userid = EldoradoActiveDirectoryUtil.getMapValue(mapAttributes, "cn");
          log.info("-user : " + userid);
          reportGenerator.addLine("<span style=\"color:#92D050\">Begin User </span> : " + userid, ReportLineType.OK);
          try {
            user = retrieveUser(userid);
            EldoradoPaymentModeEnumDto paymentMode = eldoradoFlowService.retrieveP2000PaymentMode(user.getCodeValue());
            updateAdUser(userid, user, paymentMode);
            log.info("*** OK User " + userid);
            reportGenerator.addLine("<span style=\"color:#92D050\">END User </span> : " + userid, ReportLineType.OK);
          }
          catch (Exception ex) {
            reportGenerator.addLine(
                "<span style=\"color:#92D050\">ERROR User </span> : "
                    + EldoradoActiveDirectoryUtil.getMapValue(mapAttributes, "cn"), ReportLineType.KO);
            log.info("*** ERROR User " + EldoradoActiveDirectoryUtil.getMapValue(mapAttributes, "cn"));
          }
        }
        reportGenerator.addLine("<span style=\"color:#92D050\">END Criteria </span> : " + criteria.get(from)
            + "/Filter:" + fillFilter(from, criteria.get(from)) + "/Nombre : " + nbre, ReportLineType.OK);
        log.info(from + "/" + criteria.get(from) + "/Filter:" + fillFilter(from, criteria.get(from)) + "/Nombre:"
            + nbre);
        nbre_tot = nbre_tot + nbre;
      }
      catch (PartnerNotFoundException ex) {
        reportGenerator.addLine("<span style=\"color:#92D050\">END Criteria </span> : " + criteria.get(from)
            + "/Filter:" + fillFilter(from, criteria.get(from)) + "/NOTFOUND", ReportLineType.OK);
        log.info(from + "/" + criteria.get(from) + "/Filter:" + fillFilter(from, criteria.get(from)) + "/notFound");
      }
      catch (Exception ex) {
        reportGenerator.addLine("<span style=\"color:#92D050\">END Criteria </span> : " + criteria.get(from)
            + "/Filter:" + fillFilter(from, criteria.get(from)) + "/ERROR" + ex.getMessage(), ReportLineType.OK);
        log.info(from + "/" + criteria.get(from) + "/Filter:" + fillFilter(from, criteria.get(from)) + "/ERROR:"
            + ex.getMessage());
      }
    }
    reportGenerator.addLine("<span style=\"color:#92D050\">END Criteria </span> TOTAL TRAITE : " + nbre_tot,
        ReportLineType.OK);
    log.info("TOTAL : " + nbre_tot);

  }

  public void updateAdUser(String userId, PartnerDto partner, EldoradoPaymentModeEnumDto paymenetMode) {
    String advId = "";
    if (partner.getLinksByType(PartnerLinkTypeEnumDto.ADVERTISER) != null
        & partner.getLinksByType(PartnerLinkTypeEnumDto.ADVERTISER).size() > 0)
      advId = partner.getLinksByType(PartnerLinkTypeEnumDto.ADVERTISER).get(0).getPartner().getId().getId();
    ;
    String remark = "";
    if (partner.getSpecials() != null && !partner.getSpecials().isEmpty()) {
      remark = partner.getSpecials().get(0).getValue();
    }
    if (advId.equals("") && paymenetMode.equals(EldoradoPaymentModeEnumDto.INVOICE_PAYMENT)) {
      paymenetMode = EldoradoPaymentModeEnumDto.ONLINE_PAYMENT;
      reportGenerator.addLine("<span style=\"color:#92D050\">ERROR User </span> : " + userId, ReportLineType.KO);
      log.info("***** payment mode changed to online Payment : " + userId);
    }
    List<PartnerAttributeDto> attributes = new ArrayList<PartnerAttributeDto>();
    attributes.add(new PartnerAttributeDto(EldoradoActiveDirectoryUtil.getAttributeNameExternalPartnerId("ADAM"),
        EldoradoActiveDirectoryUtil.fillEmptyField(advId)));
    attributes.add(new PartnerAttributeDto(EldoradoActiveDirectoryUtil.getAttributeNamePaymentMode("ADAM"),
        EldoradoActiveDirectoryUtil.fillEmptyField(paymenetMode.toString())));
    attributes.add(new PartnerAttributeDto(EldoradoActiveDirectoryUtil.getAttributeNameDescription("ADAM"),
        EldoradoActiveDirectoryUtil.textareaToText(remark)));
    activeDirectoryService.updateUser(userId, getRegisterdeOu(),
        EldoradoActiveDirectoryUtil.fillAdUserAttributes("ADAM", partner, attributes));

    return;
  }

  public PartnerDto retrieveUser(String userId) {
    userId = userId.toUpperCase();
    PartnerIdDto partnerId = new PartnerIdDto();
    partnerId.setOwnerId(PartnerConstantP2000.PARTNER_P2000);
    partnerId.setId(PartnerP2000.constructPartnerIdUser(userId));
    partnerId.setType(PartnerTypeEnumDto.USER);
    List<PartnerCriteriaDto> criterias = new ArrayList<PartnerCriteriaDto>();
    criterias.add(new PartnerCriteriaDto(PartnerCriteriaKeyEnumDto.EXTRACT_LINK_ADVERTISER, "TRUE"));
    criterias.add(new PartnerCriteriaDto(PartnerCriteriaKeyEnumDto.EXTRACT_SPECIAL, "true"));
    PartnerDto user = partnerService.retrievePartner(partnerId, criterias);
    return user;
  }

}
