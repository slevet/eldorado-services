/**
 * 
 */
package com.publigroupe.eldorado.batch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.publigroupe.basis.domain.PropertyLoader;
import com.publigroupe.basis.mail.Mail;
import com.publigroupe.basis.mail.MailService;

/**
 * Permits to handle and generate a report.
 * 
 * @author U930dvxd
 */
public class ReportGenerator {

  private final Properties properties = PropertyLoader.loadProperties(this);

  private static final String LINEBREAK = "<br />";

  private String[] addTo;
  private String smtpServer;
  private String environnment;

  public ReportGenerator() {
    this.addTo = properties.getProperty("Notification.Email.Developpment").split(",");
    this.smtpServer = properties.getProperty("Notification.Email.SMTP");
    this.environnment = "PROD";
  }

  public ReportGenerator(String[] addTo, String smtpServer) {
    this.addTo = addTo;
    this.smtpServer = smtpServer;
  }

  private Map<ReportLineType, List<String>> reportLines = new HashMap<ReportLineType, List<String>>();
  {
    for (ReportLineType type : ReportLineType.values()) {
      reportLines.put(type, new ArrayList<String>());
    }
  }

  /**
   * Add a line to the repport
   * 
   * @param line
   * @param type
   */
  public void addLine(String line, ReportLineType type) {
    reportLines.get(type).add(line);
  }

  /**
   * Sends the report by email.
   * 
   * @param subject
   *          Subject of the email
   * @param addTo
   *          Array of destinators of mail
   * @param smtpServer
   *          Smtp server
   * @throws MessagingException
   */
  public void sendByMail(String subject) throws MessagingException {

    Mail mail = new Mail(new MailService(smtpServer, "blaise.evequoz@xentive.ch"));
    mail.setFrom("blaise.evequoz@xentive.ch");

    for (String reciever : addTo) {
      mail.addTo(reciever);
    }

    MimeMessage message = mail.getMessage();
    message.setContent(this.toString(), "text/html");
    mail.setSubject(this.environnment + ":" + subject);
    mail.send();
  }

  public void clear() {
    for (ReportLineType type : ReportLineType.values()) {
      reportLines.get(type).clear();
    }
  }

  /**
   * specify if the report contain errors.
   * 
   * @return true if the report has any errors.
   */
  public boolean hasError() {
    return !reportLines.get(ReportLineType.KO).isEmpty();
  }

  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer();
    for (ReportLineType type : ReportLineType.values()) {
      for (String line : reportLines.get(type)) {
        sb.append(line);
        sb.append(LINEBREAK);
      }
    }
    return sb.toString();
  }
}
