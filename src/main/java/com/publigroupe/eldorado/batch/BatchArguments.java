package com.publigroupe.eldorado.batch;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.publigroupe.javaBase.property.PropertyLoader;

final class BatchArguments {
    private static Log log = LogFactory.getLog(BatchArguments.class);
    
    /** true if command line arguments contain value 'sendErrors' */
    public boolean sendErrorsEmail;
    
    public BatchArguments(String[] args) {
        processArgs(args);
    }

    private void processArgs(String[] args) {
        if(args.length != 0) {
            PropertyLoader pl = new PropertyLoader();
            String sendEmailErrors = pl.getProperty("Batch.Argument.SendEmailErrors");

            StringBuffer ignored = new StringBuffer();
            int size = args.length;
            for(int i = 0; i < size; i++) {
                if(args[i].equalsIgnoreCase(sendEmailErrors)) {
                    this.sendErrorsEmail = true;
                    log.info("ENABLED batch option '"+sendEmailErrors+"'.");
                }
                else {
                    ignored.append(args[i]+", ");
                }
            }
            if(ignored.length() != 0) {
                log.info("Ignoring command line arguments '"+ignored.toString()+"'.");
            }
        }
    }
    
    public boolean shouldSendErrorsEmail() {
        return this.sendErrorsEmail;
    }
}
