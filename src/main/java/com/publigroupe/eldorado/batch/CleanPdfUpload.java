package com.publigroupe.eldorado.batch;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.publigroupe.javaBase.property.PropertyLoader;
import com.publigroupe.javaBase.sql.support.SqlBuilder;
import com.publigroupe.order.dao.p2000.OrderClause;

public class CleanPdfUpload {

  private static Log log = LogFactory.getLog(CleanPdfUpload.class);
  private static DateFormat displayFormatter = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");

  public static void main(String[] args) {
    CleanPdfUpload cleandPdfUpload = new CleanPdfUpload();
    try {
      log.info("*** Start Cleaning Pdf Upload");
      log.info("***");
      cleandPdfUpload.run();

      log.info("***");
      log.info("*** End successfully Cleaning Pdf Upload");
    }
    catch (Exception ex) {
      log.error(ex);
    }

  }

  public void run() {

    String catalinaBase = System.getProperty("catalina.base");
    if (catalinaBase == null) {
      catalinaBase = "";
    }
    else {
      catalinaBase = catalinaBase + File.separator;
    }
    String pdfUploadBaseDir = catalinaBase + PropertyLoader.getPropertyWithContext(this, "pdf.upload.base.dir");
    System.out.println("BaseDir:" + pdfUploadBaseDir);
    File directory = new File(pdfUploadBaseDir);
    File[] listOfFiles = directory.listFiles();

    Date criteriaDate = new Date();
    Calendar cal = Calendar.getInstance();
    cal.setTime(criteriaDate);
    System.out.println("CurrentDate" + displayFormatter.format(criteriaDate));
    cal.add(Calendar.DAY_OF_MONTH, -2);
    criteriaDate = cal.getTime();
    System.out.println("CurrentDate" + displayFormatter.format(criteriaDate));

    for (int i = 0; i < listOfFiles.length; i++) {

      if (listOfFiles[i].isFile()) {
        Date fileDate = new Date(listOfFiles[i].lastModified());
        if (fileDate.compareTo(criteriaDate) < 0) {
          System.out.println("DeleteFile:" + displayFormatter.format(fileDate) + "-FileName-"
              + listOfFiles[i].getName());
          listOfFiles[i].delete();
        }
        else {
          System.out.println("KeepFile:" + displayFormatter.format(fileDate) + "-FileName-" + listOfFiles[i].getName());
        }
      }
    }
  }

  private void updateCurrentUser(String usrMyp, String usrMypUpperCase) {
    OrderClause sqlClause = new OrderClause();
    SqlBuilder sqlBuilder = new SqlBuilder();
    sqlBuilder.append("UPDATE RSGV30");
    sqlBuilder.append("SET usrmyp  = " + sqlClause.param(usrMypUpperCase));

    Boolean registered = false;
    if (!usrMyp.contains(":0") && !usrMyp.contains(":1") && !usrMyp.contains(":2")) {
      sqlBuilder.append("  , usrrmdr = 'REGISTERED'");
      registered = true;
    }

    sqlBuilder.append("WHERE usrmyp = " + sqlClause.param(usrMyp));

    sqlClause.set(sqlBuilder.toString());

    sqlClause.prepareAndExecute();

    sqlClause.close();

    log.info("*** " + usrMyp + " == " + usrMypUpperCase + " == REGISTERED=" + registered.toString());
  }

  /* RSGV01 */

}
