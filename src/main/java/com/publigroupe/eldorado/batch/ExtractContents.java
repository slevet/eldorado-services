package com.publigroupe.eldorado.batch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.publigroupe.order.dao.p2000.OrderAccessor;
import com.publigroupe.order.dao.p2000.OrderClause;

public class ExtractContents {

  private static Log log = LogFactory.getLog(UpdateEldoradoUsers.class);

  private String ueditorEcmsUrlPrefix = "http://content.ecms.publicitas.com/ECMSSearchServlet/ECMSGetContentServlet?filename=";

  public static void main(String[] args) {
    ExtractContents extr = new ExtractContents();
    try {
      log.info("*** Start extract contents");
      extr.run();

      OrderAccessor.commit();
      log.info("***");
      log.info("*** End successfully updating Eldorado users");
    }
    catch (Exception ex) {
      OrderAccessor.rollBack();
      log.error(ex);
    }

  }

  public void run() {
    OrderClause sqlClause = new OrderClause();
    sqlClause.set("SELECT distinct X.UTIENTO, X.CDENUM, y.saidat, z.ctarubga concat '-' concat t.lichmra as RUBRIK"
        + ", CASE when z.ctarubsa = '00' then '' else z.ctarubsa concat '-' concat rs.txtstd1 end as URUBRIK"
        + ", z.grdgenr, value(e.lichmra, '') as GROESSE, z.grdcoln, z.grdhau, z.grdtot"
        + ", j.jnlcod concat '-' concat j.jnlcods as Zietungskode, j.jnlnom, X.TXTLGN"
        + " from rs7a07 x join rs7a01 y on y.utiento = x.utiento and y.cdenum = x.cdenum"
        + " join rs7a03 z on z.utiento = x.utiento and z.cdenum = x.cdenum and z.txtno = x.txtno"
        + " left outer join rs0323 t on t.heldon = 'CTARUBG' and t.helval = z.ctarubga  and t.utilng = '01'"
        + " left outer join tb8507 rs    on rs.jnlcod = z.jnlcod  and rs.jnlcods = z.jnlcods  and rs.JNLDON = 'CTARUBS'  and rs.JNLVDONB = z.ctarubga  and rs.jnlvdon = z.ctarubsa"
        + " left outer join rs0323 e on e.heldon = 'GRDGENR'  and e.helval = z.grdgenr and e.utilng = '01'"
        + " left outer join tb8510 j on j.jnlcod = z.jnlcod  and j.jnlcods = z.jnlcods  and (j.valdatf = 99999999 or j.anncod = 'A')"
        + " where x.utiento = '10012' and x.TXTLGN like 'FILE UE:%' UNION "
        + " SELECT distinct X.UTIENTO, X.CDENUM, y.saidat, z.ctarubga concat '-' concat t.lichmra as RUBRIK"
        + ", CASE when z.ctarubsa = '00' then '' else z.ctarubsa concat '-' concat rs.txtstd1 end as URUBRIK"
        + ", z.grdgenr, value(e.lichmra, '') as GROESSE, z.grdcoln, z.grdhau, z.grdtot"
        + ", j.jnlcod concat '-' concat j.jnlcods as Zietungskode, j.jnlnom, X.TXTLGN"
        + " from rs7h07 x join rs7h01 y on y.utiento = x.utiento and y.cdenum = x.cdenum"
        + " join rs7h03 z on z.utiento = x.utiento and z.cdenum = x.cdenum and z.txtno = x.txtno"
        + " left outer join rs0323 t on t.heldon = 'CTARUBG' and t.helval = z.ctarubga  and t.utilng = '01'"
        + " left outer join tb8507 rs    on rs.jnlcod = z.jnlcod  and rs.jnlcods = z.jnlcods  and rs.JNLDON = 'CTARUBS'  and rs.JNLVDONB = z.ctarubga  and rs.jnlvdon = z.ctarubsa"
        + " left outer join rs0323 e on e.heldon = 'GRDGENR'  and e.helval = z.grdgenr and e.utilng = '01'"
        + " left outer join tb8510 j on j.jnlcod = z.jnlcod  and j.jnlcods = z.jnlcods  and (j.valdatf = 99999999 or j.anncod = 'A')"
        + " where x.utiento = '10012' and x.TXTLGN like 'FILE UE:%'");
    sqlClause.prepareAndExecute();
    try {
      FileOutputStream fos = new FileOutputStream("C:\\dev\\workspaces\\workspace370_64\\contents.txt");
      while (sqlClause.next()) {
        String contentId = sqlClause.getString("TXTLGN");
        log.info(sqlClause.getString("UTIENTO") + "-" + sqlClause.getString("CDENUM") + "contentId :"
            + contentId.substring(9));
        String prefixContent = "\r\n\"" + sqlClause.getString("UTIENTO") + "\";\"" + sqlClause.getString("CDENUM")
            + "\";\"" + contentId.substring(9) + "\";\"" + sqlClause.getString("SAIDAT") + "\";\""
            + sqlClause.getString("RUBRIK") + "\";\"" + sqlClause.getString("URUBRIK") + "\";\""
            + sqlClause.getString("GRDGENR") + "-" + sqlClause.getString("GROESSE") + "\";\"";
        if (sqlClause.getString("GRDGENR").equals("01")) {
          prefixContent = prefixContent + sqlClause.getString("GRDCOLN") + "X" + sqlClause.getString("GRDHAU") + "="
              + sqlClause.getString("GRDTOT") + "\";\"";
        }
        else if (sqlClause.getString("GRDGENR").equals("04")) {
          prefixContent = prefixContent + sqlClause.getString("GRDCOLN") + "/" + sqlClause.getString("GRDHAU")
              + "\";\"";
        }
        else {
          prefixContent = prefixContent + sqlClause.getString("GRDTOT") + "\";\"";
        }
        prefixContent = prefixContent + sqlClause.getString("Zietungskode") + "\";\"" + sqlClause.getString("JNLNOM")
            + "\";\"";
        try {
          extractUeditorFile(prefixContent, contentId.substring(9), "txt", fos);
        }
        catch (Exception ex) {
        }
        finally {
        }
      }
      sqlClause.close();
      fos.close();
    }
    catch (Exception ex) {
    }
    finally {
    }
  }

  private class MailFile {
    public String filename;
    public File file;
  }

  private void extractUeditorFile(String prefixContent, String cmsUuid, String extension, FileOutputStream fos)
      throws IOException, FileNotFoundException {

    URL url = new URL(ueditorEcmsUrlPrefix + cmsUuid + "." + extension);
    InputStream is = url.openStream();
    fos.write(prefixContent.getBytes());
    try {
      byte[] buf = new byte[2048];
      int len;
      while ((len = is.read(buf)) > 0) {
        fos.write(buf, 0, len);
      }
      fos.write("\";".getBytes());
    }
    finally {
      is.close();
    }
    return;
  }

}
