/**
 * 
 */
package com.publigroupe.eldorado.batch;

/**
 * @author U930dvxd
 */
public enum ReportLineType {
  HEADER,
  OK,
  KO
}
