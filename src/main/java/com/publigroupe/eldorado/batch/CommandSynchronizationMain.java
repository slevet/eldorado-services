package com.publigroupe.eldorado.batch;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.publigroupe.eldorado.domain.EldoradoFlowState;
import com.publigroupe.eldorado.domain.EldoradoNotification;
import com.publigroupe.eldorado.domain.EldoradoPayment;
import com.publigroupe.eldorado.domain.datatrans.exception.DatatransException;
import com.publigroupe.eldorado.dto.EldoradoFlowStateDto;
import com.publigroupe.eldorado.dto.EldoradoPaymentDto;
import com.publigroupe.eldorado.manager.EldoradoOrderManager;
import com.publigroupe.javaBase.jvmProcess.JVMReturnCodeManager;
import com.publigroupe.order.dao.p2000.OrderAccessor;
import com.publigroupe.order.dto.OrderInsertionDto;
import com.publigroupe.order.dto.OrderPlacementDto;
import com.publigroupe.order.dto.OrderPlacementGroupDto;
import com.publigroupe.order.exception.OrderNotFoundException;

public class CommandSynchronizationMain {
  private static Log log = LogFactory.getLog(CommandSynchronizationMain.class);

  private final static String P2ABDRE_BATCH_NAME = "BAT";
  private final static String ERROR_ORDER_PAYED_BUT_NOT_IN_PUB2000 = "Sever error Payment reserved but Order not found inPUB2000 : order flagged as cancelled";
  private final static String ERROR_ORDER_SHOULD_BE_TREATED_IN_PUB2000 = "Warning Order should be processed by the CAP";
  private final static String UNEXPECTED_ERROR = "Sever error unexpected statusg";
  private final static String ERROR_ORDER_DATATRANS_ERROR = "Sever error datatrans error returned see detail below";
  private final static String ERROR_ORDER_GENERAL_ERROR = "Sever error inconsitent order error";
  private final static String WARNING_NO_PROOFOFPUBLICATIONDONE = "No proof of Publication Done";
  private final static String WARNING_NO_MORE_ISSUEDATE = "No more issueDate in the p2000 Order";

  private static ClassPathXmlApplicationContext APPCONTEXT;

  /**
   * Batch start method
   */
  public static void main(String[] args) {
    try {
      log.info("################## Loading Spring Configuration & Beans ###############");

      APPCONTEXT = new ClassPathXmlApplicationContext(new String[] {"eldoradoServicesContext.xml"});

      log.info("################## Processing Command-line Arguments ##################");

      BatchArguments bargs = new BatchArguments(args);
      CommandSynchronizationMain main = (CommandSynchronizationMain) APPCONTEXT.getBean("commandSynchronizationMain");

      log.info("################## Start Processing Penging Orders ##################");
      main.run(bargs);

      log.info("#####################################################################");
    }
    catch (Throwable t) {
      t.printStackTrace();
      JVMReturnCodeManager.exitJVM(8);
    }

    JVMReturnCodeManager.exitJVM(0);
  }

  /**
   * Start "asynchronous" processes for updating payment status Loop on RSGV26
   * and handle appropriate operations
   */
  public void run(BatchArguments bargs) {
    EldoradoFlowState state = new EldoradoFlowState();
    EldoradoPayment payment = new EldoradoPayment();
    EldoradoNotification notification = (EldoradoNotification) APPCONTEXT.getBean("eldoradoNotification");
    List<EldoradoFlowStateDto> paymentStates = state.findAll();
    StringBuffer errMsg = new StringBuffer();
    boolean existCriticalError;

    existCriticalError = false;
    for (EldoradoFlowStateDto paymentStateDto : paymentStates) {
      notification.initVariables(paymentStateDto.getSubsidiaryCode().substring(0, 2));
      try {

        // Retrieve payment information
        EldoradoPaymentDto paymentDto = payment.retrieve(paymentStateDto.getSubsidiaryCode(),
            paymentStateDto.getOrderNr());

        // Order with payment reservation but not loaded (loading error) -->
        // after one day cancel payment & flag without notification
        if (!EldoradoFlowState.hasOrderBeenLoadedInPub2000(paymentStateDto)) {
          if (isPaymentAuthorisedLastDay(paymentStateDto.getAuthorizationDate()) && bargs.shouldSendErrorsEmail()) {
            try {
              // cancel Datatrans' payment authorization
              payment.cancelPaymentAuthorization(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr());
            }
            catch (DatatransException de) {
              String msg = getOrderNumberAsString(paymentStateDto) + " : " + ERROR_ORDER_PAYED_BUT_NOT_IN_PUB2000;
              errMsg.append("- " + msg + "\n");
              existCriticalError = true;
              log.error(msg);
            }
            // cancel notification has been sent --> turn on flag
            state.setCancelled(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr(), P2ABDRE_BATCH_NAME);
            state.setCancellationNotified(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr());
          }
        }
        else {

          // Retrieve order information
          EldoradoOrderManager orderManager = retrieveOrder(paymentStateDto.getSubsidiaryCode(),
              paymentStateDto.getOrderNr());

          if (EldoradoFlowState.isConfirmed(paymentStateDto)) {
            if (!existOnePublication(orderManager)) {
              state.setFeedBackIssueDateDeleted(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr());
              String msg = getOrderNumberAsString(paymentStateDto) + " : " + WARNING_NO_MORE_ISSUEDATE;
              errMsg.append("- " + msg + "\n");
              log.error(msg);
            }
            else {
              if (isFirstProofOfPublicationDone(orderManager)) {
                String userEmail = getEmail(orderManager);
                // notification.notifyFeedBackIssueDate(orderManager,
                // userEmail);
                state.setFeedBackIssueDateNotified(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr());

              }
              else {
                if (!isOneInsertionGreater(orderManager)) {
                  String msg = getOrderNumberAsString(paymentStateDto) + " : " + WARNING_NO_PROOFOFPUBLICATIONDONE;
                  errMsg.append("- " + msg + "\n");
                  log.error(msg);
                }
              }
            }
          }
          // Valid command --> Debit amount to Datatrans
          else if (EldoradoFlowState.isValidated(paymentStateDto)) {
            if (!EldoradoFlowState.isDebited(paymentStateDto)) {
              // Handle payment
              payment.debitPayment(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr(), orderManager);

              // Update state of payment
              state.setDebited(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr());
            }
            String userEmail = getEmail(orderManager);

            // send notification to user telling him the order has been
            // validated by the CAP
            notification.notifyValidation(orderManager, userEmail);
            // ELDWEB-535 CAP WON'T recieve an e-mail
            // if (EldoradoFlowState.isValidatedByEndUser(paymentStateDto)) {
            // notification.notifyValidationCap(orderManager);
            // }

            // Update state of payment
            state.setConfirmationNotified(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr());
          }

          // Suspended Command --> Notify Customer (if not already done)
          else if (EldoradoFlowState.isSuspended(paymentStateDto)
              && !EldoradoFlowState.isSuspensionNotifiedToClient(paymentStateDto)) {
            // Handle notification
            notification.notifySuspend(orderManager, getEmail(paymentDto));

            // Update state of payment
            state.setClientSuspensionNotification(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr());
          }

          // Cancelled Command --> Notify Customer (if not already done)
          else if (EldoradoFlowState.isCancelled(paymentStateDto)
              && !EldoradoFlowState.isCancellationNotifiedToClient(paymentStateDto)) {
            // cancel Datatrans' payment authorization
            payment.cancelPaymentAuthorization(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr());

            if (state.hasOrderBeenCancelledByUser(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr())) {
              // ELDWEB-535 CAP WON'T recieve an e-mail
              // Handle notification
              // notification.notifyCancellationCap(orderManager);
            }
            else if (state.hasOrderBeenCancelledByBatch(paymentStateDto.getSubsidiaryCode(),
                paymentStateDto.getOrderNr(), P2ABDRE_BATCH_NAME)) {
              // ELDWEB-535 CAP WON'T recieve an e-mail
              // Handle notification
              // notification.notifyCancellationCap(orderManager);
              notification.notifyCancellationUser(orderManager);
            }
            else {
              // Handle notification
              notification.notifyCancellationUser(orderManager);
            }
            // cancel notification has been sent --> turn on flag
            state.setCancellationNotified(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr());
          }

          // Suspended Command AND Customer Notified AND Suspension time has
          // expired --> Cancel the command and notify customer
          else if (EldoradoFlowState.isSuspended(paymentStateDto)
              && EldoradoFlowState.isSuspensionNotifiedToClient(paymentStateDto)
              && EldoradoFlowState.isSuspensionTimeExpired(paymentStateDto)) {
            // Send Cancellation to P2000
            orderManager.sendP2000Cancelation();

            // Update state of payment
            state.setCancelled(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr(), P2ABDRE_BATCH_NAME);

            // Handle notification
            notification.notifyCancellationCap(orderManager);

            // Handle notification
            notification.notifyCancellationUser(orderManager);

            // cancel notification has been sent --> turn on flag
            state.setCancellationNotified(paymentStateDto.getSubsidiaryCode(), paymentStateDto.getOrderNr());
          }

          else {
            // Suspended Command AND Customer Notified AND Suspension time has
            // NOT expired --> OK
            if (EldoradoFlowState.isSuspended(paymentStateDto)
                && EldoradoFlowState.isSuspensionNotifiedToClient(paymentStateDto)
                && !EldoradoFlowState.isSuspensionTimeExpired(paymentStateDto)) {
              // this is OK
              // Customer got the email informing him his order has been held
              // and he still
              // has time for updating and revalidating the order.
              log.info("Although order " + getOrderNumberAsString(paymentStateDto) + " is suspended, the customer "
                  + "has already been notified. The customer still has time for editing and validating its offer.");
            }

            // Order has not been processed
            else if (!EldoradoFlowState.hasOrderBeenHandled(paymentStateDto)) {
              // Order should be processed --> Remind to CAP
              if (EldoradoFlowState.shouldRemindCapToProcessThisOrder(paymentStateDto)) {

                // EV inclus dans le mail 2 X par jour
                // notification.notifyCapToProcessOrder(getOrderNumberAsString(paymentStateDto));
                // log.info("Order "+getOrderNumberAsString(paymentStateDto)+" should have been processed. "+
                // "A remainder has been sent to the CAP.");
                String msg = getOrderNumberAsString(paymentStateDto) + " : " + ERROR_ORDER_SHOULD_BE_TREATED_IN_PUB2000;
                errMsg.append("- " + msg + "\n");
                log.error(msg);
              }
              // There is still time to process the order --> OK
              else {
                log.info("Time still remains for the CAP for processing order "
                    + getOrderNumberAsString(paymentStateDto) + ".");
              }
            }
            else {
              String msg = getOrderNumberAsString(paymentStateDto) + " : " + UNEXPECTED_ERROR;
              errMsg.append("- " + msg + "\n");
              existCriticalError = true;
              log.error(msg);
            }
          }
        }
        OrderAccessor.commit();
      }
      catch (DatatransException de) {
        // This can happen, means that the payment transition had a problem,
        // which has to be solved manually
        // by the CAP.
        OrderAccessor.rollBack();
        errMsg.append(getOrderNumberAsString(paymentStateDto) + " : " + ERROR_ORDER_DATATRANS_ERROR + "\n");
        errMsg.append(de.getMessage() + "\n");
        existCriticalError = true;
      }
      catch (Throwable t) {
        // This should never happen! Though, if it does happen, then log the
        // problem and continue with
        // the next payment order!
        OrderAccessor.rollBack();
        errMsg.append(getOrderNumberAsString(paymentStateDto) + " : " + ERROR_ORDER_GENERAL_ERROR + "\n");
        errMsg.append(t.getMessage() + "\n");
        existCriticalError = true;
      }
    }

    // Send all unresolved errors
    if (errMsg.length() != 0) {
      if (bargs.shouldSendErrorsEmail()) {
        notification.sendUnresolvedErrorsNotification(errMsg, existCriticalError);
        log.info("Sent email for notifying unresolved order errors.");
      }
      else {
        log.info("Reminder: Email notification for unresolved order errors was disabled for this batch execution.");
      }
    }
  }

  private boolean existOnePublication(EldoradoOrderManager orderManager) {
    if (orderManager.getOrderDto().getPlacementGroups() == null) {
      return false;
    }
    for (OrderPlacementGroupDto placementGroupDto : orderManager.getOrderDto().getPlacementGroups()) {
      if (placementGroupDto.getPlacements() == null) {
        return false;
      }
      for (OrderPlacementDto placementDto : placementGroupDto.getPlacements()) {
        if (placementDto.getInsertions() == null) {
          return false;
        }
        for (OrderInsertionDto insertionDto : placementDto.getInsertions()) {
          return true;
        }
      }
    }

    return false;
  }

  private boolean isFirstProofOfPublicationDone(EldoradoOrderManager orderManager) {
    for (OrderPlacementGroupDto placementGroupDto : orderManager.getOrderDto().getPlacementGroups()) {
      for (OrderPlacementDto placementDto : placementGroupDto.getPlacements()) {
        for (OrderInsertionDto insertionDto : placementDto.getInsertions()) {
          if (insertionDto.getStatus().isProofOfPublicationDone()) {
            return true;
          }
        }
      }
    }

    return false;
  }

  private boolean isOneInsertionGreater(EldoradoOrderManager orderManager) {
    Calendar cal = Calendar.getInstance();
    Date now = cal.getTime();
    for (OrderPlacementGroupDto placementGroupDto : orderManager.getOrderDto().getPlacementGroups()) {
      for (OrderPlacementDto placementDto : placementGroupDto.getPlacements()) {
        for (OrderInsertionDto insertionDto : placementDto.getInsertions()) {
          if (insertionDto.getIssueDate().compareTo(now) > 0) {
            return true;
          }
        }
      }
    }

    return false;
  }

  private String getEmail(EldoradoPaymentDto paymentDto) {
    try {
      return paymentDto.getUser().getQualifierInfo().getEmailAddress();
    }
    catch (Throwable t) {
      throw new RuntimeException("Problem occured while fetching the customer email.", t);
    }
  }

  private String getEmail(EldoradoOrderManager orderManager) {
    try {
      return orderManager.getOrderDto().getInfo().getUser().getQualifierInfo().getEmailAddress();
    }
    catch (Throwable t) {
      throw new RuntimeException("Problem occured while fetching the customer email.", t);
    }
  }

  private String getOrderNumberAsString(EldoradoFlowStateDto paymentStateDto) {
    return paymentStateDto.getSubsidiaryCode() + "-" + paymentStateDto.getOrderNr();
  }

  private EldoradoOrderManager retrieveOrder(String subsidiaryCode, String orderNr) throws OrderNotFoundException {
    log.info("Retrieving order " + subsidiaryCode + "-" + orderNr + " ...");

    EldoradoOrderManager orderManager = (EldoradoOrderManager) APPCONTEXT.getBean("eldoradoOrderManager");

    orderManager.retrieveP2000(subsidiaryCode + "-" + orderNr);

    log.info("OK. Retrived order " + subsidiaryCode + "-" + orderNr + ".");
    return orderManager;
  }

  private static boolean isPaymentAuthorisedLastDay(Date authorizationDate) {
    long millisecondsInDay = 24 * 60 * 60 * 1000;
    if (authorizationDate == null) {
      return false;
    }
    else {
      long delta = System.currentTimeMillis() - authorizationDate.getTime();
      return delta >= millisecondsInDay * 1;
    }
  }

}
