package com.publigroupe.eldorado.batch.solr;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

import com.publigroupe.basis.domain.CodeListBase;
import com.publigroupe.eldorado.batch.solr.OfferGroup.RateType;
import com.publigroupe.eldorado.dao.Rsel10Dao;
import com.publigroupe.eldorado.dao.bean.Rsel10;
import com.publigroupe.eldorado.service.SolrService;
import com.publigroupe.javaBase.base.DateUtil;
import com.publigroupe.javaBase.base.StringUtil;
import com.publigroupe.media.domain.CodeList;
import com.publigroupe.media.domain.p2000.MediaP2000;
import com.publigroupe.media.dto.CriteriaDto;
import com.publigroupe.media.dto.CriteriaKeyEnumDto;
import com.publigroupe.media.dto.MediaDto;
import com.publigroupe.media.dto.MediaGroupTypeEnumDto;
import com.publigroupe.media.dto.MediaIdDto;
import com.publigroupe.media.dto.MediaLanguageDto;
import com.publigroupe.media.dto.MediaNameDto;
import com.publigroupe.media.dto.MediaSupportEnumDto;
import com.publigroupe.media.dto.MediaTypeDto;
import com.publigroupe.media.dto.MediaTypeEnumDto;
import com.publigroupe.media.dto.OptionDto;
import com.publigroupe.media.dto.OptionIdDto;
import com.publigroupe.media.dto.OptionRateTypeDto;
import com.publigroupe.media.dto.OptionTypeEnumDto;
import com.publigroupe.media.dto.StandardClientStatusEnumDto;
import com.publigroupe.media.service.MediaService;
import com.publigroupe.media.service.OptionService;
import com.publigroupe.order.dao.p2000.Rs0702Dao;
import com.publigroupe.order.dao.p2000.bean.Rs0702;

/**
 * The Solr indexer that creates documents and adds them to Solr.
 */
public final class SolrIndexer {

  private static Log LOG = LogFactory.getLog(SolrIndexer.class);

  private SolrServer server;
  private int count = 1;

  private MediaService mediaService;

  private OptionService optionService;

  private List<CriteriaDto> bpoCriterias = new ArrayList<CriteriaDto>();

  public MediaService getMediaService() {
    return mediaService;
  }

  private void fillBpoRestrictionCriteria(String companyId) {
    if (companyId != null && !companyId.trim().equals("")) {
      bpoCriterias = new ArrayList<CriteriaDto>();
      bpoCriterias.add(new CriteriaDto(CriteriaKeyEnumDto.ADVERTISER_CODE_LIST, "UTISTE"));
      bpoCriterias.add(new CriteriaDto(CriteriaKeyEnumDto.ADVERTISER_CODE_VALUE, companyId));
    }
    else {
      bpoCriterias = null;
    }
  }

  public void setMediaService(MediaService mediaService) {
    this.mediaService = mediaService;
  }

  public OptionService getOptionService() {
    return optionService;
  }

  public void setOptionService(OptionService optionService) {
    this.optionService = optionService;
  }

  /**
   * Connects to the Solr instance at the given url
   * 
   * @param url
   * @throws MalformedURLException
   */
  public void connectTo(final String url) throws MalformedURLException {
    this.server = new CommonsHttpSolrServer(url);
  }

  public void setConnection(SolrServer connectionSolrServer) {
    this.server = connectionSolrServer;
  }

  /**
   * Deletes all documents of the Solr instance.
   * 
   * @throws SolrServerException
   * @throws IOException
   */
  public void deleteAll() throws SolrServerException, IOException {
    LOG.info("Deleting all documents...");
    this.server.deleteByQuery("*:*");
  }

  /**
   * Commits transaction.
   * 
   * @throws SolrServerException
   * @throws IOException
   */
  public void deleteBrand(String brandId) throws SolrServerException, IOException {
    LOG.info("Deleting one brand");
    this.server.deleteByQuery("*:* AND " + SolrService.BRAND_ID + ":" + SolrService.PREFIX_ELDORADO_BRAND + brandId);
  }

  /**
   * Commits transaction.
   * 
   * @throws SolrServerException
   * @throws IOException
   */

  public void commit() throws SolrServerException, IOException {
    LOG.info("Committing...");
    this.server.commit();
  }

  /**
   * Rolls transaction back.
   * 
   * @throws SolrServerException
   * @throws IOException
   */
  public void rollback() throws SolrServerException, IOException {
    LOG.info("Rolling back...");
    this.server.rollback();
  }

  /**
   * Imports all offers to Solr.
   * 
   * @throws SolrServerException
   * @throws IOException
   */
  public void indexAll(String companyId, String brandId) throws SolrServerException, IOException {

    fillBpoRestrictionCriteria(companyId);

    final MediaSupportEnumDto[] allMediaSupports = new MediaSupportEnumDto[] {MediaSupportEnumDto.PRINT,
        MediaSupportEnumDto.ONLINE};
    final OfferHeadingCode[] allOfferHeadings = OfferHeadingCode.values();
    for (final MediaSupportEnumDto mediaSupport : allMediaSupports) {
      for (final OfferHeadingCode heading : allOfferHeadings) {
        LOG.info("Loading brands for " + mediaSupport + ", " + heading + "...");
        final List<CriteriaDto> criterias = fillPrintStandardCriteria(mediaSupport, heading);
        final List<MediaDto> brands;
        if (brandId != null) {

          brands = new ArrayList<MediaDto>();
          MediaIdDto mediaIdDto = new MediaIdDto();
          MediaTypeDto mediaTypeDto = new MediaTypeDto();
          mediaIdDto.setOwnerId(MediaP2000.MEDIA_P2000);
          mediaIdDto.setId(SolrService.PREFIX_ELDORADO_BRAND + brandId);
          mediaIdDto.setMediaType(mediaTypeDto);
          mediaTypeDto.setType(MediaTypeEnumDto.ELDORADO);
          mediaTypeDto.setGroupType(MediaGroupTypeEnumDto.ELDORADO_BRAND);

          brands.add(this.mediaService.retrieveMedia(mediaIdDto, criterias));

        }
        else {
          brands = this.mediaService.findMedias(criterias);
        }
        LOG.info("Loading offers for " + mediaSupport + ", " + heading + "...");
        indexAll(mediaSupport, heading, brands, criterias);
      }
    }
  }

  /**
   * Builds the list of search criteria for the given mediaSupport, offer
   * heading and client status
   * 
   * @param mediaSupport
   * @param heading
   * @return
   */

  private List<CriteriaDto> fillPrintStandardCriteria(final MediaSupportEnumDto mediaSupport,
      final OfferHeadingCode heading) {
    final List<CriteriaDto> criterias = new ArrayList<CriteriaDto>();
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.MEDIA_SUPPORT, mediaSupport.toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.MEDIA_TYPE, MediaTypeEnumDto.ELDORADO.toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.VALIDITY, new DateUtil("yyyyMMdd", new Date()).toString()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.STANDARD_HEADING_ELDORADO, heading.getCode()));
    criterias.add(new CriteriaDto(CriteriaKeyEnumDto.PLACEMENT_REMOVE_DUPLICATE_SIZE, "true"));
    // criterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_NAME,
    // "Oltner Tagblatt"));
    return criterias;
  }

  /**
   * Indexes all the offers of the given brands
   * 
   * @param clientStatus
   * @param mediaSupport
   * @param heading
   * @param brands
   * @param criterias
   * @throws SolrServerException
   * @throws IOException
   */
  private void indexAll(final MediaSupportEnumDto mediaSupport, final OfferHeadingCode heading,
      final List<MediaDto> brands, final List<CriteriaDto> criterias) throws SolrServerException, IOException {
    final Map<String, Integer> sponsoredSequences = getSponsoredSequences(criterias);
    for (final MediaDto brand : brands) {
      LOG.info("Traetment Brand : " + brand.getMediaId().getId() + " Type : "
          + brand.getMediaId().getMediaType().getType() + " rubrique : " + heading);
      final int sponsoredSequence = getSponsoredSequence(brand, sponsoredSequences);
      // setGif(brand, heading);
      final List<MediaDto> medias = this.mediaService.findChilds(brand.getMediaId(), criterias);
      for (final MediaDto media : medias) {
        // setGif(media, heading);
        final List<MediaDto> offers = this.mediaService.findChilds(media.getMediaId(), criterias);
        // for (final MediaDto offer : offers) {
        // setGif(offer, heading);
        // }
        final List<OfferGroup> offerGroups = buildOfferGroups(brand, media, offers);
        for (final OfferGroup offerGroup : offerGroups) {
          index(mediaSupport, heading, offerGroup, sponsoredSequence, medias.indexOf(media));
        }
      }
    }
  }

  private boolean isBpoOffer(MediaIdDto mediaId) {
    if (bpoCriterias == null) {
      return true;
    }
    return mediaService.isMediaTreatable(mediaId, bpoCriterias);
  }

  /**
   * Merges offers into offer groups. Two offers belong to the same group if
   * they only differ by their rate type.
   * 
   * @param brand
   * @param media
   * @param offers
   * @return
   */
  private List<OfferGroup> buildOfferGroups(final MediaDto brand, final MediaDto media, final List<MediaDto> offers) {
    final Map<String, List<OfferGroup>> map = new HashMap<String, List<OfferGroup>>();
    for (final MediaDto offer : offers) {
      if (isBpoOffer(offer.getMediaId())) {
        final List<OptionIdDto> optionIds = this.optionService.findDefaultOptionIds(offer.getMediaId());
        // String key = "";
        // for (OptionIdDto optionId : optionIds) {
        // if (optionId.getOptionType() != OptionTypeEnumDto.RATE_TYPE) {
        // key += optionId.getId();
        // }
        // }

        String key = "";
        if (offer.getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
          Rsel10Dao rsel10Dao = new Rsel10Dao();
          int offno = Integer.valueOf(CodeListBase.extractCodeValue("OFFNO", offer.getCodeList(), offer.getCodeValue()));
          Date valdatd = DateUtil.formatDate(
              CodeListBase.extractCodeValue("VALDATD", offer.getCodeList(), offer.getCodeValue()), "yyyyMMdd", true);
          Rsel10 rsel10 = rsel10Dao.read(offno, valdatd);
          StringBuilder sb = new StringBuilder();
          sb.append(rsel10.jnlcod);
          sb.append(rsel10.jnlcods);
          sb.append(rsel10.ctacatp);
          sb.append(rsel10.ctainsp);
          sb.append(rsel10.ctarubg);
          sb.append(rsel10.ctaempl);
          sb.append(rsel10.ctacoul);
          sb.append(rsel10.ctaexe);
          sb.append(rsel10.ctarubt);
          sb.append(rsel10.grdgenr);
          sb.append(rsel10.offtyp);
          sb.append(rsel10.augmtyp);
          sb.append(rsel10.exetyp);
          sb.append(rsel10.grdcoln);
          key = sb.toString();
        }
        else if (offer.getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.ONLINE) {
          key = CodeListBase.extractCodeValue("OFFNO", offer.getCodeList(), offer.getCodeValue());
        }

        // Offers with different keys are in different groups for sure.
        // Offers with the same key are however not necessarily in the same
        // group. This is why we get a list of groups for the same key, and not
        // just one group.
        List<OfferGroup> groups = map.get(key);
        if (groups == null) {
          groups = new ArrayList<OfferGroup>();
          map.put(key, groups);
        }
        boolean added = false;
        for (final OfferGroup group : groups) {
          if (group.matchesOptions(optionIds)) {
            group.addOffer(offer, optionIds);
            added = true;
            break;
          }
        }
        if (!added) {
          final String clientStatusValueBasis = extractClientStatusValueBasis(offer, optionIds);
          final OfferGroup group = new OfferGroup(brand, media, offer, optionIds, clientStatusValueBasis);
          groups.add(group);
        }
      }
    }

    final List<OfferGroup> offerGroups = new ArrayList<OfferGroup>();
    // Flatten the collection of lists of groups into one single list
    for (final List<OfferGroup> groups : map.values()) {
      offerGroups.addAll(groups);
    }
    return offerGroups;
  }

  private String extractClientStatusValueBasis(final MediaDto offer, final List<OptionIdDto> optionIds) {
    String clientStatusValue = "";
    for (final OptionIdDto optionId : optionIds) {
      if (optionId.getOptionType() == OptionTypeEnumDto.CLIENT_STATUS) {
        final List<CriteriaDto> criterias = new ArrayList<CriteriaDto>();
        criterias.add(new CriteriaDto(CriteriaKeyEnumDto.OPTION_EXTRACT_INFORMATION, "true"));
        final OptionDto option = this.optionService.retrieveOption(offer.getMediaId(), optionId, criterias);
        clientStatusValue = option.getCodeValueBasis();
        break;
      }
    }
    return clientStatusValue;
  }

  /**
   * Returns a map of external references (ids) to sponsored sequence numbers
   * for all sponsored brands.
   * 
   * @param criterias
   * @return
   */
  private Map<String, Integer> getSponsoredSequences(final List<CriteriaDto> criterias) {
    final List<CriteriaDto> sponsoredCriterias = new ArrayList<CriteriaDto>(criterias);
    sponsoredCriterias.add(new CriteriaDto(CriteriaKeyEnumDto.SEARCH_SPONSORED, "true"));
    final List<MediaDto> sponsoredBrands = this.mediaService.findMedias(sponsoredCriterias);
    final Map<String, Integer> brandsToSponsoredSequence = new HashMap<String, Integer>();
    for (int i = 0; i < sponsoredBrands.size(); i++) {
      brandsToSponsoredSequence.put(sponsoredBrands.get(i).getMediaId().getExternalReference(), i);
    }
    return brandsToSponsoredSequence;
  }

  /**
   * Gets the sponsored sequence of the given brand, or -1 if the brand is not
   * sponsored.
   * 
   * @param brand
   * @param sponsoredSequences
   *          a map of external references (ids) to sponsored sequence numbers
   * @return
   */
  private int getSponsoredSequence(final MediaDto brand, final Map<String, Integer> sponsoredSequences) {
    final Integer seq = sponsoredSequences.get(brand.getMediaId().getExternalReference());
    return seq == null ? -1 : seq;
  }

  /**
   * Sets the thumbnail gif's path of the offer
   * 
   * @param offer
   * @param heading
   */
  private void setGif(final MediaDto media, final OfferHeadingCode heading, String colorOption) {
    for (final MediaNameDto mediaName : media.getNames()) {
      if (StringUtil.isEmpty(mediaName.getGif())) {
        if (media.getMediaId().getMediaType().getSupport() == MediaSupportEnumDto.PRINT) {
          final String group = heading.getGroup() == null ? "" : heading.getGroup() + "_";
          if (StringUtil.isEmpty(colorOption) || colorOption.equals("00"))
            mediaName.setGif("default_print_" + group + mediaName.getPackageName() + "_" + mediaName.getLanguage()
                + ".gif");
          else
            mediaName.setGif("default_print_" + group + mediaName.getPackageName() + "_" + mediaName.getLanguage()
                + "_colored" + ".gif");
        }
        else {
          mediaName.setGif("default_online.gif");
        }
      }
    }
  }

  /**
   * Indexes the given offer group.
   * 
   * @param clientStatus
   * @param mediaSupport
   * @param heading
   * @param group
   * @param sponsoredSequence
   * @throws SolrServerException
   * @throws IOException
   */
  private void index(final MediaSupportEnumDto mediaSupport, final OfferHeadingCode heading, final OfferGroup group,
      final int sponsoredSequence, final int mediaIndex) throws SolrServerException, IOException {
    final Set<String> exclude = new HashSet<String>();
    final Set<String> empty = new HashSet<String>();
    final Set<String> all = new HashSet<String>();
    all.add(SolrService.ALL_NPA);
    final List<MediaDto> offers = group.getSortedOffers();
    for (int i = 0; i < offers.size() - 1; i++) {
      final MediaDto offer = offers.get(i);
      final Set<String> npas = getNPAs(offer, group.getRateTypeOptionId(offer));
      npas.removeAll(exclude);
      exclude.addAll(npas);
      index(group.getClientStatus(), mediaSupport, heading, group.getBrand(), group.getMedia(), offer,
          sponsoredSequence, npas, empty, group.getColorOptionCode(), mediaIndex);
    }
    index(group.getClientStatus(), mediaSupport, heading, group.getBrand(), group.getMedia(),
        offers.get(offers.size() - 1), sponsoredSequence, all, exclude, group.getColorOptionCode(), mediaIndex);
  }

  private Set<String> getNPAs(final MediaDto offer, final OptionIdDto rateTypeOptionId) {
    final Set<String> npas = new HashSet<String>();
    if (rateTypeOptionId != null) {
      if (RateType.fromId(rateTypeOptionId.getId()) == RateType.Abroad) {
        npas.add(SolrService.ABROAD_NPA);
      }
      else {
        final List<CriteriaDto> criterias = new ArrayList<CriteriaDto>();
        criterias.add(new CriteriaDto(CriteriaKeyEnumDto.OPTION_EXTRACT_INFORMATION, "true"));
        final OptionRateTypeDto option = (OptionRateTypeDto) this.optionService.retrieveOption(offer.getMediaId(),
            rateTypeOptionId, criterias);
        npas.addAll(option.getPostalCodes());
      }
    }
    return npas;
  }

  /**
   * Indexes the given offer group.
   * 
   * @param clientStatus
   * @param mediaSupport
   * @param heading
   * @param group
   * @param sponsoredSequence
   * @param exclude
   * @param include
   * @throws SolrServerException
   * @throws IOException
   */
  private void index(final StandardClientStatusEnumDto clientStatus, final MediaSupportEnumDto mediaSupport,
      final OfferHeadingCode heading, final MediaDto brand, final MediaDto media, final MediaDto offer,
      final int sponsoredSequence, final Set<String> include, final Set<String> exclude, String colorOption,
      int mediaIndex) throws SolrServerException, IOException {
    final SolrInputDocument doc = new SolrInputDocument();

    final String id = mediaSupport.toString() + SolrService.KEY_SEPARATOR + heading.getCode()
        + SolrService.KEY_SEPARATOR + offer.getMediaId().getId();
    LOG.info("[" + (this.count++) + "] Indexing " + id);

    setField(doc, SolrService.ID, id);
    setField(doc, SolrService.HEADING, heading.getCode());
    setField(doc, SolrService.MEDIA_SUPPORT, mediaSupport.toString());
    setField(doc, SolrService.COLOR_OPTION, colorOption);
    switch (clientStatus) {
      case PRIVATE:
        doc.addField(SolrService.CLIENT_STATUS, clientStatus.toString());
        break;
      case COMMERCIAL:
        doc.addField(SolrService.CLIENT_STATUS, clientStatus.toString());
        break;
      case STANDARD:
        doc.addField(SolrService.CLIENT_STATUS, StandardClientStatusEnumDto.PRIVATE.toString());
        doc.addField(SolrService.CLIENT_STATUS, StandardClientStatusEnumDto.COMMERCIAL.toString());
    }

    setField(doc, SolrService.BRAND_ID, brand.getMediaId().getId());
    setField(doc, SolrService.MEDIA_ID, media.getMediaId().getId());
    setField(doc, SolrService.MEDIA_SEQUENCE, mediaIndex);
    setField(doc, SolrService.OFFER_ID, offer.getMediaId().getId());
    setField(doc, SolrService.OFFER_START_DATE, offer.getValidityFrom());
    setField(doc, SolrService.OFFER_END_DATE, offer.getValidityUntil());

    setField(doc, SolrService.SPONSORED, sponsoredSequence > -1);
    setField(doc, SolrService.SPONSORED_SEQUENCE, sponsoredSequence);

    if (brand.getEditor() != null) {
      setField(doc, SolrService.SITE, brand.getEditor().getSite());
    }

    setField(doc, SolrService.BRAND_JNLCOD,
        CodeList.extractCodeValue("JNLCOD", brand.getCodeList(), brand.getCodeValue()));
    setField(doc, SolrService.BRAND_JNLCODS,
        CodeList.extractCodeValue("JNLCODS", brand.getCodeList(), brand.getCodeValue()));

    int circulation = 0;
    if (brand.getInformation().getCirculationUncertified() > 1) {
      circulation = brand.getInformation().getCirculationUncertified();
    }
    else if (brand.getInformation().getCirculationSold() > 0) {
      circulation = brand.getInformation().getCirculationSold();
    }
    else if (brand.getInformation().getCirculationFree() > 0) {
      circulation = brand.getInformation().getCirculationFree();
    }

    // int circulation = brand.getInformation().getCirculationCopy();
    // if (circulation == 0) {
    // circulation = brand.getInformation().getCirculationUncertified();
    // }
    if (circulation > 0) {
      setField(doc, SolrService.BRAND_CIRCULATION, circulation);
    }
    setField(doc, SolrService.BRAND_IS_CIRCULATION, brand.getInformation().isCirculation());

    setField(doc, SolrService.BRAND_CIRCULATION_UNCERTIFIED, brand.getInformation().getCirculationUncertified());
    setField(doc, SolrService.BRAND_CIRCULATION_SOLD, brand.getInformation().getCirculationSold());
    setField(doc, SolrService.BRAND_CIRCULATION_FREE, brand.getInformation().getCirculationFree()
        + brand.getInformation().getCirculationMember());
    setField(doc, SolrService.BRAND_DISTRIBUTION_TYPE, brand.getInformation().getDistributionType());

    // media circulation
    int mediacirculation = media.getInformation().getCirculationCopy();
    if (mediacirculation == 0) {
      mediacirculation = media.getInformation().getCirculationUncertified();
    }
    if (mediacirculation > 0) {
      setField(doc, SolrService.MEDIA_CIRCULATION, mediacirculation);
    }
    setField(doc, SolrService.MEDIA_IS_CIRCULATION, media.getInformation().isCirculation());

    setField(doc, SolrService.MEDIA_CIRCULATION_UNCERTIFIED, media.getInformation().getCirculationUncertified());
    setField(doc, SolrService.MEDIA_CIRCULATION_SOLD, media.getInformation().getCirculationSold());
    setField(doc, SolrService.MEDIA_CIRCULATION_FREE, media.getInformation().getCirculationFree()
        + media.getInformation().getCirculationMember());
    setField(doc, SolrService.MEDIA_DISTRIBUTION_TYPE, media.getInformation().getDistributionType());
    String state = completeState(CodeList.extractCodeValue("JNLCOD", offer.getCodeList(), offer.getCodeValue()),
        CodeList.extractCodeValue("JNLCODS", offer.getCodeList(), offer.getCodeValue()),
        media.getInformation().getStateProvince());
    setField(doc, SolrService.STATE_PROVINCE, state);
    if (media.getInformation().getEconomicZones() != null) {
      for (final String area : media.getInformation().getEconomicZones()) {
        doc.addField(SolrService.ECONOMIC_AREA, area);
      }
    }
    if (media.getKeywords() != null) {
      for (final String keyword : media.getKeywords()) {
        doc.addField(SolrService.KEYWORDS, keyword);
      }
    }
    if (media.getLanguages() != null) {
      for (final MediaLanguageDto language : media.getLanguages()) {
        doc.addField(SolrService.LANGUAGE, language.getIsoCode());
      }
    }
    else if (brand.getLanguages() != null) {
      for (final MediaLanguageDto language : brand.getLanguages()) {
        doc.addField(SolrService.LANGUAGE, language.getIsoCode());
      }
    }

    final String offerDuplicateKey;
    if (mediaSupport == MediaSupportEnumDto.PRINT) {
      final String jnlcod = CodeList.extractCodeValue("JNLCOD", offer.getCodeList(), offer.getCodeValue());
      final String jnlgrc = CodeList.extractCodeValue("JNLGRC", offer.getCodeList(), offer.getCodeValue());
      offerDuplicateKey = jnlcod + jnlgrc;
    }
    else {
      offerDuplicateKey = CodeList.extractCodeValue("EDITNO", offer.getCodeList(), offer.getCodeValue());
    }
    setField(doc, SolrService.OFFER_DUPLICATE_KEY, offerDuplicateKey);

    setField(doc, SolrService.PRICE, offer.getBasicPrice().toPlainString());
    setField(doc, SolrService.CURRENCY, offer.getBasicCurrency());

    for (final String npa : include) {
      doc.addField(SolrService.NPA_INCLUDE, npa);
    }
    for (final String npa : exclude) {
      doc.addField(SolrService.NPA_EXCLUDE, npa);
    }
    setGif(offer, heading, colorOption);
    for (final MediaNameDto name : brand.getNames()) {
      setField(doc, prefix(name.getLanguage(), SolrService.BRAND_NAME), name.getName());
      setField(doc, prefix(name.getLanguage(), SolrService.BRAND_DESC), name.getDescription());
      setField(doc, prefix(name.getLanguage(), SolrService.BRAND_GIF), name.getGif());
    }

    for (final MediaNameDto name : media.getNames()) {
      setField(doc, prefix(name.getLanguage(), SolrService.MEDIA_NAME), name.getName());
      setField(doc, prefix(name.getLanguage(), SolrService.MEDIA_DESC), name.getDescription());
      setField(doc, prefix(name.getLanguage(), SolrService.MEDIA_GIF), name.getGif());
    }

    for (final MediaNameDto name : offer.getNames()) {
      setField(doc, prefix(name.getLanguage(), SolrService.OFFER_NAME), name.getName());
      setField(doc, prefix(name.getLanguage(), SolrService.OFFER_DESC), name.getDescription());
      setField(doc, prefix(name.getLanguage(), SolrService.OFFER_GIF), name.getGif());
      // OK, it should be the same for all names!
      setField(doc, SolrService.OFFER_PACKAGE, name.getPackageName());
    }

    this.server.add(doc);
  }

  private String completeState(String jnlcod, String jnlcods, String state) {
    Rs0702Dao rs0702Dao = new Rs0702Dao();
    Rs0702 rs0702 = rs0702Dao.read("P00", "000", "85", "100", jnlcod, jnlcods, new Date());
    if (rs0702 != null) {
      return state + "," + rs0702.parval2.trim();
    }
    rs0702 = rs0702Dao.read("P00", "000", "85", "100", jnlcod, "", new Date());
    if (rs0702 != null) {
      return state + "," + rs0702.parval2.trim();
    }
    return state;
  }

  /**
   * Sets the value of a field in the solr document (if the field itself is not
   * null)
   * 
   * @param doc
   * @param name
   * @param value
   */
  private void setField(final SolrInputDocument doc, final String name, final Object value) {
    if (name != null) {
      doc.setField(name, value);
    }
  }

  /**
   * Prefixes a field name by the given language. If the language is not
   * supported, returns null.
   * 
   * @param lang
   * @param fieldName
   * @return
   */
  private String prefix(final String lang, final String fieldName) {
    if ("de".equals(lang) || "fr".equals(lang) || "it".equals(lang)) {
      return lang + "_" + fieldName;
    }
    return null;
  }

}
