package com.publigroupe.eldorado.batch.solr;

/**
 * The different kinds of heading for an offer
 */
public enum OfferHeadingCode {

  IMMO_SELL_BUY("10", "realestate"),
  IMMO_LOCATION("11", "realestate"),
  IMMO_HOLIDAY("12", "realestate"),

  JOB_OFFERS("20", "job"),
  JOB_ASKS("21", "job"),
  JOB_STUDY("22", "job"),

  PRIVATE_AUTO_MOTO("30", "car"),
  PRIVATE_SELL_BUY("31", "private"),

  OTHERS_FRIENDS("40"),
  OTHERS_SEX("41"),

  OTHERS_OPINION_CLUB("50"),
  OTHERS_OPINION_LOTO("51"),
  OTHERS_OPINION_BORN("52"),

  OTHERS_COMMERCIAL("60"),
  OTHERS_HOTEL("61"),
  OTHERS_CINEMA("62"),
  OTHERS_STUDY("63"),
  OTHERS_TRIP("64"),
  OBITURAY("70"),
  OTHERS_POLITIC("71");

  private final String code;
  private final String group;

  OfferHeadingCode(final String code) {
    this(code, null);
  }

  OfferHeadingCode(final String code, final String group) {
    this.code = code;
    this.group = group;
  }

  public String getCode() {
    return this.code;
  }

  public String getGroup() {
    return this.group;
  }

}
