package com.publigroupe.eldorado.batch.solr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.publigroupe.media.domain.p2000.OptionP2000Tools;
import com.publigroupe.media.dto.MediaDto;
import com.publigroupe.media.dto.OptionIdDto;
import com.publigroupe.media.dto.OptionTypeEnumDto;
import com.publigroupe.media.dto.StandardClientStatusEnumDto;

/**
 * A group offer that should be stored as a single document in Solr. A group
 * aggregates all offers that only differ from their rate type
 */
public class OfferGroup {

  public static enum RateType {
    All(0),
    Local(1),
    Cantonal(2),
    Switzerland(3),
    Abroad(4),
    District(5),
    Regional(6),
    DiffusionArea(7);
    private int code;

    RateType(final int code) {
      this.code = code;
    }

    static RateType fromId(final String ctagen) {
      for (final RateType rateType : RateType.values()) {
        if (("CTAGEN_" + rateType.code).equals(ctagen)) {
          return rateType;
        }
      }
      throw new IllegalArgumentException("Unknown rate type: " + ctagen);
    }
  }

  private final MediaDto brand;
  private final MediaDto media;
  private final Map<MediaDto, OptionIdDto> offers = new HashMap<MediaDto, OptionIdDto>();
  private final List<OptionIdDto> optionIds;
  private final StandardClientStatusEnumDto clientStatus;

  public OfferGroup(
      final MediaDto brand,
      final MediaDto media,
      final MediaDto offer,
      final List<OptionIdDto> optionIds,
      final String clientStatusValueBasis) {
    this.brand = brand;
    this.media = media;
    addOffer(offer, optionIds);
    this.optionIds = optionIds;
    if ("01".equals(clientStatusValueBasis)) {
      this.clientStatus = StandardClientStatusEnumDto.COMMERCIAL;
    }
    else if ("02".equals(clientStatusValueBasis)) {
      this.clientStatus = StandardClientStatusEnumDto.PRIVATE;
    }
    else {
      this.clientStatus = StandardClientStatusEnumDto.STANDARD;
    }
  }

  /**
   * Adds one offer to the group
   * 
   * @param offer
   */
  public void addOffer(final MediaDto offer, final List<OptionIdDto> optionIds) {
    OptionIdDto rateTypeOptionId = null;
    for (final OptionIdDto optionId : optionIds) {
      if (optionId.getOptionType() == OptionTypeEnumDto.RATE_TYPE) {
        rateTypeOptionId = optionId;
        break;
      }
    }
    this.offers.put(offer, rateTypeOptionId);
  }

  /**
   * The brand of this group.
   * 
   * @return
   */
  public MediaDto getBrand() {
    return this.brand;
  }

  /**
   * The media of this group.
   * 
   * @return
   */
  public MediaDto getMedia() {
    return this.media;
  }

  /**
   * Whether the options are identical apart from the rate type.
   * 
   * @param options
   * @return
   */
  public boolean matchesOptions(final List<OptionIdDto> optionIds) {
    boolean isSameRateType = false;
    if (this.optionIds.size() != optionIds.size()) {
      return false;
    }
    for (int i = 0; i < this.optionIds.size(); i++) {
      final OptionIdDto a = this.optionIds.get(i);
      final OptionIdDto b = optionIds.get(i);
      if (a.getOptionType() != b.getOptionType()) {
        return false;
      }
      if (a.getOptionType() != OptionTypeEnumDto.RATE_TYPE && !a.getId().equals(b.getId())) {
        return false;
      }
      if (a.getOptionType() == OptionTypeEnumDto.RATE_TYPE && a.getId().equals(b.getId())) {
        isSameRateType = true;
      }
    }
    // EV when all same options, RATE_TYPE too : offer will be different
    if (isSameRateType) {
      return false;
    }
    return true;
  }

  /**
   * Gets a the list of offers in the group in the following order: abroad first
   * if any, then by price asc.
   * 
   * @return
   */
  public List<MediaDto> getSortedOffers() {
    final List<MediaDto> offers = new ArrayList<MediaDto>();
    MediaDto abroadOffer = null;
    for (final Map.Entry<MediaDto, OptionIdDto> e : this.offers.entrySet()) {
      if (e.getValue() != null && RateType.fromId(e.getValue().getId()) == RateType.Abroad) {
        abroadOffer = e.getKey();
      }
      else {
        offers.add(e.getKey());
      }
    }
    Collections.sort(offers, new Comparator<MediaDto>() {
      @Override
      public int compare(final MediaDto a, final MediaDto b) {
        return a.getBasicPrice().compareTo(b.getBasicPrice());
      }
    });
    if (abroadOffer != null) {
      offers.add(0, abroadOffer);
    }
    return offers;
  }

  /**
   * Gets the option id (for the rate type) of the given offer.
   * 
   * @param offer
   * @return
   */
  public OptionIdDto getRateTypeOptionId(final MediaDto offer) {
    return this.offers.get(offer);
  }

  /**
   * Gets whether this group is private, commercial, or standard (i.e. both
   * private and commercial).
   * 
   * @return
   */
  public StandardClientStatusEnumDto getClientStatus() {
    return this.clientStatus;
  }

  /**
   * Gets whether this group is private, commercial, or standard (i.e. both
   * private and commercial).
   * 
   * @return
   */
  public String getColorOptionCode() {
    for (final OptionIdDto optionId : optionIds) {
      if (optionId.getOptionType() == OptionTypeEnumDto.COLOR) {
        return optionId.getId().replace(OptionP2000Tools.OPTION_COLOR, "");
      }
    }
    return "00";
  }
}
