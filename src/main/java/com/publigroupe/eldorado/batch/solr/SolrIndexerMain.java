package com.publigroupe.eldorado.batch.solr;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.publigroupe.eldorado.service.SolrService;
import com.publigroupe.javaBase.jvmProcess.JVMReturnCodeManager;

/**
 * The main class for indexing Eldorado offers data into Solr.
 */
public class SolrIndexerMain {

  private static Log LOG = LogFactory.getLog(SolrIndexerMain.class);
  private static ClassPathXmlApplicationContext appContext;

  /**
   * @param args
   */
  public static void main(final String[] args) {
    String companyId = null;
    if (args.length > 0) {
      companyId = args[0];
    }
    appContext = new ClassPathXmlApplicationContext(new String[] {"eldoradoServicesContext.xml"});

    final SolrService service = (SolrService) appContext.getBean("solrService");
    try {
      LOG.info("Init Company : " + companyId);
      service.init(companyId);
      // service.index(companyId, null);
      service.index("10", "1007");
    }
    catch (final Exception e) {
      LOG.error("Indexing failed", e);
      e.printStackTrace();
      JVMReturnCodeManager.exitJVM(8);
    }
  }
}
