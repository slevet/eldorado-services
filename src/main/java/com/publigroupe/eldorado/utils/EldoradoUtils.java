package com.publigroupe.eldorado.utils;


/**
 * General purpose utility class.
 * 
 * @author Gon�alo Rodrigues - Consultant IT
 */
public final class EldoradoUtils {

  /**
   * Rounds up the given double to nearest int.
   * 
   * @param d
   *          The number to round up.
   * @return The rounded int.
   */
  public static int roundUp(double d) {
    return (d > (int) d) ? (int) d + 1 : (int) d;
  }
}
