package com.publigroupe.eldorado.utils;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.publigroupe.partner.dto.PartnerAttributeDto;
import com.publigroupe.partner.dto.PartnerCountryEnumDto;
import com.publigroupe.partner.dto.PartnerDto;
import com.publigroupe.partner.dto.PartnerQualifierEnumDto;

public class EldoradoActiveDirectoryUtil {

  public static final String USER_ANONYMOUS = "ANONYMOUS";
  public static final String PREFIX_USER = "USER_";
  public static final String ADAM_TYPE = "ADAM";

  public static boolean isUtf16PAssword(String ldapType) {
    if (ldapType.equals(ADAM_TYPE)) {
      return false;
    }
    else {
      return true;
    }
  }

  public static String getAttributeNameDescription(String ldapType) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.AD_DESCRIPTION;
    }
    else {
      return EldoradoBpoAdUtil.AD_DESCRIPTION;
    }
  }

  public static String getAttributeNameExternalPartnerId(String ldapType) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.AD_EXTERNAL_PARTNER_ID;
    }
    else {
      return EldoradoBpoAdUtil.AD_EXTERNAL_PARTNER_ID;
    }
  }

  public static String getAttributeNamePaymentMode(String ldapType) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.AD_PAYMENT_MODE;
    }
    else {
      return EldoradoBpoAdUtil.AD_PAYMENT_MODE;
    }
  }

  public static String getAttributeNamePassword(String ldapType) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.AD_PASSWORD;
    }
    else {
      return EldoradoBpoAdUtil.AD_PASSWORD;
    }
  }

  public static String getAttributeAccountDisable(String ldapType) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.AD_ACCOUNT_DISABLE;
    }
    else {
      return EldoradoBpoAdUtil.AD_ACCOUNT_DISABLE;
    }
  }

  public static String getValueAccountDisable(String ldapType) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.AD_VALUE_ACCOUNT_DESABLE;
    }
    else {
      return EldoradoBpoAdUtil.AD_VALUE_ACCOUNT_DESABLE;
    }
  }

  public static String getValueAccountEnable(String ldapType) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.AD_VALUE_ACCOUNT_ENABLE;
    }
    else {
      return EldoradoBpoAdUtil.AD_VALUE_ACCOUNT_ENABLE;
    }
  }

  // public static String getAttributeNamePassrodExpire(String ldapType) {
  // if (ldapType.equals(ADAM_TYPE)) {
  // return EldoradoAdamUtil.AD_PASSWORD_EXPIRE;
  // }
  // else {
  // return EldoradoBpoAdUtil.AD_PASSWORD_EXPIRE;
  // }
  // }
  //
  // public static String getAttributeNamePasswordNotPaymentMode(String
  // ldapType) {
  // if (ldapType.equals(ADAM_TYPE)) {
  // return EldoradoAdamUtil.AD_PAYMENT_MODE;
  // }
  // else {
  // return EldoradoBpoAdUtil.AD_PAYMENT_MODE;
  // }
  // }

  public static String fillEmptyField(String value) {
    if (value == null) {
      return "-";
    }
    if (value.isEmpty()) {
      return "-";
    }
    return value;
  }

  public static String texteToTextArea(String value) {
    if (value.isEmpty() || value.equals("-")) {
      return "";
    }
    return value.replaceAll("##", "\n");
  }

  public static String textareaToText(String value) {
    if (value.isEmpty()) {
      return "-";
    }
    return value.replaceAll("\n", "##");
  }

  public static String getMapValue(Map<String, String> mapAttributes, String key) {
    String value = mapAttributes.get(key);
    if (value == null)
      value = "";
    if (value.equals("-")) {
      value = "";
    }
    return value;
  }

  public static Map<String, String> convertListToMap(List<PartnerAttributeDto> attributes) {
    Map<String, String> mapAttributes = new TreeMap<String, String>();
    for (PartnerAttributeDto attribute : attributes) {
      mapAttributes.put(attribute.getKey(), attribute.getValue());
    }
    return mapAttributes;
  }

  public static String constructPartnerIdUser(String user) {
    StringBuilder id = new StringBuilder();

    id.append(PREFIX_USER);
    id.append(user);

    return id.toString();
  }

  public static PartnerQualifierEnumDto convertQualifier(String value) {
    if ("MR".equals(value))
      return PartnerQualifierEnumDto.MR;
    if ("MRS".equals(value))
      return PartnerQualifierEnumDto.MRS;
    if ("MISS".equals(value))
      return PartnerQualifierEnumDto.MISS;

    return PartnerQualifierEnumDto.COMPANY;

  }

  public static PartnerCountryEnumDto getPartnerEnumCountry(String countryCode) {
    if (countryCode == null || countryCode.length() == 0 || countryCode.equals("CH")) {
      return PartnerCountryEnumDto.CH;
    }
    else if (countryCode.equals("AT")) {
      return PartnerCountryEnumDto.AT;
    }
    else if (countryCode.equals("DE")) {
      return PartnerCountryEnumDto.DE;
    }
    else if (countryCode.equals("FR")) {
      return PartnerCountryEnumDto.FR;
    }
    else if (countryCode.equals("IT")) {
      return PartnerCountryEnumDto.IT;
    }
    return PartnerCountryEnumDto.OTHER;
  }

  public static List<String> fillSearchAttribute(String ldapType) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.fillSearchAttribute();
    }
    else {
      return EldoradoBpoAdUtil.fillSearchAttribute();
    }
  }

  public static List<String> fillAddressAttribute(String ldapType) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.fillAddressAttribute();
    }
    else {
      return EldoradoBpoAdUtil.fillAddressAttribute();
    }
  }

  public static PartnerDto fillPartnerFromActiveDirectory(String ldapType, List<PartnerAttributeDto> attributes,
      String bpoPrefix) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.fillPartnerFromActiveDirectory(attributes, bpoPrefix);
    }
    else {
      return EldoradoBpoAdUtil.fillPartnerFromActiveDirectory(attributes, bpoPrefix);
    }
  }

  public static List<PartnerAttributeDto> fillAdUserAttributes(String ldapType, PartnerDto userDto,
      List<PartnerAttributeDto> attributes) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.fillAdUserAttributes(userDto, attributes);
    }
    else {
      return EldoradoBpoAdUtil.fillAdUserAttributes(userDto, attributes);
    }
  }

  public static List<PartnerAttributeDto> fillStandardInsertAttributes(String ldapType, String password) {
    if (ldapType.equals(ADAM_TYPE)) {
      return EldoradoAdamUtil.fillStandardInsertAttributes(password);
    }
    else {
      return EldoradoBpoAdUtil.fillStandardInsertAttributes(password);
    }
  }
}
