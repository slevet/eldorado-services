package com.publigroupe.eldorado.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.publigroupe.partner.domain.p2000.PartnerConstantP2000;
import com.publigroupe.partner.dto.PartnerAddressTypeEnumDto;
import com.publigroupe.partner.dto.PartnerAttributeDto;
import com.publigroupe.partner.dto.PartnerCountryEnumDto;
import com.publigroupe.partner.dto.PartnerDto;
import com.publigroupe.partner.dto.PartnerIdDto;
import com.publigroupe.partner.dto.PartnerPhysicalAddressDto;
import com.publigroupe.partner.dto.PartnerPhysicalAddressTypeEnumDto;
import com.publigroupe.partner.dto.PartnerQualifierEnumDto;
import com.publigroupe.partner.dto.PartnerQualifierInfoDto;
import com.publigroupe.partner.dto.PartnerTypeEnumDto;

public class EldoradoAdamUtil extends EldoradoActiveDirectoryUtil {
  public static final String AD_DESCRIPTION = "adminDescription";
  public static final String AD_EXTERNAL_PARTNER_ID = "employeeNumber";
  public static final String AD_PAYMENT_MODE = "employeeType";
  public static final String AD_PASSWORD = "userpassword";
  public static final String AD_ACCOUNT_DISABLE = "msDS-UserAccountDisabled";
  public static final String AD_VALUE_ACCOUNT_DESABLE = "TRUE";
  public static final String AD_VALUE_ACCOUNT_ENABLE = "FALSE";

  public static List<PartnerAttributeDto> fillAdUserAttributes(PartnerDto userDto, List<PartnerAttributeDto> attributes) {

    attributes.add(new PartnerAttributeDto("preferredLanguage", fillEmptyField(userDto.getLanguage())));
    attributes.add(new PartnerAttributeDto("title",
        fillEmptyField(userDto.getQualifierInfo().getQualifier().toString())));
    if (userDto.getAddressType().equals(PartnerAddressTypeEnumDto.PERSON)) {
      attributes.add(new PartnerAttributeDto("company", fillEmptyField(userDto.getQualifierInfo().getLastName())));
      attributes.add(new PartnerAttributeDto("givenName", fillEmptyField(userDto.getQualifierInfo().getFirstName())));
      attributes.add(new PartnerAttributeDto("uid", fillEmptyField("")));
    }
    else {
      attributes.add(new PartnerAttributeDto("company", fillEmptyField(userDto.getQualifierInfo().getCompanyName())));
      attributes.add(new PartnerAttributeDto("givenName",
          fillEmptyField(userDto.getQualifierInfo().getCompanyContactName())));
      attributes.add(new PartnerAttributeDto("uid", fillEmptyField(userDto.getFree2())));
    }
    attributes.add(new PartnerAttributeDto("mail", fillEmptyField(userDto.getQualifierInfo().getEmailAddress())));
    attributes.add(new PartnerAttributeDto("homePhone", fillEmptyField(merge2Fields(
        userDto.getQualifierInfo().getPhoneCountryCode(), userDto.getQualifierInfo().getPhoneNumber()))));
    attributes.add(new PartnerAttributeDto("mobile", fillEmptyField(merge2Fields(
        userDto.getQualifierInfo().getMobileCountryCode(), userDto.getQualifierInfo().getMobilePhone()))));

    attributes.add(new PartnerAttributeDto("c", fillEmptyField(userDto.getPhysicalAddress().getCountry().toString())));
    if (userDto.getPhysicalAddress().getCountry().equals(PartnerCountryEnumDto.CH)) {
      attributes.add(new PartnerAttributeDto("st", fillEmptyField("")));

    }
    else {
      attributes.add(new PartnerAttributeDto("st", fillEmptyField(userDto.getPhysicalAddress().getRegion())));
    }
    attributes.add(new PartnerAttributeDto("street", fillEmptyField(userDto.getPhysicalAddress().getStreet())));
    attributes.add(new PartnerAttributeDto("houseIdentifier", fillEmptyField(merge2Fields(
        userDto.getPhysicalAddress().getStreetNr(), userDto.getPhysicalAddress().getStreetNrComp()))));
    attributes.add(new PartnerAttributeDto("l", merge2Fields(userDto.getPhysicalAddress().getZip(),
        userDto.getPhysicalAddress().getCity())));
    // attributes.add(new PartnerAttributeDto("l",
    // fillEmptyField(userDto.getPhysicalAddress().getCity())));
    attributes.add(new PartnerAttributeDto("postOfficeBox", fillEmptyField(userDto.getPhysicalAddress().getPoBoxNr())));
    attributes.add(new PartnerAttributeDto("postalCode", fillEmptyField(userDto.getPhysicalAddress().getPoBoxZip())));
    attributes.add(new PartnerAttributeDto("postalAddress", fillEmptyField(userDto.getPhysicalAddress().getPoBoxCity())));
    attributes.add(new PartnerAttributeDto("description", fillEmptyField(merge2Fields(userDto.isReceiveNews() ? "TRUE"
        : "FALSE", userDto.isReceiveOffers() ? "TRUE" : "FALSE"))));

    return attributes;
  }

  private static String getFirstField(String codeValue) {
    String[] codeValueSplit = codeValue.split("#");
    if (codeValueSplit == null) {
      return "";
    }
    return codeValueSplit[0];
  }

  private static String getSecondField(String codeValue) {
    String[] codeValueSplit = codeValue.split("#");
    if (codeValueSplit == null || codeValueSplit.length < 2) {
      return "";
    }
    return codeValueSplit[1];
  }

  private static String merge2Fields(String firstField, String secondField) {
    if (secondField == null) {
      return "";
    }
    if (firstField == null) {
      return "" + "#" + secondField;
    }
    return firstField + "#" + secondField;
  }

  public static List<String> fillAddressAttribute() {
    List<String> attributeKeys = new ArrayList<String>();
    attributeKeys.add("msDS-UserAccountDisabled");
    attributeKeys.add("cn");
    attributeKeys.add("preferredLanguage");
    attributeKeys.add("title");
    attributeKeys.add("company");
    attributeKeys.add("givenName");
    attributeKeys.add("uid");
    attributeKeys.add("mail");
    attributeKeys.add("homePhone");
    attributeKeys.add("mobile");
    attributeKeys.add("c");
    attributeKeys.add("st");
    attributeKeys.add("street");
    attributeKeys.add("houseIdentifier");
    attributeKeys.add("l");
    // attributeKeys.add("locality");
    attributeKeys.add("postOfficeBox");
    attributeKeys.add("postalCode");
    attributeKeys.add("postalAddress");
    attributeKeys.add("description");
    return attributeKeys;
  }

  public static List<String> fillSearchAttribute() {
    List<String> attributeKeys = new ArrayList<String>();
    attributeKeys.add("cn");
    attributeKeys.add("title");
    attributeKeys.add("company");
    attributeKeys.add("givenName");
    attributeKeys.add("l");
    attributeKeys.add("employeeNumber");
    return attributeKeys;
  }

  public static PartnerDto fillPartnerFromActiveDirectory(List<PartnerAttributeDto> attributes, String bpoPrefix) {
    Map<String, String> mapAttributes = convertListToMap(attributes);
    PartnerDto user = new PartnerDto();
    PartnerIdDto id = new PartnerIdDto();
    id.setOwnerId("ADAM");
    id.setType(PartnerTypeEnumDto.USER);
    id.setId(constructPartnerIdUser(getMapValue(mapAttributes, "cn")));
    user.setId(id);
    user.setCodeList(PartnerConstantP2000.PARTNER_USER_CODELIST);
    user.setCodeValue(bpoPrefix + getMapValue(mapAttributes, "cn"));
    user.setLanguage(getMapValue(mapAttributes, "preferredLanguage"));
    user.setFree2(getMapValue(mapAttributes, "uid"));
    user.setReceiveNews(getFirstField(getMapValue(mapAttributes, "description")).equals("TRUE"));
    user.setReceiveOffers(getSecondField(getMapValue(mapAttributes, "description")).equals("TRUE"));

    PartnerQualifierInfoDto qualifierInfoDto = new PartnerQualifierInfoDto();
    qualifierInfoDto.setQualifier(convertQualifier(getMapValue(mapAttributes, "title")));
    if (qualifierInfoDto.getQualifier().equals(PartnerQualifierEnumDto.COMPANY)) {
      qualifierInfoDto.setLastName("");
      qualifierInfoDto.setFirstName("");
      qualifierInfoDto.setCompanyName(getMapValue(mapAttributes, "company"));
      qualifierInfoDto.setComplement("");
      qualifierInfoDto.setComplement2("");
      qualifierInfoDto.setCompanyContactName(getMapValue(mapAttributes, "givenName"));
      user.setAddressType(PartnerAddressTypeEnumDto.COMPANY);
    }
    else {
      qualifierInfoDto.setCompanyName("");
      qualifierInfoDto.setCompanyContactName("");
      qualifierInfoDto.setLastName(getMapValue(mapAttributes, "company"));
      qualifierInfoDto.setFirstName(getMapValue(mapAttributes, "givenName"));
      qualifierInfoDto.setComplement("");
      qualifierInfoDto.setComplement2("");
      user.setAddressType(PartnerAddressTypeEnumDto.PERSON);
    }

    qualifierInfoDto.setEmailAddress(getMapValue(mapAttributes, "mail"));
    qualifierInfoDto.setPhoneNumber(getSecondField(getMapValue(mapAttributes, "homePhone")));
    qualifierInfoDto.setPhoneCountryCode(getFirstField(getMapValue(mapAttributes, "homePhone")));
    qualifierInfoDto.setMobilePhone(getSecondField(getMapValue(mapAttributes, "mobile")));
    qualifierInfoDto.setMobileCountryCode(getFirstField(getMapValue(mapAttributes, "mobile")));
    qualifierInfoDto.setFaxPhone("");
    qualifierInfoDto.setFaxCountryCode("");
    user.setQualifierInfo(qualifierInfoDto);

    PartnerPhysicalAddressDto physicalAddressDto = new PartnerPhysicalAddressDto();
    physicalAddressDto.setStreet(getMapValue(mapAttributes, "street"));
    physicalAddressDto.setStreetNr(getFirstField(getMapValue(mapAttributes, "houseIdentifier")));
    physicalAddressDto.setStreetNrComp(getSecondField(getMapValue(mapAttributes, "houseIdentifier")));
    physicalAddressDto.setCountry(getPartnerEnumCountry(getMapValue(mapAttributes, "c")));
    if (physicalAddressDto.getCountry() == PartnerCountryEnumDto.CH) {
      physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.STANDARD);
    }
    else {
      physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.FOREIGN);
    }
    physicalAddressDto.setRegion(getMapValue(mapAttributes, "st"));
    physicalAddressDto.setZip(getFirstField(getMapValue(mapAttributes, "l")));
    physicalAddressDto.setCity(getSecondField(getMapValue(mapAttributes, "l")));

    physicalAddressDto.setPoBoxCity(getMapValue(mapAttributes, "postalAddress"));
    physicalAddressDto.setPoBoxZip(getMapValue(mapAttributes, "postalCode"));
    physicalAddressDto.setPoBoxNr(getMapValue(mapAttributes, "postOfficeBox"));

    user.setPhysicalAddress(physicalAddressDto);

    return user;
  }

  public static PartnerDto fillSearchPartnerFromActiveDirectory(List<PartnerAttributeDto> attributes, String bpoPrefix) {
    Map<String, String> mapAttributes = convertListToMap(attributes);
    PartnerDto user = new PartnerDto();
    PartnerIdDto id = new PartnerIdDto();
    id.setOwnerId("ADAM");
    id.setType(PartnerTypeEnumDto.USER);
    id.setId(constructPartnerIdUser(getMapValue(mapAttributes, "cn")));
    user.setId(id);
    user.setCodeList(PartnerConstantP2000.PARTNER_USER_CODELIST);
    user.setCodeValue(bpoPrefix + getMapValue(mapAttributes, "cn"));
    user.setLanguage(getMapValue(mapAttributes, "preferredLanguage"));
    user.setFree2(getMapValue(mapAttributes, "uid"));
    user.setReceiveNews(getFirstField(getMapValue(mapAttributes, "description")).equals("TRUE"));
    user.setReceiveOffers(getSecondField(getMapValue(mapAttributes, "description")).equals("TRUE"));

    PartnerQualifierInfoDto qualifierInfoDto = new PartnerQualifierInfoDto();
    qualifierInfoDto.setQualifier(convertQualifier(getMapValue(mapAttributes, "title")));
    if (qualifierInfoDto.getQualifier().equals(PartnerQualifierEnumDto.COMPANY)) {
      qualifierInfoDto.setLastName("");
      qualifierInfoDto.setFirstName("");
      qualifierInfoDto.setCompanyName(getMapValue(mapAttributes, "company"));
      qualifierInfoDto.setComplement("");
      qualifierInfoDto.setComplement2("");
      qualifierInfoDto.setCompanyContactName(getMapValue(mapAttributes, "givenName"));
      user.setAddressType(PartnerAddressTypeEnumDto.COMPANY);
    }
    else {
      qualifierInfoDto.setCompanyName("");
      qualifierInfoDto.setCompanyContactName("");
      qualifierInfoDto.setLastName(getMapValue(mapAttributes, "company"));
      qualifierInfoDto.setFirstName(getMapValue(mapAttributes, "givenName"));
      qualifierInfoDto.setComplement("");
      qualifierInfoDto.setComplement2("");
      user.setAddressType(PartnerAddressTypeEnumDto.PERSON);
    }

    qualifierInfoDto.setEmailAddress(getMapValue(mapAttributes, "mail"));
    qualifierInfoDto.setPhoneNumber(getSecondField(getMapValue(mapAttributes, "homePhone")));
    qualifierInfoDto.setPhoneCountryCode(getFirstField(getMapValue(mapAttributes, "homePhone")));
    qualifierInfoDto.setMobilePhone(getSecondField(getMapValue(mapAttributes, "mobile")));
    qualifierInfoDto.setMobileCountryCode(getFirstField(getMapValue(mapAttributes, "mobile")));
    qualifierInfoDto.setFaxPhone("");
    qualifierInfoDto.setFaxCountryCode("");
    user.setQualifierInfo(qualifierInfoDto);

    PartnerPhysicalAddressDto physicalAddressDto = new PartnerPhysicalAddressDto();
    physicalAddressDto.setStreet(getMapValue(mapAttributes, "street"));
    physicalAddressDto.setStreetNr(getFirstField(getMapValue(mapAttributes, "houseIdentifier")));
    physicalAddressDto.setStreetNrComp(getSecondField(getMapValue(mapAttributes, "houseIdentifier")));
    physicalAddressDto.setCountry(getPartnerEnumCountry(getMapValue(mapAttributes, "c")));
    if (physicalAddressDto.getCountry() == PartnerCountryEnumDto.CH) {
      physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.STANDARD);
    }
    else {
      physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.FOREIGN);
    }
    physicalAddressDto.setRegion(getMapValue(mapAttributes, "st"));
    physicalAddressDto.setZip(getFirstField(getMapValue(mapAttributes, "l")));
    physicalAddressDto.setCity(getSecondField(getMapValue(mapAttributes, "l")));

    physicalAddressDto.setPoBoxCity(getMapValue(mapAttributes, "postalAddress"));
    physicalAddressDto.setPoBoxZip(getMapValue(mapAttributes, "postalCode"));
    physicalAddressDto.setPoBoxNr(getMapValue(mapAttributes, "postOfficeBox"));

    user.setPhysicalAddress(physicalAddressDto);

    return user;
  }

  public static List<PartnerAttributeDto> fillStandardInsertAttributes(String password) {
    List<PartnerAttributeDto> attributes = new ArrayList<PartnerAttributeDto>();
    attributes.add(new PartnerAttributeDto("msDS-UserAccountDisabled", "TRUE"));
    attributes.add(new PartnerAttributeDto("msDS-UserDontExpirePassword", "FALSE"));
    attributes.add(new PartnerAttributeDto("ms-DS-UserPasswordNotRequired", "FALSE"));
    attributes.add(new PartnerAttributeDto("lockoutTime", "0"));
    if (password != null) {
      attributes.add(new PartnerAttributeDto(AD_PASSWORD, password));
    }
    return attributes;
  }

}
