package com.publigroupe.eldorado.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.publigroupe.partner.domain.p2000.PartnerConstantP2000;
import com.publigroupe.partner.dto.PartnerAddressTypeEnumDto;
import com.publigroupe.partner.dto.PartnerAttributeDto;
import com.publigroupe.partner.dto.PartnerCountryEnumDto;
import com.publigroupe.partner.dto.PartnerDto;
import com.publigroupe.partner.dto.PartnerIdDto;
import com.publigroupe.partner.dto.PartnerPhysicalAddressDto;
import com.publigroupe.partner.dto.PartnerPhysicalAddressTypeEnumDto;
import com.publigroupe.partner.dto.PartnerQualifierEnumDto;
import com.publigroupe.partner.dto.PartnerQualifierInfoDto;
import com.publigroupe.partner.dto.PartnerTypeEnumDto;

public class EldoradoBpoAdUtil extends EldoradoActiveDirectoryUtil {
  public static final String AD_DESCRIPTION = "comment";
  public static final String AD_EXTERNAL_PARTNER_ID = "extensionAttribute8";
  public static final String AD_PAYMENT_MODE = "info";
  public static final String AD_PASSWORD = "userpassword";
  public static final String AD_ACCOUNT_DISABLE = "userAccountControl";
  public static final String AD_VALUE_ACCOUNT_DESABLE = "66050";
  public static final String AD_VALUE_ACCOUNT_ENABLE = "66048";
  public static final String AD_UNREGISTERED_PASSWORD = "Self2015@Xentive";

  public static List<PartnerAttributeDto> fillAdUserAttributes(PartnerDto userDto, List<PartnerAttributeDto> attributes) {

    attributes.add(new PartnerAttributeDto("language", fillEmptyField(userDto.getLanguage())));
    attributes.add(new PartnerAttributeDto("title",
        fillEmptyField(userDto.getQualifierInfo().getQualifier().toString())));
    if (userDto.getAddressType().equals(PartnerAddressTypeEnumDto.PERSON)) {
      attributes.add(new PartnerAttributeDto("sn", fillEmptyField(userDto.getQualifierInfo().getLastName())));
      attributes.add(new PartnerAttributeDto("givenName", fillEmptyField(userDto.getQualifierInfo().getFirstName())));
      attributes.add(new PartnerAttributeDto("extensionAttribute5", fillEmptyField("")));
    }
    else {
      attributes.add(new PartnerAttributeDto("sn", fillEmptyField(userDto.getQualifierInfo().getCompanyName())));
      attributes.add(new PartnerAttributeDto("givenName",
          fillEmptyField(userDto.getQualifierInfo().getCompanyContactName())));
      attributes.add(new PartnerAttributeDto("extensionAttribute5", fillEmptyField(userDto.getFree2())));
    }
    attributes.add(new PartnerAttributeDto("mail", fillEmptyField(userDto.getQualifierInfo().getEmailAddress())));
    attributes.add(new PartnerAttributeDto("telephoneNumber", fillEmptyField(merge2Fields(
        userDto.getQualifierInfo().getPhoneCountryCode(), userDto.getQualifierInfo().getPhoneNumber()))));
    attributes.add(new PartnerAttributeDto("mobile", fillEmptyField(merge2Fields(
        userDto.getQualifierInfo().getMobileCountryCode(), userDto.getQualifierInfo().getMobilePhone()))));

    attributes.add(new PartnerAttributeDto("co", fillEmptyField(userDto.getPhysicalAddress().getCountry().toString())));
    if (userDto.getPhysicalAddress().getCountry().equals(PartnerCountryEnumDto.CH)) {
      attributes.add(new PartnerAttributeDto("st", fillEmptyField("")));

    }
    else {
      attributes.add(new PartnerAttributeDto("st", fillEmptyField(userDto.getPhysicalAddress().getRegion())));
    }
    attributes.add(new PartnerAttributeDto("streetAddress", fillEmptyField(userDto.getPhysicalAddress().getStreet())));
    attributes.add(new PartnerAttributeDto("postalAddress", fillEmptyField(merge2Fields(
        userDto.getPhysicalAddress().getStreetNr(), userDto.getPhysicalAddress().getStreetNrComp()))));
    attributes.add(new PartnerAttributeDto("postalCode", fillEmptyField(userDto.getPhysicalAddress().getZip())));
    attributes.add(new PartnerAttributeDto("l", fillEmptyField(userDto.getPhysicalAddress().getCity())));
    attributes.add(new PartnerAttributeDto("postOfficeBox", fillEmptyField(userDto.getPhysicalAddress().getPoBoxNr())));
    attributes.add(new PartnerAttributeDto("homePostalAddress",
        fillEmptyField(userDto.getPhysicalAddress().getPoBoxZip())));
    attributes.add(new PartnerAttributeDto("physicalDeliveryOfficeName",
        fillEmptyField(userDto.getPhysicalAddress().getPoBoxCity())));
    attributes.add(new PartnerAttributeDto("extensionAttribute6", fillEmptyField(userDto.isReceiveNews() ? "TRUE"
        : "FALSE")));
    attributes.add(new PartnerAttributeDto("extensionAttribute7", fillEmptyField(userDto.isReceiveOffers() ? "TRUE"
        : "FALSE")));

    return attributes;
  }

  private static String getFirstField(String codeValue) {
    String[] codeValueSplit = codeValue.split("#");
    if (codeValueSplit == null) {
      return "";
    }
    return codeValueSplit[0];
  }

  private static String getSecondField(String codeValue) {
    String[] codeValueSplit = codeValue.split("#");
    if (codeValueSplit == null || codeValueSplit.length < 2) {
      return "";
    }
    return codeValueSplit[1];
  }

  private static String merge2Fields(String firstField, String secondField) {
    if (secondField == null) {
      return "";
    }
    if (firstField == null) {
      return "" + "#" + secondField;
    }
    return firstField + "#" + secondField;
  }

  public static List<String> fillAddressAttribute() {
    List<String> attributeKeys = new ArrayList<String>();
    attributeKeys.add("msDS-UserAccountDisabled");
    attributeKeys.add("cn");
    attributeKeys.add("language");
    attributeKeys.add("title");
    attributeKeys.add("sn");
    attributeKeys.add("givenName");
    attributeKeys.add("extensionAttribute5");
    attributeKeys.add("mail");
    attributeKeys.add("telephoneNumber");
    attributeKeys.add("mobile");
    attributeKeys.add("co");
    attributeKeys.add("st");
    attributeKeys.add("streetAddress");
    attributeKeys.add("postalAddress");
    attributeKeys.add("postalCode");
    attributeKeys.add("l");
    attributeKeys.add("postOfficeBox");
    attributeKeys.add("homePostalAddress");
    attributeKeys.add("physicalDeliveryOfficeName");
    attributeKeys.add("extensionAttribute6");
    attributeKeys.add("extensionAttribute7");
    return attributeKeys;
  }

  public static List<String> fillSearchAttribute() {
    List<String> attributeKeys = new ArrayList<String>();
    attributeKeys.add("cn");
    attributeKeys.add("title");
    attributeKeys.add("sn");
    attributeKeys.add("givenName");
    attributeKeys.add("postalCode");
    attributeKeys.add("l");
    attributeKeys.add("employeeNumber");
    return attributeKeys;
  }

  public static PartnerDto fillPartnerFromActiveDirectory(List<PartnerAttributeDto> attributes, String bpoPrefix) {
    Map<String, String> mapAttributes = convertListToMap(attributes);
    PartnerDto user = new PartnerDto();
    PartnerIdDto id = new PartnerIdDto();
    id.setOwnerId("ADAM");
    id.setType(PartnerTypeEnumDto.USER);
    id.setId(constructPartnerIdUser(getMapValue(mapAttributes, "cn")));
    user.setId(id);
    user.setCodeList(PartnerConstantP2000.PARTNER_USER_CODELIST);
    user.setCodeValue(bpoPrefix + getMapValue(mapAttributes, "cn"));
    user.setLanguage(getMapValue(mapAttributes, "language"));
    user.setFree2(getMapValue(mapAttributes, "extensionAttribute5"));
    user.setReceiveNews(getMapValue(mapAttributes, "extensionAttribute6").equals("TRUE"));
    user.setReceiveOffers(getMapValue(mapAttributes, "extensionAttribute7").equals("TRUE"));

    PartnerQualifierInfoDto qualifierInfoDto = new PartnerQualifierInfoDto();
    qualifierInfoDto.setQualifier(convertQualifier(getMapValue(mapAttributes, "title")));
    if (qualifierInfoDto.getQualifier().equals(PartnerQualifierEnumDto.COMPANY)) {
      qualifierInfoDto.setLastName("");
      qualifierInfoDto.setFirstName("");
      qualifierInfoDto.setCompanyName(getMapValue(mapAttributes, "sn"));
      qualifierInfoDto.setComplement("");
      qualifierInfoDto.setComplement2("");
      qualifierInfoDto.setCompanyContactName(getMapValue(mapAttributes, "givenName"));
      user.setAddressType(PartnerAddressTypeEnumDto.COMPANY);
    }
    else {
      qualifierInfoDto.setCompanyName("");
      qualifierInfoDto.setCompanyContactName("");
      qualifierInfoDto.setLastName(getMapValue(mapAttributes, "sn"));
      qualifierInfoDto.setFirstName(getMapValue(mapAttributes, "givenName"));
      qualifierInfoDto.setComplement("");
      qualifierInfoDto.setComplement2("");
      user.setAddressType(PartnerAddressTypeEnumDto.PERSON);
    }

    qualifierInfoDto.setEmailAddress(getMapValue(mapAttributes, "mail"));
    qualifierInfoDto.setPhoneNumber(getSecondField(getMapValue(mapAttributes, "telephoneNumber")));
    qualifierInfoDto.setPhoneCountryCode(getFirstField(getMapValue(mapAttributes, "telephoneNumber")));
    qualifierInfoDto.setMobilePhone(getSecondField(getMapValue(mapAttributes, "mobile")));
    qualifierInfoDto.setMobileCountryCode(getFirstField(getMapValue(mapAttributes, "mobile")));
    qualifierInfoDto.setFaxPhone("");
    qualifierInfoDto.setFaxCountryCode("");
    user.setQualifierInfo(qualifierInfoDto);

    PartnerPhysicalAddressDto physicalAddressDto = new PartnerPhysicalAddressDto();
    physicalAddressDto.setStreet(getMapValue(mapAttributes, "streetAddress"));
    physicalAddressDto.setStreetNr(getFirstField(getMapValue(mapAttributes, "postalAddress")));
    physicalAddressDto.setStreetNrComp(getSecondField(getMapValue(mapAttributes, "postalAddress")));
    physicalAddressDto.setCountry(getPartnerEnumCountry(getMapValue(mapAttributes, "co")));
    if (physicalAddressDto.getCountry() == PartnerCountryEnumDto.CH) {
      physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.STANDARD);
    }
    else {
      physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.FOREIGN);
    }
    physicalAddressDto.setRegion(getMapValue(mapAttributes, "st"));
    physicalAddressDto.setZip(getMapValue(mapAttributes, "postalCode"));
    physicalAddressDto.setCity(getMapValue(mapAttributes, "l"));

    physicalAddressDto.setPoBoxCity(getMapValue(mapAttributes, "physicalDeliveryOfficeName"));
    physicalAddressDto.setPoBoxZip(getMapValue(mapAttributes, "homePostalAddress"));
    physicalAddressDto.setPoBoxNr(getMapValue(mapAttributes, "postOfficeBox"));

    user.setPhysicalAddress(physicalAddressDto);

    return user;
  }

  public static PartnerDto fillSearchPartnerFromActiveDirectory(List<PartnerAttributeDto> attributes, String bpoPrefix) {
    Map<String, String> mapAttributes = convertListToMap(attributes);
    PartnerDto user = new PartnerDto();
    PartnerIdDto id = new PartnerIdDto();
    id.setOwnerId("ADAM");
    id.setType(PartnerTypeEnumDto.USER);
    id.setId(constructPartnerIdUser(getMapValue(mapAttributes, "cn")));
    user.setId(id);
    user.setCodeList(PartnerConstantP2000.PARTNER_USER_CODELIST);
    user.setCodeValue(bpoPrefix + getMapValue(mapAttributes, "cn"));
    user.setLanguage(getMapValue(mapAttributes, "language"));
    user.setFree2(getMapValue(mapAttributes, "extensionAttribute5"));
    user.setReceiveNews(getMapValue(mapAttributes, "extensionAttribute6").equals("TRUE"));
    user.setReceiveOffers(getMapValue(mapAttributes, "extensionAttribute7").equals("TRUE"));

    PartnerQualifierInfoDto qualifierInfoDto = new PartnerQualifierInfoDto();
    qualifierInfoDto.setQualifier(convertQualifier(getMapValue(mapAttributes, "title")));
    if (qualifierInfoDto.getQualifier().equals(PartnerQualifierEnumDto.COMPANY)) {
      qualifierInfoDto.setLastName("");
      qualifierInfoDto.setFirstName("");
      qualifierInfoDto.setCompanyName(getMapValue(mapAttributes, "sn"));
      qualifierInfoDto.setComplement("");
      qualifierInfoDto.setComplement2("");
      qualifierInfoDto.setCompanyContactName(getMapValue(mapAttributes, "givenName"));
      user.setAddressType(PartnerAddressTypeEnumDto.COMPANY);
    }
    else {
      qualifierInfoDto.setCompanyName("");
      qualifierInfoDto.setCompanyContactName("");
      qualifierInfoDto.setLastName(getMapValue(mapAttributes, "sn"));
      qualifierInfoDto.setFirstName(getMapValue(mapAttributes, "givenName"));
      qualifierInfoDto.setComplement("");
      qualifierInfoDto.setComplement2("");
      user.setAddressType(PartnerAddressTypeEnumDto.PERSON);
    }

    qualifierInfoDto.setEmailAddress(getMapValue(mapAttributes, "mail"));
    qualifierInfoDto.setPhoneNumber(getSecondField(getMapValue(mapAttributes, "telephoneNumber")));
    qualifierInfoDto.setPhoneCountryCode(getFirstField(getMapValue(mapAttributes, "telephoneNumber")));
    qualifierInfoDto.setMobilePhone(getSecondField(getMapValue(mapAttributes, "mobile")));
    qualifierInfoDto.setMobileCountryCode(getFirstField(getMapValue(mapAttributes, "mobile")));
    qualifierInfoDto.setFaxPhone("");
    qualifierInfoDto.setFaxCountryCode("");
    user.setQualifierInfo(qualifierInfoDto);

    PartnerPhysicalAddressDto physicalAddressDto = new PartnerPhysicalAddressDto();
    physicalAddressDto.setStreet(getMapValue(mapAttributes, "streetAddress"));
    physicalAddressDto.setStreetNr(getFirstField(getMapValue(mapAttributes, "postalAddress")));
    physicalAddressDto.setStreetNrComp(getSecondField(getMapValue(mapAttributes, "postalAddress")));
    physicalAddressDto.setCountry(getPartnerEnumCountry(getMapValue(mapAttributes, "co")));
    if (physicalAddressDto.getCountry() == PartnerCountryEnumDto.CH) {
      physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.STANDARD);
    }
    else {
      physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.FOREIGN);
    }
    physicalAddressDto.setRegion(getMapValue(mapAttributes, "st"));
    physicalAddressDto.setZip(getMapValue(mapAttributes, "postalCode"));
    physicalAddressDto.setCity(getMapValue(mapAttributes, "l"));

    physicalAddressDto.setPoBoxCity(getMapValue(mapAttributes, "physicalDeliveryOfficeName"));
    physicalAddressDto.setPoBoxZip(getMapValue(mapAttributes, "homePostalAddress"));
    physicalAddressDto.setPoBoxNr(getMapValue(mapAttributes, "postOfficeBox"));

    user.setPhysicalAddress(physicalAddressDto);

    return user;
  }

  public static List<PartnerAttributeDto> fillStandardInsertAttributes(String password) {
    List<PartnerAttributeDto> attributes = new ArrayList<PartnerAttributeDto>();
    attributes.add(new PartnerAttributeDto("lockoutTime", "0"));
    if (password != null) {
      attributes.add(new PartnerAttributeDto("userAccountControl", AD_VALUE_ACCOUNT_DESABLE));
    }
    return attributes;
  }
}
