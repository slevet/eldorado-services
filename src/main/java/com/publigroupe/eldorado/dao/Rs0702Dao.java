package com.publigroupe.eldorado.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.publigroupe.basis.sql.SqlBuilder;
import com.publigroupe.order.dao.p2000.OrderClause;
import com.publigroupe.order.dao.p2000.bean.Rs0702;

public class Rs0702Dao {

  public Rs0702 read(String utiste3, String uticpr, String dicapp, String parno, String parcle1, String parcle2,
      Date valdatd) {
    Rs0702 rs0702 = null;

    OrderClause clause = new OrderClause();
    SqlBuilder sb = new SqlBuilder();
    sb.append("select *");
    sb.append("from RS0702");
    sb.append("where UTISTE3 = " + clause.param(utiste3));
    sb.append("  and UTICPR = " + clause.param(uticpr));
    sb.append("  and DICAPP = " + clause.param(dicapp));
    sb.append("  and PARNO = " + clause.param(parno));
    sb.append("  and PARCLE1 = " + clause.param(parcle1));
    sb.append("  and PARCLE2 in ('',  " + clause.param(parcle2) + ")");
    sb.append("  and VALDATD <= " + clause.paramDate(valdatd));
    sb.append("  and VALDATF >= " + clause.paramDate(valdatd));
    sb.append("  order by PARCLE2 DESC");
    clause.set(sb.toString());
    clause.prepareAndExecute();
    if (clause.next()) {
      rs0702 = new Rs0702();
      rs0702.utiste3 = clause.getString("UTISTE3");
      rs0702.uticpr = clause.getString("UTICPR");
      rs0702.dicapp = clause.getString("DICAPP");
      rs0702.parno = clause.getString("PARNO");
      rs0702.parcle1 = clause.getString("PARCLE1");
      rs0702.parcle2 = clause.getString("PARCLE2");
      rs0702.valdatd = clause.getDate("VALDATD");
      rs0702.valdatf = clause.getDate("VALDATF");
      rs0702.parval2 = clause.getString("PARVAL2");
      rs0702.mutdat = clause.getDate("MUTDAT");
      rs0702.mutref = clause.getString("MUTREF");
      rs0702.dicver = clause.getInt("DICVER");
      rs0702.anncod = clause.getString("ANNCOD");
      rs0702.parval3 = clause.getString("PARVAL3");
    }
    clause.close();

    return rs0702;
  }

  public List<Rs0702> find(String utiste3, String uticpr, String dicapp, String parno, Date valdatd) {
    List<Rs0702> rs0702s = new ArrayList<Rs0702>();

    OrderClause clause = new OrderClause();
    SqlBuilder sb = new SqlBuilder();
    sb.append("select *");
    sb.append("from RS0702");
    sb.append("where UTISTE3 = " + clause.param(utiste3));
    sb.append("  and UTICPR = " + clause.param(uticpr));
    sb.append("  and DICAPP = " + clause.param(dicapp));
    sb.append("  and PARNO = " + clause.param(parno));
    sb.append("  and VALDATD <= " + clause.paramDate(valdatd));
    sb.append("  and VALDATF >= " + clause.paramDate(valdatd));
    sb.append("order by PARCLE1, PARCLE2");
    clause.set(sb.toString());
    clause.prepareAndExecute();
    while (clause.next()) {
      Rs0702 rs0702 = new Rs0702();
      rs0702 = new Rs0702();
      rs0702.utiste3 = clause.getString("UTISTE3");
      rs0702.uticpr = clause.getString("UTICPR");
      rs0702.dicapp = clause.getString("DICAPP");
      rs0702.parno = clause.getString("PARNO");
      rs0702.parcle1 = clause.getString("PARCLE1");
      rs0702.parcle2 = clause.getString("PARCLE2");
      rs0702.valdatd = clause.getDate("VALDATD");
      rs0702.valdatf = clause.getDate("VALDATF");
      rs0702.parval2 = clause.getString("PARVAL2");
      rs0702.mutdat = clause.getDate("MUTDAT");
      rs0702.mutref = clause.getString("MUTREF");
      rs0702.dicver = clause.getInt("DICVER");
      rs0702.anncod = clause.getString("ANNCOD");
      rs0702.parval3 = clause.getString("PARVAL3");
      rs0702s.add(rs0702);
    }
    clause.close();

    return rs0702s;
  }

}
