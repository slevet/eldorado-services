package com.publigroupe.eldorado.dao;

import java.util.Date;

import com.publigroupe.eldorado.dao.bean.Rsel10;
import com.publigroupe.javaBase.sql.support.SqlBuilder;
import com.publigroupe.order.dao.p2000.OrderClause;

public class Rsel10Dao {
  public Rsel10 read(int offno, Date valdatd) {
    Rsel10 rsel10 = null;

    OrderClause clause = new OrderClause();
    SqlBuilder sb = new SqlBuilder();
    sb.append("select *");
    sb.append(" from RSEL10");
    sb.append(" where OFFNO = " + clause.param(offno));
    sb.append(" and VALDATD = " + clause.paramDate(valdatd));
    clause.set(sb.toString());
    clause.prepareAndExecute();
    if (clause.next()) {
      rsel10 = new Rsel10();
      fill(rsel10, clause);
    }
    clause.close();

    return rsel10;
  }

  private void fill(Rsel10 rsel10, OrderClause clause) {
    rsel10.offno = clause.getInt("OFFNO");
    rsel10.valdatd = clause.getDate("VALDATD");
    rsel10.valdatf = clause.getDate("VALDATF");
    rsel10.eltradid = clause.getInt("ELTRADID");
    rsel10.eloffstat = clause.getString("ELOFFSTAT");
    rsel10.elmedid = clause.getInt("ELMEDID");
    rsel10.jnlcod = clause.getString("JNLCOD");
    rsel10.jnlcods = clause.getString("JNLCODS");
    rsel10.ctacatp = clause.getString("CTACATP");
    rsel10.ctainsp = clause.getString("CTAINSP");
    rsel10.ctarubg = clause.getString("CTARUBG");
    rsel10.ctarubs = clause.getString("CTARUBS");
    rsel10.ctaempl = clause.getString("CTAEMPL");
    rsel10.ctacoul = clause.getString("CTACOUL");
    rsel10.ctagen = clause.getString("CTAGEN");
    rsel10.ctaexe = clause.getString("CTAEXE");
    rsel10.ctarubt = clause.getString("CTARUBT");
    rsel10.grdgenr = clause.getString("GRDGENR");
    rsel10.offtyp = clause.getString("OFFTYP");
    rsel10.augmtyp = clause.getString("AUGMTYP");
    rsel10.exetyp = clause.getString("EXETYP");
    rsel10.grdcoln = clause.getInt("GRDCOLN");
    rsel10.grdhau = clause.getInt("GRDHAU");
    rsel10.grdtot = clause.getInt("GRDTOT");
    rsel10.mtvmin = clause.getBigDecimal("MTVMIN");
    rsel10.mutdat = clause.getDate("MUTDAT");
    rsel10.mutheu = clause.getTime("MUTHEU");
    rsel10.mutref = clause.getString("MUTREF");
    rsel10.mutver = clause.getInt("MUTVER");
    rsel10.printpobl = clause.getString("PRINTPOBL");
  }
}
