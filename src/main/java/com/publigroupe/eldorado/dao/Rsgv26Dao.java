package com.publigroupe.eldorado.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;

import com.publigroupe.eldorado.dao.bean.Rsgv26;
import com.publigroupe.javaBase.sql.support.SqlBuilder;
import com.publigroupe.order.dao.p2000.OrderClause;

public class Rsgv26Dao {
	
	/**
	 * Find Rsgv26 entries 
	 * @return Full list of Rsgv26 
	 */
	public List<Rsgv26> findAll()
	{
		List<Rsgv26> rsgv26s = new ArrayList<Rsgv26>();

		OrderClause clause = new OrderClause();
		SqlBuilder sqlBuilder = new SqlBuilder();
		sqlBuilder.append("select * from rsgv26");
		sqlBuilder.append("where");
//		sqlBuilder.append("CONFMDT = 0");   // should notifyValidation
		sqlBuilder.append("OKPARUDT = 0"); // parutions
		sqlBuilder.append("and ABDMDT = 0"); // should notifyCancellation
        
		clause.set(sqlBuilder.toString());
		clause.prepareAndExecute();

		while (clause.next())
		{
			rsgv26s.add(fill(clause));
		}
		clause.close();

		return rsgv26s;
	}
	
	/**
	 * Retrieve unique Rsgv26 entry
	 * @param utiento 
	 * @param cdenum
	 * @return
	 */
	public void delete(String utiento,String cdenum)
	{
		OrderClause clause = new OrderClause();
		SqlBuilder sqlBuilder = new SqlBuilder();
		sqlBuilder.append("delete from rsgv26 ");
		sqlBuilder.append("  where utiento = " + clause.param(utiento));
		sqlBuilder.append("  and cdenum = " + clause.param(cdenum));
		clause.set(sqlBuilder.toString());
		clause.prepareAndExecute();
		clause.close();
	}
	
	/**
	 * Retrieve unique Rsgv26 entry
	 * @param utiento 
	 * @param cdenum
	 * @return
	 */
	public Rsgv26 retrieve(String utiento, String cdenum)
	{
		Rsgv26 rsgv26 = null;
		OrderClause clause = new OrderClause();
		SqlBuilder sqlBuilder = new SqlBuilder();
		sqlBuilder.append("select * from rsgv26 ");
		sqlBuilder.append("  where utiento = " + clause.param(utiento));
		sqlBuilder.append("  and cdenum = " + clause.param(cdenum));
		clause.set(sqlBuilder.toString());
		clause.prepareAndExecute();
		if (clause.next())
		{
			rsgv26 = fill(clause);
		}
		clause.close();
		return rsgv26;
	}
	
	/**
	 * Insert entry in resel01 table
	 * @param rsel01 entry corresponding to a brand
	 * @return ID of the inserted brand 
	 */
	public boolean insert(Rsgv26 rsgv26)
	{
		Validate.isTrue(rsgv26.utiste!=null, "insert into Rsgv26, utiste shouldn't be null");
		Validate.isTrue(rsgv26.utiento!=null, "insert into Rsgv26, utiento shouldn't be null");
		Validate.isTrue(rsgv26.cdenum!=null, "insert into Rsgv26, cdenum shouldn't be null");
		Validate.isTrue(rsgv26.authdt!=null, "insert into Rsgv26, authdt shouldn't be null");
		Validate.isTrue(rsgv26.authhe!=null, "insert into Rsgv26, authhe shouldn't be null");
		
		OrderClause clause = new OrderClause();
		clause.set("Insert into rsgv26");		
		clause.paramInsert("utiste",rsgv26.utiste);
		clause.paramInsert("utiento",rsgv26.utiento);
		clause.paramInsert("cdenum",rsgv26.cdenum);
		clause.paramInsertDate("authdt",rsgv26.authdt);
		clause.paramInsertTime("authhe",rsgv26.authhe);
		clause.paramInsert("utient",rsgv26.utient);
		clause.paramInsert("cdenump",rsgv26.cdenump);
		clause.paramInsertDate("p2loaddt",rsgv26.p2loaddt);
		clause.paramInsertTime("p2loadhe",rsgv26.p2loadhe);
		clause.paramInsertDate("p2valdt",rsgv26.p2valdt);
		clause.paramInsertTime("p2valhe",rsgv26.p2valhe);
		clause.paramInsert("p2valre",rsgv26.p2valre);
		clause.paramInsertDate("debdt",rsgv26.debdt);
		clause.paramInsertTime("debhe",rsgv26.debhe);
		clause.paramInsertDate("confmdt",rsgv26.confmdt);
		clause.paramInsertTime("confmhe",rsgv26.confmhe);
		clause.paramInsertDate("p2susdt",rsgv26.p2susdt);
		clause.paramInsertTime("p2sushe",rsgv26.p2sushe);
		clause.paramInsert("p2susre",rsgv26.p2susre);
		clause.paramInsertDate("susmdt",rsgv26.susmdt);
		clause.paramInsertTime("susmhe",rsgv26.susmhe);
		clause.paramInsertDate("p2abddt",rsgv26.p2abddt);
		clause.paramInsertTime("p2abdhe",rsgv26.p2abdhe);
		clause.paramInsert("p2abdre",rsgv26.p2abdre);
		clause.paramInsertDate("abdmdt",rsgv26.abdmdt);
		clause.paramInsertTime("abdmhe",rsgv26.abdmhe);
		clause.paramInsertDate("okparudt", rsgv26.okparudt);
		clause.paramInsertTime("okparuhe", rsgv26.okparuhe);
		clause.prepareAndExecute();
		clause.close();
		return true;
	}
	
	/**
	 * Update unique Rsgv26 entry
	 * @param rsgv26 database object 
	 */
	public void update(Rsgv26 rsgv26)
	{
		Validate.notNull(rsgv26.utiento,"utiento is null");
		Validate.notNull(rsgv26.cdenum,"cdenum is null");
		OrderClause clause = new OrderClause();
		SqlBuilder sqlBuilder = new SqlBuilder();
		sqlBuilder.append("update rsgv26 set");
		sqlBuilder.append("   utiste = " + clause.param(rsgv26.utiste));
		sqlBuilder.append(",  utiento = " + clause.param(rsgv26.utiento));
		sqlBuilder.append(",  cdenum = " + clause.param(rsgv26.cdenum));
		sqlBuilder.append(",  authdt = " + clause.paramDate(rsgv26.authdt));
		sqlBuilder.append(",  authhe = " + clause.paramTime(rsgv26.authhe));
		sqlBuilder.append(",  utient = " + clause.param(rsgv26.utient));
		sqlBuilder.append(",  cdenump = " + clause.param(rsgv26.cdenump));
		sqlBuilder.append(",  p2loaddt = " + clause.paramDate(rsgv26.p2loaddt));
		sqlBuilder.append(",  p2loadhe = " + clause.paramTime(rsgv26.p2loadhe));
		sqlBuilder.append(",  p2valdt = " + clause.paramDate(rsgv26.p2valdt));
		sqlBuilder.append(",  p2valhe = " + clause.paramTime(rsgv26.p2valhe));
		sqlBuilder.append(",  p2valre = " + clause.param(rsgv26.p2valre));
		sqlBuilder.append(",  debdt = " + clause.paramDate(rsgv26.debdt));
		sqlBuilder.append(",  debhe = " + clause.paramTime(rsgv26.debhe));
		sqlBuilder.append(",  confmdt = " + clause.paramDate(rsgv26.confmdt));
		sqlBuilder.append(",  confmhe = " + clause.paramTime(rsgv26.confmhe));
		sqlBuilder.append(",  p2susdt = " + clause.paramDate(rsgv26.p2susdt));
		sqlBuilder.append(",  p2sushe = " + clause.paramTime(rsgv26.p2sushe));
		sqlBuilder.append(",  p2susre = " + clause.param(rsgv26.p2susre));
		sqlBuilder.append(",  susmdt = " + clause.paramDate(rsgv26.susmdt));
		sqlBuilder.append(",  susmhe = " + clause.paramTime(rsgv26.susmhe));
		sqlBuilder.append(",  p2abddt = " + clause.paramDate(rsgv26.p2abddt));
		sqlBuilder.append(",  p2abdhe = " + clause.paramTime(rsgv26.p2abdhe));
		sqlBuilder.append(",  p2abdre = " + clause.param(rsgv26.p2abdre));
		sqlBuilder.append(",  abdmdt = " + clause.paramDate(rsgv26.abdmdt));
		sqlBuilder.append(",  abdmhe = " + clause.paramTime(rsgv26.abdmhe));
		sqlBuilder.append(", okparudt = " + clause.paramDate(rsgv26.okparudt));
		sqlBuilder.append(", okparuhe = " + clause.paramTime(rsgv26.okparuhe));
		sqlBuilder.append("  where utiento = " + clause.param(rsgv26.utiento));
		sqlBuilder.append("  and cdenum = " + clause.param(rsgv26.cdenum));
		clause.set(sqlBuilder.toString());
		clause.prepareAndExecute();
		clause.close();
	}
	
	
	/**
	 * Fill a Rsgv26 DB object
	 * @param clause DB datas
	 * @return Rsgv26 instance with filled datas
	 */
	private static Rsgv26 fill(OrderClause clause)
	{		
		Rsgv26 rsgv26 = new Rsgv26();
		
		rsgv26.utiste = clause.getString("utiste");
		rsgv26.utiento = clause.getString("utiento");
		rsgv26.cdenum = clause.getString("cdenum");
		rsgv26.authdt = clause.getDate("authdt");
		rsgv26.authhe = clause.getTime("authhe");
		rsgv26.utient = clause.getString("utient");
		rsgv26.cdenump = clause.getString("cdenump");
		rsgv26.p2loaddt = clause.getDate("p2loaddt");
		rsgv26.p2loadhe = clause.getTime("p2loadhe");
		rsgv26.p2valdt = clause.getDate("p2valdt");
		rsgv26.p2valhe = clause.getTime("p2valhe");
		rsgv26.p2valre = clause.getString("p2valre");
		rsgv26.debdt = clause.getDate("debdt");
		rsgv26.debhe = clause.getTime("debhe");
		rsgv26.confmdt = clause.getDate("confmdt");
		rsgv26.confmhe = clause.getTime("confmhe");
		rsgv26.p2susdt = clause.getDate("p2susdt");
		rsgv26.p2sushe = clause.getTime("p2sushe");
		rsgv26.p2susre = clause.getString("p2susre");
		rsgv26.susmdt = clause.getDate("susmdt");
		rsgv26.susmhe = clause.getTime("susmhe");
		rsgv26.p2abddt = clause.getDate("p2abddt");
		rsgv26.p2abdhe = clause.getTime("p2abdhe");
		rsgv26.p2abdre = clause.getString("p2abdre");
		rsgv26.abdmdt = clause.getDate("abdmdt");
		rsgv26.abdmhe = clause.getTime("abdmhe");
		rsgv26.okparudt = clause.getDate("okparudt");
		rsgv26.okparuhe = clause.getTime("okparuhe");
		
		return rsgv26;
	}
}
