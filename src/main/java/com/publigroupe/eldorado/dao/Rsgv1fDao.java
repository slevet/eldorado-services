package com.publigroupe.eldorado.dao;

import com.publigroupe.eldorado.dao.bean.Rsgv1f;
import com.publigroupe.javaBase.sql.support.SqlBuilder;
import com.publigroupe.order.dao.p2000.OrderClause;

public class Rsgv1fDao
{
	/**
	 * Find last Rsgv1f entry.
	 * 
	 * @return
	 */
	public Rsgv1f findLast(String utiento, String cdenum)
	{
		Rsgv1f rsgv1f = null;

		OrderClause clause = new OrderClause();
		SqlBuilder sqlBuilder = new SqlBuilder();
		sqlBuilder.append("select * from rsgv1f");
		sqlBuilder.append("where utiento = " + clause.param(utiento));
		sqlBuilder.append("  and cdenum = " + clause.param(cdenum));
		sqlBuilder.append("order by saidat desc, saiheu desc");

		clause.set(sqlBuilder.toString());
		clause.prepareAndExecute();
		if (clause.next())
		{
			rsgv1f = fill(clause);
		}
		clause.close();

		return rsgv1f;
	}
	
	private static Rsgv1f fill(OrderClause clause)
	{		
		Rsgv1f rsgv1f = new Rsgv1f();
		
		rsgv1f.utiento = clause.getString("utiento");
		rsgv1f.cdenum = clause.getString("cdenum");
		rsgv1f.saidat = clause.getDate("saidat");
		rsgv1f.saiheu = clause.getTime("saiheu");
		rsgv1f.sairef = clause.getString("sairef");
		rsgv1f.mailtxt = clause.getString("mailtxt");
		
		return rsgv1f;
	}

}
