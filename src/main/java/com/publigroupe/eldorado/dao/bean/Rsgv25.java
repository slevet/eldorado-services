package com.publigroupe.eldorado.dao.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class Rsgv25 {
	public String utiste;
	public String utiento;
	public String cdenum;
	public String libcde;
	public BigDecimal cdeneta;	
	public BigDecimal tvamt;
	public BigDecimal cdenet;	
	public String tvatxc;	
	public BigDecimal tvatx;
	public Timestamp inlogdt;	
	public String metpaie;	
	public String modpaie;	
	public Stransa stransa = Stransa.NU;	
	public Date dtempaie;
	public Timestamp outlogdt;	
	public Orimaj orimaj = Orimaj.NU;
	public String gvpren;
	public String gvnom;
	public String gvrue;
	public String gvnpa;
	public String gvloc;
	public String gvpays;	
	public String gvntel;	
	public String gvnfax;	
	public String gvemail;	
	public String gvcdlng;	
	public String utientop;	
	public String cdenump;
	public Timestamp valcdedt;
	public String utistec;
	public String utientc;
	public String clino;
	public String clicch;	
	public String cligenr;	
	public String adrnom;
	public String adrloc;	
	public String procat;
	public String progenr;
	public String prono;
	public Timestamp valclidt;	
	public String txordid;	
	public Timestamp vallogdt;	
	public String gvtxcon;
	public Timestamp conlogdt;	
	public String gvpro	;
	public String adrreg;	
	public String adrtele;	
	public String upptrid;	
	public String authcod;
	
	public enum Stransa
	{
		OK,VA,NU;
	}
	
	public enum Orimaj
	{
		EM,NU;
	}
}
