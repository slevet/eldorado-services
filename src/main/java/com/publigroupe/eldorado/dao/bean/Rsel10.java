package com.publigroupe.eldorado.dao.bean;

import java.math.BigDecimal;
import java.util.Date;

public class Rsel10 {
  public int offno;
  public Date valdatd;
  public Date valdatf;
  public int eltradid;
  public String eloffstat;
  public int elmedid;
  public String jnlcod;
  public String jnlcods;
  public String ctacatp;
  public String ctainsp;
  public String ctarubg;
  public String ctarubs;
  public String ctaempl;
  public String ctacoul;
  public String ctagen;
  public String ctaexe;
  public String ctarubt;
  public String grdgenr;
  public String offtyp;
  public String augmtyp;
  public String exetyp;
  public int grdcoln;
  public int grdhau;
  public int grdtot;
  public BigDecimal mtvmin;
  public Date mutdat;
  public Date mutheu;
  public String mutref;
  public int mutver;
  public String printpobl;
}
