package com.publigroupe.eldorado.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;

import com.publigroupe.eldorado.dao.bean.Rsgv25;
import com.publigroupe.javaBase.sql.support.SqlBuilder;
import com.publigroupe.order.dao.p2000.OrderClause;

public class Rsgv25Dao {
	
	/**
	 * Find Rsgv25 entries 
	 * @return Full list of Rsgv25
	 */
	public List<Rsgv25> findAll()
	{
		List<Rsgv25> rsgv25s = new ArrayList<Rsgv25>();

		OrderClause clause = new OrderClause();
		SqlBuilder sb = new SqlBuilder();
		sb.append("select * from rsgv25");
		clause.set(sb.toString());
		clause.prepareAndExecute();
		
		while (clause.next())
		{
			rsgv25s.add(fill(clause));
		}
		clause.close();

		return rsgv25s;
	}
	
	
	public boolean insert(Rsgv25 rsgv25)
	{
		Validate.isTrue(rsgv25.utiento!=null, "insert into rsgv25, utiento shouldn't be null");
		Validate.isTrue(rsgv25.cdenum!=null, "insert into rsgv25, cdenum shouldn't be null");
		
		OrderClause clause = new OrderClause();
		clause.set("Insert into rsgv25");		
		clause.paramInsert("utiste",rsgv25.utiste);
		clause.paramInsert("utiento",rsgv25.utiento);
		clause.paramInsert("cdenum",rsgv25.cdenum);
		clause.paramInsert("libcde",rsgv25.libcde);
		clause.paramInsert("cdeneta",rsgv25.cdeneta);	
		clause.paramInsert("tvamt",rsgv25.tvamt);
		clause.paramInsert("cdenet",rsgv25.cdenet);	
		clause.paramInsert("tvatxc",rsgv25.tvatxc);	
		clause.paramInsert("tvatx",rsgv25.tvatx);
		clause.paramInsertTimeStamp("inlogdt",rsgv25.inlogdt);	
		clause.paramInsert("metpaie",rsgv25.metpaie);	
		clause.paramInsert("modpaie",rsgv25.modpaie);	
		clause.paramInsert("stransa",rsgv25.stransa.name());	
		clause.paramInsertDate("dtempaie",rsgv25.dtempaie);
		clause.paramInsertTimeStamp("outlogdt",rsgv25.outlogdt);	
		clause.paramInsert("orimaj",rsgv25.orimaj.name());
		clause.paramInsert("gvpren",rsgv25.gvpren);
		clause.paramInsert("gvnom",rsgv25.gvnom);
		clause.paramInsert("gvrue",rsgv25.gvrue);
		clause.paramInsert("gvnpa",rsgv25.gvnpa);
		clause.paramInsert("gvloc",rsgv25.gvloc);
		clause.paramInsert("gvpays",rsgv25.gvpays);	
		clause.paramInsert("gvntel",rsgv25.gvntel);	
		clause.paramInsert("gvnfax",rsgv25.gvnfax);	
		clause.paramInsert("gvemail",rsgv25.gvemail);	
		clause.paramInsert("gvcdlng",rsgv25.gvcdlng);	
		clause.paramInsert("utientop",rsgv25.utientop);	
		clause.paramInsert("cdenump",rsgv25.cdenump);
		clause.paramInsertTimeStamp("valcdedt",rsgv25.valcdedt);
		clause.paramInsert("utistec",rsgv25.utistec);
		clause.paramInsert("utientc",rsgv25.utientc);
		clause.paramInsert("clino",rsgv25.clino);
		clause.paramInsert("clicch",rsgv25.clicch);	
		clause.paramInsert("cligenr",rsgv25.cligenr);	
		clause.paramInsert("adrnom",rsgv25.adrnom);
		clause.paramInsert("adrloc",rsgv25.adrloc);	
		clause.paramInsert("procat",rsgv25.procat);
		clause.paramInsert("progenr",rsgv25.progenr);
		clause.paramInsert("prono",rsgv25.prono);
		clause.paramInsertTimeStamp("valclidt",rsgv25.valclidt);	
		clause.paramInsert("txordid",rsgv25.txordid);	
		clause.paramInsertTimeStamp("vallogdt",rsgv25.vallogdt);	
		clause.paramInsert("gvtxcon",rsgv25.gvtxcon);
		clause.paramInsertTimeStamp("conlogdt",rsgv25.conlogdt);	
		clause.paramInsert("gvpro",rsgv25.gvpro);
		clause.paramInsert("adrreg",rsgv25.adrreg);	
		clause.paramInsert("adrtele",rsgv25.adrtele);	
		clause.paramInsert("upptrid",rsgv25.upptrid);	
		clause.paramInsert("authcod",rsgv25.authcod);
		clause.prepareAndExecute();
		clause.close();
		return true;		
	}
	
	/**
	 * Retrieve unique Rsgv25 entry
	 * @param utiento 
	 * @param cdenum
	 * @return
	 */
	public void delete(String utiento,String cdenum)
	{
		OrderClause clause = new OrderClause();
		SqlBuilder sqlBuilder = new SqlBuilder();
		sqlBuilder.append("delete from rsgv25 ");
		sqlBuilder.append("  where utiento = " + clause.param(utiento));
		sqlBuilder.append("  and cdenum = " + clause.param(cdenum));
		clause.set(sqlBuilder.toString());
		clause.prepareAndExecute();
		clause.close();
	}
	
	/**
	 * Retrieve unique Rsgv25 entry
	 * @param utiento 
	 * @param cdenum
	 * @return
	 */
	public Rsgv25 retrieve(String utiento,String cdenum)
	{
		Rsgv25 rsgv25 = null;
		OrderClause clause = new OrderClause();
		SqlBuilder sqlBuilder = new SqlBuilder();
		sqlBuilder.append("select * from rsgv25 ");
		sqlBuilder.append("  where utiento = " + clause.param(utiento));
		sqlBuilder.append("  and cdenum = " + clause.param(cdenum));
		clause.set(sqlBuilder.toString());
		clause.prepareAndExecute();
		if (clause.next())
		{
			rsgv25 = fill(clause);
		}
		clause.close();
		return rsgv25;
	}
	
	/**
	 * Update unique Rsgv25 entry
	 * @param rsgv25 database object 
	 */
	public void update(Rsgv25 rsgv25)
	{
		Validate.notNull(rsgv25.utiento,"utiento is null");
		Validate.notNull(rsgv25.cdenum,"cdenum is null");
		OrderClause clause = new OrderClause();
		SqlBuilder sqlBuilder = new SqlBuilder();
		sqlBuilder.append("update rsgv25 set");
		sqlBuilder.append("   utiste = " + clause.param(rsgv25.utiste));
		sqlBuilder.append(",  utiento = " + clause.param(rsgv25.utiento));
		sqlBuilder.append(",  cdenum = " + clause.param(rsgv25.cdenum));
		sqlBuilder.append(" , libcde = " + clause.param(rsgv25.libcde));
		sqlBuilder.append(" , cdeneta = " + clause.param(rsgv25.cdeneta));	
		sqlBuilder.append(" , tvamt = " + clause.param(rsgv25.tvamt));
		sqlBuilder.append(" , cdenet = " + clause.param(rsgv25.cdenet));	
		sqlBuilder.append(" , tvatxc = " + clause.param(rsgv25.tvatxc));	
		sqlBuilder.append(" , tvatx = " + clause.param(rsgv25.tvatx));
		sqlBuilder.append(" , inlogdt = " + clause.param(rsgv25.inlogdt));	
		sqlBuilder.append(" , metpaie = " + clause.param(rsgv25.metpaie));	
		sqlBuilder.append(" , modpaie = " + clause.param(rsgv25.modpaie));	
		sqlBuilder.append(" , stransa = " + clause.param(rsgv25.stransa.name()));	
		sqlBuilder.append(" , dtempaie = " + clause.paramDate(rsgv25.dtempaie));
		sqlBuilder.append(" , outlogdt = " + clause.param(rsgv25.outlogdt));	
		sqlBuilder.append(" , orimaj = " + clause.param(rsgv25.orimaj.name()));
		sqlBuilder.append(" , gvpren = " + clause.param(rsgv25.gvpren));
		sqlBuilder.append(" , gvnom = " + clause.param(rsgv25.gvnom));
		sqlBuilder.append(" , gvrue = " + clause.param(rsgv25.gvrue));
		sqlBuilder.append(" , gvnpa = " + clause.param(rsgv25.gvnpa));
		sqlBuilder.append(" , gvloc = " + clause.param(rsgv25.gvloc));
		sqlBuilder.append(" , gvpays = " + clause.param(rsgv25.gvpays));	
		sqlBuilder.append(" , gvntel = " + clause.param(rsgv25.gvntel));	
		sqlBuilder.append(" , gvnfax = " + clause.param(rsgv25.gvnfax));	
		sqlBuilder.append(" , gvemail = " + clause.param(rsgv25.gvemail));	
		sqlBuilder.append(" , gvcdlng = " + clause.param(rsgv25.gvcdlng));	
		sqlBuilder.append(" , utientop = " + clause.param(rsgv25.utientop));	
		sqlBuilder.append(" , cdenump = " + clause.param(rsgv25.cdenump));
		sqlBuilder.append(" , valcdedt = " + clause.param(rsgv25.valcdedt));
		sqlBuilder.append(" , utistec = " + clause.param(rsgv25.utistec));
		sqlBuilder.append(" , utientc = " + clause.param(rsgv25.utientc));
		sqlBuilder.append(" , clino = " + clause.param(rsgv25.clino));
		sqlBuilder.append(" , clicch = " + clause.param(rsgv25.clicch));	
		sqlBuilder.append(" , cligenr = " + clause.param(rsgv25.cligenr));	
		sqlBuilder.append(" , adrnom = " + clause.param(rsgv25.adrnom));
		sqlBuilder.append(" , adrloc = " + clause.param(rsgv25.adrloc));	
		sqlBuilder.append(" , procat = " + clause.param(rsgv25.procat));
		sqlBuilder.append(" , progenr = " + clause.param(rsgv25.progenr));
		sqlBuilder.append(" , prono = " + clause.param(rsgv25.prono));
		sqlBuilder.append(" , valclidt = " + clause.param(rsgv25.valclidt));	
		sqlBuilder.append(" , txordid = " + clause.param(rsgv25.txordid));	
		sqlBuilder.append(" , vallogdt = " + clause.param(rsgv25.vallogdt));	
		sqlBuilder.append(" , gvtxcon = " + clause.param(rsgv25.gvtxcon));
		sqlBuilder.append(" , conlogdt = " + clause.param(rsgv25.conlogdt));	
		sqlBuilder.append(" , gvpro = " + clause.param(rsgv25.gvpro));
		sqlBuilder.append(" , adrreg = " + clause.param(rsgv25.adrreg));	
		sqlBuilder.append(" , adrtele = " + clause.param(rsgv25.adrtele));	
		sqlBuilder.append(" , upptrid = " + clause.param(rsgv25.upptrid));	
		sqlBuilder.append(" , authcod = " + clause.param(rsgv25.authcod));		
		sqlBuilder.append("  where utiento = " + clause.param(rsgv25.utiento));
		sqlBuilder.append("  and cdenum = " + clause.param(rsgv25.cdenum));
		clause.set(sqlBuilder.toString());
		clause.prepareAndExecute();
		clause.close();
	}
	
	/**
	 * Fill a Rsgv25 DB object
	 * @param clause DB datas
	 * @return Rsgv25 instance with filled datas
	 */
	private static Rsgv25 fill(OrderClause clause)
	{		
		Rsgv25 rsgv25 = new Rsgv25();
		rsgv25.utiste = clause.getString("utiste");
		rsgv25.utiento = clause.getString("utiento");
		rsgv25.cdenum = clause.getString("cdenum");
		rsgv25.libcde = clause.getString("libcde");
		rsgv25.cdeneta = clause.getBigDecimal("cdeneta");	
		rsgv25.tvamt = clause.getBigDecimal("tvamt");
		rsgv25.cdenet = clause.getBigDecimal("cdenet");	
		rsgv25.tvatxc = clause.getString("tvatxc");	
		rsgv25.tvatx = clause.getBigDecimal("tvatx");
		rsgv25.inlogdt = clause.getTimestamp("inlogdt");	
		rsgv25.metpaie = clause.getString("metpaie");	
		rsgv25.modpaie = clause.getString("modpaie");	
		rsgv25.stransa = Rsgv25.Stransa.valueOf(clause.getString("stransa"));	
		rsgv25.dtempaie = clause.getDate("dtempaie");
		rsgv25.outlogdt = clause.getTimestamp("outlogdt");	
		rsgv25.orimaj = Rsgv25.Orimaj.valueOf(clause.getString("orimaj"));
		rsgv25.gvpren = clause.getString("gvpren");
		rsgv25.gvnom = clause.getString("gvnom");
		rsgv25.gvrue = clause.getString("gvrue");
		rsgv25.gvnpa = clause.getString("gvnpa");
		rsgv25.gvloc = clause.getString("gvloc");
		rsgv25.gvpays = clause.getString("gvpays");	
		rsgv25.gvntel = clause.getString("gvntel");	
		rsgv25.gvnfax = clause.getString("gvnfax");	
		rsgv25.gvemail = clause.getString("gvemail");	
		rsgv25.gvcdlng = clause.getString("gvcdlng");	
		rsgv25.utientop = clause.getString("utientop");	
		rsgv25.cdenump = clause.getString("cdenump");
		rsgv25.valcdedt = clause.getTimestamp("valcdedt");
		rsgv25.utistec = clause.getString("utistec");
		rsgv25.utientc = clause.getString("utientc");
		rsgv25.clino = clause.getString("clino");
		rsgv25.clicch = clause.getString("clicch");	
		rsgv25.cligenr = clause.getString("cligenr");	
		rsgv25.adrnom = clause.getString("adrnom");
		rsgv25.adrloc = clause.getString("adrloc");	
		rsgv25.procat = clause.getString("procat");
		rsgv25.progenr = clause.getString("progenr");
		rsgv25.prono = clause.getString("prono");
		rsgv25.valclidt = clause.getTimestamp("valclidt");	
		rsgv25.txordid = clause.getString("txordid");	
		rsgv25.vallogdt = clause.getTimestamp("vallogdt");	
		rsgv25.gvtxcon = clause.getString("gvtxcon");
		rsgv25.conlogdt = clause.getTimestamp("conlogdt");	
		rsgv25.gvpro = clause.getString("gvpro");
		rsgv25.adrreg = clause.getString("adrreg");	
		rsgv25.adrtele = clause.getString("adrtele");	
		rsgv25.upptrid = clause.getString("upptrid");	
		rsgv25.authcod = clause.getString("authcod");
		return rsgv25;
	}
	
}
