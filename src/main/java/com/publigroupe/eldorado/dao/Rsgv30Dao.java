package com.publigroupe.eldorado.dao;

import com.publigroupe.eldorado.dto.EldoradoPaymentModeEnumDto;
import com.publigroupe.javaBase.sql.support.SqlBuilder;
import com.publigroupe.order.dao.p2000.OrderClause;

public class Rsgv30Dao {

	/**
	 * Retrieves the payment mode for the given user id.
	 * 
	 * @param userId The user id to get the payment mode for.
	 * @return The payment mode.
	 */
	public EldoradoPaymentModeEnumDto retrieve(String userId) {
		String mode = null;
		OrderClause clause = new OrderClause();
		SqlBuilder sqlBuilder = new SqlBuilder();
		sqlBuilder.append("select gvpai from rsgv30");
		sqlBuilder.append(" where usrmyp = " + clause.param(userId));
		clause.set(sqlBuilder.toString());
		clause.prepareAndExecute();
		if (clause.next()) {
			mode = clause.getString("gvpai");
		}
		clause.close();
		return EldoradoPaymentModeEnumDto.getValueForCode(mode);
	}

	/**
	 * Updates the payment mode of the user corresponding to the given user id.
	 * 
	 * @param userId The id of the user to update.
	 * @param paymentMode The payment mode to set.
	 */
	public void update(String userId, EldoradoPaymentModeEnumDto paymentMode) {
		OrderClause clause = new OrderClause();
		SqlBuilder sqlBuilder = new SqlBuilder();
		sqlBuilder.append("update rsgv30 set");
		sqlBuilder.append(" gvpai = " + clause.param(paymentMode.getCode()));
		sqlBuilder.append(" where usrmyp = " + clause.param(userId));
		clause.set(sqlBuilder.toString());
		clause.prepareAndExecute();
		clause.close();
	}

}
