package com.publigroupe.eldorado.dto;

import java.util.List;

public class EldoradoSummaryListDto {
  private List<EldoradoSummaryDto> summaries;
  private int start;
  private int end;
  private int total;

  public List<EldoradoSummaryDto> getSummaries() {
    return summaries;
  }

  public void setSummaries(List<EldoradoSummaryDto> summaries) {
    this.summaries = summaries;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public int getEnd() {
    return end;
  }

  public void setEnd(int end) {
    this.end = end;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public EldoradoSummaryListDto() {
  }

  public EldoradoSummaryListDto(List<EldoradoSummaryDto> summaries, int start, int end, int total) {
    super();
    this.summaries = summaries;
    this.start = start;
    this.end = end;
    this.total = total;
  }

}
