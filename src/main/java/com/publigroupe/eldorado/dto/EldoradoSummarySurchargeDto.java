package com.publigroupe.eldorado.dto;

import java.math.BigDecimal;

public class EldoradoSummarySurchargeDto
{
	private String surchargeName;
	private BigDecimal amount;

	public String getSurchargeName()
	{
		return surchargeName;
	}

	public void setSurchargeName(String surchargeName)
	{
		this.surchargeName = surchargeName;
	}

	public BigDecimal getAmount()
	{
		return amount;
	}

	public void setAmount(BigDecimal amount)
	{
		this.amount = amount;
	}
}
