package com.publigroupe.eldorado.dto;

public enum EldoradoAdOperationIDEnumDto {
  Offer,
  Request;
}
