package com.publigroupe.eldorado.dto;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum EldoradoPaymentTypeEnumDto
{
	VISA("03"), MASTERCARD("04"), POSTCARD("02"), EMPTY("-1");

	private String code;

	private static final Map<String, EldoradoPaymentTypeEnumDto> lookup = new HashMap<String, EldoradoPaymentTypeEnumDto>();

	static
	{
		for (EldoradoPaymentTypeEnumDto s : EnumSet.allOf(EldoradoPaymentTypeEnumDto.class))
		{
			lookup.put(s.getCode(), s);
		}
	}

	private EldoradoPaymentTypeEnumDto(String code)
	{
		this.code = code;
	}

	public String getCode()
	{
		return code;
	}

	public static EldoradoPaymentTypeEnumDto get(String code)
	{
		return lookup.get(code);
	}

	/**
	 * Check if provided code is a valid Enum value
	 * 
	 * @param code
	 *            String of a language code
	 * @return true if key is present in the Enum
	 */
	public static boolean contains(String code)
	{
		return lookup.containsKey(code);
	}
}
