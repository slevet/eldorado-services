package com.publigroupe.eldorado.dto;

import java.util.Date;

public class EldoradoFlowStateDto
{
	public String companyCode; // utiste
	public String subsidiaryCode; // utiento
	public String orderNr; // cdenum
	public String p2000SubsidiaryCode; // utient
	public String p2000OrderNr;// cdenump
	public Date authorizationDate; // authdt
	public Date authorizationTime;// authhe
	public Date loadingDate; // p2loaddt
	public Date loadingTime; // p2loadhe
	public Date validationDate; // p2valdt
	public Date validationTime; // p2valhe
	public String validationReference; // p2valre
	public Date debitDate; // debdt
	public Date debitTime; // debhe
	public Date confirmationDate; // confmdt
	public Date confirmationTime; // confmhe
	public Date suspendPaimentDate; // p2susdt
	public Date suspendPaimentTime; // p2sushe
	public String suspendPaimentReference; // p2susre
	public Date suspendMailClientDate; // susmdt
	public Date suspendMailClientTime; // susmhe
	public Date cancellationDate; // p2abddt
	public Date cancellationTime; // p2abdhe
	public String cancellationReference; // p2abdre
	public Date cancellationMailClientDate; // abdmdt
	public Date cancellationMailClientTime; // abdmhe
	public Date feedBackIssueDateMailDate; // okparudt
	public Date feedBackIssueDateMailTime; // okparuhe

	public Date getFeedBackIssueDateMailDate() {
		return feedBackIssueDateMailDate;
	}

	public void setFeedBackIssueDateMailDate(Date feedBackIssueDateMailDate) {
		this.feedBackIssueDateMailDate = feedBackIssueDateMailDate;
	}

	public Date getFeedBackIssueDateMailTime() {
		return feedBackIssueDateMailTime;
	}

	public void setFeedBackIssueDateMailTime(Date feedBackIssueDateMailTime) {
		this.feedBackIssueDateMailTime = feedBackIssueDateMailTime;
	}

	public String getCompanyCode()
	{
		return companyCode;
	}

	public void setCompanyCode(String companyCode)
	{
		this.companyCode = companyCode;
	}

	public String getSubsidiaryCode()
	{
		return subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode)
	{
		this.subsidiaryCode = subsidiaryCode;
	}

	public String getOrderNr()
	{
		return orderNr;
	}

	public void setOrderNr(String orderNr)
	{
		this.orderNr = orderNr;
	}

	public String getP2000SubsidiaryCode()
	{
		return p2000SubsidiaryCode;
	}

	public void setP2000SubsidiaryCode(String p2000SubsidiaryCode)
	{
		this.p2000SubsidiaryCode = p2000SubsidiaryCode;
	}

	public String getP2000OrderNr()
	{
		return p2000OrderNr;
	}

	public void setP2000OrderNr(String p2000OrderNr)
	{
		this.p2000OrderNr = p2000OrderNr;
	}

	public Date getLoadingDate()
	{
		return loadingDate;
	}

	public void setLoadingDate(Date loadingDate)
	{
		this.loadingDate = loadingDate;
	}

	public Date getLoadingTime()
	{
		return loadingTime;
	}

	public void setLoadingTime(Date loadingTime)
	{
		this.loadingTime = loadingTime;
	}

	public Date getValidationDate()
	{
		return validationDate;
	}

	public void setValidationDate(Date validationDate)
	{
		this.validationDate = validationDate;
	}

	public Date getValidationTime()
	{
		return validationTime;
	}

	public void setValidationTime(Date validationTime)
	{
		this.validationTime = validationTime;
	}

	public String getValidationReference()
	{
		return validationReference;
	}

	public void setValidationReference(String validationReference)
	{
		this.validationReference = validationReference;
	}

	public Date getDebitDate()
	{
		return debitDate;
	}

	public void setDebitDate(Date debitDate)
	{
		this.debitDate = debitDate;
	}

	public Date getDebitTime()
	{
		return debitTime;
	}

	public void setDebitTime(Date debitTime)
	{
		this.debitTime = debitTime;
	}

	public Date getConfirmationDate()
	{
		return confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate)
	{
		this.confirmationDate = confirmationDate;
	}

	public Date getConfirmationTime()
	{
		return confirmationTime;
	}

	public void setConfirmationTime(Date confirmationTime)
	{
		this.confirmationTime = confirmationTime;
	}

	public Date getSuspendPaimentDate()
	{
		return suspendPaimentDate;
	}

	public void setSuspendPaimentDate(Date suspendPaimentDate)
	{
		this.suspendPaimentDate = suspendPaimentDate;
	}

	public Date getSuspendPaimentTime()
	{
		return suspendPaimentTime;
	}

	public void setSuspendPaimentTime(Date suspendPaimentTime)
	{
		this.suspendPaimentTime = suspendPaimentTime;
	}

	public String getSuspendPaimentReference()
	{
		return suspendPaimentReference;
	}

	public void setSuspendPaimentReference(String suspendPaimentReference)
	{
		this.suspendPaimentReference = suspendPaimentReference;
	}

	public Date getSuspendMailClientDate()
	{
		return suspendMailClientDate;
	}

	public void setSuspendMailClientDate(Date suspendMailClientDate)
	{
		this.suspendMailClientDate = suspendMailClientDate;
	}

	public Date getSuspendMailClientTime()
	{
		return suspendMailClientTime;
	}

	public void setSuspendMailClientTime(Date suspendMailClientTime)
	{
		this.suspendMailClientTime = suspendMailClientTime;
	}

	public Date getCancellationDate()
	{
		return cancellationDate;
	}

	public void setCancellationDate(Date cancellationDate)
	{
		this.cancellationDate = cancellationDate;
	}

	public Date getCancellationTime()
	{
		return cancellationTime;
	}

	public void setCancellationTime(Date cancellationTime)
	{
		this.cancellationTime = cancellationTime;
	}

	public String getCancellationReference()
	{
		return cancellationReference;
	}

	public void setCancellationReference(String cancellationReference)
	{
		this.cancellationReference = cancellationReference;
	}

	public Date getCancellationMailClientDate()
	{
		return cancellationMailClientDate;
	}

	public void setCancellationMailClientDate(Date cancellationMailClientDate)
	{
		this.cancellationMailClientDate = cancellationMailClientDate;
	}

	public Date getCancellationMailClientTime()
	{
		return cancellationMailClientTime;
	}

	public void setCancellationMailClientTime(Date cancellationMailClientTime)
	{
		this.cancellationMailClientTime = cancellationMailClientTime;
	}

	public Date getAuthorizationDate()
	{
		return authorizationDate;
	}

	public void setAuthorizationDate(Date authorizationDate)
	{
		this.authorizationDate = authorizationDate;
	}

	public Date getAuthorizationTime()
	{
		return authorizationTime;
	}

	public void setAuthorizationTime(Date authorizationTime)
	{
		this.authorizationTime = authorizationTime;
	}
}
