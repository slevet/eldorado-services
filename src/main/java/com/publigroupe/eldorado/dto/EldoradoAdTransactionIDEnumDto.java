package com.publigroupe.eldorado.dto;

public enum EldoradoAdTransactionIDEnumDto {
  Rent,
  Sale;
}
