package com.publigroupe.eldorado.dto;

import java.util.List;

import com.publigroupe.media.dto.MediaDto;

public class EldoradoBrandDto
{
	private MediaDto brand;
	private List<EldoradoMediaDto> medias;
	private boolean isSponsored;
	private int sponsoredSequence;

	public MediaDto getBrand()
	{
		return brand;
	}

	public void setBrand(MediaDto brand)
	{
		this.brand = brand;
	}

	public List<EldoradoMediaDto> getMedias()
	{
		return medias;
	}

	public void setMedias(List<EldoradoMediaDto> medias)
	{
		this.medias = medias;
	}

	public boolean isSponsored()
	{
		return isSponsored;
	}

	public void setSponsored(boolean isSponsored)
	{
		this.isSponsored = isSponsored;
	}

	public int getSponsoredSequence()
	{
		return sponsoredSequence;
	}

	public void setSponsoredSequence(int sponsoredSequence)
	{
		this.sponsoredSequence = sponsoredSequence;
	}
}
