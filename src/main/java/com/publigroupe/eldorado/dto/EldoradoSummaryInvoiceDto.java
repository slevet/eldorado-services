package com.publigroupe.eldorado.dto;

import java.util.Date;

public class EldoradoSummaryInvoiceDto {
  private String id;
  private Date date;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

}
