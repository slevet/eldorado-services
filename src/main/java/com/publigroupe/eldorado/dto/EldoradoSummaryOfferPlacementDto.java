package com.publigroupe.eldorado.dto;

import java.util.List;

import com.publigroupe.media.dto.MediaDto;

public class EldoradoSummaryOfferPlacementDto
{
	private MediaDto media;

	private String placementName;

	private List<EldoradoSummaryOfferInsertionDto> insertions;

	private List<EldoradoSummarySurchargeDto> surcharges;

	public MediaDto getMedia()
	{
		return media;
	}

	public void setMedia(MediaDto media)
	{
		this.media = media;
	}

	public String getPlacementName()
	{
		return placementName;
	}

	public void setPlacementName(String placementName)
	{
		this.placementName = placementName;
	}

	public List<EldoradoSummaryOfferInsertionDto> getInsertions()
	{
		return insertions;
	}

	public void setInsertions(List<EldoradoSummaryOfferInsertionDto> insertions)
	{
		this.insertions = insertions;
	}

	public List<EldoradoSummarySurchargeDto> getSurcharges()
	{
		return surcharges;
	}

	public void setSurcharges(List<EldoradoSummarySurchargeDto> surcharges)
	{
		this.surcharges = surcharges;
	}
}
