package com.publigroupe.eldorado.dto;

/**
 * Enum listing the PDF validation errors.
 * 
 * @author Gon�alo Rodrigues - Consultant IT
 */
public enum EldoradoPdfErrorEnumDto {

  BLOCKER_DOCUMENT_PROBLEM("PDF.DOCUMENT_ERROR"),
  BLOCKER_MORE_THAN_ONE_PAGE("PDF.MORE_THAN_ONE_PAGE"),
  BLOCKER_TECHNICAL_PROBLEM("PDF.TECHNICAL_ERROR"),
  BLOCKER_WRONG_SIZE("PDF.WRONG_SIZE"),
  ERROR_FONT_EMBEDEDDED("PDF.FONT_EMBEDEDDED"),
  ERROR_GENERAL_REDISPLAY_ERROR("PDF.REDISPLAY_ERROR"),
  ERROR_PICTURE_RESOLUTION("PDF.PICTURE_RESOLUTION"),
  WARNING_COLOR_BW("PDF.COLOR_BW_WARNING"),
  WARNING_COLOR_CONVERSION_BW("PDF.COLOR_WARNING_CONVERSION_BW"),
  WARNING_RESIZED_WITH_BLANK("PDF.COLOR_BW_WARNING");

  private String errorMessageKey;

  private EldoradoPdfErrorEnumDto(String errorMessageKey) {
    this.errorMessageKey = errorMessageKey;
  }

  public String getErrorMessageKey() {
    return errorMessageKey;
  }

}
