package com.publigroupe.eldorado.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.publigroupe.partner.dto.PartnerDto;
import com.publigroupe.partner.dto.PartnerPhysicalAddressDto;
import com.publigroupe.partner.dto.PartnerQualifierInfoDto;

public class EldoradoPaymentDto
{
	private String companyCode; // utiste
	private String subsidiaryCode; // utiento
	private String orderNr; // cdenum
	private String commandLabel; // libcde
	private BigDecimal commandValueFreeOfVat; // cdeneta
	private BigDecimal commandValueWithVat; // tvamt
	private BigDecimal commandValue; // cdenet
	private String vatRatioCode; // tvatxc
	private BigDecimal vatRatio; // tvatx
	private Date creationDateIn; // inlogdt
	private String paymentMethod; // metpaie
	private String paymentStatus; // stransa
	private Date dateEMPaymentdtempaie; // dtempaie
	private Date creationDateOut; // outlogdt
	private String updateStatus; // orimaj
	private String fax; // gvnfax
	private String p2000SubsidiaryCode; // utientop
	private String p2000OrderNr; // cdenump
	private Date loadDateP2000; // valcdedt
	private String clientCompanyCode; // utistec
	private String clienSubsidiaryCode; // utientc
	private String clientNumber; // clino
	private String clientCchNumber; // clicch
	private String clientCategory; // cligenr
	private String clientName; // adrnom
	private String clientLocation; // adrloc
	private String fromCategory; // procat
	private String fromCodeGenre; // progenr
	private String fromCode; // prono
	private Date clientValidationDate; // valclidt
	private String commandNumberGv; // txordid
	private Date creationDateBatch; // vallogdt
	private String transactionNumberGv; // gvtxcon
	private Date dateControlBatch; // conlogdt
	private String occupation;// gvpro

	private PartnerDto user = new PartnerDto();
	private EldoradoPaymentDatatransDto paymentDataTransDto = new EldoradoPaymentDatatransDto(); 

	public String getCompanyCode()
	{
		return companyCode;
	}

	public void setCompanyCode(String companyCode)
	{
		this.companyCode = companyCode;
	}

	public String getSubsidiaryCode()
	{
		return subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode)
	{
		this.subsidiaryCode = subsidiaryCode;
	}

	public String getOrderNr()
	{
		return orderNr;
	}

	public void setOrderNr(String orderNr)
	{
		this.orderNr = orderNr;
	}

	public String getCommandLabel()
	{
		return commandLabel;
	}

	public void setCommandLabel(String commandLabel)
	{
		this.commandLabel = commandLabel;
	}

	public BigDecimal getCommandValueFreeOfVat()
	{
		return commandValueFreeOfVat;
	}

	public void setCommandValueFreeOfVat(BigDecimal commandValueFreeOfVat)
	{
		this.commandValueFreeOfVat = commandValueFreeOfVat;
	}

	public BigDecimal getCommandValueWithVat()
	{
		return commandValueWithVat;
	}

	public void setCommandValueWithVat(BigDecimal commandValueWithVat)
	{
		this.commandValueWithVat = commandValueWithVat;
	}

	public BigDecimal getCommandValue()
	{
		return commandValue;
	}

	public void setCommandValue(BigDecimal commandValue)
	{
		this.commandValue = commandValue;
	}

	public String getVatRatioCode()
	{
		return vatRatioCode;
	}

	public void setVatRatioCode(String vatRatioCode)
	{
		this.vatRatioCode = vatRatioCode;
	}

	public BigDecimal getVatRatio()
	{
		return vatRatio;
	}

	public void setVatRatio(BigDecimal vatRatio)
	{
		this.vatRatio = vatRatio;
	}

	public Date getCreationDateIn()
	{
		return creationDateIn;
	}

	public void setCreationDateIn(Date creationDateIn)
	{
		this.creationDateIn = creationDateIn;
	}

	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public String getPaymentStatus()
	{
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}

	public Date getDateEMPaymentdtempaie()
	{
		return dateEMPaymentdtempaie;
	}

	public void setDateEMPaymentdtempaie(Date dateEMPaymentdtempaie)
	{
		this.dateEMPaymentdtempaie = dateEMPaymentdtempaie;
	}

	public Date getCreationDateOut()
	{
		return creationDateOut;
	}

	public void setCreationDateOut(Date creationDateOut)
	{
		this.creationDateOut = creationDateOut;
	}

	public String getUpdateStatus()
	{
		return updateStatus;
	}

	public void setUpdateStatus(String updateStatus)
	{
		this.updateStatus = updateStatus;
	}

	public String getFax()
	{
		return fax;
	}

	public void setFax(String fax)
	{
		this.fax = fax;
	}

	public String getP2000SubsidiaryCode()
	{
		return p2000SubsidiaryCode;
	}

	public void setP2000SubsidiaryCode(String p2000SubsidiaryCode)
	{
		this.p2000SubsidiaryCode = p2000SubsidiaryCode;
	}

	public String getP2000OrderNr()
	{
		return p2000OrderNr;
	}

	public void setP2000OrderNr(String p2000OrderNr)
	{
		this.p2000OrderNr = p2000OrderNr;
	}

	public Date getLoadDateP2000()
	{
		return loadDateP2000;
	}

	public void setLoadDateP2000(Date loadDateP2000)
	{
		this.loadDateP2000 = loadDateP2000;
	}

	public String getClientCompanyCode()
	{
		return clientCompanyCode;
	}

	public void setClientCompanyCode(String clientCompanyCode)
	{
		this.clientCompanyCode = clientCompanyCode;
	}

	public String getClienSubsidiaryCode()
	{
		return clienSubsidiaryCode;
	}

	public void setClienSubsidiaryCode(String clienSubsidiaryCode)
	{
		this.clienSubsidiaryCode = clienSubsidiaryCode;
	}

	public String getClientNumber()
	{
		return clientNumber;
	}

	public void setClientNumber(String clientNumber)
	{
		this.clientNumber = clientNumber;
	}

	public String getClientCchNumber()
	{
		return clientCchNumber;
	}

	public void setClientCchNumber(String clientCchNumber)
	{
		this.clientCchNumber = clientCchNumber;
	}

	public String getClientCategory()
	{
		return clientCategory;
	}

	public void setClientCategory(String clientCategory)
	{
		this.clientCategory = clientCategory;
	}

	public String getClientName()
	{
		return clientName;
	}

	public void setClientName(String clientName)
	{
		this.clientName = clientName;
	}

	public String getClientLocation()
	{
		return clientLocation;
	}

	public void setClientLocation(String clientLocation)
	{
		this.clientLocation = clientLocation;
	}

	public String getFromCategory()
	{
		return fromCategory;
	}

	public void setFromCategory(String fromCategory)
	{
		this.fromCategory = fromCategory;
	}

	public String getFromCodeGenre()
	{
		return fromCodeGenre;
	}

	public void setFromCodeGenre(String fromCodeGenre)
	{
		this.fromCodeGenre = fromCodeGenre;
	}

	public String getFromCode()
	{
		return fromCode;
	}

	public void setFromCode(String fromCode)
	{
		this.fromCode = fromCode;
	}

	public Date getClientValidationDate()
	{
		return clientValidationDate;
	}

	public void setClientValidationDate(Date clientValidationDate)
	{
		this.clientValidationDate = clientValidationDate;
	}

	public String getCommandNumberGv()
	{
		return commandNumberGv;
	}

	public void setCommandNumberGv(String commandNumberGv)
	{
		this.commandNumberGv = commandNumberGv;
	}

	public Date getCreationDateBatch()
	{
		return creationDateBatch;
	}

	public void setCreationDateBatch(Date creationDateBatch)
	{
		this.creationDateBatch = creationDateBatch;
	}

	public String getTransactionNumberGv()
	{
		return transactionNumberGv;
	}

	public void setTransactionNumberGv(String transactionNumberGv)
	{
		this.transactionNumberGv = transactionNumberGv;
	}

	public Date getDateControlBatch()
	{
		return dateControlBatch;
	}

	public void setDateControlBatch(Date dateControlBatch)
	{
		this.dateControlBatch = dateControlBatch;
	}

	public String getOccupation()
	{
		return occupation;
	}

	public void setOccupation(String occupation)
	{
		this.occupation = occupation;
	}

	public void setPaymentDataTransDto(EldoradoPaymentDatatransDto paymentDataTransDto)
	{
		this.paymentDataTransDto = paymentDataTransDto;
	}

	public EldoradoPaymentDatatransDto getPaymentDataTransDto()
	{
		return paymentDataTransDto;
	}

	public void setUser(PartnerDto user)
	{
		this.user = user;
	}

	public PartnerDto getUser()
	{
		if (user.getPhysicalAddress() == null)
		{
			user.setPhysicalAddress(new PartnerPhysicalAddressDto());
		}
		if (user.getQualifierInfo() == null)
		{
			user.setQualifierInfo(new PartnerQualifierInfoDto());
		}
		return user;
	}
}
