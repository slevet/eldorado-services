package com.publigroupe.eldorado.dto;

import java.util.Date;

public class EldoradoInvoiceDto {

  private String id;
  private Date date;
  private String documentId;
  private String documentName;
  private byte[] documentContent;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getDocumentId() {
    return documentId;
  }

  public void setDocumentId(String documentId) {
    this.documentId = documentId;
  }

  public String getDocumentName() {
    return documentName;
  }

  public void setDocumentName(String documentName) {
    this.documentName = documentName;
  }

  public byte[] getDocumentContent() {
    return documentContent;
  }

  public void setDocumentContent(byte[] documentContent) {
    this.documentContent = documentContent;
  }

}
