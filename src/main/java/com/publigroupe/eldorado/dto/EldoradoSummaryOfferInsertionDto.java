package com.publigroupe.eldorado.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.publigroupe.order.dto.OrderValorizationCalculationDto;

public class EldoradoSummaryOfferInsertionDto
{
	private Date issueDate;
	private Date endDate;
	private int duration;
	private boolean isExtendedCirculation;

	private BigDecimal amount;
	private OrderValorizationCalculationDto calculation;

	private BigDecimal discount;
	private OrderValorizationCalculationDto discountCalculation;

	private List<EldoradoSummarySurchargeDto> surcharges;

	public Date getIssueDate()
	{
		return issueDate;
	}

	public void setIssueDate(Date issueDate)
	{
		this.issueDate = issueDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public int getDuration()
	{
		return duration;
	}

	public void setDuration(int duration)
	{
		this.duration = duration;
	}

	public boolean isExtendedCirculation()
	{
		return isExtendedCirculation;
	}

	public void setExtendedCirculation(boolean isExtendedCirculation)
	{
		this.isExtendedCirculation = isExtendedCirculation;
	}

	public BigDecimal getAmount()
	{
		return amount;
	}

	public void setAmount(BigDecimal amount)
	{
		this.amount = amount;
	}

	public OrderValorizationCalculationDto getCalculation()
	{
		return calculation;
	}

	public void setCalculation(OrderValorizationCalculationDto calculation)
	{
		this.calculation = calculation;
	}

	public BigDecimal getDiscount()
	{
		return discount;
	}

	public void setDiscount(BigDecimal discount)
	{
		this.discount = discount;
	}

	public OrderValorizationCalculationDto getDiscountCalculation()
	{
		return discountCalculation;
	}

	public void setDiscountCalculation(OrderValorizationCalculationDto discountCalculation)
	{
		this.discountCalculation = discountCalculation;
	}

	public List<EldoradoSummarySurchargeDto> getSurcharges()
	{
		return surcharges;
	}

	public void setSurcharges(List<EldoradoSummarySurchargeDto> surcharges)
	{
		this.surcharges = surcharges;
	}
}
