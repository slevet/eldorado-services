package com.publigroupe.eldorado.dto;

import java.math.BigDecimal;
import java.util.List;

import com.publigroupe.media.dto.MediaSupportEnumDto;
import com.publigroupe.media.dto.SizeMeasurementTypeEnumDto;

/**
 * @author u930rk
 */
public class EldoradoSummaryOfferDto {
  private MediaSupportEnumDto mediaSupport;

  private String offerId;
  private String offerName;
  private String brandId;
  private String brandName;
  private String brandLogo;
  private String mediaId;
  private String mediaName;
  private String subHeadingName;

  private boolean isUeditor;
  private String materialCmsUuid;
  private String uploadThumbnailurl;
  private String highresUrl;
  private String sizeName;
  private SizeMeasurementTypeEnumDto sizeMeasurementType;

  private BigDecimal amount;

  private List<EldoradoSummaryOfferPlacementDto> placements;
  private List<EldoradoSummaryOfferLocalPointDto> localPoints;

  private EldoradoSummaryOfferPrintPlusDto printPlus;

  private List<EldoradoSummarySurchargeDto> groupSurcharges;

  private EldoradoSummaryOfferStatusDto status;

  public MediaSupportEnumDto getMediaSupport() {
    return mediaSupport;
  }

  public void setMediaSupport(MediaSupportEnumDto mediaSupport) {
    this.mediaSupport = mediaSupport;
  }

  public String getOfferId() {
    return offerId;
  }

  public void setOfferId(String offerId) {
    this.offerId = offerId;
  }

  public String getOfferName() {
    return offerName;
  }

  public void setOfferName(String offerName) {
    this.offerName = offerName;
  }

  public String getBrandId() {
    return brandId;
  }

  public void setBrandId(String brandId) {
    this.brandId = brandId;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  public String getBrandLogo() {
    return brandLogo;
  }

  public void setBrandLogo(String brandLogo) {
    this.brandLogo = brandLogo;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaName() {
    return mediaName;
  }

  public void setMediaName(String mediaName) {
    this.mediaName = mediaName;
  }

  public String getSubHeadingName() {
    return subHeadingName;
  }

  public void setSubHeadingName(String subHeadingName) {
    this.subHeadingName = subHeadingName;
  }

  public String getMaterialCmsUuid() {
    return materialCmsUuid;
  }

  public void setMaterialCmsUuid(String materialCmsUuid) {
    this.materialCmsUuid = materialCmsUuid;
  }

  public boolean isUeditor() {
    return isUeditor;
  }

  public void setUeditor(boolean isUeditor) {
    this.isUeditor = isUeditor;
  }

  public String getUploadThumbnailurl() {
    return uploadThumbnailurl;
  }

  public void setUploadThumbnailurl(String uploadThumbnailurl) {
    this.uploadThumbnailurl = uploadThumbnailurl;
  }

  public String getHighresUrl() {
    return highresUrl;
  }

  public void setHighresUrl(String highresUrl) {
    this.highresUrl = highresUrl;
  }

  public EldoradoSummaryOfferPrintPlusDto getPrintPlus() {
    return printPlus;
  }

  public void setPrintPlus(EldoradoSummaryOfferPrintPlusDto printPlus) {
    this.printPlus = printPlus;
  }

  public List<EldoradoSummaryOfferLocalPointDto> getLocalPoints() {
    return localPoints;
  }

  public void setLocalPoints(List<EldoradoSummaryOfferLocalPointDto> localPoints) {
    this.localPoints = localPoints;
  }

  public String getSizeName() {
    return sizeName;
  }

  public void setSizeName(String sizeName) {
    this.sizeName = sizeName;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public List<EldoradoSummaryOfferPlacementDto> getPlacements() {
    return placements;
  }

  public void setPlacements(List<EldoradoSummaryOfferPlacementDto> placements) {
    this.placements = placements;
  }

  /**
   * @param sizeMeasurementType
   *          the sizeMeasurementType to set
   */
  public void setSizeMeasurementType(SizeMeasurementTypeEnumDto sizeMeasurementType) {
    this.sizeMeasurementType = sizeMeasurementType;
  }

  /**
   * @return the sizeMeasurementType
   */
  public SizeMeasurementTypeEnumDto getSizeMeasurementType() {
    return sizeMeasurementType;
  }

  public List<EldoradoSummarySurchargeDto> getGroupSurcharges() {
    return groupSurcharges;
  }

  public void setGroupSurcharges(List<EldoradoSummarySurchargeDto> groupSurcharges) {
    this.groupSurcharges = groupSurcharges;
  }

  public EldoradoSummaryOfferStatusDto getStatus() {
    return status;
  }

  public void setStatus(EldoradoSummaryOfferStatusDto status) {
    this.status = status;
  }

  public boolean containsExtendedDate() {
    if (placements != null) {
      for (EldoradoSummaryOfferPlacementDto placement : placements) {
        if (placement.getInsertions() == null) {
          continue;
        }
        List<EldoradoSummaryOfferInsertionDto> insertions = placement.getInsertions();
        for (EldoradoSummaryOfferInsertionDto insertion : insertions) {
          if (insertion.isExtendedCirculation()) {
            return true;
          }
        }
      }
    }
    return false;
  }

  public EldoradoSummaryOfferInsertionDto getFirstInsertionInFirstPlacement() {
    if (placements != null && !placements.isEmpty()) {
      EldoradoSummaryOfferPlacementDto placement = placements.get(0);
      if (placement != null) {
        List<EldoradoSummaryOfferInsertionDto> insertions = placement.getInsertions();
        if (insertions != null && !insertions.isEmpty()) {
          EldoradoSummaryOfferInsertionDto insertion = insertions.get(0);
          return insertion;
        }
      }
    }
    return null;
  }
}
