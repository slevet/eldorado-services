package com.publigroupe.eldorado.dto;

import java.util.List;


/**
 * Wrapper around Solr search results that includes both the matching brand
 * trees and the count information for all requested facets.
 */
public class SolrSearchResultDto {

  public static class FacetField {

    private String name;
    private int count;
    private List<FacetValue> values;

    public FacetField(final String name, final int count, final List<FacetValue> values) {
      super();
      this.name = name;
      this.count = count;
      this.values = values;
    }

    public String getName() {
      return this.name;
    }

    public int getCount() {
      return this.count;
    }

    public List<FacetValue> getValues() {
      return this.values;
    }

  }

  public static class FacetValue {

    private String name;
    private long count;

    public FacetValue(final String name, final long count) {
      super();
      this.name = name;
      this.count = count;
    }

    public String getName() {
      return this.name;
    }

    public long getCount() {
      return this.count;
    }

  }

  private List<FacetField> facetFields;
  private List<EldoradoBrandDto> brands;

  private int rowCount;
  private int startRow;

  public SolrSearchResultDto(
      final List<FacetField> facetFields,
      final List<EldoradoBrandDto> brands,
      final int startRow,
      final int rowCount) {
    this.facetFields = facetFields;
    this.brands = brands;
    this.rowCount = rowCount;
    this.startRow = startRow;
  }

  public List<FacetField> getFacetFields() {
    return this.facetFields;
  }

  public List<EldoradoBrandDto> getBrands() {
    return this.brands;
  }

  public int getRowCount() {
    return this.rowCount;
  }

  public int getStartRow() {
    return this.startRow;
  }

}
