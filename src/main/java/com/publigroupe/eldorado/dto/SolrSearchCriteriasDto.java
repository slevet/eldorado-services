package com.publigroupe.eldorado.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.springframework.util.StringUtils;

/**
 * Criterias to drive the Solr search.
 */
public class SolrSearchCriteriasDto {

  public static final int DEFAULT_ROW_COUNT = 50;

  private String fullTextQuery;

  private String npa;
  private String localeName;
  private String offerPackage;

  private List<String> facetFields = new ArrayList<String>();
  private List<String> facetQueries = new ArrayList<String>();
  private List<String> filters = new ArrayList<String>();

  private String sortField;
  private ORDER sortOrder;
  private int startRow;
  private int rowCount = DEFAULT_ROW_COUNT;

  public SolrSearchCriteriasDto() {
  }

  public String getFullTextQuery() {
    return this.fullTextQuery;
  }

  public void setFullTextQuery(final String fullTextQuery) {
    this.fullTextQuery = fullTextQuery;
  }

  public String getNpa() {
    return this.npa;
  }

  public void setNpa(final String npa) {
    this.npa = npa;
  }

  public String getLocaleName() {
    return this.localeName;
  }

  public void setLocaleName(final String localeName) {
    this.localeName = localeName;
  }

  public List<String> getFacetFields() {
    return this.facetFields;
  }

  public void addFacetField(final String name) {
    this.facetFields.add(name);
  }

  public List<String> getFacetQueries() {
    return this.facetQueries;
  }

  public void addFacetQuery(final String fieldName, final String value) {
    this.facetQueries.add(fieldName + ":" + value);
  }

  public List<String> getFilters() {
    return this.filters;
  }

  public void addFilter(final String fieldName, final String value) {
    this.filters.add(fieldName + ":" + value);
  }

  public int getStartRow() {
    return this.startRow;
  }

  public void setStartRow(final int startRow) {
    this.startRow = startRow;
  }

  public int getRowCount() {
    return this.rowCount;
  }

  public void setRowCount(final int rowCount) {
    this.rowCount = rowCount;
  }

  public String buildQuery() {
    final StringBuffer query = new StringBuffer();
    if (StringUtils.hasText(this.fullTextQuery)) {
      query.append("(" + escape(this.fullTextQuery) + ")");
    }
    else {
      query.append("*:*");
    }
    if (StringUtils.hasText(this.npa)) {
      query.append(" AND ((npaInclude:ALL AND -npaExclude:" + this.npa + ") OR (npaInclude:"
          + this.npa + "))");
    }

    if (StringUtils.hasText(this.offerPackage)) {
      query.append(" AND offerPackage:" + this.offerPackage);
    }
    return query.toString();
  }

  public String getSortField() {
    return sortField;
  }

  public ORDER getSortOrder() {
    return sortOrder;
  }

  public void setSortField(String sortField) {
    this.sortField = sortField;
  }

  public void setSortOrder(ORDER sortOrder) {
    this.sortOrder = sortOrder;
  }

  private String escape(final String query) {
    final StringBuffer sb = new StringBuffer();
    for (int i = 0; i < query.length(); i++) {
      final char c = query.charAt(i);
      switch (c) {
        // all the following chars should be escaped in Solr
        case '+':
        case '-':
        case '&':
        case '|':
        case '!':
        case '(':
        case ')':
        case '{':
        case '}':
        case '[':
        case ']':
        case '^':
        case '"':
        case '~':
        case '*':
        case '?':
        case ':':
        case '\\':
          sb.append('\\');
      }
      sb.append(c);
    }
    return sb.toString();
  }

  public String getOfferPackage() {
    return offerPackage;
  }

  public void setOfferPackage(String offerPackage) {
    this.offerPackage = offerPackage;
  }
}
