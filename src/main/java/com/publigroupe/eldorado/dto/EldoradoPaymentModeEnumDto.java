package com.publigroupe.eldorado.dto;

/**
 * Enumeration listing all possible payment modes.
 * 
 * @author Gon�alo Rodrigues - Consultant IT
 */
public enum EldoradoPaymentModeEnumDto {

  ONLINE_PAYMENT(" "),
  INVOICE_REQUESTED("R"),
  INVOICE_PAYMENT("I");

  private String code;

  private EldoradoPaymentModeEnumDto(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  public static EldoradoPaymentModeEnumDto getValueForCode(String code) {
    for (EldoradoPaymentModeEnumDto paymentMode : EldoradoPaymentModeEnumDto.values()) {
      if (paymentMode.getCode().equals(code)) {
        return paymentMode;
      }
    }
    return ONLINE_PAYMENT;
  }
}
