package com.publigroupe.eldorado.dto;

import com.publigroupe.media.dto.MediaUpsellingGenerationTypeEnumDto;
import com.publigroupe.media.dto.MediaUpsellingTreatmentTypeEnumDto;

/**
 * @author U930PL
 */
public class EldoradoSummaryOfferLocalPointDto {
  private String offerId;
  private String mediaSite;
  private int onlineduration;
  private MediaUpsellingGenerationTypeEnumDto upSellingGenerationType;
  private MediaUpsellingTreatmentTypeEnumDto upSellingTreatmentType;
  private String materialCmsUuid;

  /**
   * @return the mediaSite
   */
  public String getMediaSite() {
    return mediaSite;
  }

  /**
   * @param mediaSite
   *          the mediaSite to set
   */
  public void setMediaSite(String mediaSite) {
    this.mediaSite = mediaSite;
  }

  /**
   * @return the onlineduration
   */
  public int getOnlineduration() {
    return onlineduration;
  }

  /**
   * @param onlineduration
   *          the onlineduration to set
   */
  public void setOnlineduration(int onlineduration) {
    this.onlineduration = onlineduration;
  }

  /**
   * @return the upSellingGenerationType
   */
  public MediaUpsellingGenerationTypeEnumDto getUpSellingGenerationType() {
    return upSellingGenerationType;
  }

  /**
   * @param upSellingGenerationType
   *          the upSellingGenerationType to set
   */
  public void setUpSellingGenerationType(MediaUpsellingGenerationTypeEnumDto upSellingGenerationType) {
    this.upSellingGenerationType = upSellingGenerationType;
  }

  /**
   * @return the upSellingTreatmentType
   */
  public MediaUpsellingTreatmentTypeEnumDto getUpSellingTreatmentType() {
    return upSellingTreatmentType;
  }

  /**
   * @param upSellingTreatmentType
   *          the upSellingTreatmentType to set
   */
  public void setUpSellingTreatmentType(MediaUpsellingTreatmentTypeEnumDto upSellingTreatmentType) {
    this.upSellingTreatmentType = upSellingTreatmentType;
  }

  /**
   * @return the offerId
   */
  public String getOfferId() {
    return offerId;
  }

  /**
   * @param offerId
   *          the offerId to set
   */
  public void setOfferId(String offerId) {
    this.offerId = offerId;
  }

  public String getMaterialCmsUuid() {
    return materialCmsUuid;
  }

  public void setMaterialCmsUuid(String materialCmsUuid) {
    this.materialCmsUuid = materialCmsUuid;
  }

}
