package com.publigroupe.eldorado.dto;

import java.math.BigDecimal;
import java.util.List;

import com.publigroupe.order.dto.OrderApplicationEnumDto;
import com.publigroupe.order.dto.OrderQuotationMessageDto;
import com.publigroupe.order.dto.OrderStatusEnumDto;

public class EldoradoSummaryDto {
  private OrderApplicationEnumDto application;
  private OrderStatusEnumDto status;
  private boolean isAllPublicationDone;
  private String commandNumber;
  private String description;
  private String eldoradoHeading;

  private String currency;
  private BigDecimal amountWithoutVAT;
  private BigDecimal amountVAT;
  private BigDecimal percentVAT;
  private boolean isIncludedVAT;
  private BigDecimal amountWithVAT;

  private BigDecimal amountSurcharge;
  private List<EldoradoSummarySurchargeDto> surcharges;

  private List<EldoradoSummaryOfferDto> offers;

  private List<EldoradoSummaryInvoiceDto> invoices;

  private boolean isValid;
  private List<OrderQuotationMessageDto> quotationMessages;

  public OrderApplicationEnumDto getApplication() {
    return application;
  }

  public void setApplication(OrderApplicationEnumDto application) {
    this.application = application;
  }

  public OrderStatusEnumDto getStatus() {
    return status;
  }

  public void setStatus(OrderStatusEnumDto status) {
    this.status = status;
  }

  public boolean isAllPublicationDone() {
    return isAllPublicationDone;
  }

  public void setAllPublicationDone(boolean isAllPublicationDone) {
    this.isAllPublicationDone = isAllPublicationDone;
  }

  public void setCommandNumber(String commandNumber) {
    this.commandNumber = commandNumber;
  }

  public String getCommandNumber() {
    return commandNumber;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getEldoradoHeading() {
    return eldoradoHeading;
  }

  public void setEldoradoHeading(String eldoradoHeading) {
    this.eldoradoHeading = eldoradoHeading;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public BigDecimal getAmountWithoutVAT() {
    return amountWithoutVAT;
  }

  public void setAmountWithoutVAT(BigDecimal amountWithoutVAT) {
    this.amountWithoutVAT = amountWithoutVAT;
  }

  public BigDecimal getAmountVAT() {
    return amountVAT;
  }

  public void setAmountVAT(BigDecimal amountVAT) {
    this.amountVAT = amountVAT;
  }

  public BigDecimal getPercentVAT() {
    return percentVAT;
  }

  public void setPercentVAT(BigDecimal percentVAT) {
    this.percentVAT = percentVAT;
  }

  public boolean isIncludedVAT() {
    return isIncludedVAT;
  }

  public void setIncludedVAT(boolean isIncludedVAT) {
    this.isIncludedVAT = isIncludedVAT;
  }

  public BigDecimal getAmountWithVAT() {
    return amountWithVAT;
  }

  public void setAmountWithVAT(BigDecimal amountWithVAT) {
    this.amountWithVAT = amountWithVAT;
  }

  public BigDecimal getAmountSurcharge() {
    return amountSurcharge;
  }

  public void setAmountSurcharge(BigDecimal amountSurcharge) {
    this.amountSurcharge = amountSurcharge;
  }

  public List<EldoradoSummarySurchargeDto> getSurcharges() {
    return surcharges;
  }

  public void setSurcharges(List<EldoradoSummarySurchargeDto> surcharges) {
    this.surcharges = surcharges;
  }

  public List<EldoradoSummaryOfferDto> getOffers() {
    return offers;
  }

  public void setOffers(List<EldoradoSummaryOfferDto> offers) {
    this.offers = offers;
  }

  public List<EldoradoSummaryInvoiceDto> getInvoices() {
    return invoices;
  }

  public void setInvoices(List<EldoradoSummaryInvoiceDto> invoices) {
    this.invoices = invoices;
  }

  public boolean isValid() {
    return isValid;
  }

  public void setValid(boolean isValid) {
    this.isValid = isValid;
  }

  public List<OrderQuotationMessageDto> getQuotationMessages() {
    return quotationMessages;
  }

  public void setQuotationMessages(List<OrderQuotationMessageDto> quotationMessages) {
    this.quotationMessages = quotationMessages;
  }
}
