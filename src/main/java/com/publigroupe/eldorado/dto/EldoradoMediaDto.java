package com.publigroupe.eldorado.dto;

import java.util.List;

import com.publigroupe.media.dto.MediaDto;

public class EldoradoMediaDto
{
	private MediaDto media;
	private List<MediaDto> offers;

	public MediaDto getMedia()
	{
		return media;
	}

	public void setMedia(MediaDto media)
	{
		this.media = media;
	}

	public List<MediaDto> getOffers()
	{
		return offers;
	}

	public void setOffers(List<MediaDto> offers)
	{
		this.offers = offers;
	}
}
