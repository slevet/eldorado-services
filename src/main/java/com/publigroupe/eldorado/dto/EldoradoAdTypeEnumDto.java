package com.publigroupe.eldorado.dto;

public enum EldoradoAdTypeEnumDto {
  RealEstate,
  Vehicle,
  Job,
  Private,
  Erotic;

}
