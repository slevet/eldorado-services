package com.publigroupe.eldorado.dto;

import java.math.BigDecimal;

import com.publigroupe.media.dto.OptionSizeDto;

public class EldoradoRateDto {
  OptionSizeDto optionSize;
  private String currency;
  private BigDecimal amount;

  private int forcedSize;

  private int heightInMmWithGutter;

  public OptionSizeDto getOptionSize() {
    return optionSize;
  }

  public void setOptionSize(OptionSizeDto optionSize) {
    this.optionSize = optionSize;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public int getForcedSize() {
    return forcedSize;
  }

  public void setForcedSize(int forcedSize) {
    this.forcedSize = forcedSize;
  }

  public int getHeightInMmWithGutter() {
    return heightInMmWithGutter;
  }

  public void setHeightInMmWithGutter(int heightInMmWithGutter) {
    this.heightInMmWithGutter = heightInMmWithGutter;
  }
}
