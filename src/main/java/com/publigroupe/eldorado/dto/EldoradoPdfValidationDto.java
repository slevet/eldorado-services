package com.publigroupe.eldorado.dto;

import java.util.ArrayList;
import java.util.List;

import com.publigroupe.media.dto.OptionSizeDto;

public class EldoradoPdfValidationDto {

  private String filename;
  private String filePath;
  private String thumbnailName;
  private String thumbnailPath;
  private OptionSizeDto optionSize;
  private int pdfWidth;
  private int pdfHeight;

  public int getPdfHeight() {
    return pdfHeight;
  }

  public void setPdfHeight(int pdfHeight) {
    this.pdfHeight = pdfHeight;
  }

  private boolean inColor;
  private boolean valid;
  private List<EldoradoPdfErrorEnumDto> errors;
  private String message;

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public String getThumbnailName() {
    return thumbnailName;
  }

  public void setThumbnailName(String thumbnailName) {
    this.thumbnailName = thumbnailName;
  }

  public String getThumbnailPath() {
    return thumbnailPath;
  }

  public void setThumbnailPath(String thumbnailPath) {
    this.thumbnailPath = thumbnailPath;
  }

  public OptionSizeDto getOptionSize() {
    return optionSize;
  }

  public void setOptionSize(OptionSizeDto optionSize) {
    this.optionSize = optionSize;
  }

  public int getPdfWidth() {
    return pdfWidth;
  }

  public void setPdfWidth(int pdfWidth) {
    this.pdfWidth = pdfWidth;
  }

  public boolean isInColor() {
    return inColor;
  }

  public void setInColor(boolean inColor) {
    this.inColor = inColor;
  }

  public boolean isValid() {
    return valid;
  }

  public void setValid(boolean valid) {
    this.valid = valid;
  }

  public List<EldoradoPdfErrorEnumDto> getErrors() {
    return errors;
  }

  public void setErrors(List<EldoradoPdfErrorEnumDto> errors) {
    this.errors = errors;
  }

  public void addError(EldoradoPdfErrorEnumDto error) {
    if (this.errors == null) {
      this.errors = new ArrayList<EldoradoPdfErrorEnumDto>();
    }
    this.errors.add(error);
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
