package com.publigroupe.eldorado.dto;


public class EldoradoSummaryOfferPrintPlusDto
{
	private int duration;
	private String mediaSite;
	private String materialCmsUuid;
	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}
	/**
	 * @return the mediaSite
	 */
	public String getMediaSite() {
		return mediaSite;
	}
	/**
	 * @param mediaSite the mediaSite to set
	 */
	public void setMediaSite(String mediaSite) {
		this.mediaSite = mediaSite;
	}
	/**
	 * @return the material
	 */
	public String getMaterialCmsUuid() {
		return materialCmsUuid;
	}
	/**
	 * @param material the material to set
	 */
	public void setMaterialCmsUuid(String material) {
		this.materialCmsUuid = material;
	}



	
}
