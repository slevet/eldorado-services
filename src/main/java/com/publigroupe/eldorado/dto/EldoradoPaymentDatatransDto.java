package com.publigroupe.eldorado.dto;

public class EldoradoPaymentDatatransDto
{
	private EldoradoPaymentTypeEnumDto paymentType = EldoradoPaymentTypeEnumDto.EMPTY;
	private String uppId;
	private String authorizationCode;

	public void setPaymentType(EldoradoPaymentTypeEnumDto paymentType)
	{
		this.paymentType = paymentType;
	}

	public EldoradoPaymentTypeEnumDto getPaymentType()
	{
		return paymentType;
	}

	public void setUppId(String uppId)
	{
		this.uppId = uppId;
	}

	public String getUppId()
	{
		return uppId;
	}

	public void setAuthorizationCode(String authorizationCode)
	{
		this.authorizationCode = authorizationCode;
	}

	public String getAuthorizationCode()
	{
		return authorizationCode;
	}
}
