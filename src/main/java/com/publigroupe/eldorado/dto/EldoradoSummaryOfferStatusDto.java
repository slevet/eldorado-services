package com.publigroupe.eldorado.dto;

public class EldoradoSummaryOfferStatusDto
{
	private boolean isValid;

	private boolean isInsertionValid;
	private boolean isMaterialPrintValid;
	private boolean isMaterialOnlineValid;

	public boolean isValid()
	{
		return isValid;
	}

	public void setValid(boolean isValid)
	{
		this.isValid = isValid;
	}

	public boolean isInsertionValid()
	{
		return isInsertionValid;
	}

	public void setInsertionValid(boolean isInsertionValid)
	{
		this.isInsertionValid = isInsertionValid;
	}

	public boolean isMaterialPrintValid()
	{
		return isMaterialPrintValid;
	}

	public void setMaterialPrintValid(boolean isMaterialPrintValid)
	{
		this.isMaterialPrintValid = isMaterialPrintValid;
	}

	public boolean isMaterialOnlineValid()
	{
		return isMaterialOnlineValid;
	}

	public void setMaterialOnlineValid(boolean isMaterialOnlineValid)
	{
		this.isMaterialOnlineValid = isMaterialOnlineValid;
	}
}
