package com.publigroupe.eldorado.dto;

import java.util.HashMap;

/**
 * @author u930rk
 */
public enum EldoradoCategoryEnumDto {
  IMMO_SELL_BUY("IMMO_SELL_BUY", "10", EldoradoAdTypeEnumDto.RealEstate, EldoradoAdTransactionIDEnumDto.Sale, null),
  IMMO_LOCATION("IMMO_LOCATION", "11", EldoradoAdTypeEnumDto.RealEstate, EldoradoAdTransactionIDEnumDto.Rent, null),
  IMMO_HOLIDAY("IMMO_HOLIDAY", "12", EldoradoAdTypeEnumDto.RealEstate, null, null),

  JOB_OFFERS("JOB_OFFERS", "20", EldoradoAdTypeEnumDto.Job, null, EldoradoAdOperationIDEnumDto.Offer),
  JOB_ASKS("JOB_ASKS", "21", EldoradoAdTypeEnumDto.Job, null, EldoradoAdOperationIDEnumDto.Request),
  JOB_STUDY("JOB_STUDY", "22", EldoradoAdTypeEnumDto.Job, null, null),

  PRIVATE_AUTO_MOTO("PRIVATE_AUTO_MOTO", "30", EldoradoAdTypeEnumDto.Vehicle, null, null),
  PRIVATE_SELL_BUY("PRIVATE_SELL_BUY", "31", EldoradoAdTypeEnumDto.Private, null, null),

  OTHERS_FRIENDS("OTHERS_FRIENDS", "40", EldoradoAdTypeEnumDto.Erotic, null, null),
  OTHERS_SEX("OTHERS_SEX", "41", EldoradoAdTypeEnumDto.Erotic, null, null),

  OTHERS_OPINION_CLUB("OTHERS_OPINION_CLUB", "50", EldoradoAdTypeEnumDto.Private, null, null),
  OTHERS_OPINION_LOTO("OTHERS_OPINION_LOTO", "51", EldoradoAdTypeEnumDto.Private, null, null),
  OTHERS_OPINION_BORN("OTHERS_OPINION_BORN", "52", EldoradoAdTypeEnumDto.Private, null, null),

  OTHERS_COMMERCIAL("OTHERS_COMMERCIAL", "60", EldoradoAdTypeEnumDto.Private, null, null),
  OTHERS_HOTEL("OTHERS_HOTEL", "61", EldoradoAdTypeEnumDto.Private, null, null),
  OTHERS_CINEMA("OTHERS_CINEMA", "62", EldoradoAdTypeEnumDto.Private, null, null),
  OTHERS_STUDY("OTHERS_STUDY", "63", EldoradoAdTypeEnumDto.Private, null, null),
  OTHERS_TRIP("OTHERS_TRIP", "64", EldoradoAdTypeEnumDto.Private, null, null);

  private String label;
  private String code;
  private EldoradoAdTypeEnumDto adType;
  private EldoradoAdTransactionIDEnumDto adTransactionId;
  private EldoradoAdOperationIDEnumDto adOperationId;

  private static HashMap<String, EldoradoCategoryEnumDto> code2CatagoryEnumMap;
  private static HashMap<String, EldoradoCategoryEnumDto> name2CatagoryEnumMap;

  /**
   * @param name
   * @param code
   * @param adType
   */
  private EldoradoCategoryEnumDto(
      String name,
      String code,
      EldoradoAdTypeEnumDto adType,
      EldoradoAdTransactionIDEnumDto transactionId,
      EldoradoAdOperationIDEnumDto operationId) {
    this.label = name;
    this.code = code;
    this.adType = adType;
    this.adTransactionId = transactionId;
    this.adOperationId = operationId;
    EldoradoCategoryEnumDto.storeCategory(code, this);
  }

  /**
   * @param code
   * @param categoryEnum
   */
  private static void storeCategory(String code, EldoradoCategoryEnumDto categoryEnum) {
    if (code2CatagoryEnumMap == null) {
      code2CatagoryEnumMap = new HashMap<String, EldoradoCategoryEnumDto>();
      name2CatagoryEnumMap = new HashMap<String, EldoradoCategoryEnumDto>();
    }
    if (!code2CatagoryEnumMap.containsKey(code)) {
      code2CatagoryEnumMap.put(code, categoryEnum);
      name2CatagoryEnumMap.put(categoryEnum.label, categoryEnum);
    }
  }

  /**
   * @param code
   * @return code
   */
  public static EldoradoCategoryEnumDto getCategoryForCode(String code) {
    return code2CatagoryEnumMap.get(code);
  }

  /**
   * @param code
   * @return code
   */
  public static EldoradoCategoryEnumDto getCategoryFromName(String name) {
    return name2CatagoryEnumMap.get(name);
  }

  /**
   * @return the code
   */
  public String getCode() {
    return code;
  }

  /**
   * @return the adType
   */
  public EldoradoAdTypeEnumDto getAdType() {
    return adType;
  }

  /**
   * @return the adTransactionId
   */
  public EldoradoAdTransactionIDEnumDto getAdTransactionId() {
    return adTransactionId;
  }

  /**
   * @return the adOperationId
   */
  public EldoradoAdOperationIDEnumDto getAdOperationId() {
    return adOperationId;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

}
