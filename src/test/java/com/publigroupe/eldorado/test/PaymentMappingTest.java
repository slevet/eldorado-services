package com.publigroupe.eldorado.test;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.dao.bean.Rsgv25;
import com.publigroupe.eldorado.domain.EldoradoPayment;
import com.publigroupe.eldorado.domain.EldoradoPaymentMapping;
import com.publigroupe.eldorado.dto.EldoradoPaymentDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =
{ "classpath:eldoradoServicesContext.xml" })
@TestExecutionListeners(
{ DependencyInjectionTestExecutionListener.class })
public class PaymentMappingTest extends TestCase
{
	private static Log log = LogFactory.getLog(PaymentTest.class);

	EldoradoPayment payment = new EldoradoPayment();

	@Test
	public void testPayementMapping()
	{
		Rsgv25 rsgv25 = new Rsgv25();
		rsgv25.adrloc = "ad";
		rsgv25.gvemail = "ad@dv.com";
		EldoradoPaymentDto dto = EldoradoPaymentMapping.convert(rsgv25);
		assertNotNull(dto);
		Rsgv25 rsgv25ctrl = EldoradoPaymentMapping.convert(dto);
		assertNotNull(rsgv25ctrl);
		assertEquals(rsgv25.adrloc, rsgv25ctrl.adrloc);
		assertEquals(rsgv25.gvemail, rsgv25ctrl.gvemail);
	}
}