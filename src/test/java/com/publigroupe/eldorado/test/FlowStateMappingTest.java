package com.publigroupe.eldorado.test;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.dao.bean.Rsgv26;
import com.publigroupe.eldorado.domain.EldoradoFlowStateMapping;
import com.publigroupe.eldorado.domain.EldoradoPayment;
import com.publigroupe.eldorado.dto.EldoradoFlowStateDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =
{ "classpath:eldoradoServicesContext.xml" })
@TestExecutionListeners(
{ DependencyInjectionTestExecutionListener.class })
public class FlowStateMappingTest extends TestCase
{
	private static Log log = LogFactory.getLog(PaymentTest.class);

	EldoradoPayment payment = new EldoradoPayment();

	@Test
	public void testPayementMapping()
	{
		Rsgv26 rsgv26 = new Rsgv26();
		rsgv26.cdenum = "ad";
		rsgv26.utiento = "ad";
		EldoradoFlowStateDto dto = EldoradoFlowStateMapping.convert(rsgv26);
		assertNotNull(dto);
		Rsgv26 rsgv26ctrl = EldoradoFlowStateMapping.convert(dto);
		assertNotNull(rsgv26ctrl);
		assertEquals(rsgv26.cdenum, rsgv26ctrl.cdenum);
		assertEquals(rsgv26.utiento, rsgv26ctrl.utiento);
	}
}