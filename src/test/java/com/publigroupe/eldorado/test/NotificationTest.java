package com.publigroupe.eldorado.test;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.domain.EldoradoNotification;
import com.publigroupe.eldorado.manager.EldoradoOrderManager;
import com.publigroupe.media.domain.CodeList;
import com.publigroupe.order.dto.OrderDto;
import com.publigroupe.order.dto.OrderInfoDto;
import com.publigroupe.partner.dto.PartnerDto;
import com.publigroupe.partner.dto.PartnerQualifierInfoDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:eldoradoServicesContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class NotificationTest extends TestCase {
  private static Log log = LogFactory.getLog(NotificationTest.class);

  @Autowired
  private EldoradoNotification notification;
  static EldoradoOrderManager orderManager;

  @BeforeClass
  public static void beforeClass() {
    orderManager = new EldoradoOrderManager();
  }

  /*
   * @Test
   * public void notifyCancellationTestFailed()
   * {
   * boolean isCatched = false;
   * log.debug("--sendmailTest Start--");
   * try
   * {
   * notification.notifyCancellation(orderManager, "test");
   * }
   * catch (RuntimeException e)
   * {
   * isCatched = true;
   * if (e.getCause() instanceof SendFailedException)
   * {
   * assertTrue(true);
   * }
   * else
   * {
   * assertTrue(false);
   * }
   * }
   * assertTrue(isCatched);
   * log.debug("--sendmailTest End --");
   * }
   */

  @Test
  public void notifyCancellationTest() {
    log.debug("--sendmailTest Start--");
    EldoradoOrderManager eldoradoOrderManager = new EldoradoOrderManager();
    OrderDto order = new OrderDto();
    OrderInfoDto info = new OrderInfoDto();
    PartnerDto partner = new PartnerDto();
    PartnerQualifierInfoDto qual = new PartnerQualifierInfoDto();
    qual.setEmailAddress("test@test.test");
    partner.setQualifierInfo(qual);
    info.setUser(partner);
    order.setInfo(info);
    eldoradoOrderManager.setOrderDto(order);
    notification.initVariables(CodeList.extractCodeValue("UTIENTO", orderManager.getOrderDto().getCodeList(),
        orderManager.getOrderDto().getCodeValue()).substring(0, 2));
    notification.notifyCancellationUser(eldoradoOrderManager);
    log.debug("--sendmailTest End --");
  }

  @Test
  public void notifyInsertP2000Test() {
    log.debug("--sendmailTest Start--");
    notification.notifyInsertP2000(orderManager, "test@test.test");
    log.debug("--sendmailTest End --");
  }

  @Test
  public void notifySuspendTest() {
    log.debug("--sendmailTest Start--");
    notification.notifySuspend(orderManager, "test@test.test");
    log.debug("--sendmailTest End --");
  }

  @Test
  public void notifyValidationTest() {
    log.debug("--sendmailTest Start--");
    notification.notifyValidation(orderManager, "test@test.testtest");
    log.debug("--sendmailTest End --");
  }
}
