package com.publigroupe.eldorado.test;

import java.io.File;

import junit.framework.TestCase;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.dto.EldoradoPdfValidationDto;
import com.publigroupe.eldorado.manager.EldoradoOrderManager;
import com.publigroupe.pdfcheckservice._1_0.PdfCheckService;

/**
 * Eldorado Order manager test.
 * 
 * @author Barman Dominique
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:eldoradoServicesContext.xml", "eldoradoServicesContextTest.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class EldoradoPdfTest extends TestCase {
  @Autowired
  EldoradoOrderManager manager;
  @Autowired
  private PdfCheckService pdfCheckService;

  @Test
  public void validateUploadMaterialTest() throws Exception {
    manager.setPdfCheckService(pdfCheckService);
    String offerId = "ELDORADOOFFER1000-1000-17001-00-1126-20110101";
    String path = "C:\\dev\\workspaces\\eldomaven\\Eldorado-Services\\src\\test\\resources\\pdfs\\";
    manager.placePrint(offerId);

    File file = new File(path + "010.pdf");

    EldoradoPdfValidationDto result = manager.validateUploadMaterial(offerId, FileUtils.openInputStream(file));
    System.out.println("--> File:" + file.getName() + " size" + result.getPdfWidth() + " X " + result.getPdfHeight());

    assertNotNull(result);
    assertTrue(result.isValid());
  }

  @Test
  public void getPdfSizeUploadMaterialTest() throws Exception {
    manager.setPdfCheckService(pdfCheckService);
    String offerId = "ELDORADOOFFER1000-1000-17001-00-1126-20110101";
    String path = "C:\\dev\\workspaces\\eldomaven\\Eldorado-Services\\src\\test\\resources\\pdfs\\";
    manager.placePrint(offerId);

    File dir = new File(path);
    for (File file : dir.listFiles()) {
      if (file.getName().contains(".pdf")) {
        manager.getGrandeurMaterial(file);
      }
    }
    System.out.println("--> END");
  }
}
