package com.publigroupe.eldorado.test;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.dao.Rsgv25Dao;
import com.publigroupe.eldorado.dao.bean.Rsgv25;
import com.publigroupe.order.dao.p2000.OrderAccessor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =
{ "classpath:eldoradoServicesContext.xml" })
@TestExecutionListeners(
{ DependencyInjectionTestExecutionListener.class })
public class Rsgv25DaoTest extends TestCase
{
	private static Log log = LogFactory.getLog(Rsgv25DaoTest.class);

	private Rsgv25Dao rsgv25Dao = new Rsgv25Dao();

	@Test(expected = IllegalArgumentException.class)
	public void testInsertWithoutUtiste()
	{
		log.debug("-- Start Test testInsertWithoutUtiste --");
		Rsgv25 rsgv25 = new Rsgv25();
		rsgv25Dao.insert(rsgv25);
		OrderAccessor.rollBack();
		OrderAccessor.returnConnectionToPool();
		log.debug("-- End Test testInsertWithoutUtiste --");
	}

	@Test
	public void testInsert()
	{
		log.debug("-- Start Test insert --");
		Rsgv25 rsgv25 = new Rsgv25();
		rsgv25.utiste = "-1";
		rsgv25.utiento = "-1";
		rsgv25.cdenum = "-1";
		assertTrue(rsgv25Dao.insert(rsgv25));
		OrderAccessor.rollBack();
		OrderAccessor.returnConnectionToPool();
		log.debug("-- End Test insert --");
	}

	@Test
	public void testInsertUpdateEmptyRemove()
	{
		log.debug("-- Start Test testInsertUpdateEmptyRemove --");
		Rsgv25 rsgv25 = new Rsgv25();
		rsgv25.utiste = "-1";
		rsgv25.utiento = "-1";
		rsgv25.cdenum = "-1";
		assertTrue(rsgv25Dao.insert(rsgv25));
		OrderAccessor.commit();
		rsgv25Dao.update(rsgv25);
		OrderAccessor.commit();
		Rsgv25 result = rsgv25Dao.retrieve("-1", "-1");
		log.debug(result);
		assertNotNull(result);
		rsgv25Dao.delete(rsgv25.utiento, rsgv25.cdenum);
		Rsgv25 result2 = rsgv25Dao.retrieve("-1", "-1");
		assertNull(result2);
		OrderAccessor.commit();
		OrderAccessor.returnConnectionToPool();
		log.debug("-- End Test testInsertUpdateEmptyRemove --");
	}

	@Test
	public void testInsertUpdateRemove()
	{
		log.debug("-- Start Test testInsertUpdateEmptyRemove --");
		Date current = new Date();
		Rsgv25 rsgv25 = new Rsgv25();
		rsgv25.utiste = "-1";
		rsgv25.utiento = "-1";
		rsgv25.cdenum = "-1";
		assertTrue(rsgv25Dao.insert(rsgv25));
		OrderAccessor.commit();

		rsgv25.utiste = "u";
		rsgv25.libcde = "l";
		rsgv25.cdeneta = new BigDecimal(1.0);
		rsgv25.tvamt = new BigDecimal(1.0);
		rsgv25.cdenet = new BigDecimal(1.0);
		rsgv25.tvatxc = "t";
		rsgv25.tvatx = new BigDecimal(1.0);
		rsgv25.inlogdt = new Timestamp(current.getTime());
		rsgv25.metpaie = "m";
		rsgv25.modpaie = "m";
		rsgv25.stransa = Rsgv25.Stransa.valueOf("VA");
		rsgv25.dtempaie = current;
		rsgv25.outlogdt = new Timestamp(current.getTime());
		rsgv25.orimaj = Rsgv25.Orimaj.EM;
		rsgv25.gvpren = "g";
		rsgv25.gvnom = "g";
		rsgv25.gvrue = "g";
		rsgv25.gvnpa = "g";
		rsgv25.gvloc = "g";
		rsgv25.gvpays = "g";
		rsgv25.gvntel = "g";
		rsgv25.gvnfax = "g";
		rsgv25.gvemail = "g";
		rsgv25.gvcdlng = "g";
		rsgv25.utientop = "u";
		rsgv25.cdenump = "c";
		rsgv25.valcdedt = new Timestamp(current.getTime());
		rsgv25.utistec = "u";
		rsgv25.utientc = "u";
		rsgv25.clino = "c";
		rsgv25.clicch = "c";
		rsgv25.cligenr = "c";
		rsgv25.adrnom = "a";
		rsgv25.adrloc = "a";
		rsgv25.procat = "p";
		rsgv25.progenr = "r";
		rsgv25.prono = "o";
		rsgv25.valclidt = new Timestamp(current.getTime());
		rsgv25.txordid = "d";
		rsgv25.vallogdt = new Timestamp(current.getTime());
		rsgv25.gvtxcon = "c";
		rsgv25.conlogdt = new Timestamp(current.getTime());
		rsgv25.gvpro = "g";
		rsgv25.adrreg = "a";
		rsgv25.adrtele = "t";
		rsgv25.upptrid = "u";
		rsgv25.authcod = "a";

		rsgv25Dao.update(rsgv25);
		OrderAccessor.commit();
		Rsgv25 result = rsgv25Dao.retrieve("-1", "-1");
		log.debug(result);
		assertNotNull(result);

		assertEquals(result.utiste, "u");
		assertEquals(result.libcde, "l");
		assertEquals(result.cdeneta.doubleValue(), 1.0);
		assertEquals(result.tvamt.doubleValue(), 1.0);
		assertEquals(result.cdenet.doubleValue(), 1.0);
		assertEquals(result.tvatxc, "t");
		assertEquals(result.tvatx.doubleValue(), 1.0);
		assertEquals(result.inlogdt, new Timestamp(current.getTime()));
		assertEquals(result.metpaie, "m");
		assertEquals(result.modpaie, "m");
		assertEquals(result.stransa.name(), "VA");
		assertEquals(DateUtils.getFragmentInDays(result.dtempaie, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(result.outlogdt.getTime(), current.getTime());
		assertEquals(result.orimaj.name(), "EM");
		assertEquals(result.gvpren, "g");
		assertEquals(result.gvnom, "g");
		assertEquals(result.gvrue, "g");
		assertEquals(result.gvnpa, "g");
		assertEquals(result.gvloc, "g");
		assertEquals(result.gvpays, "g");
		assertEquals(result.gvntel, "g");
		assertEquals(result.gvnfax, "g");
		assertEquals(result.gvemail, "g");
		assertEquals(result.gvcdlng, "g");
		assertEquals(result.utientop, "u");
		assertEquals(result.cdenump, "c");
		assertEquals(result.valcdedt.getTime(), current.getTime());
		assertEquals(result.utistec, "u");
		assertEquals(result.utientc, "u");
		assertEquals(result.clino, "c");
		assertEquals(result.clicch, "c");
		assertEquals(result.cligenr, "c");
		assertEquals(result.adrnom, "a");
		assertEquals(result.adrloc, "a");
		assertEquals(result.procat, "p");
		assertEquals(result.progenr, "r");
		assertEquals(result.prono, "o");
		assertEquals(result.valclidt.getTime(), current.getTime());
		assertEquals(result.txordid, "d");
		assertEquals(result.vallogdt.getTime(), current.getTime());
		assertEquals(result.gvtxcon, "c");
		assertEquals(result.conlogdt.getTime(), current.getTime());
		assertEquals(result.gvpro, "g");
		assertEquals(result.adrreg, "a");
		assertEquals(result.adrtele, "t");
		assertEquals(result.upptrid, "u");
		assertEquals(result.authcod, "a");

		rsgv25Dao.delete(rsgv25.utiento, rsgv25.cdenum);
		Rsgv25 result2 = rsgv25Dao.retrieve("-1", "-1");
		assertNull(result2);
		OrderAccessor.commit();
		OrderAccessor.returnConnectionToPool();
		log.debug("-- End Test testInsertUpdateEmptyRemove --");
	}
}
