package com.publigroupe.eldorado.test;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.domain.EldoradoPayment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =
{ "classpath:eldoradoServicesContext.xml" })
@TestExecutionListeners(
{ DependencyInjectionTestExecutionListener.class })
public class PaymentTest extends TestCase
{
	private static Log log = LogFactory.getLog(PaymentTest.class);

	EldoradoPayment payment = new EldoradoPayment();

	@Test
	public void testSaveDebitCancel()
	{
		log.debug("start test process");
		// PartnerDto partnerDto = new PartnerDto();
		// PartnerPhysicalAddressDto partnerPhysicalAddressDto = new
		// PartnerPhysicalAddressDto();
		// partnerPhysicalAddressDto.setCountry(PartnerCountryEnumDto.CH);
		// partnerDto.setPhysicalAddress(partnerPhysicalAddressDto);
		// partnerDto.setQualifierInfo(new PartnerQualifierInfoDto());
		// partnerDto.setLanguage(LanguageId.French);
		// PaymentDto paymentDtoControl =
		// payment.savePaymentAuthorization(partnerDto,new
		// PaymentDatatransDto(),"-1","-1");
		// assertEquals(paymentDtoControl.getUser().getPhysicalAddress().getCountry().name(),PartnerCountryEnumDto.CH.name());
		// assertEquals(paymentDtoControl.getUser().getLanguage(),LanguageId.French);
		// assertEquals(paymentDtoControl.getUpdateStatus(),"EM");
		// assertEquals(paymentDtoControl.getPaymentStatus(),"VA");
		// assertEquals(paymentDtoControl.getTransactionNumberGv(),"0");
		// assertEquals(paymentDtoControl.getPaymentMethod(),"00");
		// PaymentDto paymentDtoControl2 = payment.debitPayment("-1", "-1");
		// assertEquals(paymentDtoControl2.getPaymentStatus(),"OK");
		// assertEquals(paymentDtoControl2.getCommandNumberGv(),"-1");
		// assertEquals(paymentDtoControl2.getUpdateStatus(),"EM");
		// assertEquals(paymentDtoControl2.getTransactionNumberGv(),"0");
		// assertEquals(paymentDtoControl2.getPaymentMethod(),"00");
		// payment.cancelPaymentAuthorization("-1", "-1");
		// PaymentDto paymentDtoControl3 = payment.retreive("-1", "-1");
		// assertNull(paymentDtoControl3);
		log.debug("end test process");
	}
	
	@Test
	public void debitPaymentShouldSucceed() {
//	    payment.debitPayment("10012", "700010");
	}
}
