package com.publigroupe.eldorado.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.manager.EldoradoPartnerManager;
import com.publigroupe.partner.dto.PartnerAddressTypeEnumDto;
import com.publigroupe.partner.dto.PartnerCountryEnumDto;
import com.publigroupe.partner.dto.PartnerDto;
import com.publigroupe.partner.dto.PartnerPhysicalAddressDto;
import com.publigroupe.partner.dto.PartnerPhysicalAddressTypeEnumDto;
import com.publigroupe.partner.dto.PartnerQualifierEnumDto;
import com.publigroupe.partner.dto.PartnerQualifierInfoDto;

/**
 * Eldorado User test.
 * 
 * @author Jilali Raki
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:eldoradoServicesContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class EldoradoPartnerManagerTest {
  @Autowired
  EldoradoPartnerManager manager;

  @Test
  public void testmodifyUser() {
    System.out.println("----------------------");
    System.out.println("testmodifyUser");

    // Personal address information
    String user = "blaise.evequoz@xentive.ch";
    // boolean isOk = manager.modifyUserInActiveDirectory(user,
    // retrievePartnerDto());
    manager.modifyUser(retrievePartnerDto(), user);
    System.out.println("testmodifyUser");

  }

  @Test
  public void testfindUsersInActiveDirectory() {
    System.out.println("----------------------");
    System.out.println("testfindUsersInActiveDirectory");

    // criteria
    // only one : employeeNumber=*123456*
    // multiple : (&(employeeNumber=*123456*)(mail=*xentive*))
    // "& : and" "| : or"
    String criteria = "(|(cn=*blaise*)(mail=*xentive*))";
    PartnerDto usr = manager.retrieveUserInActiveDirectory("blaise.evequoz.registerd@xentive.ch");
    System.out.println("testfindUsersInActiveDirectory");

  }

  private void checkpwd(String password) {

    String user = "blaise.evequoz.registerd2@xentive.ch";
    boolean hasUppercase = !password.equals(password.toLowerCase());
    boolean hasLowercase = !password.equals(password.toUpperCase());
    boolean hasNumber = password.matches(".*\\d.*");
    boolean noSpecialChar = password.matches("[a-zA-Z0-9 ]*");
    int nbre = 0;
    if (hasUppercase)
      nbre++;
    if (hasLowercase)
      nbre++;
    if (hasNumber)
      nbre++;
    if (!noSpecialChar)
      nbre++;

    System.out.println("/Length:" + password.length() + "/MATCHCRIT:" + nbre + "/digits:" + hasNumber
        + "/hasLowercase:" + hasLowercase + "/hasUppercase:" + hasUppercase + "/hasSpecialChar:" + noSpecialChar
        + ":::::" + password);
    boolean isOk = manager.modifyUserPasswordInActiveDirectory(user, password);
    System.out.println("modifyUserPasswordInActiveDirectory" + isOk);

    isOk = manager.isUserInActiveDirectoryPasswordValid(user, password);
    System.out.println("isUserInActiveDirectoryPasswordValid" + isOk);
  }

  @Test
  public void testCheckPassword() {
    checkpwd("");
    checkpwd("123a9sdT"); // short
    checkpwd("abc45678"); // not 3
    checkpwd("12345678"); // not 3
    checkpwd("123qwt@@"); // OK
    checkpwd("123asdTaaa");
    checkpwd("123 QWR�=");
    checkpwd("1.2.3a45678");

  }

  @Test
  public void testResetPassword() {
    System.out.println("----------------------");
    System.out.println("testResetOassword");

    // Personal address information
    // String user = "blaise.evequoz.registerd1@xentive.ch";
    // String password = "Blaise_63";
    String user = "blaise.evequoz.registerd2@xentive.ch";
    manager.resetUserPasswordInActiveDirectory(user, "FR");
    System.out.println("testResetOassword_OK");
  }

  @Test
  public void testActiveDirectory() {
    System.out.println("----------------------");
    System.out.println("testCreateUserPasswordInActiveDirectory");

    // Personal address information
    // String user = "blaise.evequoz.registerd1@xentive.ch";
    // String password = "Blaise_63";
    String user = "blaise.evequoz.registerd2@xentive.ch";
    // String password = "Blaise33"; // OK
    // String password = "blaise_33"; // OK
    // String password = "blaise_aa"; // KO
    // String password = "BlaiseBBB"; // KO
    String password = "Bla33_12"; // KO
    // String password = "blaise33"; // KO
    boolean isOk = false;
    // isOk = manager.createUserInActiveDirectory(user, password,
    // retrievePartnerDto());
    // System.out.println("testCreateUserPasswordInActiveDirectory" + isOk);
    //
    // isOk = manager.isUserInActiveDirectory(user);
    // System.out.println("isUserInActiveDirectory" + isOk);
    //
    // isOk = manager.isUserInActiveDirectoryActivated(user);
    // System.out.println("isUserInActiveDirectoryActivated" + isOk);
    //
    // isOk = manager.isUserInActiveDirectoryPasswordValid(user, password);
    // System.out.println("isUserInActiveDirectoryPasswordValid" + isOk);

    isOk = manager.modifyUserPasswordInActiveDirectory(user, password);
    System.out.println("modifyUserPasswordInActiveDirectory" + isOk);

    manager.activateUserInActiveDirectory(user);
    System.out.println("activateUserInActiveDirectory");

    isOk = manager.isUserInActiveDirectoryActivated(user);
    System.out.println("isUserInActiveDirectoryActivated" + isOk);

    isOk = manager.isUserInActiveDirectoryPasswordValid(user, password);
    System.out.println("isUserInActiveDirectoryPasswordValid" + isOk);

  }

  private PartnerDto retrievePartnerDto() {
    // Personal address information
    PartnerDto user = new PartnerDto();
    user.setAddressType(PartnerAddressTypeEnumDto.PERSON);
    user.setLanguage("de");
    user.setReceiveNews(false);
    user.setReceiveOffers(true);

    PartnerPhysicalAddressDto physicalAddressDto = new PartnerPhysicalAddressDto();
    physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.STANDARD);
    physicalAddressDto.setStreet("Av. des Mousquines");
    physicalAddressDto.setStreetNr("4");
    physicalAddressDto.setCountry(PartnerCountryEnumDto.CH);
    physicalAddressDto.setCity("Lausanne");
    physicalAddressDto.setZip("1001");
    user.setPhysicalAddress(physicalAddressDto);

    // Personal qualifier information
    PartnerQualifierInfoDto qualifierInfoDto = new PartnerQualifierInfoDto();
    qualifierInfoDto.setQualifier(PartnerQualifierEnumDto.MRS);
    qualifierInfoDto.setFirstName("Blaies");
    qualifierInfoDto.setLastName("Ev�quoz");
    qualifierInfoDto.setEmailAddress("blaise.evequoz@xentive.ch");
    qualifierInfoDto.setPhoneNumber("0212136111");
    qualifierInfoDto.setPhoneCountryCode("+41");
    qualifierInfoDto.setMobilePhone("0212136112");
    qualifierInfoDto.setMobileCountryCode("+42");
    user.setQualifierInfo(qualifierInfoDto);
    return user;

  }

  @Test
  public void testSaveStandardUser() {
    System.out.println("----------------------");
    System.out.println("testSaveStandardUser");

    manager.createUser(retrievePartnerDto(), "JUnit_testSaveStandardUser");
  }

  @Test
  public void testSaveForeignUser() {
    System.out.println("----------------------");
    System.out.println("testSaveForeignUser");

    PartnerDto user = new PartnerDto();
    user.setAddressType(PartnerAddressTypeEnumDto.PERSON);
    user.setLanguage("fr");

    // Personal address information
    PartnerPhysicalAddressDto physicalAddressDto = new PartnerPhysicalAddressDto();
    physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.FOREIGN);
    physicalAddressDto.setRegion("Haute-Savoie");
    physicalAddressDto.setStreet("Av. des boutiques");
    physicalAddressDto.setStreetNr("80A");
    physicalAddressDto.setCountry(PartnerCountryEnumDto.FR);
    physicalAddressDto.setCity("Annemasse");
    physicalAddressDto.setZip("74000");
    user.setPhysicalAddress(physicalAddressDto);

    // Personal qualifier information
    PartnerQualifierInfoDto qualifierInfoDto = new PartnerQualifierInfoDto();
    qualifierInfoDto.setQualifier(PartnerQualifierEnumDto.MR);
    qualifierInfoDto.setFirstName("Fran�ois");
    qualifierInfoDto.setLastName("Dupont");
    qualifierInfoDto.setEmailAddress("francois.dupont@company.fr");
    qualifierInfoDto.setPhoneNumber("0453958761");
    qualifierInfoDto.setPhoneCountryCode("+33");
    user.setQualifierInfo(qualifierInfoDto);
    manager.createUser(user, "JUnit_testSaveForeignUser");
  }

  @Test
  public void testSaveStandardCompany() {
    System.out.println("----------------------");
    System.out.println("testSaveStandardCompany");

    // Personal address information
    PartnerDto user = new PartnerDto();
    user.setAddressType(PartnerAddressTypeEnumDto.COMPANY);
    user.setLanguage("fr");

    PartnerPhysicalAddressDto physicalAddressDto = new PartnerPhysicalAddressDto();
    physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.STANDARD);
    physicalAddressDto.setStreet("Av. des Mousquines");
    physicalAddressDto.setStreetNr("4");
    physicalAddressDto.setCountry(PartnerCountryEnumDto.CH);
    physicalAddressDto.setCity("Lausanne");
    physicalAddressDto.setZip("1001");
    user.setPhysicalAddress(physicalAddressDto);

    // Personal qualifier information
    PartnerQualifierInfoDto qualifierInfoDto = new PartnerQualifierInfoDto();
    qualifierInfoDto.setQualifier(PartnerQualifierEnumDto.COMPANY);
    qualifierInfoDto.setCompanyName("Pixedia s.a.");
    qualifierInfoDto.setCompanyContactName("Jilali Raki");
    qualifierInfoDto.setEmailAddress("jilali.raki@pixedia.ch");
    qualifierInfoDto.setPhoneNumber("0212136759");
    user.setQualifierInfo(qualifierInfoDto);
    manager.createUser(user, "JUnit_testSaveStandardCompany");
  }

  @Test
  public void testSaveForeignCompany() {
    System.out.println("----------------------");
    System.out.println("testSaveForeignCompany");

    // Personal address information
    PartnerDto user = new PartnerDto();
    user.setAddressType(PartnerAddressTypeEnumDto.COMPANY);
    user.setLanguage("fr");

    PartnerPhysicalAddressDto physicalAddressDto = new PartnerPhysicalAddressDto();
    physicalAddressDto.setType(PartnerPhysicalAddressTypeEnumDto.FOREIGN);
    physicalAddressDto.setRegion("Haute-Savoie");
    physicalAddressDto.setStreet("Av. des boutiques");
    physicalAddressDto.setStreetNr("80A");
    physicalAddressDto.setCountry(PartnerCountryEnumDto.FR);
    physicalAddressDto.setCity("Annemasse");
    physicalAddressDto.setZip("74000");
    user.setPhysicalAddress(physicalAddressDto);

    // Personal qualifier information
    PartnerQualifierInfoDto qualifierInfoDto = new PartnerQualifierInfoDto();
    qualifierInfoDto.setQualifier(PartnerQualifierEnumDto.COMPANY);
    qualifierInfoDto.setCompanyName("Foreign Company s.a.");
    qualifierInfoDto.setCompanyContactName("Fran�ois Dupont");
    qualifierInfoDto.setEmailAddress("francois.dupont@server.fr");
    qualifierInfoDto.setPhoneNumber("0453958761");
    qualifierInfoDto.setPhoneCountryCode("+33");
    user.setQualifierInfo(qualifierInfoDto);
    manager.createUser(user, "JUnit_testSaveForeignCompany");
  }

  @Test
  public void testRetrieveUser() {
    System.out.println("----------------------");
    System.out.println("testRetrieveUser");

    // Personal address information
    PartnerDto userDto = manager.retrieveUser("blaise.evequoz@pixedia.ch");
    System.out.println("testRetrieveUser");
  }

  @Test
  public void testRetrieveUserInActiveDirectory() {
    System.out.println("----------------------");
    System.out.println("testRetrieveUserInActiveDirectory");

    // Personal address information
    // boolean isActivated =
    // PartnerDto userDto = manager.retrieveUser("blaise.evequoz1@pixedia.ch");
    PartnerDto usr = manager.retrieveUserInActiveDirectory("blaise.evequoz.registerd@xentive.ch");
    manager.activateUserInActiveDirectory("blaise.evequoz.registerd@xentive.ch");
    System.out.println("testRetrieveUser");
  }

  @Test
  public void testDeleteUser() {
    System.out.println("----------------------");
    System.out.println("testDeleteUser");

    manager.deleteUserInActiveDirectory("office@hibag-sg.ch");
    System.out.println("testRetrieveUser");
  }
}
