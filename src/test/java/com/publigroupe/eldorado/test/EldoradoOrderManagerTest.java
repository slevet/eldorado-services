package com.publigroupe.eldorado.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.w3c.dom.Document;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import com.publigroupe.eldorado.dto.EldoradoBrandDto;
import com.publigroupe.eldorado.dto.EldoradoMediaDto;
import com.publigroupe.eldorado.dto.EldoradoRateDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryListDto;
import com.publigroupe.eldorado.manager.EldoradoOrderManager;
import com.publigroupe.eldorado.manager.EldoradoPartnerManager;
import com.publigroupe.media.dto.MediaDto;
import com.publigroupe.media.dto.MediaNameDto;
import com.publigroupe.media.dto.OptionDto;
import com.publigroupe.media.dto.OptionIssueDateDto;
import com.publigroupe.media.dto.OptionNameDto;
import com.publigroupe.media.dto.OptionOnlinePeriodDto;
import com.publigroupe.media.dto.OptionSizeDto;
import com.publigroupe.order.domain.p2000.OrderConstantP2000;
import com.publigroupe.order.dto.OrderMaterialUEditorDto;
import com.publigroupe.order.dto.OrderMaterialUEditorPrintDto;
import com.publigroupe.order.dto.OrderMaterialUploadDto;
import com.publigroupe.order.dto.OrderMediaPrintPlusDto;
import com.publigroupe.order.dto.OrderStatusEnumDto;
import com.publigroupe.order.exception.OrderNotFoundException;
import com.publigroupe.order.manager.OrderManager;
import com.publigroupe.partner.dto.CityFindResultDto;
import com.publigroupe.partner.dto.PartnerDto;
import com.thoughtworks.xstream.XStream;

/**
 * Eldorado Order manager test.
 * 
 * @author Barman Dominique
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:eldoradoServicesContext.xml", "eldoradoServicesContextTest.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class EldoradoOrderManagerTest extends TestCase {
  @Autowired
  EldoradoOrderManager manager;
  @Autowired
  EldoradoPartnerManager partnerManager;

  @Autowired
  private Resource pdf;

  String offerIdPrint = "ELDORADOOFFER1007-1010-36001-00-5967-20140602";
  String offerIdOnline = "ELDORADOWLCOFFER1378-1420-1109000-1051-20110725-1";

  @Test
  public void testFindOffersWithEditor() {
    System.out.println("----------------------");
    System.out.println("testFindOffersWithEditor");

    manager.setEldoradoHeading("10");
    manager.setClientStatus("02");
    manager.setZipCode("170000");
    List<EldoradoBrandDto> brands = manager.findOffersByEditor("001");
    printBrands(brands);
  }

  @Test
  public void testFindPrintsWithName() {
    System.out.println("----------------------");
    System.out.println("testFindPrintsWithName");

    manager.setEldoradoHeading("20");
    manager.setClientStatus("02");
    manager.setZipCode("170000");
    List<EldoradoBrandDto> brands = manager.findPrintsByName("lib");
    printBrands(brands);
  }

  @Test
  public void testFindPrintsWithGeoLoc() {
    System.out.println("----------------------");
    System.out.println("testFindPrintsWithGeoLoc");

    manager.setEldoradoHeading("11");
    manager.setClientStatus("00");
    manager.setZipCode("195000");
    List<EldoradoBrandDto> brands = manager.findPrintsByGeoloc(46.74220623948396, 46.86938940570729, 7.063744066372338,
        7.2495563308597015);
    printBrands(brands);
  }

  @Test
  public void testFindCitiesWithName() {
    System.out.println("----------------------");
    System.out.println("testFindCitiesWithName");
    List<CityFindResultDto> cities = manager.findCities("lau", 30);
    System.out.println(cities.size());
  }

  @Test
  public void testFindOrdersForUser() {
    System.out.println("----------------------");
    System.out.println("testFindOrdersForUser");
    PartnerDto user = partnerManager.retrieveUser("blaise.evequoz@xentive.ch");
    EldoradoSummaryListDto summarylist = manager.findOrdersForUser(user, 1, 10);
    System.out.println("Summary Start:" + summarylist.getStart() + " /End:" + summarylist.getEnd() + " /Total:"
        + summarylist.getTotal());
    for (EldoradoSummaryDto summary : summarylist.getSummaries()) {
      System.out.println("Summary Order:" + summary.getCommandNumber() + "-" + summary.getStatus());
    }
    assertNotNull(summarylist.getSummaries());
    // assertEquals(2, summaries.size());
    // summaries = manager.findOrdersForUser(user, 1, 2);
  }

  @Test
  public void testFindDraftOrdersForUser() {
    System.out.println("----------------------");
    System.out.println("findDraftOrdersForUser");
    PartnerDto user = partnerManager.retrieveUser("hanne.andre@fellmanntreuhand.ch");
    EldoradoSummaryListDto summarylist = manager.findDraftOrdersForUser(user, 1, 999);
    System.out.println("Summary Start:" + summarylist.getStart() + " /End:" + summarylist.getEnd() + " /Total:"
        + summarylist.getTotal());
    for (EldoradoSummaryDto summary : summarylist.getSummaries()) {
      System.out.println("Summary Order:" + summary.getCommandNumber() + "-" + summary.getStatus());
    }
    assertNotNull(summarylist.getSummaries());
    // assertEquals(2, summaries.size());
    // summaries = manager.findOrdersForUser(user, 1, 2);
  }

  @Test
  public void testFindCitiesWithZipPostal() {
    System.out.println("----------------------");
    System.out.println("testFindCitiesWithZipPostal");
    List<CityFindResultDto> cities = manager.findCities("200", 30);
    System.out.println(cities.size());
  }

  @Test
  public void testPlacePrint() {
    System.out.println("----------------------");
    System.out.println("testPlacePrint");
    manager.placePrint(offerIdPrint);
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testRetrievePrint() {
    System.out.println("----------------------");
    System.out.println("testRetrievePrint");
    manager.setClientStatus("01");
    manager.setZipCode("192000");
    // String offerIdPrint = "ELDORADOOFFER1021-1057-24006-00-1179-20091221";
    manager.placePrint(offerIdPrint);
    EldoradoBrandDto brand = manager.retrievePrint(offerIdPrint);
    printBrand(brand);
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testGetPrintCurrentOfferId() {
    manager.placePrint(offerIdPrint);
    manager.setCurrentOfferId(offerIdPrint);
    assertTrue(manager.getCurrentOfferId() != null);
    assertTrue(manager.getCurrentOfferId().getId().equals(offerIdPrint));
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testRetrievePrintInsertionValidity() {
    System.out.println("----------------------");
    System.out.println("testRetrievePrintInsertionValidity");
    manager.placePrint(offerIdPrint);
    System.out.println("validity from :" + manager.retrievePrintInsertionValidityFrom(offerIdPrint));
    System.out.println("validity until:" + manager.retrievePrintInsertionValidityUntil(offerIdPrint));
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testFindPrintSubHeadings() {
    System.out.println("----------------------");
    System.out.println("testFindPrintSubHeadings");
    manager.placePrint(offerIdPrint);
    manager.setEldoradoHeading("20");
    List<OptionDto> subHeadings = manager.findPrintSubHeadings(offerIdPrint);
    printOptions(subHeadings);
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testPlacePrintSubHeading() {
    System.out.println("----------------------");
    System.out.println("testPlacePrintSubHeading");
    manager.placePrint(offerIdPrint);
    manager.placePrintSubHeading(offerIdPrint, "CTARUBS_02-00");
    manager.retrievePrintPlusMaterial(offerIdPrint);
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testFindPrintInsertions() {
    System.out.println("----------------------");
    System.out.println("testFindPrintInsertions");
    manager.placePrint(offerIdPrint);
    Date initialDate = new GregorianCalendar(2009, Calendar.NOVEMBER, 01).getTime();
    Date finalDate = new GregorianCalendar(2009, Calendar.DECEMBER, 31).getTime();
    List<OptionIssueDateDto> insertions = manager.findPrintInsertions(offerIdPrint, initialDate, finalDate);
    printInsertions(insertions);
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testPlacePrintInsertion() {
    System.out.println("----------------------");
    System.out.println("testPlacePrintSubHeading");
    manager.placePrint(offerIdPrint);
    manager.placePrintSubHeading(offerIdPrint, "CTARUBS_02-00");
    List<Date> dates = new ArrayList<Date>();
    dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 15).getTime());
    dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 16).getTime());
    manager.placePrintInsertions(offerIdPrint, dates);
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testFindPrintSizes() {
    System.out.println("----------------------");
    System.out.println("testFindPrintSizes");
    manager.placePrint(offerIdPrint);
    manager.placePrintSubHeading(offerIdPrint, "CTARUBS_01-00");
    List<Date> dates = new ArrayList<Date>();
    dates.add(new GregorianCalendar(2012, Calendar.OCTOBER, 15).getTime());
    dates.add(new GregorianCalendar(2012, Calendar.OCTOBER, 16).getTime());
    manager.placePrintInsertions(offerIdPrint, dates);
    List<OptionSizeDto> size = manager.findPrintSizes(offerIdPrint);
    OrderMaterialUEditorPrintDto material;
    EldoradoRateDto rate;
    for (OptionSizeDto optionDto : size) {
      material = new OrderMaterialUEditorPrintDto();
      material.setHeightInMm(optionDto.getHeightMinimum());
      rate = manager.retrievePrintMaterialRate(offerIdPrint, optionDto.getOptionId().getId(), material);
      System.out.println("/" + optionDto.getCodeValue() + "/" + rate.getAmount());
    }
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testPlacePrintSize() {
    System.out.println("----------------------");
    System.out.println("testFindPrintSizes");
    manager.placePrint(offerIdPrint);
    manager.placePrintSubHeading(offerIdPrint, "CTARUBS_02-00");
    List<Date> dates = new ArrayList<Date>();
    dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 15).getTime());
    dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 16).getTime());
    manager.placePrintInsertions(offerIdPrint, dates);
    manager.placePrintSize(offerIdPrint, "SIZE_01-2.0000-2-2-54-54-20-433-20-433-0-0-288-433-4330-0");
    manager.placePrintSizeHeightTotal(offerIdPrint, 50, 0);
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testretrievePrintMaterialRate() {
    System.out.println("----------------------");
    System.out.println("testretrievePrintMaterialRate");
    manager.placePrint(offerIdPrint);
    manager.placePrintSubHeading(offerIdPrint, "CTARUBS_01-00");
    List<Date> dates = new ArrayList<Date>();
    dates.add(new GregorianCalendar(2013, Calendar.MARCH, 8).getTime());
    // dates.add(new GregorianCalendar(2012, Calendar.MARCH, 5).getTime());
    manager.placePrintInsertions("ELDORADOOFFER1169-1178-24001-00-4179-20110101", dates);
    OrderMaterialUEditorPrintDto material = new OrderMaterialUEditorPrintDto();
    material.setHeightInMm(34);
    EldoradoRateDto rate = manager.retrievePrintMaterialRate(offerIdPrint, null, material);
    // EldoradoRateDto rate = manager.retrievePrintMaterialRate(offerIdPrint,
    // "SIZE_01-2.0000-2-2-54-54-35-440-35-440-0-0-290-440-4400-0-01-2",
    // material);
    System.out.println("Rate:" + rate.getAmount());
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testPrintPlus() {
    System.out.println("----------------------");
    System.out.println("testPrintPlus");
    manager.placePrint(offerIdPrint);
    manager.placePrintSubHeading(offerIdPrint, "CTARUBS_05-00");
    OrderMediaPrintPlusDto printPlus = manager.retrievePrintPlus(offerIdPrint);
    System.out.println("PrintPlus:" + printPlus.isPrintPlus() + " - " + printPlus.getName() + " - "
        + printPlus.getDuration() + " - " + printPlus.getCurrency() + " - " + printPlus.getAmount());

    // R�gle printplus
    // ===============
    // Si existence printplus
    // si withoutPrintPlus false, il faut allumer le checkbox suppl�ment
    // printplus
    // si withoutPrintPlus true, il faut �teindre le checkbox suppl�ment
    // printplus
    //
    // quand on d�coche un suppl�ment printplus, il faut
    // manager.setWithoutPrintPlus(true);
    // quand on coche un suppl�ment printplus, il faut
    // manager.setWithoutPrintPlus(false);
    manager.setWithoutPrintPlus(offerIdPrint, true);
    manager.removePrintOrOnline(offerIdPrint);
  }

  @Test
  public void testBlindBox() {
    System.out.println("----------------------");
    System.out.println("testBlindBox");
    // temp pour ne pas cr�er chaque fois une nouvelle commande
    manager.getOrderDto().setCodeList(OrderConstantP2000.ORDER_CODELIST);
    manager.getOrderDto().setCodeValue("10012-700024");
    // temp

    manager.setBlindBox(true);
    String text = manager.getBlindBoxText();
    System.out.println("BlindBox:" + text);
  }

  @Test
  public void testFindOnlines() {
    System.out.println("----------------------");
    System.out.println("testFindOnlines");

    manager.setEldoradoHeading("11");
    List<EldoradoBrandDto> brands = manager.findOnlines();
    printBrands(brands);
  }

  @Test
  public void testFindOnlinesByName() {
    System.out.println("----------------------");
    System.out.println("testFindOnlinesByName");

    manager.setEldoradoHeading("11");
    List<EldoradoBrandDto> brands = manager.findOnlinesByName("nf");
    System.out.println(brands.size());
  }

  @Test
  public void testPlaceOnline() {
    System.out.println("----------------------");
    System.out.println("testPlaceOnline");
    manager.placeOnline(offerIdOnline);
    manager.removePrintOrOnline(offerIdOnline);
  }

  @Test
  public void testRetrieveOnline() {
    System.out.println("----------------------");
    System.out.println("testRetrieveOnline");
    manager.setClientStatus("02");
    manager.setZipCode("170000");
    manager.placeOnline(offerIdOnline);
    EldoradoBrandDto brand = manager.retrieveOnline(offerIdOnline);
    printBrand(brand);
    manager.removePrintOrOnline(offerIdOnline);
  }

  @Test
  public void testGetOnlineCurrentOfferId() {
    manager.placeOnline(offerIdOnline);
    manager.setCurrentOfferId(offerIdOnline);
    assertTrue(manager.getCurrentOfferId() != null);
    assertTrue(manager.getCurrentOfferId().getId().equals(offerIdOnline));
    manager.removePrintOrOnline(offerIdOnline);
  }

  @Test
  public void testRetrieveOnlinePeriodValidity() {
    System.out.println("----------------------");
    System.out.println("testRetrieveOnlinePeriodValidity");
    manager.placeOnline(offerIdOnline);
    System.out.println("validity from :" + manager.retrieveOnlinePeriodValidityFrom(offerIdOnline));
    System.out.println("validity until:" + manager.retrieveOnlinePeriodValidityUntil(offerIdOnline));
    manager.removePrintOrOnline(offerIdOnline);
  }

  @Test
  public void testFindOnlinePeriods() {
    System.out.println("----------------------");
    System.out.println("testFindOnlinePeriods");
    manager.placeOnline(offerIdOnline);
    List<OptionOnlinePeriodDto> periods = manager.findOnlinePeriods(offerIdOnline);
    printOptions(periods);
    manager.removePrintOrOnline(offerIdOnline);
  }

  @Test
  public void testPlaceOnlinePeriod() {
    System.out.println("----------------------");
    System.out.println("testPlaceOnlinePeriod");
    manager.placeOnline(offerIdOnline);
    manager.placeOnlinePeriod(offerIdOnline, "PERIOD_1-1-1-28-");
    manager.placeOnlinePeriodDate(offerIdOnline, new GregorianCalendar(2010, Calendar.JANUARY, 1).getTime(),
        new GregorianCalendar(2010, Calendar.JANUARY, 28).getTime());
    OptionOnlinePeriodDto optionOnlinePeriodDto = manager.retrieveOnlinePeriod(offerIdOnline);
    printOption(optionOnlinePeriodDto);
    manager.removePrintOrOnline(offerIdOnline);
  }

  @Test
  public void testValorize() {
    System.out.println("----------------------");
    System.out.println("testValorize");
    // temp pour ne pas cr�er chaque fois une nouvelle commande
    manager.getOrderDto().setCodeList(OrderConstantP2000.ORDER_CODELIST);
    manager.getOrderDto().setCodeValue("10012-700024");
    // temp
    manager.setBlindBox(false);

    // Print
    // manager.placePrint(offerIdPrint);
    // manager.placePrintSubHeading(offerIdPrint, "CTARUBS_02-00");
    // List<Date> dates = new ArrayList<Date>();
    // dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 15).getTime());
    // dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 16).getTime());
    // manager.placePrintInsertions(offerIdPrint, dates);
    // manager.placePrintSize(offerIdPrint,
    // "SIZE_01-2.0000-2-2-54-54-20-433-20-433-0-0-288-433-4330-0");
    // OrderMaterialUEditorPrintDto orderMaterialPrint = new
    // OrderMaterialUEditorPrintDto();
    // orderMaterialPrint.setCmsUuid(OrderManager.generateUUID());
    // orderMaterialPrint.setHeightInMm(50);
    // manager.updatePrintMaterial(offerIdPrint, orderMaterialPrint);

    // Online
    manager.getOrderDto().setDescription("MON CONCERNANT COMMANDE");
    manager.placeOnline(offerIdOnline);
    manager.placeOnlinePeriod(offerIdOnline, "PERIOD_1-1-1-28-");
    manager.placeOnlinePeriodDate(offerIdOnline, new GregorianCalendar(2009, Calendar.DECEMBER, 1).getTime(),
        new GregorianCalendar(2009, Calendar.DECEMBER, 28).getTime());
    OrderMaterialUEditorDto orderMaterialOnline = new OrderMaterialUEditorDto();
    orderMaterialOnline.setCmsUuid(OrderManager.generateUUID());
    orderMaterialOnline.setDescription("Mon Concernant ONLINE");
    manager.updateOnlineMaterial(offerIdOnline, orderMaterialOnline);

    boolean isWithUpSelling = true;
    EldoradoSummaryDto summary = manager.retrieveSummary(false, true, isWithUpSelling);
    System.out.println("Summary amount:" + summary.getAmountWithVAT());

    manager.removePrintOrOnline(offerIdPrint);
    manager.removePrintOrOnline(offerIdOnline);
  }

  @Test
  public void testUpdatePrintMaterialUpload() {

    System.out.println("----------------------");
    System.out.println("testUpdatePrintMaterialUpload");
    // temp pour ne pas cr�er chaque fois une nouvelle commande
    manager.getOrderDto().setCodeList(OrderConstantP2000.ORDER_CODELIST);
    manager.getOrderDto().setCodeValue("10012-700024");
    // temp
    manager.setBlindBox(true);

    // Print
    manager.placePrint(offerIdPrint);
    manager.placePrintSubHeading(offerIdPrint, "CTARUBS_02-00");
    List<Date> dates = new ArrayList<Date>();
    dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 15).getTime());
    dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 16).getTime());
    manager.placePrintInsertions(offerIdPrint, dates);
    manager.placePrintSize(offerIdPrint, "SIZE_01-2.0000-2-2-54-54-20-433-20-433-0-0-288-433-4330-0");
    OrderMaterialUploadDto orderMaterialPrint = new OrderMaterialUploadDto();
    orderMaterialPrint.setHeightInMm(50);
    orderMaterialPrint.setAdminjobType("Gini");

    // OrderValorizationCalculationDto valorizationCalculation = new
    // OrderValorizationCalculationDto();
    // valorizationCalculation.setPricePerUnit(new BigDecimal(10));
    // manager.placeOrderSurcharge("SUPCAT-SUPNO", "0-815",
    // "Frais de traitement", valorizationCalculation);

    manager.updatePrintMaterialUpload(offerIdPrint, "SIZE_01-2.0000-2-2-54-54-20-433-20-433-0-0-288-433-4330-0",
        orderMaterialPrint);
    System.out.println("----------------------");
    System.out.println("End testUpdatePrintMaterialUpload");
  }

  // Mrt
  // @Test
  /*
   * public void testInsertP2000()
   * {
   * System.out.println("----------------------");
   * System.out.println("testInsertP2000");
   * manager.setBlindBox(true);
   * manager.setEldoradoHeading("20");
   * // Print
   * String offerIdPrint = "ELDORADOOFFER1015-1032-22002-00-1129-20091120";
   * manager.placePrint(offerIdPrint);
   * manager.placePrintSubHeading(offerIdPrint, "CTARUBS_02-00");
   * List<Date> dates = new ArrayList<Date>();
   * dates.add(new GregorianCalendar(2010, Calendar.FEBRUARY, 22).getTime());
   * //dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 16).getTime());
   * manager.placePrintInsertions(offerIdPrint, dates);
   * manager.placePrintSize(offerIdPrint,
   * "SIZE_01-2.0000-2-2-50-50-19-270-19-270-0-0-210-290-2320-0");
   * OrderMaterialUEditorPrintDto orderMaterialPrint = new
   * OrderMaterialUEditorPrintDto();
   * orderMaterialPrint.setDescription("Test BM");
   * orderMaterialPrint.setCmsUuid("cf906901-120b-464c-9c71-eaff616c86c9");
   * orderMaterialPrint.setHeightInMm(10);
   * manager.updatePrintMaterial(offerIdPrint, orderMaterialPrint);
   * // manager.setWithoutPrintPlus(true);
   * OrderMaterialUEditorDto orderMaterialPrintPlus = new
   * OrderMaterialUEditorDto();
   * orderMaterialPrintPlus.setCmsUuid("781FEE6A-48A8-4E86-9194-546501610F78");
   * manager.updatePrintPlusMaterial(offerIdPrint, orderMaterialPrintPlus);
   * // manager.placeOnline(offerIdOnline);
   * // manager.placeOnlinePeriod(offerIdOnline, "PERIOD_1-1-1-28-");
   * // manager.placeOnlinePeriodDate(offerIdOnline, new GregorianCalendar(2009,
   * Calendar.DECEMBER, 1).getTime(), new GregorianCalendar(2009,
   * Calendar.DECEMBER, 28).getTime());
   * // OrderMaterialUEditorDto orderMaterialOnline = new
   * OrderMaterialUEditorDto();
   * // orderMaterialOnline.setCmsUuid("781FEE6A-48A8-4E86-9194-546501610F78");
   * // manager.updateOnlineMaterial(offerIdOnline, orderMaterialOnline);
   * EldoradoSummaryDto summary = manager.retrieveSummary(false, true);
   * System.out.println("Summary amount:" + summary.getAmountWithVAT());
   * System.out.println("Order:"+manager.getOrderDto().getCodeValue());
   * PartnerDto user = partnerManager.retrieveUser("u930bm");
   * manager.setUserInfo(user);
   * manager.saveUserInfo();
   * EldoradoPaymentDatatransDto paymentData = new
   * EldoradoPaymentDatatransDto();
   * paymentData.setPaymentType(EldoradoPaymentTypeEnumDto.VISA);
   * paymentData.setAuthorizationCode("123");
   * paymentData.setUppId("456");
   * manager.savePaymentAuthorization(paymentData);
   * manager.insertP2000();
   * manager.removePrint(offerIdPrint);
   * manager.removeOnline(offerIdOnline);
   * }
   */
  // Mrt
  // @Test
  /*
   * public void testInsertP2000Online()
   * {
   * System.out.println("----------------------");
   * System.out.println("testInsertP2000Online");
   * manager.setBlindBox(true);
   * manager.setEldoradoHeading("20");
   * // Print
   * manager.placeOnline(offerIdOnline);
   * manager.placeOnlinePeriod(offerIdOnline, "PERIOD_1-1-1-28-");
   * manager.placeOnlinePeriodDate(offerIdOnline, new GregorianCalendar(2010,
   * Calendar.APRIL, 1).getTime(), new GregorianCalendar(2010, Calendar.APRIL,
   * 28).getTime());
   * OrderMaterialUEditorDto orderMaterialOnline = new
   * OrderMaterialUEditorDto();
   * orderMaterialOnline.setCmsUuid("781FEE6A-48A8-4E86-9194-546501610F78");
   * manager.updateOnlineMaterial(offerIdOnline, orderMaterialOnline);
   * EldoradoSummaryDto summary = manager.retrieveSummary(false, true);
   * System.out.println("Summary amount:" + summary.getAmountWithVAT());
   * System.out.println("Order:"+manager.getOrderDto().getCodeValue());
   * PartnerDto user = partnerManager.retrieveUser("u930bm");
   * manager.setUserInfo(user);
   * manager.saveUserInfo();
   * EldoradoPaymentDatatransDto paymentData = new
   * EldoradoPaymentDatatransDto();
   * paymentData.setPaymentType(EldoradoPaymentTypeEnumDto.VISA);
   * paymentData.setAuthorizationCode("123");
   * paymentData.setUppId("456");
   * manager.savePaymentAuthorization(paymentData);
   * manager.insertP2000();
   * manager.removeOnline(offerIdOnline);
   * }
   */
  @Test
  public void testSaveOrderDraft() {
    System.out.println("----------------------");
    System.out.println("testSaveOrderDraft");
    // temp pour ne pas cr�er chaque fois une nouvelle commande
    manager.getOrderDto().setCodeList(OrderConstantP2000.ORDER_CODELIST);
    manager.getOrderDto().setCodeValue("10012-700024");
    // temp
    manager.setBlindBox(true);
    manager.getOrderDto().setStatus(OrderStatusEnumDto.DRAFT);

    manager.placePrint(offerIdPrint);
    manager.placePrintSubHeading(offerIdPrint, "CTARUBS_02-00");
    List<Date> dates = new ArrayList<Date>();
    dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 15).getTime());
    dates.add(new GregorianCalendar(2010, Calendar.JANUARY, 16).getTime());
    manager.placePrintInsertions(offerIdPrint, dates);
    manager.placePrintSize(offerIdPrint, "SIZE_01-2.0000-2-2-54-54-20-433-20-433-0-0-288-433-4330-0");
    manager.placePrintSizeHeightTotal(offerIdPrint, 50, 0);

    manager.saveOrderDraft();
    System.out.println("Test");
    manager.removePrintOrOnline(offerIdPrint);
    manager.removePrintOrOnline(offerIdOnline);
  }

  @Test
  public void testLoadOrderDraft() {
    System.out.println("----------------------");
    System.out.println("testLoadOrderDraft");
    // PartnerDto user = null;
    // ;
    manager.setUserInfo(partnerManager.getUser());
    manager.loadOrderDraft("ea8c9b9c-fae9-49cd-8313-aea185d34f87");
    writeXml(manager.getOrderDto(), "C:\\temp\\elodOrder\\orderdto.xml");

    // manager.loadOrderDraft("10012-003788");
    // Document doc =
    // manager.retrieveOnlineMaterialAdmin("ELDORADOOFFER1007-1010-36001-00-5967-20140602",
    // "myadminfile.xml");
    // manager.placePrintSubHeading("ELDORADOOFFER1007-1010-36001-00-1072-20130101",
    // "CTARUBS_04-00");
    // manager.setWithoutPrintPlus("ELDORADOOFFER1007-1010-36001-00-1072-20130101",
    // true);
    manager.retrieveSummary(false, true, true);
    System.out.println("Test");
  }

  public void writeXml(Object msg, String filename) {
    File file = new File(filename);
    try {
      FileOutputStream stream = new FileOutputStream(file);
      XStream xmlUtil = new XStream();
      stream.write(xmlUtil.toXML(msg).getBytes());
      stream.close();
    }
    catch (Exception e) {
      // TODO
      e.printStackTrace();
    }
  }

  @Test
  public void testRetrieveP2000() {
    System.out.println("----------------------");
    System.out.println("testRetrieveP2000");
    // ////////////////////
    // String specialCode = "####";
    // String[] specialCodeSplit = specialCode.split("#");
    // // if (specialCodeSplit.length < 3) {
    // // return;
    // // }
    //
    // String specialEldoradoHeading = specialCodeSplit[0];
    // String specialClientStatus = specialCodeSplit[1];
    // String specialCountryCode = specialCodeSplit[2];
    // String specialPostalCode = "";
    // String specialCity = "";
    // if (specialCodeSplit.length > 3) {
    // specialPostalCode = specialCodeSplit[3];
    // specialCity = specialCodeSplit[4];
    // }
    // ////////////
    try {
      String cdenumL = "10012-274192";
      // String cdenumL = "10012-265299";
      // String cdenum = "10012-003241"
      manager.retrieveP2000(cdenumL);
      // manager.duplicate(true);
      manager.retrieveSummary(false, true, true);
    }
    catch (OrderNotFoundException ex) {
      ex.printStackTrace();
    }
    // manager.retrieveP2000("10012-700356");
    System.out.println("Test");
  }

  private void printBrands(List<EldoradoBrandDto> brands) {
    for (EldoradoBrandDto brand : brands) {
      printBrand(brand);
    }
  }

  private void printBrand(EldoradoBrandDto brand) {
    printMedia(brand.getBrand());
    for (EldoradoMediaDto media : brand.getMedias()) {
      System.out.println("  --> medias");
      printMedia(media.getMedia());
      for (MediaDto offer : media.getOffers()) {
        System.out.println("     --> offers");
        printMedia(offer);
      }
    }
  }

  private void printMedia(MediaDto media) {
    // Media code list / code value
    System.out.println("-----------------------------------------");
    System.out.println("Media: " + media.getCodeList() + " " + media.getCodeValue());

    // parent media id
    System.out.println("parentId:");
    System.out.println(" -ownerId- " + media.getParentMediaId().getOwnerId());
    System.out.println(" -id-      " + media.getParentMediaId().getId());
    System.out.print(" mediaType: ");
    if (media.getParentMediaId().getMediaType() == null) {
      System.out.println("null");
    }
    else {
      System.out.println("  -type- " + media.getParentMediaId().getMediaType().getType());
      System.out.println("  -group- " + media.getParentMediaId().getMediaType().getGroupType());
      System.out.println("  -support- " + media.getParentMediaId().getMediaType().getSupport());
    }

    // media id
    System.out.println("mediaId:");
    System.out.println(" -ownerId- " + media.getMediaId().getOwnerId());
    System.out.println(" -id-      " + media.getMediaId().getId());
    System.out.print(" mediaType: ");
    if (media.getMediaId().getMediaType() == null) {
      System.out.println("null");
    }
    else {
      System.out.println("  -type- " + media.getMediaId().getMediaType().getType());
      System.out.println("  -group- " + media.getMediaId().getMediaType().getGroupType());
      System.out.println("  -support- " + media.getMediaId().getMediaType().getSupport());
    }

    // code list - code value
    System.out.println("codelist:" + media.getCodeList());
    System.out.println("codevalue:" + media.getCodeValue());

    // BasisPrice
    System.out.print("BasisPrice: ");
    if (media.getBasicPrice() == null) {
      System.out.println("null");
    }
    else {
      System.out.println(media.getBasicPrice());
    }

    // Media Information
    System.out.println("Media Information: ");
    System.out.println(" -city- " + media.getInformation().getCity());
    System.out.println(" -postalCode- " + media.getInformation().getPostalCode());
    System.out.println(" -isCirculation- " + media.getInformation().isCirculation());
    System.out.println(" -circulationCopy- " + media.getInformation().getCirculationCopy());
    System.out.println(" -circulationCopyFromGeoloc- " + media.getInformation().getCirculationCopyFromGeoloc());

    // Traductions
    System.out.print("Name: ");
    if (media.getNames() == null) {
      System.out.println("null");
    }
    else {
      System.out.println();
    }
    for (MediaNameDto name : media.getNames()) {
      System.out.println("--> " + name.getLanguage() + " : " + "-name-" + name.getName() + "-url-" + name.getUrl()
          + "-gif-" + name.getGif());
    }
    System.out.println("-----------------------------------------");
  }

  private void printOption(OptionDto option) {
    System.out.println("Option:" + option.getOptionId().getOptionType() + " " + option.getOptionId().getId());
    if (option.getNames() != null) {
      for (OptionNameDto optionName : option.getNames()) {
        System.out.println("--> Name:" + optionName.getLanguage() + " " + optionName.getName() + " "
            + optionName.getNameVariable());
      }
    }
    System.out.println("--> CodeList-CodeValue:" + option.getCodeList() + " " + option.getCodeValue());
    if (option instanceof OptionIssueDateDto) {
      OptionIssueDateDto optionIssueDate = (OptionIssueDateDto) option;
      System.out.println("--> Date:" + optionIssueDate.getIssueDate() + " " + optionIssueDate.getDelayDate() + " "
          + optionIssueDate.isExtendedCirculation());
    }
  }

  private void printOptions(List<? extends OptionDto> options) {
    for (OptionDto option : options) {
      printOption(option);
    }
  }

  private void printInsertions(List<OptionIssueDateDto> options) {
    for (OptionDto option : options) {
      printOption(option);
    }
  }

  @Test
  public void findPrintSizesTest() throws Exception {
    String offerId = "ELDORADOOFFER1000-1000-17001-00-1126-20110101";
    manager.placePrint(offerId);

    OptionSizeDto optionSize = manager.findPrintSizes(offerId, 25, 358);
    assertNotNull(optionSize);
    assertEquals(25, optionSize.getWidthEquivalenceInMm());

    optionSize = manager.findPrintSizes(offerId, 27, 358);
    assertNotNull(optionSize);
    assertEquals(54, optionSize.getWidthEquivalenceInMm());
  }

  @Test
  public void retrieveOnlineMaterialAdmin() {
    System.out.println("----------------------");
    System.out.println("testPlaceOnline");
    manager.placeOnline(offerIdOnline);
    manager.setEldoradoHeading("11");
    manager.setBlindBox(true);
    Document doc = manager.retrieveOnlineMaterialAdmin(offerIdOnline, "myadminfile.xml");
    try {
      StringWriter stringWriter = new StringWriter();
      Result result = new StreamResult(stringWriter);

      DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
      DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
      LSSerializer writer = impl.createLSSerializer();
      System.out.println(writer.writeToString(doc));
      System.out.println("testPlaceOnlineEnd");
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

  @Test
  public void retrieveContactOnlineAdmin() {
    System.out.println("----------------------");
    System.out.println("testretrieveContactOnlineAdmin");
    PartnerDto user = partnerManager.retrieveUser("bevequoz@consultas.ch");
    Document doc = manager.retrieveContactOnlineAdmin(user);
    try {
      StringWriter stringWriter = new StringWriter();
      Result result = new StreamResult(stringWriter);

      DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
      DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
      LSSerializer writer = impl.createLSSerializer();
      System.out.println(writer.writeToString(doc));
      System.out.println("testretrieveContactOnlineAdmin");
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

}
