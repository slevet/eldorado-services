package com.publigroupe.eldorado.test;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.dao.Rsgv26Dao;
import com.publigroupe.eldorado.dao.bean.Rsgv26;
import com.publigroupe.order.dao.p2000.OrderAccessor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =
{ "classpath:eldoradoServicesContext.xml" })
@TestExecutionListeners(
{ DependencyInjectionTestExecutionListener.class })
public class Rsgv26DaoTest extends TestCase
{
	private static Log log = LogFactory.getLog(Rsgv26DaoTest.class);

	private Rsgv26Dao rsgv26Dao = new Rsgv26Dao();

	@Test(expected = IllegalArgumentException.class)
	public void testInsertWithoutUtiste()
	{
		log.debug("-- Start Test testInsertWithoutUtiste --");
		Rsgv26 rsgv26 = new Rsgv26();
		rsgv26Dao.insert(rsgv26);
		OrderAccessor.rollBack();
		OrderAccessor.returnConnectionToPool();
		log.debug("-- End Test testInsertWithoutUtiste --");
	}

	@Test
	public void testInsert()
	{
		log.debug("-- Start Test insert --");
		Rsgv26 rsgv26 = new Rsgv26();
		rsgv26.utiste = "-1";
		rsgv26.utiento = "-1";
		rsgv26.cdenum = "-1";
		rsgv26.authdt = new Date();
		rsgv26.authhe = new Date();
		assertTrue(rsgv26Dao.insert(rsgv26));
		OrderAccessor.rollBack();
		OrderAccessor.returnConnectionToPool();
		log.debug("-- End Test insert --");
	}

	@Test
	public void testInsertUpdateEmptyRemove()
	{
		Date current = new Date();
		log.debug("-- Start Test testInsertUpdateEmptyRemove --");
		Rsgv26 rsgv26 = new Rsgv26();
		rsgv26.utiste = "-1";
		rsgv26.utiento = "-1";
		rsgv26.cdenum = "-1";
		rsgv26.authdt = current;
		rsgv26.authhe = current;
		assertTrue(rsgv26Dao.insert(rsgv26));
		OrderAccessor.commit();
		rsgv26Dao.update(rsgv26);
		OrderAccessor.commit();
		Rsgv26 result = rsgv26Dao.retrieve("-1", "-1");
		assertEquals(DateUtils.getFragmentInDays(result.authdt, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(DateUtils.getFragmentInSeconds(result.authhe, Calendar.DAY_OF_YEAR), DateUtils.getFragmentInSeconds(current, Calendar.DAY_OF_YEAR));
		rsgv26Dao.delete(rsgv26.utiento, rsgv26.cdenum);
		OrderAccessor.commit();
		OrderAccessor.returnConnectionToPool();
		log.debug("-- End Test testInsertUpdateEmptyRemove --");
	}

	@Test
	public void testInsertUpdateFullRemove()
	{
		Date current = new Date();
		log.debug("-- Start Test testInsertUpdateFullRemove --");
		Rsgv26 rsgv26 = new Rsgv26();
		rsgv26.utiste = "-1";
		rsgv26.utiento = "-1";
		rsgv26.cdenum = "-1";
		rsgv26.authdt = current;
		rsgv26.authhe = current;
		assertTrue(rsgv26Dao.insert(rsgv26));
		OrderAccessor.commit();
		rsgv26.abdmdt = current;
		rsgv26.abdmhe = current;
		rsgv26.cdenump = "-2";
		rsgv26.confmdt = current;
		rsgv26.confmhe = current;
		rsgv26.debdt = current;
		rsgv26.debhe = current;
		rsgv26.p2abddt = current;
		rsgv26.p2abdhe = current;
		rsgv26.p2abdre = "-2";
		rsgv26.p2loaddt = current;
		rsgv26.p2loadhe = current;
		rsgv26.p2susdt = current;
		rsgv26.p2sushe = current;
		rsgv26.p2susre = "-2";
		rsgv26.p2valdt = current;
		rsgv26.p2valhe = current;
		rsgv26.susmdt = current;
		rsgv26.susmhe = current;
		rsgv26.utient = "-2";
		rsgv26.utiste = "-2";
		rsgv26Dao.update(rsgv26);
		OrderAccessor.commit();
		Rsgv26 result = rsgv26Dao.retrieve("-1", "-1");
		assertEquals(DateUtils.getFragmentInDays(result.abdmdt, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(DateUtils.getFragmentInSeconds(result.abdmhe, Calendar.DAY_OF_YEAR), DateUtils.getFragmentInSeconds(current, Calendar.DAY_OF_YEAR));
		assertEquals(DateUtils.getFragmentInDays(result.confmdt, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(DateUtils.getFragmentInSeconds(result.confmhe, Calendar.DAY_OF_YEAR), DateUtils.getFragmentInSeconds(current, Calendar.DAY_OF_YEAR));
		assertEquals(DateUtils.getFragmentInDays(result.debdt, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(DateUtils.getFragmentInSeconds(result.debhe, Calendar.DAY_OF_YEAR), DateUtils.getFragmentInSeconds(current, Calendar.DAY_OF_YEAR));
		assertEquals(DateUtils.getFragmentInDays(result.p2abddt, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(DateUtils.getFragmentInSeconds(result.p2abdhe, Calendar.DAY_OF_YEAR), DateUtils.getFragmentInSeconds(current, Calendar.DAY_OF_YEAR));
		assertEquals(DateUtils.getFragmentInDays(result.p2loaddt, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(DateUtils.getFragmentInSeconds(result.p2loadhe, Calendar.DAY_OF_YEAR), DateUtils.getFragmentInSeconds(current, Calendar.DAY_OF_YEAR));
		assertEquals(DateUtils.getFragmentInDays(result.authdt, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(DateUtils.getFragmentInSeconds(result.authhe, Calendar.DAY_OF_YEAR), DateUtils.getFragmentInSeconds(current, Calendar.DAY_OF_YEAR));
		assertEquals(DateUtils.getFragmentInDays(result.p2susdt, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(DateUtils.getFragmentInSeconds(result.p2sushe, Calendar.DAY_OF_YEAR), DateUtils.getFragmentInSeconds(current, Calendar.DAY_OF_YEAR));
		assertEquals(DateUtils.getFragmentInDays(result.p2valdt, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(DateUtils.getFragmentInSeconds(result.p2valhe, Calendar.DAY_OF_YEAR), DateUtils.getFragmentInSeconds(current, Calendar.DAY_OF_YEAR));
		assertEquals(DateUtils.getFragmentInDays(result.susmdt, Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
		assertEquals(DateUtils.getFragmentInSeconds(result.susmhe, Calendar.DAY_OF_YEAR), DateUtils.getFragmentInSeconds(current, Calendar.DAY_OF_YEAR));
		assertEquals(result.utient, "-2");
		assertEquals(result.utiento, "-1");
		assertEquals(result.utiste, "-2");
		assertEquals(result.p2abdre, "-2");
		assertEquals(result.cdenum, "-1");
		assertEquals(result.cdenump, "-2");
		rsgv26Dao.delete(rsgv26.utiento, rsgv26.cdenum);
		OrderAccessor.commit();
		OrderAccessor.returnConnectionToPool();
		log.debug("-- End Test testInsertUpdateFullRemove --");
	}
}
