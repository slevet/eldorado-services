package com.publigroupe.eldorado.test;

import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.archiving.dto.DocumentDto;
import com.publigroupe.eldorado.manager.EldoradoDmsDocumentManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:eldoradoServicesContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class EldoradoDmsDocumentManagerTest extends TestCase {

  @Autowired
  private EldoradoDmsDocumentManager eldoradoDmsDocumentManager;

  @Test
  public void searchDmsInvoicesTest() throws Exception {
    Date invoiceDate = DateUtils.parseDate("07.05.2012", new String[] {"dd.MM.yyyy"});
    List<DocumentDto> docs = eldoradoDmsDocumentManager.searchDmsInvoices("10012", "000064", invoiceDate);
    assertNotNull(docs);
    assertFalse(docs.isEmpty());
  }
}
