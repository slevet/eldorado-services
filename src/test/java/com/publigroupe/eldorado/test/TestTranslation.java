package com.publigroupe.eldorado.test;

import java.util.Map;

import com.publigroupe.basis.dto.LanguageEnumDto;
import com.publigroupe.basis.manager.TranslationManager;

public class TestTranslation {

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{	
//		LanguageId languageId = LanguageId.French;
		LanguageEnumDto languageEnum = LanguageEnumDto.getByValue("04");
		String test = TranslationManager.getLabel("ELDORADO.WEB.ELDORADO_SUMMARY.MODIFY_BUTTON", languageEnum);		
		System.out.println(test);
		
		Map<String, String> testMap = TranslationManager.getListLabel("ELDORADO.WEB.MAIL", languageEnum, true);
	}
}
