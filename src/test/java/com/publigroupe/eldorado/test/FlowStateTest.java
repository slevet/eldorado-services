package com.publigroupe.eldorado.test;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.dao.Rsgv26Dao;
import com.publigroupe.eldorado.domain.EldoradoFlowState;
import com.publigroupe.eldorado.dto.EldoradoFlowStateDto;
import com.publigroupe.order.dao.p2000.OrderAccessor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =
{ "classpath:eldoradoServicesContext.xml" })
@TestExecutionListeners(
{ DependencyInjectionTestExecutionListener.class })
public class FlowStateTest extends TestCase
{
	private static Log log = LogFactory.getLog(PaymentTest.class);

	EldoradoFlowState state = new EldoradoFlowState();
	Rsgv26Dao rsgv26Dao = new Rsgv26Dao();

	@Test
	public void testCreate() throws Exception
	{
		try
		{
			Date current = new Date();
			log.debug("start test process");
			state.create("-1", "-1");
			OrderAccessor.commit();
			EldoradoFlowStateDto paymentStateDto = state.retrieve("-1", "-1");
			assertEquals(DateUtils.getFragmentInDays(paymentStateDto.getAuthorizationDate(), Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
			rsgv26Dao.delete("-1", "-1");
			OrderAccessor.commit();
		}
		catch (Exception ex)
		{
			OrderAccessor.rollBack();
			throw ex;
		}
		finally
		{
			OrderAccessor.returnConnectionToPool();
		}
		log.debug("end test process");
	}

	@Test
	public void testCancel() throws Exception
	{
		try
		{
			log.debug("start testCancel");
			state.create("-1", "-1");
			state.cancelAuthorization("-1", "-1");
			OrderAccessor.commit();
			EldoradoFlowStateDto paymentStateDto = state.retrieve("-1", "-1");
			assertNull(paymentStateDto.getCancellationDate());
			rsgv26Dao.delete("-1", "-1");
			OrderAccessor.commit();
		}
		catch (Exception ex)
		{
			OrderAccessor.rollBack();
			throw ex;
		}
		finally
		{
			OrderAccessor.returnConnectionToPool();
		}
		log.debug("end testCancel");
	}

	@Test
	public void testClientSuspensionNotification() throws Exception
	{
		try
		{
			Date current = new Date();
			log.debug("start testClientSuspensionNotification");
			state.create("-1", "-1");
			state.setClientSuspensionNotification("-1", "-1");
			OrderAccessor.commit();
			EldoradoFlowStateDto paymentStateDto = state.retrieve("-1", "-1");
			assertEquals(DateUtils.getFragmentInDays(paymentStateDto.getSuspendMailClientDate(), Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));

			state.resetClientSuspensionNotification("-1", "-1");
			OrderAccessor.commit();
			EldoradoFlowStateDto paymentStateDto2 = state.retrieve("-1", "-1");
			assertNull(paymentStateDto2.getSuspendMailClientDate());
			rsgv26Dao.delete("-1", "-1");
			OrderAccessor.commit();
		}
		catch (Exception ex)
		{
			OrderAccessor.rollBack();
			throw ex;
		}
		finally
		{
			OrderAccessor.returnConnectionToPool();
		}
		log.debug("end testClientSuspensionNotification");
	}

	@Test
	public void testAbandonned() throws Exception
	{
		try
		{
			Date current = new Date();
			log.debug("start testClientSuspensionNotification");
			state.create("-1", "-1");
			state.setAbanbonned("-1", "-1", "-1");
			OrderAccessor.commit();
			EldoradoFlowStateDto paymentStateDto = state.retrieve("-1", "-1");
			assertEquals(DateUtils.getFragmentInDays(paymentStateDto.getCancellationDate(), Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
			rsgv26Dao.delete("-1", "-1");
			OrderAccessor.commit();
		}
		catch (Exception ex)
		{
			OrderAccessor.rollBack();
			throw ex;
		}
		finally
		{
			OrderAccessor.returnConnectionToPool();
		}
		log.debug("end testClientSuspensionNotification");
	}

	@Test
	public void testCancelled() throws Exception
	{
		try
		{
			Date current = new Date();
			log.debug("start testCancelled");
			state.create("-1", "-1");
			state.setCancelled("-1", "-1", "TEST");
			OrderAccessor.commit();
			EldoradoFlowStateDto paymentStateDto = state.retrieve("-1", "-1");
			assertEquals(DateUtils.getFragmentInDays(paymentStateDto.getCancellationMailClientDate(), Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
			rsgv26Dao.delete("-1", "-1");
			OrderAccessor.commit();
		}
		catch (Exception ex)
		{
			OrderAccessor.rollBack();
			throw ex;
		}
		finally
		{
			OrderAccessor.returnConnectionToPool();
		}
		log.debug("end testCancelled");
	}

	@Test
	public void testAuthorized() throws Exception
	{
		try
		{
			Date current = new Date();
			log.debug("start testClientSuspensionNotification");
			state.create("-1", "-1");
			state.setAuthorized("-1", "-1");
			OrderAccessor.commit();
			EldoradoFlowStateDto paymentStateDto = state.retrieve("-1", "-1");
			assertEquals(DateUtils.getFragmentInDays(paymentStateDto.getAuthorizationDate(), Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
			rsgv26Dao.delete("-1", "-1");
			OrderAccessor.commit();
		}
		catch (Exception ex)
		{
			OrderAccessor.rollBack();
			throw ex;
		}
		finally
		{
			OrderAccessor.returnConnectionToPool();
		}
		log.debug("end testClientSuspensionNotification");
	}

	@Test
	public void testConfirmationNotified() throws Exception
	{
		try
		{
			Date current = new Date();
			log.debug("start testConfirmationNotified");
			state.create("-1", "-1");
			state.setConfirmationNotified("-1", "-1");
			OrderAccessor.commit();
			EldoradoFlowStateDto paymentStateDto = state.retrieve("-1", "-1");
			assertEquals(DateUtils.getFragmentInDays(paymentStateDto.getConfirmationDate(), Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
			assertTrue(EldoradoFlowState.isConfirmed(paymentStateDto));
			rsgv26Dao.delete("-1", "-1");
			OrderAccessor.commit();
		}
		catch (Exception ex)
		{
			OrderAccessor.rollBack();
			throw ex;
		}
		finally
		{
			OrderAccessor.returnConnectionToPool();
		}
		log.debug("end testConfirmationNotified");
	}

	@Test
	public void testDebited() throws Exception
	{
		try
		{
			Date current = new Date();
			log.debug("start testDebited");
			state.create("-1", "-1");
			state.setDebited("-1", "-1");
			OrderAccessor.commit();
			EldoradoFlowStateDto paymentStateDto = state.retrieve("-1", "-1");
			assertEquals(DateUtils.getFragmentInDays(paymentStateDto.getDebitDate(), Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
			assertTrue(EldoradoFlowState.isDebited(paymentStateDto));
			rsgv26Dao.delete("-1", "-1");
			OrderAccessor.commit();
		}
		catch (Exception ex)
		{
			OrderAccessor.rollBack();
			throw ex;
		}
		finally
		{
			OrderAccessor.returnConnectionToPool();
		}
		log.debug("end testDebited");
	}

	@Test
	public void testLoaded() throws Exception
	{
		try
		{
			Date current = new Date();
			log.debug("start testLoaded");
			state.create("-1", "-1");
			state.setLoaded("-1", "-1", "-1", "-1");
			OrderAccessor.commit();
			EldoradoFlowStateDto paymentStateDto = state.retrieve("-1", "-1");
			assertEquals(DateUtils.getFragmentInDays(paymentStateDto.getLoadingDate(), Calendar.YEAR), DateUtils.getFragmentInDays(current, Calendar.YEAR));
			rsgv26Dao.delete("-1", "-1");
			OrderAccessor.commit();
		}
		catch (Exception ex)
		{
			OrderAccessor.rollBack();
			throw ex;
		}
		finally
		{
			OrderAccessor.returnConnectionToPool();
		}
		log.debug("end testLoaded");
	}
}
