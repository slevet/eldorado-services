/**
 * 
 */
package com.publigroupe.eldorado.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.dto.EldoradoBrandDto;
import com.publigroupe.eldorado.dto.EldoradoMediaDto;
import com.publigroupe.eldorado.dto.EldoradoSummaryDto;
import com.publigroupe.eldorado.manager.EldoradoOrderManager;
import com.publigroupe.media.dto.MediaDto;
import com.publigroupe.media.dto.OptionDto;
import com.publigroupe.media.dto.OptionIssueDateDto;
import com.publigroupe.media.dto.OptionSizeDto;
import com.publigroupe.media.dto.SizeMeasurementTypeEnumDto;
import com.publigroupe.order.domain.p2000.OrderConstantP2000;
import com.publigroupe.order.dto.OrderMediaPrintPlusDto;
import com.publigroupe.partner.dto.CityFindResultDto;

/**
 * Eldorado Summary test.
 * 
 * @author Jilali Raki
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =
{ "classpath:eldoradoServicesContext.xml" })
@TestExecutionListeners(
{ DependencyInjectionTestExecutionListener.class })
public class EldoradoSummaryTest
{

	@Autowired
	EldoradoOrderManager manager;

	final static String LANGUAGES[] =
	{ "fr", "fr", "fr" };
	final static String HEADINGS[] =
	{ "10", "11", "20", "21" };
	final static String CLIENTSTATUS[] =
	{ "01", "02" };
	final static char[] CITIESSEARCH = "abcdefghijklmnopqrstuvwxyz".toCharArray();
	final static String COUNTRIES[] =
	{ "CH" };
	final static String BRANDNAMESEARCH[] =
	{ "lib", "24", "nou", "ex" };
	final static boolean PRINTPLUS[] =
	{ true, false };
	final static boolean PRINTLOCALPOINT[] =
	{ true, false };
	final static boolean BLINDBOX[] =
	{ true, false };
	final static int INSERTIONS_NUM = 3;
	final static int OFFER_NUM = 10;

	// FIXME[mdt]: This is not workin. Jilali said that it is fine for now and
	// that he will fix it
	// once the spec for this are clear.
	@Test
	public void testCommandSummary()
	{
		System.out.println("----------------------");
		System.out.println("testCommandSummary");

		manager.getOrderDto().setCodeList(OrderConstantP2000.ORDER_CODELIST);
		manager.getOrderDto().setCodeValue("10012-700024");

		// Language
		manager.setLanguage(LANGUAGES[(int) (Math.random() * LANGUAGES.length)]);
		System.out.println("language  --> " + manager.getLanguage());

		manager.setEldoradoHeading(HEADINGS[(int) (Math.random() * HEADINGS.length)]);
		System.out.println("Heading  --> " + manager.getEldoradoHeading());

		manager.setClientStatus(CLIENTSTATUS[(int) (Math.random() * CLIENTSTATUS.length)]);
		System.out.println("Client status  --> " + manager.getClientStatus());

		List<CityFindResultDto> cities;
		do
		{
			cities = manager.findCities(String.valueOf(CITIESSEARCH[(int) (Math.random() * CITIESSEARCH.length)]), 1);
		}
		while (cities.size() == 0);
		manager.setCountryCode(COUNTRIES[(int) (Math.random() * COUNTRIES.length)]);
		manager.setZipCode(cities.get(0).getZipCode());
		manager.setCity(cities.get(0).getCity());
		System.out.println("City  --> " + manager.getCity() + ", Zip --> " + manager.getZipCode() + ", Country --> " + manager.getCountryCode());

		for (int i = 0; i < OFFER_NUM; i++)
		{
			addNewRandomOffer();
		}

		boolean isWithUpSelling = true;
		// Summary
		EldoradoSummaryDto summary = manager.retrieveSummary(false, true, isWithUpSelling);
	}

	private void addNewRandomOffer()
	{

		String offerId = selectRandomBrand();
		selectRandomSubHeading(offerId);
		selectInsertions(offerId);
		selectRandomBindBox(offerId);
		selectRandomPrintPlus(offerId);
		selectRandomPrintLocalPoint(offerId);
		selectRandomSize(offerId);
	}

	private void selectRandomSize(String offerIdStr)
	{
		List<OptionSizeDto> sizes = manager.findPrintSizes(offerIdStr);
		int randSize = (int) (Math.random() * sizes.size());
		int szCounter = 0;
		for (OptionSizeDto option : sizes)
		{
			if (randSize == szCounter)
			{
				System.out.println("Size  --> " + option.getName(manager.getLanguage()).getName());
				manager.placePrintSize(offerIdStr, option.getOptionId().getId());
				if (option.getMeasurementType() == SizeMeasurementTypeEnumDto.MILLIMETERS)
				{
					manager.placePrintSizeHeightTotal(offerIdStr, option.getHeightMaximumInMm() / 2, 0);
				}
				else if (option.getMeasurementType() == SizeMeasurementTypeEnumDto.CHARS || option.getMeasurementType() == SizeMeasurementTypeEnumDto.CPMS
						|| option.getMeasurementType() == SizeMeasurementTypeEnumDto.ENTRIES || option.getMeasurementType() == SizeMeasurementTypeEnumDto.LINES
						|| option.getMeasurementType() == SizeMeasurementTypeEnumDto.WORDS)
				{
					manager.placePrintSizeHeightTotal(offerIdStr, 0, 20);
				}

				break;
			}
			++szCounter;
		}
	}

	private void selectRandomPrintPlus(String offerIdStr)
	{
		OrderMediaPrintPlusDto mediaPrintPlus = manager.retrievePrintPlus(offerIdStr);
		if (mediaPrintPlus.isPrintPlus() == true)
		{
			manager.setWithoutPrintPlus(PRINTPLUS[(int) (Math.random() * PRINTPLUS.length)]);
		}
		else
		{
			manager.setWithoutPrintPlus(true);
		}
		System.out.println("PrintPlus  --> " + !manager.isWithoutPrintPlus());
	}

	private void selectRandomPrintLocalPoint(String offerIdStr)
	{
//		OrderMediaPrintLocalPointDto mediaPrintLocalPoint = manager.retrievePrintLocalPoint(offerIdStr);
//		if (mediaPrintLocalPoint.isLocalPoint() == true)
//		{
//			manager.setWithoutPrintPlus(PRINTLOCALPOINT[(int) (Math.random() * PRINTLOCALPOINT.length)]);
//		}
//		else
//		{
//			manager.setWithoutPrintPlus(true);
//		}
//		System.out.println("PrintPlus  --> " + !manager.isWithoutPrintPlus());
	}

	private void selectRandomBindBox(String offerIdStr)
	{
		manager.setBlindBox(BLINDBOX[(int) (Math.random() * BLINDBOX.length)]);
		System.out.println("BlindBox  --> " + manager.isBlindBox());

		if (manager.isBlindBox() == true)
		{
			System.out.println("BlindBox text  --> " + manager.getBlindBoxText());
		}
	}
	
	private void selectInsertions(String offerId)
	{
		Calendar currentCal = Calendar.getInstance();
		Date initialDate = currentCal.getTime();
		Date finalDate = new Date();
		finalDate.setTime(initialDate.getTime() + (long) 30 * 24 * 60 * 60 * 1000);
		List<OptionIssueDateDto> insertions = manager.findPrintInsertions(offerId, initialDate, finalDate);
		List<Date> dates = new ArrayList<Date>();
		int inCounter = 0;
		for (OptionDto option : insertions)
		{
			OptionIssueDateDto optionIssueDate = (OptionIssueDateDto) option;
			System.out.println("Insertion  --> " + optionIssueDate.getIssueDate() + ", Ext. Circ. --> " + optionIssueDate.isExtendedCirculation());
			dates.add(optionIssueDate.getIssueDate());
			if (++inCounter == INSERTIONS_NUM)
				break;
		}
		manager.placePrintInsertions(offerId, dates);
	}

	private void selectRandomSubHeading(String offerId)
	{
		List<OptionDto> subHeadings = manager.findPrintSubHeadings(offerId);
		int randSubHeading = (int) (Math.random() * subHeadings.size());
		int shCounter = 0;
		for (OptionDto option : subHeadings)
		{
			if (randSubHeading == shCounter)
			{
				System.out.println("SubHeading  --> " + option.getName(manager.getLanguage()).getName());
				manager.placePrintSubHeading(offerId, option.getOptionId().getId());
				break;
			}
			++shCounter;
		}
	}

	private String selectRandomBrand()
	{
		String offerId = null;
		List<EldoradoBrandDto> brands;
		do
		{
			brands = manager.findPrintsByName(BRANDNAMESEARCH[(int) (Math.random() * BRANDNAMESEARCH.length)]);
		}
		while (brands.size() == 0);
		int brandId = (int) (Math.random() * brands.size());
		int brCounter = 0;
		for (EldoradoBrandDto brand : brands)
		{
			if (brandId == brCounter)
			{
				System.out.println("Brand  --> " + brand.getBrand().getName(manager.getLanguage()).getName());
				int mediaId = (int) (Math.random() * brand.getMedias().size());
				int mdCounter = 0;
				for (EldoradoMediaDto media : brand.getMedias())
				{
					if (mediaId == mdCounter)
					{
						System.out.println("Media  --> " + media.getMedia().getName(manager.getLanguage()).getName());
						int offerIndex = (int) (Math.random() * media.getOffers().size());
						int ofCounter = 0;
						for (MediaDto offer : media.getOffers())
						{
							if (offerIndex == ofCounter)
							{
								offerId = offer.getMediaId().getId();
								System.out.println("Offer  --> " + offer.getName(manager.getLanguage()).getName() + ", Offer Id --> " + offerId);
								manager.placePrint(offerId);
								break;
							}
							++ofCounter;
						}
						break;
					}
					++mdCounter;
				}
				break;
			}
			++brCounter;
		}
		return offerId;
	}
}
