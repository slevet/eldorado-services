package com.publigroupe.eldorado.service;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class EldoradoCheckServiceTest {

  @Autowired
  private EldoradoCheckService eldoradoCheckService;

  @Test
  public void testCheckDatabaseUDB() {
    boolean returnService = eldoradoCheckService.checkDatabaseUDB();
    Assert.assertTrue(returnService);
  }

  @Test
  public void testCheckLdapAdam() {
    boolean returnService = eldoradoCheckService.checkLdapAdam();
    Assert.assertTrue(returnService);
  }

  @Test
  public void testCheckTranslationService() {
    boolean returnService = eldoradoCheckService.checkTranslationService();
    Assert.assertTrue(returnService);
  }

}
