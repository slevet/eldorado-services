package com.publigroupe.eldorado.service;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.publigroupe.eldorado.dto.SolrSearchCriteriasDto;
import com.publigroupe.eldorado.dto.SolrSearchResultDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:eldoradoServicesContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class EldoradoSolrTest {
  // private SolrService solrService;

  @Autowired
  private SolrService solrService;

  @Test
  public void testFindByFacet() {

    // SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss",
    // Locale.US);
    // Date dates = new Date();
    // String results = formatter.format(dates);
    // // formatter.setTimeZone(TimeZone.getTimeZone("Universal"));
    // try {
    // Date date = formatter.parse("Mon Jul 25 00:00:00");
    // }
    // catch (ParseException ex) {
    // Date date = new Date();
    // }
    //
    SolrSearchCriteriasDto searchCriterias = new SolrSearchCriteriasDto();
    searchCriterias.setFullTextQuery("martigny");
    searchCriterias.setNpa("1950");
    searchCriterias.setStartRow(0);
    searchCriterias.setRowCount(100);
    searchCriterias.addFilter(SolrService.HEADING, "12");
    searchCriterias.addFilter(SolrService.CLIENT_STATUS, "PRIVATE");
    // searchCriterias.addFilter(SolrService.BRAND_ID, "");

    final SolrSearchResultDto result = this.solrService.findByFacet(searchCriterias);

    Assert.assertTrue(true);
  }

}
